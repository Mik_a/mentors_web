import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/routes/app_pages.dart';

class LoginController extends GetxController {
  TextEditingController id = TextEditingController();
  TextEditingController pw = TextEditingController();
  bool isLoading = false;
  FocusNode pwNode = FocusNode();
  RxInt testInt = 0.obs;

  @override
  void onInit() {
    super.onInit();
    id.text = '';
    pw.text = '';
  }

  void onClickBtn() {
    testInt.value++;
  }

  void onClickLoginButton() {
    if (isLoading) return;

    if (id.text == '' || pw.text == '') {
      EasyLoading.showError("정보를 입력해주세요");
      return;
    }

    EasyLoading.show(status: "로그인 중 입니다.. \n잠시만 기다려주세요");
    isLoading = true;

    API.store.storeLogin(id.text, pw.text).then((value) async {
      if (value == null) {
        EasyLoading.showError("로그인에 실패했습니다. \n확인 후 다시 시도해주세요");
        return;
      }

      isLoading = false;
      if (value['success']) {
        await GetStorage().write('jwt', value['data']);
        await GetStorage().write('id', value['id']);
        EasyLoading.showSuccess("로그인에 성공했습니다.");
        Get.offAndToNamed(Routes.HOME);
      } else {
        EasyLoading.showError("로그인에 실패했습니다. \n확인 후 다시 시도해주세요");
      }
    });
  }
}
