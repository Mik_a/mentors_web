import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mentors_web/app/modules/login/controllers/login_controller.dart';
import 'package:mentors_web/components/icon_inpurt_box.dart';

class LoginForm extends StatelessWidget {
  final LoginController controller;
  final Function() onEnter;

  const LoginForm({
    Key? key,
    required this.onEnter,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(35.0),
        child: Column(
          children: [
            IconInputBox(
              width: 360,
              height: 44,
              icon: SvgPicture.asset('assets/icons/account.svg'),
              hintText: "아이디 입력",
              controller: controller.id,
              onClickEnter: (_) => controller.pwNode.nextFocus(),
            ),
            SizedBox(height: 16),
            IconInputBox(
              width: 360,
              height: 44,
              icon: SvgPicture.asset('assets/icons/password.svg'),
              hintText: "비밀번호 입력",
              obscureText: true,
              controller: controller.pw,
              focusNode: controller.pwNode,
              onClickEnter: (_) => controller.onClickLoginButton(),
            ),
          ],
        ),
      ),
    );
  }
}
