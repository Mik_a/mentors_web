import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/login/views/login_form.dart';
import 'package:mentors_web/components/rounded_button.dart';
import 'package:mentors_web/global_colors.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MediaQuery.of(context).size.width < 480
          ? mobile(context)
          : web(context),
    );
  }

  Widget mobile(context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 40,
            vertical: 80,
          ),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(16),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  blurRadius: 3,
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 8.0,
                    bottom: 29,
                  ),
                  child: SvgPicture.asset(
                    'assets/icons/logo.svg',
                    color: CustomColors.cf2df88,
                    width: 86,
                    height: 86,
                  ),
                ),
                Text(
                  "MENTORS ADMIN",
                  style: TextStyle(
                    color: CustomColors.cf2df88,
                    fontSize: 24,
                    fontFamily: "Mont",
                  ),
                ),
                SizedBox(height: 30),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "아이디",
                          style: TextStyle(
                            color: CustomColors.ca9abaf,
                          ),
                        ),
                      ),
                      TextField(
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        decoration: InputDecoration(
                          focusColor: CustomColors.cdfdfdf,
                        ),
                        controller: controller.id,
                      ),
                      SizedBox(height: 40),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "비밀번호",
                          style: TextStyle(
                            color: CustomColors.ca9abaf,
                          ),
                        ),
                      ),
                      TextField(
                        controller: controller.pw,
                        obscureText: true,
                        style: TextStyle(
                          fontFamily: "Mont",
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 60,
                  ),
                  child: RoundedButton(
                    borderRadius: 8,
                    onClickButton: controller.onClickLoginButton,
                    title: "로그인",
                    width: double.infinity,
                    height: 60,
                    backgroundColor: CustomColors.c19A980,
                    textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget web(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/imgs/background.png'),
        ),
      ),
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            width: max(MediaQuery.of(context).size.width * 0.3, 500),
            height: max(MediaQuery.of(context).size.height * 0.8, 800),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(59)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 100),
                SvgPicture.asset('assets/imgs/logo.svg'),
                SizedBox(height: 50),
                LoginForm(
                  controller: controller,
                  onEnter: controller.onClickLoginButton,
                ),
                SizedBox(height: 30),
                RoundedButton(
                  onClickButton: controller.onClickLoginButton,
                  title: "로그인",
                  width: 260,
                  height: 48,
                  backgroundColor: Color(0xff00a6a6),
                  textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
