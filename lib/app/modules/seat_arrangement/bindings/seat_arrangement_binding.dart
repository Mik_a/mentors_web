import 'package:get/get.dart';

import '../controllers/seat_arrangement_controller.dart';

class SeatArrangementBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SeatArrangementController>(
      () => SeatArrangementController(),
    );
  }
}
