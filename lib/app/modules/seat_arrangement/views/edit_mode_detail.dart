import 'package:flutter/material.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/components/form_with_title.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';

class EditModeDetail extends StatelessWidget {
  final SeatArrangementController controller;
  const EditModeDetail({required this.controller, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormWithTitle(
      height: 200,
      title: "편집모드",
      child: Column(
        children: [
          Row(
            children: [
              editInput(
                "X좌표값",
                controller.posXController,
                ChangeSeatDataType.positionX,
              ),
              SizedBox(width: 16),
              editInput(
                "Y좌표값",
                controller.posYController,
                ChangeSeatDataType.positionY,
              ),
            ],
          ),
          SizedBox(height: 12),
          Row(
            children: [
              editInput(
                "넓이",
                controller.sizeXController,
                ChangeSeatDataType.sizeX,
              ),
              SizedBox(width: 16),
              editInput(
                "높이",
                controller.sizeYController,
                ChangeSeatDataType.sizeY,
              ),
            ],
          ),
          SizedBox(height: 12),
          Row(
            children: [
              editInput(
                "좌석번호",
                controller.indexController,
                null,
              ),
            ],
          ),
        ],
      ),
    );
  }

  RawKeyboardListener editInput(
      String title, TextEditingController tc, ChangeSeatDataType? type) {
    return RawKeyboardListener(
      focusNode: FocusNode(
        onKey: (node, event) {
          if (type == null) return KeyEventResult.ignored;
          controller.onKeyDown(event, tc, type);
          return KeyEventResult.ignored;
        },
      ),
      child: InputboxWithTitle(
        title: title,
        containerWidth: 112,
        inputWidth: 55,
        titleWidth: 44,
        textInputType: TextInputType.number,
        onChanged: (str) => controller.onChangeSeatDataOnEditForm(str, type),
        controller: tc,
      ),
    );
  }
}
