import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/edit_mode_view.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/edit_tools.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/no_section_list.dart';
import 'package:mentors_web/components/seat_arrangement/seat_arrangement_image.dart';
import 'package:mentors_web/components/section_list/section_list.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';

class SeatArrangementView extends StatelessWidget {
  final controller = Get.put(SeatArrangementController());
  final SectionListController sectionController =
      Get.put(SectionListController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => sectionController.sectionList.length == 0
          ? NoSectionList()
          : LayoutBuilder(builder: (context, constraints) {
              return Obx(() {
                return Row(
                  children: [
                    Expanded(
                      key: controller.frameSize,
                      child: SeatArrangementImage(),
                    ),
                    EditTools(),
                    Column(
                      children: [
                        Expanded(
                          child: controller.isEditingMode.isTrue
                              ? EditModeView(controller: controller)
                              : SectionList(
                                  axis: Axis.vertical,
                                  onClickSection: controller.onClickSection,
                                  selectedSection:
                                      controller.selectedSection.value,
                                ),
                        ),
                        controller.isEditingMode.isTrue
                            ? Row(
                                children: [
                                  CustomButton(
                                    title: "취소",
                                    width: 112,
                                    onPressed: controller.cancleEdit,
                                    type: CustomButtonType.cancle,
                                  ),
                                  SizedBox(width: 10),
                                  CustomButton(
                                    title: "저장",
                                    width: 112,
                                    onPressed: controller.toggleEditMode,
                                  ),
                                ],
                              )
                            : CustomButton(
                                title: "편집",
                                width: 248,
                                onPressed: controller.toggleEditMode,
                              ),
                        SizedBox(height: 20)
                      ],
                    ),
                  ],
                );
              });
            }),
    );
  }
}


// 12 : 10
