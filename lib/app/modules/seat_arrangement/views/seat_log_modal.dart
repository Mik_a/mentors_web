import 'package:flutter/material.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/components/drawer/userlog_list/user_log_item.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/modules/lib.dart';

class SeatLogModal extends StatefulWidget {
  final SeatArrangementClass locker;
  const SeatLogModal({
    Key? key,
    required this.locker,
  }) : super(key: key);

  @override
  State<SeatLogModal> createState() => _SeatLogModalState();
}

class _SeatLogModalState extends State<SeatLogModal> {
  List<IOLog> _logs = [];
  @override
  void initState() {
    super.initState();
    API.seat.getSeatLogs(widget.locker.id).then((value) {
      setState(() {
        _logs = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Modal(
      title: "${widget.locker.idx}번 좌석 로그",
      width: 432,
      height: 750,
      content: Container(
        child: ListView.separated(
          separatorBuilder: (context, index) => Divider(
            color: Color(0xffEFEFEF),
            height: 0,
          ),
          itemCount: _logs.length,
          itemBuilder: (context, index) {
            return UserLogItem(
              ioLog: _logs[index],
              showDetail: false,
              height: 44,
            );
          },
        ),
      ),
    );
  }

  Container item(IOLog log) {
    return Container(
      height: 44,
      child: Row(
        children: [
          Text(
            getKoreaTime(log.time).substring(5, 16),
            style: TextStyle(
              fontSize: 12,
              color: Color(0xffa3a2a3),
            ),
          ),
          SizedBox(width: 8),
          Text(
            log.ioType.name,
            style: TextStyle(
              color: log.ioType.color,
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          // if (log.userName != '') ...[
          //   Text(
          //     log.userName,
          //     style: TextStyle(
          //         fontWeight: FontWeight.bold, color: Color(0xff242424)),
          //   ),
          //   SizedBox(width: 8),
          // ],
          // Text(
          //   log.lockerMessage(widget.locker.idx),
          //   style: TextStyle(
          //     fontSize: 12,
          //     color: Color(0xff717071),
          //   ),
          // ),
        ],
      ),
    );
  }
}
