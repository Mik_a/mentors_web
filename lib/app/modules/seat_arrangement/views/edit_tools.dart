import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/global_colors.dart';

class EditTools extends StatelessWidget {
  EditTools({Key? key}) : super(key: key);
  final controller = Get.put(SeatArrangementController());

  Widget icon(Widget child) {
    return InkWell(
      child: SizedBox(
        width: 32,
        height: 32,
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),
      width: 40,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(
            color: CustomColors.cE3E3E3,
            width: 8,
          ),
          bottom: BorderSide(
            color: CustomColors.cE3E3E3,
            width: 8,
          ),
          left: BorderSide(
            color: CustomColors.cE3E3E3,
            width: 1,
          ),
          right: BorderSide(
            color: CustomColors.cE3E3E3,
            width: 1,
          ),
        ),
      ),
      child: Obx(
        () => Column(
          children: [
            // icon(InkWell(child: Icon(Icons.crop_free))),
            // Divider(),
            // icon(InkWell(child: Icon(Icons.open_with))),
            // icon(InkWell(child: Icon(Icons.keyboard_return))),
            // Divider(),
            icon(
              InkWell(
                onTap: controller.onClickDeleteSeatIcon,
                child: Icon(
                  Icons.delete,
                  color: controller.lastSelectedSeatIcon.value.id == ''
                      ? Colors.grey
                      : null,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
