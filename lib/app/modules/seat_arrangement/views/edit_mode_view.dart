import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/edit_mode_detail.dart';
import 'package:mentors_web/components/form_with_title.dart';
import 'package:mentors_web/components/seat_management/seat_type_list.dart';

class EditModeView extends StatelessWidget {
  EditModeView({
    Key? key,
    required this.controller,
  }) : super(key: key);
  final SeatArrangementController controller;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: 280,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            EditModeDetail(controller: controller),
            Expanded(
              child: FormWithTitle(
                title: "좌석",
                child: SeatTypeList(
                  selectedSeatType: controller.selectedSeatType.value,
                  onClickItem: controller.onClickSeatType,
                  width: double.infinity,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
