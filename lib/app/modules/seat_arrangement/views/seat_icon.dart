import 'package:flutter/material.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class SeatIcon extends StatelessWidget {
  final SeatArrangementClass? seatInfo;
  final User? user;

  final bool isDraggable;
  final bool isSelected;
  final bool isCantDraw;
  final bool isOut;
  final bool isMobile;

  final Offset position;
  final Offset size;
  final Function? onClick;

  const SeatIcon({
    required this.size,
    required this.position,
    this.isMobile = false,
    this.isOut = false,
    this.isCantDraw = false,
    this.isSelected = false,
    this.onClick,
    this.user,
    this.isDraggable = true,
    this.seatInfo,
    Key? key,
  }) : super(key: key);

  Widget icon(BuildContext context) {
    return isMobile
        ? Container(
            width: size.dx,
            height: size.dy,
            color: seatInfo != null && seatInfo!.isUsing
                ? seatInfo!.isOut
                    ? CustomColors.cFBA136
                    : CustomColors.c00C4D0
                : CustomColors.cACACAC,
            child: Center(
              child: Text(
                seatInfo!.idx.toString(),
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          )
        : GestureDetector(
            onTapDown: (detail) {
              if (onClick != null) {
                onClick!(seatInfo, context, detail);
              }
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(2)),
                border: Border.all(
                  width: 1,
                  color: isCantDraw
                      ? Colors.red
                      : isSelected
                          ? Colors.red
                          : seatInfo != null
                              ? seatInfo!.isUsing
                                  ? seatInfo!.isOut
                                      ? CustomColors.cFBA136
                                      : CustomColors.c00C4D0
                                  : CustomColors.cACACAC
                              : Colors.black,
                ),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      idxContainer(),
                      if (seatInfo != null && seatInfo!.seatUsingData != null)
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                seatInfo!.seatUsingData!.userName,
                                style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold,
                                  color: CustomColors.c717071,
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                  if (seatInfo != null && seatInfo!.seatUsingData != null) ...[
                    SizedBox(height: 5),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          seatInfo!.seatUsingData!.voucherType.toKor(),
                          style: TextStyle(
                            fontSize: 10,
                            color: CustomColors.c717071,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          seatInfo!.seatUsingData!.expiredDate == null
                              ? "-"
                              : seatInfo!.seatUsingData!.voucherType ==
                                      VoucherUseType.period
                                  ? getKoreaTime(
                                          seatInfo!.seatUsingData!.expiredDate!)
                                      .substring(5, 16)
                                  : seatInfo!.seatUsingData!.overflowTime !=
                                              null &&
                                          seatInfo!.seatUsingData!
                                                  .overflowTime! >
                                              0
                                      ? seatInfo!.seatUsingData!.overflowTime
                                              .toString() +
                                          '분 초과'
                                      : seatInfo!
                                          .seatUsingData!.remainTime.toTime,
                          style: TextStyle(
                            fontSize: 10,
                            color: seatInfo!.seatUsingData!.overflowTime !=
                                        null &&
                                    seatInfo!.seatUsingData!.overflowTime! > 0
                                ? Colors.red
                                : CustomColors.c717071,
                          ),
                        ),
                      ),
                    ),
                  ],
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned(
                          right: 3,
                          bottom: 4,
                          child: Text(
                            seatInfo != null ? seatInfo!.seatTypeName : '',
                            style: TextStyle(
                              color: CustomColors.cA3A2A3,
                              fontSize: 10,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
  }

  Container idxContainer() {
    return Container(
      decoration: BoxDecoration(
        color: seatInfo != null && seatInfo!.isUsing
            ? seatInfo!.isOut
                ? CustomColors.cFBA136
                : CustomColors.c00C4D0
            : CustomColors.cACACAC,
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(8),
        ),
      ),
      child: seatInfo != null
          ? Padding(
              padding: const EdgeInsets.only(
                left: 2.0,
                top: 1.0,
              ),
              child: Text(
                ("000" + seatInfo!.idx.toString()).substring(
                  seatInfo!.idx.toString().length,
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          : Text(""),
      width: 28,
      height: 18,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: position.dx,
      top: position.dy,
      width: size.dx,
      height: size.dy,
      child: icon(context),
    );
  }
}
