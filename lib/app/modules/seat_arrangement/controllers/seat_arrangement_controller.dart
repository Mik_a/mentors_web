import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/seat_log_modal.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/context_menu/context_menu_area.dart';
import 'package:mentors_web/components/context_menu_tile.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';

extension intExtension on int {
  String get toTime =>
      (this ~/ 60).toString().padLeft(2, '0') +
      ':' +
      (this % 60).toString().padLeft(2, '0');
}

class SeatUsingData {
  String userName;
  VoucherUseType voucherType;
  int remainTime;
  int? overflowTime;
  DateTime? expiredDate;

  SeatUsingData({
    this.expiredDate,
    this.overflowTime,
    required this.remainTime,
    required this.userName,
    required this.voucherType,
  });
}

class SeatArrangementClass {
  String id;
  Offset position;
  Offset size;

  bool isOut;
  bool isUsing;
  String seatTypeId;
  String seatTypeName;
  int idx;
  String? usingUserId;

  SeatUsingData? seatUsingData;

  Map<String, dynamic> toJson() => {
        'id': id,
        'index': idx,
        'pos': {
          "x": position.dx,
          "y": position.dy,
        },
        'size': {
          "x": size.dx,
          "y": size.dy,
        },
        'isUsing': isUsing,
        'isOut': isOut,
        'seatTypeId': seatTypeId,
        'usingUserId': usingUserId,
      };

  SeatArrangementClass({
    required this.idx,
    required this.id,
    required this.seatTypeId,
    required this.position,
    required this.size,
    this.seatTypeName = '',
    this.usingUserId,
    this.isOut = false,
    this.isUsing = false,
    this.seatUsingData,
  });
}

class SeatArrangementController extends GetxController {
  RxList<SeatArrangementClass> seatIconList = RxList<SeatArrangementClass>();
  double interval = 1;

  bool delay = false;

  Rx<bool> isMovingUser = false.obs;
  String? movingUserId;
  SeatArrangementClass? prevSeat;

  Rx<SeatArrangementClass> lastSelectedSeatIcon = SeatArrangementClass(
          id: '',
          idx: 0,
          position: Offset.zero,
          seatTypeId: '',
          size: Offset.zero,
          isUsing: false)
      .obs;

  Rx<bool> isEditingMode = false.obs;
  Rx<Section> selectedSection = Section(id: '', name: '').obs;
  Rx<SeatType> selectedSeatType = SeatType.empty().obs;

  Rx<DragStartDetails> startOffset = DragStartDetails().obs;
  Rx<DragUpdateDetails> lastOffset =
      DragUpdateDetails(globalPosition: Offset.zero).obs;

  Rx<Size> imageSize = Size(0, 0).obs;
  Rx<bool> isSectionLoading = true.obs;

  GlobalKey frameSize = GlobalKey();

  Rx<Offset> currentDrawingBoxSize = Offset.zero.obs;

  TextEditingController posXController = TextEditingController();
  TextEditingController posYController = TextEditingController();
  TextEditingController sizeXController = TextEditingController();
  TextEditingController sizeYController = TextEditingController();
  TextEditingController indexController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  void updateSeat() {
    if (selectedSection.value.id == '') {
      SectionListController sectionListController = Get.find();
      if (sectionListController.sectionList.length > 0) {
        selectedSection.value = sectionListController.sectionList[0];
      }
    }

    onClickSection(selectedSection.value);
  }

  void onClickSection(Section section) {
    seatIconList.value = [];
    selectedSection.value = section;
    isSectionLoading.value = true;

    EasyLoading.show(status: "좌석 정보를 불러오고 있습니다");

    Timer timer = Timer.periodic(Duration(seconds: 2), (timer) {
      setSeatArrangment(section, timer);
    });
    setSeatArrangment(section, timer);
  }

  void setSeatArrangment(Section section, Timer timer) {
    API.seat.getSeatArrangement(section.id).then((value) {
      seatIconList.value = value;
      timer.cancel();
      EasyLoading.dismiss();
    });
  }

  Offset normalize(Offset offset) {
    return Offset(
      offset.dx / imageSize.value.width,
      offset.dy / imageSize.value.height,
    );
  }

  // Drag ---------------------------------------
  void onDragStart(DragStartDetails event) {
    if (isEditingMode.isFalse || selectedSeatType.value.id == '') return;
    startOffset.value = event;
    lastOffset.value = DragUpdateDetails(globalPosition: event.globalPosition);
  }

  void onDragUpdate(DragUpdateDetails event) {
    if (isEditingMode.isFalse || selectedSeatType.value.id == '') return;
    currentDrawingBoxSize.value = getSizeDrawingBox();
    lastOffset.value = event;
  }

  void onDragEnd(DragEndDetails event, RenderBox renderBox) {
    if (isEditingMode.isFalse || selectedSeatType.value.id == '') return;
    Offset offset = Offset(
      min(startOffset.value.localPosition.dx,
          lastOffset.value.localPosition.dx),
      min(startOffset.value.localPosition.dy,
          lastOffset.value.localPosition.dy),
    );

    Offset position = normalize(offset);
    Offset size = normalize(getSizeDrawingBox());

    startOffset.value = DragStartDetails();
    lastOffset.value = DragUpdateDetails(globalPosition: Offset.zero);

    if (size.dx < 0.05 || size.dy < 0.05) {
      return;
    }

    SeatArrangementClass newArrangement = SeatArrangementClass(
      idx: seatIconList.length,
      id: '',
      position: position,
      size: size,
      seatTypeId: selectedSeatType.value.id,
    );

    lastSelectedSeatIcon.value = newArrangement;
    onClickSeatIcon(newArrangement, null, null);
    seatIconList.add(newArrangement);
  }

  Offset getIconPosition(SeatArrangementClass icon) {
    return Offset(
      icon.position.dx * imageSize.value.width,
      icon.position.dy * imageSize.value.height,
    );
  }

  Offset getIconSize(SeatArrangementClass icon) {
    return Offset(
      icon.size.dx * imageSize.value.width,
      icon.size.dy * imageSize.value.height,
    );
  }

  List<SeatArrangementClass> tempList = [];

  void cancleEdit() {
    isEditingMode.value = false;
    lastSelectedSeatIcon.value = SeatArrangementClass(
      idx: 0,
      id: '',
      seatTypeId: 'seatTypeId',
      position: Offset.zero,
      size: Offset.zero,
    );

    seatIconList.value = tempList;
  }

  void toggleEditMode() async {
    if (isEditingMode.isTrue) {
      EasyLoading.show(status: "저장 중 입니다");

      final res = await API.seat.saveSeatArrangement(
        getSeatArrangementJson(),
        selectedSection.value.id,
      );

      if (res) {
        EasyLoading.showSuccess("저장되었습니다");
      } else {
        EasyLoading.showError("저장에 실패했습니다");
        return;
      }

      lastSelectedSeatIcon.value = SeatArrangementClass(
        idx: 0,
        id: '',
        seatTypeId: 'seatTypeId',
        position: Offset.zero,
        size: Offset.zero,
      );
    } else {
      tempList = new List.from(seatIconList);
    }
    isEditingMode.value = !isEditingMode.value;
  }

  Offset getPositionDrawingBox(RenderBox renderBox) {
    Offset offset = Offset(
        min(startOffset.value.globalPosition.dx,
            lastOffset.value.globalPosition.dx),
        min(startOffset.value.globalPosition.dy,
            lastOffset.value.globalPosition.dy));
    return renderBox.globalToLocal(offset);
  }

  Offset getSizeDrawingBox() {
    return Offset(
        (startOffset.value.globalPosition.dx -
                lastOffset.value.globalPosition.dx)
            .abs(),
        (startOffset.value.globalPosition.dy -
                lastOffset.value.globalPosition.dy)
            .abs());
  }

  Offset getPosition(Offset offset) {
    return Offset(
      imageSize.value.width * offset.dx,
      imageSize.value.height * offset.dy,
    );
  }

  void onClickSeatType(SeatType seatType) {
    selectedSeatType.value = seatType;
  }

  void onClickDeleteSeatIcon() {
    if (lastSelectedSeatIcon.value.isUsing) {
      EasyLoading.showError("사용중인 좌석은 삭제할 수 없어요");
      return;
    }

    Get.dialog(
      Alert(
        alertType: AlertType.warning,
        onConfirm: () {
          seatIconList.remove(lastSelectedSeatIcon.value);
          Get.back();
        },
        title: "경고",
        content: "좌석을 삭제하시겠어요?",
        cancleTitle: "취소",
        onCancle: Get.back,
      ),
    );
  }

  void onClickSeatIcon(
    SeatArrangementClass seat,
    BuildContext? context,
    TapDownDetails? details,
  ) {
    if (isEditingMode.isTrue) {
      lastSelectedSeatIcon.value = seat;
      indexController.text = seat.idx.toString();
      sizeXController.text =
          (seat.size.dx * imageSize.value.width).toInt().toString();
      sizeYController.text =
          (seat.size.dy * imageSize.value.height).toInt().toString();
      posXController.text =
          (seat.position.dx * imageSize.value.width).toInt().toString();
      posYController.text =
          (seat.position.dy * imageSize.value.height).toInt().toString();
    } else {
      if (context != null && details != null) {
        List<Widget> contextMenuItems = [
          ContextMenuTile(
            title: "회원정보",
            icon: Icon(Icons.account_circle),
            onClick: () {
              UserRegisterController userRegisterController = Get.find();
              UserController userController = Get.find();

              final user = userController.userList
                  .firstWhere((element) => element.id == seat.usingUserId);
              userRegisterController.setCurrentUser(user);
              HomeController homeController = Get.find();
              homeController.showUserInfo();
              Navigator.of(context).pop();
            },
            isActive: seat.usingUserId != null,
          ),
          ContextMenuTile(
            title: "회원이동",
            icon: SvgPicture.asset('assets/icons/move.svg'),
            onClick: () {
              Get.back();
              isMovingUser.value = true;
              prevSeat = seat;
              movingUserId = seat.usingUserId;
              Get.dialog(
                Modal(
                  content: Alert(
                    title: "알림",
                    alertType: AlertType.alert,
                    onConfirm: Get.back,
                    content: "이동시킬 좌석을 선택해주세요",
                  ),
                ),
              );
            },
            isActive: seat.usingUserId != null,
          ),
          ContextMenuTile(
            icon: SvgPicture.asset('assets/icons/log.svg'),
            title: "좌석로그",
            onClick: () => {
              Get.dialog(
                SeatLogModal(locker: seat),
              )
            },
            isActive: true,
          ),
          ContextMenuTile(
            title: "퇴실처리",
            icon: SvgPicture.asset('assets/icons/exit_door.svg'),
            onClick: () {
              if (seat.usingUserId != null) {
                Navigator.of(context).pop();

                Get.dialog(
                  Modal(
                    content: Alert(
                      title: "경고",
                      alertType: AlertType.warning,
                      content: "정말로 퇴실 처리 하시겠습니까?",
                      onCancle: Get.back,
                      onConfirm: () async {
                        Get.back();
                        User? user = await Get.find<UserController>()
                            .exitThisUserById(seat.usingUserId!);

                        if (user != null) {
                          seat.isUsing = false;
                          seat.isOut = false;
                          seat.seatUsingData = null;
                          seatIconList.refresh();
                          EasyLoading.showSuccess("퇴실처리되었습니다.");
                        } else {
                          EasyLoading.showError("퇴실처리에 실패했습니다.");
                        }
                      },
                    ),
                  ),
                );
              }
            },
            isActive: seat.isUsing,
          ),
          // ContextMenuTile(
          //   title: "비활성화",
          //   icon: SvgPicture.asset('assets/icons/lock.svg'),
          //   onClick: () {
          //     if (seat.usingUserId != null) {
          //       Navigator.of(context).pop();

          //       Get.dialog(
          //         Modal(
          //           content: Alert(
          //             title: "경고",
          //             alertType: AlertType.warning,
          //             content: "정말로 퇴실 처리 하시겠습니까?",
          //             onCancle: Get.back,
          //             onConfirm: () {
          //               API.seat.outThisSeat(seat.usingUserId!).then(
          //                 (value) {
          //                   Get.back();
          //                   if (value) {
          //                     seat.isUsing = false;
          //                     seat.isOut = false;
          //                     seat.seatUsingData = null;
          //                     seatIconList.refresh();
          //                   }
          //                 },
          //               );
          //             },
          //           ),
          //         ),
          //       );
          //     }
          //   },
          //   isActive: !seat.isUsing,
          // ),
        ];

        showContextMenu(
          details.globalPosition,
          context,
          contextMenuItems,
          0.0,
          100.0,
          200,
        );
      }
    }
  }

  Size framesize = Size(0, 0);

  void frameChange(double width, double height) {
    framesize = Size(width, height);
  }

  void onImageSizeChanged(Size size) async {
    Size frame = framesize;
    double width = size.width;
    double height = size.height;

    if (size.width > frame.width) {
      width = frame.width;
      height = size.height * (frame.width / size.width);
    }

    if (height > frame.height) {
      width = width * (frame.height / height);
      height = frame.height;
    }

    imageSize.value = Size(width, height);
    isSectionLoading.value = false;
  }

  void onChangeSeatDataOnEditForm(String str, ChangeSeatDataType? type) {
    double val = double.tryParse(str) ?? 0;

    if (type == null) {
      lastSelectedSeatIcon.refresh();
      lastSelectedSeatIcon.value.idx = val.toInt();
      return;
    }

    if (type == ChangeSeatDataType.positionX ||
        type == ChangeSeatDataType.sizeX) {
      val /= imageSize.value.width;
    } else if (type == ChangeSeatDataType.positionY ||
        type == ChangeSeatDataType.sizeY) {
      val /= imageSize.value.height;
    }

    Offset offset = Offset(
      type == ChangeSeatDataType.positionX || type == ChangeSeatDataType.sizeX
          ? val
          : type == ChangeSeatDataType.positionY
              ? lastSelectedSeatIcon.value.position.dx
              : lastSelectedSeatIcon.value.size.dx,
      type == ChangeSeatDataType.positionY || type == ChangeSeatDataType.sizeY
          ? val
          : type == ChangeSeatDataType.positionX
              ? lastSelectedSeatIcon.value.position.dy
              : lastSelectedSeatIcon.value.size.dy,
    );

    if (type == ChangeSeatDataType.sizeX || type == ChangeSeatDataType.sizeY) {
      lastSelectedSeatIcon.value.size = offset;
    } else {
      lastSelectedSeatIcon.value.position = offset;
    }

    lastSelectedSeatIcon.refresh();
  }

  List<Map<String, dynamic>> getSeatArrangementJson() {
    final json = seatIconList.map((element) => element.toJson()).toList();
    return json;
  }

  void onClickMoveSeat(SeatArrangementClass seat) {
    if (seat.isUsing) return;

    Get.dialog(
      Modal(
        content: Alert(
          alertType: AlertType.alert,
          title: "알림",
          onConfirm: () async {
            Get.back();
            final res = await API.seat.moveSeat(movingUserId!, seat.id);

            if (res) {
              seat.isOut = prevSeat!.isOut;
              seat.isUsing = prevSeat!.isUsing;
              seat.seatUsingData = prevSeat!.seatUsingData;
              seat.usingUserId = prevSeat!.usingUserId;

              prevSeat!.isOut = false;
              prevSeat!.isUsing = false;
              prevSeat!.seatUsingData = null;
              prevSeat!.usingUserId = null;
            }

            cancleMove();
          },
          onCancle: Get.back,
          content: '${seat.idx}번 좌석으로 이동하시겠습니까?',
        ),
      ),
    );
  }

  void cancleMove() {
    prevSeat = null;
    movingUserId = null;
    isMovingUser.value = false;
  }

  void onKeyDown(RawKeyEvent event, TextEditingController controller,
      ChangeSeatDataType? type) {
    if (delay) return;

    Future.delayed(Duration(milliseconds: 100), () => delay = false);

    delay = true;
    switch (event.data.logicalKey.keyLabel) {
      case "Arrow Down":
        int size = int.parse(controller.text) - 1;
        if (type == ChangeSeatDataType.sizeX ||
            type == ChangeSeatDataType.sizeY) size = max(size, 60);
        controller.text = size.toString();
        onChangeSeatDataOnEditForm(controller.text, type);
        break;
      case "Arrow Up":
        controller.text = (int.parse(controller.text) + 1).toString();
        onChangeSeatDataOnEditForm(controller.text, type);
        break;
      case "Arrow Left":
        int size = int.parse(controller.text) - 10;
        if (type == ChangeSeatDataType.sizeX ||
            type == ChangeSeatDataType.sizeY) size = max(size, 60);
        controller.text = size.toString();
        onChangeSeatDataOnEditForm(controller.text, type);
        break;
      case "Arrow Right":
        controller.text = (int.parse(controller.text) + 10).toString();
        onChangeSeatDataOnEditForm(controller.text, type);
        break;
    }
  }
}

enum ChangeSeatDataType {
  positionX,
  positionY,
  sizeX,
  sizeY,
  idx,
}
