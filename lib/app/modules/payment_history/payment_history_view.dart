import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_checkbox.dart';
import 'package:mentors_web/components/datepicker.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/components/payment_detail_modal.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/controller/excel_controller.dart';
import 'package:mentors_web/controller/store_management_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';
import 'package:mentors_web/classes/voucher.dart';

class PaymentHistoryView extends StatefulWidget {
  PaymentHistoryView({Key? key}) : super(key: key);

  @override
  State<PaymentHistoryView> createState() => _PaymentHistoryViewState();
}

class _PaymentHistoryViewState extends State<PaymentHistoryView> {
  final UserController controller = Get.find();
  TextEditingController userName = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController minPrice = TextEditingController();
  TextEditingController maxPrice = TextEditingController();
  int page = 0;
  int size = 0;

  @override
  void initState() {
    super.initState();
    updateList();
    debouncing(userName);
    debouncing(minPrice);
    debouncing(maxPrice);
  }

  @override
  void dispose() {
    super.dispose();
    userName.dispose();
    minPrice.dispose();
    maxPrice.dispose();
  }

  void onClickDownloadExcel() async {
    List<String> voucherType = [];

    if (day) voucherType.add("day");
    if (time) voucherType.add("time");
    if (period) voucherType.add("period");
    if (locker) voucherType.add("locker");

    final res = await API.log.getPaymentLogs(page, 99999, {
      "voucherTypes": jsonEncode(voucherType),
      "userName": userName.text,
      "startDate": startDate.text,
      "endDate": endDate.text,
      "minPrice": minPrice.text,
      "maxPrice": maxPrice.text,
    });

    List<SalesLog> logs = res['list'];

    ExcelController().downLoadExcelFile(
      fileName:
          "${Get.find<StoreManagementController>().storeInfo.value.storeName} - 회원 목록",
      headers: ["주문일시", "결제방식", "이름", "상품", "결제금액", "결제수단", "구분"],
      data: logs
          .map<List<String>>(
            (element) => [
              getKoreaTime(element.eventTime),
              element.isWeb ? "운영PC" : '키오스크',
              element.userName ?? "-",
              element.itemName,
              element.price.toString(),
              element.payType1 ?? '-',
              "정상",
            ],
          )
          .toList(),
    );
  }

  List<List<SalesLog>> cacheList = [];
  List<SalesLog> salesLogs = [];

  bool day = false;
  bool time = false;
  bool period = false;
  bool locker = false;

  void searchInit() {
    setState(() {
      userName.text = '';
      startDate.text = '';
      endDate.text = '';
      minPrice.text = '';
      maxPrice.text = '';
      day = false;
      time = false;
      period = false;
      locker = false;
    });
  }

  void debouncing(TextEditingController c) {
    Timer? timer;

    Timer updateTimer() {
      return Timer(Duration(milliseconds: 400), () {
        updateList();
      });
    }

    c.addListener(() {
      if (timer != null) {
        timer!.cancel();
        timer = updateTimer();
      } else {
        timer = updateTimer();
      }
    });
  }

  void updateList() async {
    List<String> voucherType = [];

    if (day) voucherType.add("day");
    if (time) voucherType.add("time");
    if (period) voucherType.add("period");
    if (locker) voucherType.add("locker");

    final res = await API.log.getPaymentLogs(page, 20, {
      "voucherTypes": jsonEncode(voucherType),
      "userName": userName.text,
      "startDate": startDate.text,
      "endDate": endDate.text,
      "minPrice": minPrice.text,
      "maxPrice": maxPrice.text,
    });

    setState(() {
      page = min(page, res['size']);
      size = res['size'];
      salesLogs = res['list'];
    });
  }

  @override
  Widget build(BuildContext context) {
    var searchUser = Container(
      height: 160,
      padding: const EdgeInsets.only(left: 16, right: 16),
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 6),
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xffa3a2a3),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 44,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "결제 내역 검색",
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Color(0xff242424),
                  ),
                ),
                SquareButton(
                  height: 28,
                  onClick: searchInit,
                  icon: SvgPicture.asset('assets/icons/refresh.svg'),
                  title: "검색초기화",
                )
              ],
            ),
          ),
          Divider(height: 0),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 470,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            AutoSpacingText(
                              text: "구분",
                              width: 42,
                            ),
                            SizedBox(width: 20),
                            CustomCheckbox(
                              value: day,
                              onChange: (_) {
                                setState(() {
                                  day = !day;
                                });
                                updateList();
                              },
                              title: "당일권",
                            ),
                            SizedBox(width: 20),
                            CustomCheckbox(
                              value: time,
                              onChange: (_) {
                                setState(() {
                                  time = !time;
                                });
                                updateList();
                              },
                              title: "시간권",
                            ),
                            SizedBox(width: 20),
                            CustomCheckbox(
                              value: period,
                              onChange: (_) {
                                setState(() {
                                  period = !period;
                                });
                                updateList();
                              },
                              title: "기간권",
                            ),
                            SizedBox(width: 20),
                            // CustomCheckbox(
                            //   value: locker,
                            //   onChange: (_) {
                            //     setState(() {
                            //       locker = !locker;
                            //     });
                            //     updateList();
                            //   },
                            //   title: "사물함",
                            // ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InputboxWithTitle(
                        containerWidth: 186,
                        hintText: "회원 이름 입력",
                        inputHeight: 32,
                        inputWidth: 136,
                        title: "이름",
                        titleWidth: 42,
                        controller: userName,
                      ),
                      Row(
                        children: [
                          AutoSpacingText(
                            text: "기간",
                            width: 44,
                          ),
                          SizedBox(width: 10),
                          Container(
                            width: 136,
                            height: 32,
                            child: DatePicker(
                              hintText: "시작일 지정",
                              controller: startDate,
                              onDateChanged: (_) {
                                updateList();
                              },
                            ),
                          ),
                          Text(
                            " ~ ",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Color(0xffa3a2a3),
                            ),
                          ),
                          Container(
                            width: 136,
                            height: 32,
                            child: DatePicker(
                              hintText: "종료일 지정",
                              controller: endDate,
                              onDateChanged: (_) {
                                updateList();
                              },
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          AutoSpacingText(
                            text: "합계금액",
                            width: 44,
                          ),
                          SizedBox(width: 10),
                          InputBox(
                            width: 136,
                            height: 32,
                            hintText: "최소금액 입력",
                            textInputType: TextInputType.number,
                            controller: minPrice,
                          ),
                          Text(
                            " ~ ",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Color(0xffa3a2a3),
                            ),
                          ),
                          InputBox(
                            width: 136,
                            height: 32,
                            hintText: "최대금액 입력",
                            textInputType: TextInputType.number,
                            controller: maxPrice,
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );

    List<Widget> returnUserItem(SalesLog log) {
      return [
        Text(
          getKoreaTime(log.eventTime),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        Text(
          log.payDevice == PayDevice.pc ? "운영PC" : "키오스크",
          // log.payDevice.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        Text(
          log.userName ?? "-",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        Text(
          // log.voucherType != null ? log.voucherType!.toKor() : "-",
          log.itemName,
          textAlign: TextAlign.center,
        ),
        Text(
          addCommaInMoney(log.price),
          textAlign: TextAlign.center,
        ),
        Text(
          log.payType1 == "cash" ? "현금" : log.payType1.toString(),
          textAlign: TextAlign.center,
        ),
        // Text(
        //   user.status.toKor(),
        //   textAlign: TextAlign.center,
        // ),
        Text(
          '구매',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        GestureDetector(
          onTap: () => onClickPaymentDetail(log),
          child: Container(
            margin: EdgeInsets.only(right: 12),
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xffa3a2a3),
              ),
              borderRadius: BorderRadius.circular(4),
            ),
            width: 56,
            height: 24,
            child: Center(
              child: Text(
                "결제상세",
                style: TextStyle(fontSize: 12),
              ),
            ),
          ),
        ),
      ];
    }

    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: RegisterCard(
        action: SquareButton(
          height: 28,
          onClick: onClickDownloadExcel,
          icon: Image.asset('assets/icons/excel.png'),
          title: "엑셀 다운로드",
        ),
        title: "결제 내역",
        child: Column(
          children: [
            searchUser,
            Expanded(
              child: PagenationListView(
                currentPage: page,
                itemHeight: 32,
                data: [
                  ...List.generate(
                    salesLogs.length,
                    (index) => [
                      Text(
                        "${index + 1}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      ...returnUserItem(salesLogs[index]),
                    ],
                  )
                ],
                headerWidths: [40, 140, 80, 80, 220, 100, 100, 60, 74],
                headerTitles: [
                  Text("No."),
                  Text("주문일시"),
                  Text("결제방식"),
                  Text("이름"),
                  Text("상품"),
                  Text("결제금액"),
                  Text("결제수단"),
                  Text("구분"),
                  Text("관리"),
                ],
              ),
            ),
            Pagenation(
              height: 40,
              currentPage: page,
              totalLength: size,
              itemPerPage: 20,
              setPage: (page) {
                setState(() {
                  this.page = page;
                });
                updateList();
              },
            ),
            SizedBox(height: 10)
          ],
        ),
      ),
    );
  }
}
