import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/locker_management/controllers/locker_management_controller.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_voucher_list.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/locker.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/find_user_form.dart';
import 'package:mentors_web/components/payment_form.dart';

class LockerAddUserModal extends StatefulWidget {
  final Locker locker;
  const LockerAddUserModal({required this.locker, Key? key}) : super(key: key);

  @override
  _LockerAddUserModalState createState() => _LockerAddUserModalState();
}

class _LockerAddUserModalState extends State<LockerAddUserModal> {
  LockerVoucher? selectedVoucher;
  User? selectedUser;
  LockerManagementController controller = Get.find();

  void onClickItem(LockerVoucher voucher) {
    setState(() {
      selectedVoucher = voucher;
      controller.cashPriceController.text = voucher.price.toString();
    });
  }

  void onClickUser(User user) {
    setState(() {
      selectedUser = user;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        LockerVoucherList(
          onClickItem: onClickItem,
          selectedVoucher: selectedVoucher,
        ),
        SizedBox(width: 20),
        FindUserForm(
          onClickUser: onClickUser,
          isDisplayGuest: false,
          selectedUsers: [if (selectedUser != null) selectedUser!],
        ),
        SizedBox(width: 20),
        RegisterCard(
          title: "결제 수단 및 결제 금액",
          width: 400,
          height: 170,
          child: PaymentForm(
            cashPriceController: controller.cashPriceController,
            cardPriceController: controller.cardPriceController,
            isEnable: selectedVoucher != null && selectedUser != null,
            onClickPayment: () {
              controller.addUserInLocker(
                selectedVoucher!,
                widget.locker,
                selectedUser!,
                int.parse(controller.cardPriceController.text),
                int.parse(controller.cashPriceController.text),
              );
            },
            price: selectedVoucher == null ? 0 : selectedVoucher!.price,
          ),
        )
      ],
    );
  }
}
