import 'package:flutter/material.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/locker.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/modules/lib.dart';

class LockerLogModal extends StatefulWidget {
  final Locker locker;
  const LockerLogModal({
    Key? key,
    required this.locker,
  }) : super(key: key);

  @override
  State<LockerLogModal> createState() => _LockerLogModalState();
}

class _LockerLogModalState extends State<LockerLogModal> {
  List<Log> _logs = [];
  @override
  void initState() {
    super.initState();
    API.locker.getLockerLogs(widget.locker.id).then((value) {
      setState(() {
        _logs = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: _logs.length,
        itemBuilder: (context, index) {
          return item(_logs[index]);
        },
      ),
    );
  }

  Container item(Log log) {
    return Container(
      height: 44,
      child: Row(
        children: [
          Text(
            getKoreaTime(log.time).substring(5, 16),
            style: TextStyle(
              fontSize: 12,
              color: Color(0xffa3a2a3),
            ),
          ),
          SizedBox(width: 8),
          if (log.userName != '') ...[
            Text(
              log.userName,
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Color(0xff242424)),
            ),
            SizedBox(width: 8),
          ],
          Text(
            log.lockerMessage(widget.locker.index),
            style: TextStyle(
              fontSize: 12,
              color: Color(0xff717071),
            ),
          ),
        ],
      ),
    );
  }
}
