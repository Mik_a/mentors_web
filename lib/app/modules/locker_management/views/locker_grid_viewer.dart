import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/locker_management/controllers/locker_management_controller.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_item.dart';
import 'package:mentors_web/controller/my_custom_scroll_behavior.dart';
import 'package:mentors_web/global_colors.dart';

class LockerGridViewer extends StatelessWidget {
  final LockerManagementController controller;
  LockerGridViewer({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        int xSize = int.tryParse(controller.sizeXController.text) ?? 0;
        int ySize = int.tryParse(controller.sizeYController.text) ?? 0;
        return controller.isLoading.value
            ? Expanded(child: Center(child: CircularProgressIndicator()))
            : Expanded(
                child: Center(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: ScrollConfiguration(
                      behavior: MyCustomScrollBehavior(),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Container(
                          color: CustomColors.cF3F3F3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ...List.generate(
                                ySize,
                                (y) => Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ...List.generate(xSize, (x) {
                                      int idx = (y * xSize) + x;
                                      return LockerItem(
                                        isMovingMode:
                                            controller.movingUser.value != '',
                                        onClickLockerItem: (details) {
                                          if (controller.movingUser.value ==
                                              '') {
                                            controller.onClickLocker(
                                              details,
                                              controller.lockerList[idx],
                                              context,
                                            );
                                          } else {
                                            controller.onClickMoveTargetLocker(
                                              controller.lockerList[idx],
                                              context,
                                            );
                                          }
                                        },
                                        locker: controller.lockerList[idx],
                                      );
                                    })
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
      },
    );
  }
}
