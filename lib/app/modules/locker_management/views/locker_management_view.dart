import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_grid_viewer.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_input_form.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/no_section_list.dart';
import 'package:mentors_web/components/section_list/section_list.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';

import '../controllers/locker_management_controller.dart';

class LockerManagementView extends GetView<LockerManagementController> {
  final SectionListController sectionController =
      Get.put(SectionListController());

  final LockerManagementController controller =
      Get.put(LockerManagementController());
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => sectionController.sectionList.length == 0
          ? NoSectionList()
          : Container(
              width: double.infinity,
              height: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  controller.selectedSection.value.id == ''
                      ? Expanded(child: Center(child: Text("배치도를 선택해주세요")))
                      : LockerGridViewer(controller: controller),
                  Container(
                    width: 280,
                    color: Colors.white,
                    child: Column(
                      children: [
                        LockerInputForm(controller: controller),
                        Expanded(
                          child: SectionList(
                            selectedSection: controller.selectedSection.value,
                            onClickSection: controller.onClickSection,
                            isCanAddSection: false,
                            axis: Axis.vertical,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CustomButton(
                            width: double.infinity,
                            title: "저장",
                            onPressed: controller.setLockerStatus,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
