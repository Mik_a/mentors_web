import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/locker_management/controllers/locker_management_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class LockerVoucherList extends StatelessWidget {
  final Function(LockerVoucher) onClickItem;
  final LockerVoucher? selectedVoucher;

  LockerVoucherList({
    required this.onClickItem,
    this.selectedVoucher,
    Key? key,
  }) : super(key: key);

  final LockerManagementController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return RegisterCard(
      title: "이용권 목록",
      width: 450,
      height: 750,
      child: ListView.separated(
          itemBuilder: (context, index) =>
              item(controller.lockerVoucherList[index]),
          separatorBuilder: (context, index) => Divider(height: 0),
          itemCount: controller.lockerVoucherList.length),
    );
  }

  InkWell item(LockerVoucher voucher) {
    return InkWell(
      onTap: () {
        onClickItem(voucher);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        height: 40,
        color: selectedVoucher == voucher
            ? CustomColors.cFFF4C2
            : Colors.transparent,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('${voucher.day}일권'),
            Row(
              children: [
                Text(
                  '${addCommaInMoney(voucher.price)}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(' 원'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
