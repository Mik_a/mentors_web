import 'package:flutter/material.dart';
import 'package:mentors_web/classes/locker.dart';
import 'package:mentors_web/global_colors.dart';

class LockerItem extends StatelessWidget {
  final Function(TapDownDetails) onClickLockerItem;

  final bool isMovingMode;
  final Locker locker;
  LockerItem({
    this.isMovingMode = false,
    required this.onClickLockerItem,
    required this.locker,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final remainTime = locker.expriedDate != null
        ? locker.expriedDate!.difference(DateTime.now())
        : Duration.zero;

    return GestureDetector(
      onTapDown: onClickLockerItem,
      child: Container(
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(8),
        width: 96,
        height: 108,
        decoration: BoxDecoration(
          border: Border.all(
            color: locker.isUsing
                ? isMovingMode
                    ? CustomColors.cE3E3E3
                    : CustomColors.c23c7d1
                : isMovingMode
                    ? CustomColors.c23c7d1
                    : CustomColors.cE3E3E3,
            width: 4,
          ),
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: locker.isEnable ? Colors.white : CustomColors.cE3E3E3,
        ),
        child: Column(
          children: [
            Text(
              locker.index.toString(),
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
            Text(locker.usingUserName ?? ''),
            // if (locker.expriedDate != null)
            if (locker.expriedDate != null)
              Text(remainTime.inDays != 0
                  ? 'D-${remainTime.inDays.toString()}'
                  : '${remainTime.inHours == 0 ? '' : remainTime.inHours.toString() + '시간\n'}  ${remainTime.inMinutes - remainTime.inHours * 60}분 남음')
          ],
        ),
      ),
    );
  }
}
