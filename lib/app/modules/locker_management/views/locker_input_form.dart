import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mentors_web/app/modules/locker_management/controllers/locker_management_controller.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/global_colors.dart';

class LockerInputForm extends StatelessWidget {
  final LockerManagementController controller;
  const LockerInputForm({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "편집모드",
                style: TextStyle(color: CustomColors.c242424, fontSize: 16),
              ),
            ),
            Divider(height: 24),
            line(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: [
                  InputboxWithTitle(
                    containerWidth: 114,
                    titleWidth: 44,
                    inputWidth: 62,
                    inputHeight: 32,
                    title: "시작번호",
                    textAlign: TextAlign.center,
                    controller: controller.startIndexController,
                  ),
                  SizedBox(width: 16),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Padding line() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: [
          InputboxWithTitle(
            containerWidth: 112,
            titleWidth: 42,
            inputWidth: 62,
            inputHeight: 32,
            title: "가로",
            textAlign: TextAlign.center,
            controller: controller.sizeYController,
          ),
          SizedBox(width: 16),
          InputboxWithTitle(
            containerWidth: 112,
            titleWidth: 42,
            inputWidth: 62,
            inputHeight: 32,
            title: "세로",
            textAlign: TextAlign.center,
            controller: controller.sizeXController,
          ),
        ],
      ),
    );
  }
}
