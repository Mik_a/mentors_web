import 'package:get/get.dart';

import '../controllers/locker_management_controller.dart';

class LockerManagementBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LockerManagementController>(
      () => LockerManagementController(),
    );
  }
}
