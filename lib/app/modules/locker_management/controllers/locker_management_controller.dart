import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_add_user_modal.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_log_modal.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/classes/locker.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/context_menu/context_menu_area.dart';
import 'package:mentors_web/components/context_menu_tile.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';

class LockerManagementController extends GetxController {
  RxList<Locker> lockerList = RxList.empty();

  RxList<LockerVoucher> lockerVoucherList = RxList.empty();
  RxBool isLoading = true.obs;
  Rx<Section> selectedSection = Section(id: '', name: '').obs;

  Rx<String> movingUser = ''.obs;
  Locker? prevLocker;

  TextEditingController sizeXController = TextEditingController(text: "0");
  TextEditingController sizeYController = TextEditingController(text: "0");
  TextEditingController amountController = TextEditingController(text: "0");

  TextEditingController cardPriceController = TextEditingController(text: "0");
  TextEditingController cashPriceController = TextEditingController(text: "0");

  TextEditingController startIndexController = TextEditingController(text: "0");

  Locker? currentSelectedLocker;

  List<Locker> getLockerListFromList(List<dynamic> list) {
    return List.from(list)
        .map(
          (e) => Locker(
            index: e['index'],
            id: e['_id'],
            isEnable: e['isEnable'],
            isUsing: e['isUsing'],
            usingUserId: e['usingUserId'],
            usingUserName: e['usingUserName'],
            expriedDate: DateTime.tryParse(e['expiredDate'] ?? ''),
            startedDate: DateTime.tryParse(e['startedDate'] ?? ''),
          ),
        )
        .toList();
  }

  @override
  void onInit() {
    super.onInit();
    getLockerVouchers();
  }

  void setLockerStatus() async {
    EasyLoading.show(status: "저장 중 입니다");
    if (sizeXController.text == 'null') sizeXController.text = '0';
    if (sizeYController.text == 'null') sizeYController.text = '0';
    final result = await API.locker.setLockerStatus(selectedSection.value.id,
        sizeXController.text, sizeYController.text, startIndexController.text);

    if (result['success']) {
      EasyLoading.showSuccess("저장되었습니다");
      lockerList.value = getLockerListFromList(result['data']);
      lockerList.refresh();
    }
  }

  void addUserInLocker(
    LockerVoucher voucher,
    Locker locker,
    User user,
    int cardPrice,
    int cashPrice,
  ) async {
    EasyLoading.show(status: "이용권 구매중입니다");

    Locker? newLocker = await API.payment.buyLocker(
      lokcerId: locker.id,
      userId: user.id,
      voucherId: voucher.id,
    );

    if (newLocker != null) {
      // locker = newLocker;
      locker.expriedDate = newLocker.expriedDate;
      locker.isUsing = newLocker.isUsing;
      locker.startedDate = newLocker.startedDate;
      locker.usingUserId = newLocker.usingUserId;
      locker.usingUserName = newLocker.usingUserName;

      lockerList.refresh();
      EasyLoading.showSuccess("이용권을 구매했습니다");

      if (cardPrice != 0) {
        await API.payment.addPayLog(
          price: cardPrice,
          userid: user.id,
          voucherId: voucher.id,
          lockerTypeId: locker.id,
          method: PayMethod.card,
        );
      }

      if (cashPrice != 0) {
        await API.payment.addPayLog(
          price: cashPrice,
          userid: user.id,
          voucherId: voucher.id,
          lockerTypeId: locker.id,
          method: PayMethod.cash,
        );
      }

      Get.back();
    } else {
      EasyLoading.showError("이용권 구매에 실패했습니다.\n새로고침 후 다시 시도해주세요");
    }
  }

  void onClickMoveTargetLocker(Locker locker, BuildContext context) {
    Get.dialog(
      Modal(
        content: Alert(
          title: "알림",
          alertType: AlertType.alert,
          content: '${locker.index}번 사물함으로 이동시키겠습니까?',
          onConfirm: () async {
            Navigator.of(context).pop();

            bool res = await API.locker.moveLocker(prevLocker!.id, locker.id);

            EasyLoading.show(status: "이동 중 입니다");
            if (res) {
              EasyLoading.showSuccess("이동되었습니다");

              locker.isUsing = true;
              locker.usingUserId = prevLocker!.usingUserId;
              locker.usingUserName = prevLocker!.usingUserName;
              locker.expriedDate = prevLocker!.expriedDate;
              locker.startedDate = prevLocker!.startedDate;

              prevLocker!.isUsing = false;
              prevLocker!.usingUserId = null;
              prevLocker!.usingUserName = null;
              prevLocker!.expriedDate = null;
              prevLocker!.startedDate = null;
            } else {
              EasyLoading.showError("이동에 실패했습니다. 새로고침 후 다시 시도해주세요");
            }

            movingUser.value = '';
            prevLocker = null;
          },
          onCancle: () {
            Navigator.of(context).pop();
            movingUser.value = '';
            prevLocker = null;
          },
        ),
      ),
    );
  }

  void onClickLogView() {
    Get.dialog(
      Modal(
        title: "${currentSelectedLocker!.index}번 사물함 로그",
        width: 432,
        height: 750,
        content: LockerLogModal(
          locker: currentSelectedLocker!,
        ),
      ),
    );
  }

  void onClickUserInfo() {
    UserRegisterController userRegisterController = Get.find();
    UserController userController = Get.find();
    Get.back();

    final user = userController.userList.firstWhere(
        (element) => element.id == currentSelectedLocker!.usingUserId);
    userRegisterController.toDetailView(user);
  }

  void onClickMoveLocker() {
    Get.back();
    movingUser.value = currentSelectedLocker!.usingUserId!;
    prevLocker = currentSelectedLocker;

    Get.dialog(
      Modal(
        content: Alert(
          title: "알림",
          alertType: AlertType.alert,
          content: "이동시킬 자리를 선택해주세요",
          onConfirm: Get.back,
        ),
      ),
    );
  }

  void disposeLocker() {
    Get.back();

    Get.dialog(
      Modal(
        content: Alert(
          title: "경고",
          alertType: AlertType.warning,
          content: "사물함을 비우시겠어요?",
          onConfirm: () async {
            Get.back();

            EasyLoading.show(status: "사물함을 비우는 중 입니다");
            final res =
                await API.locker.vacateLocker(currentSelectedLocker!.id);
            if (res) {
              EasyLoading.showSuccess("사물함을 비웠습니다");
              currentSelectedLocker!.isUsing = false;
              currentSelectedLocker!.usingUserId = null;
              currentSelectedLocker!.usingUserName = null;
              currentSelectedLocker!.expriedDate = null;
              currentSelectedLocker!.startedDate = null;
              lockerList.refresh();
            } else {
              EasyLoading.showSuccess("사물함 비우는데 실패했습니다. 새로고침 후 다시 시도해주세요");
            }
          },
          onCancle: () {
            Get.back();
          },
        ),
      ),
    );
  }

  void addUser() {
    Get.back();

    Get.dialog(
      Modal(
        content: LockerAddUserModal(
          locker: currentSelectedLocker!,
        ),
      ),
    );
  }

  void onClickLocker(
    TapDownDetails details,
    Locker locker,
    BuildContext context,
  ) {
    currentSelectedLocker = locker;
    List<Widget> contextMenuItems = [
      ContextMenuTile(
        title: "회원정보",
        icon: Icon(Icons.account_circle),
        onClick: onClickUserInfo,
        isActive: currentSelectedLocker!.isUsing,
      ),
      ContextMenuTile(
        title: "자리이동",
        icon: SvgPicture.asset('assets/icons/move.svg'),
        onClick: onClickMoveLocker,
        isActive: currentSelectedLocker!.isUsing,
      ),
      ContextMenuTile(
        title: "비우기",
        icon: SvgPicture.asset('assets/icons/dispose.svg'),
        onClick: disposeLocker,
        isActive: currentSelectedLocker!.isUsing,
      ),
      ContextMenuTile(
        title: "사용자 추가",
        icon: Icon(Icons.add_circle),
        onClick: addUser,
        isActive: !currentSelectedLocker!.isUsing,
      ),
      ContextMenuTile(
        title: "로그 보기",
        icon: Icon(Icons.account_circle),
        onClick: onClickLogView,
        isActive: true,
      ),
      ContextMenuTile(
        title: currentSelectedLocker!.isEnable ? "비활성화" : "활성화",
        icon: currentSelectedLocker!.isEnable
            ? SvgPicture.asset('assets/icons/inactive.svg')
            : SvgPicture.asset('assets/icons/active.svg'),
        onClick: toggleLockerActivate,
        isActive: !currentSelectedLocker!.isUsing,
      ),
    ];

    showContextMenu(
      details.globalPosition,
      context,
      contextMenuItems,
      0.0,
      100.0,
      100,
    );
  }

  void toggleLockerActivate() {
    Get.back();

    if (currentSelectedLocker!.isUsing) {
      return;
    }

    API.locker
        .toggleLockerActivate(selectedSection.value.id, currentSelectedLocker!)
        .then(
          (value) => {
            if (value['success']!)
              {
                lockerList.value = lockerList
                    .map((element) => element == currentSelectedLocker!
                        ? Locker(
                            id: element.id,
                            isEnable: value['data']!,
                            isUsing: element.isUsing,
                            index: element.index,
                            expriedDate: element.expriedDate,
                            startedDate: element.startedDate,
                            usingUserId: element.usingUserId,
                            usingUserName: element.usingUserName,
                          )
                        : element)
                    .toList()
              }
            else
              {print("toggle locker error!")}
          },
        );
  }

  void onChangedSection(Section section) {
    API.locker.getLockerStatus(section.id).then(
          (value) => {
            sizeXController.value =
                TextEditingValue(text: value['x'].toString()),
            sizeYController.value =
                TextEditingValue(text: value['y'].toString()),
            startIndexController.value =
                TextEditingValue(text: value['startIndex'].toString()),
            lockerList.value = getLockerListFromList(value['list']),
            isLoading.value = false
          },
        );
  }

  void onClickSection(Section section) {
    if (selectedSection.value == section) return;

    selectedSection.value = section;
    onChangedSection(section);
  }

  void updateOnClickFromHome() {
    if (selectedSection.value.id == '') {
      SectionListController sectionListController = Get.find();
      if (sectionListController.sectionList.length > 0) {
        selectedSection.value = sectionListController.sectionList[0];
      }
    }

    if (selectedSection.value.id != '') {
      onChangedSection(selectedSection.value);
    }
  }

  void getLockerVouchers() {
    API.locker
        .getLockerVouchres()
        .then((value) => lockerVoucherList.value = value);
  }
}
