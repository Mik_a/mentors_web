import 'package:flutter/material.dart';
import 'package:mentors_web/components/add_section/add_section_view.dart';

class SectionManagementView extends StatelessWidget {
  const SectionManagementView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AddSectionView();
  }
}
