import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/study_room/study_room_controller.dart';
import 'package:mentors_web/app/modules/study_room/time_stamp.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/calender.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/global_colors.dart';

class StudyRoomView extends StatelessWidget {
  StudyRoomView({Key? key}) : super(key: key);

  final StudyroomController controller = Get.put(StudyroomController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      Map<int, List<Widget>> temp = {};
      Map<int, Widget> render = {};

      controller.studyRoomReservationOfMonth.forEach((key) {
        final name = key.name;
        key.reservations.forEach((reservation) {
          if (reservation.numberOfReservation > 0) {
            if (!temp.containsKey(reservation.day - 1)) {
              temp[reservation.day - 1] = [];
            }

            temp[reservation.day - 1]!.add(
              Container(
                margin: EdgeInsets.only(bottom: 2, top: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 6, vertical: 4),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(
                          color: key.color!,
                        ),
                      ),
                      child: Text(
                        name,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Text('${reservation.numberOfReservation}건')
                  ],
                ),
              ),
            );
          }
        });
      });

      temp.forEach(
        (key, value) {
          render[key] = Column(
            children: value,
          );
        },
      );

      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: RegisterCard(
          title: "스터디룸 예약 일정",
          action: controller.currentDateTime.value
                  .add(Duration(days: 1))
                  .isAfter(new DateTime.now())
              ? SquareButton(
                  onClick: controller.onClickAddStudyRoomSchedule,
                  height: 28,
                  width: 128,
                  icon: Icon(
                    Icons.add_circle_outlined,
                    color: CustomColors.cD9C87A,
                    size: 16,
                  ),
                  title: "스터디룸 예약추가",
                )
              : Container(),
          child: ListView(
            controller: controller.scrollController,
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.8,
                child: Row(
                  children: [
                    Expanded(
                      child: Calender(
                        currentDate: controller.currentDateTime.value,
                        onClickLeft: controller.onClickLeft,
                        onClickRight: controller.onClickRight,
                        onClickDate: controller.onClickDate,
                        content: render,
                      ),
                    ),
                  ],
                ),
              ),
              TimeStamp(
                reversedReservations: controller.reservationDataOfDay,
                currentDate: controller.currentDateTime.value,
              ),
            ],
          ),
        ),
      );
    });
  }
}
