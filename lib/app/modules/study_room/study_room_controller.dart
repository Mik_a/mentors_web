import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/study_room/add_studyroom_schedule_modal.dart';
import 'package:mentors_web/classes/studyrooms.dart';
import 'package:mentors_web/components/modal.dart';

class StudyroomController extends GetxController {
  ScrollController scrollController = ScrollController();

  Rx<DateTime> currentDateTime = DateTime.now().obs;

  RxList<StudyRoomReservationOfMonth> studyRoomReservationOfMonth =
      RxList<StudyRoomReservationOfMonth>();

  RxList<ReservationDataOfDay> reservationDataOfDay =
      RxList<ReservationDataOfDay>();

  final List<Color> colors = [
    Color(0xffFF9292),
    Color(0xff5FE1EF),
    Color(0xff35E8A0),
    Color(0xffFFBC58),
    Color(0xffB16DF4),
  ];

  @override
  void onInit() {
    super.onInit();
    updateMonth();
  }

  void onClickLeft() {
    currentDateTime.value = DateTime(
      currentDateTime.value.year,
      currentDateTime.value.month - 1,
    );
    updateMonth();
  }

  void onClickRight() {
    currentDateTime.value = DateTime(
      currentDateTime.value.year,
      currentDateTime.value.month + 1,
    );
    updateMonth();
  }

  void onClickAddStudyRoomSchedule() {
    Get.dialog(
      Modal(
        content: AddStudyroomScheduleModal(
          currentDate: currentDateTime.value,
          onUpdated: () {
            updateMonth();
          },
        ),
      ),
    );
  }

  void updateMonth() async {
    studyRoomReservationOfMonth.value = [];
    studyRoomReservationOfMonth.value =
        await API.studyroom.getStudyroomReservationOfMonth(
      year: currentDateTime.value.year.toString(),
      month: currentDateTime.value.month.toString(),
    );

    int idx = 0;
    studyRoomReservationOfMonth.forEach((element) {
      element.color = colors[idx % 5];
      idx++;
    });
  }

  bool isLoading = false;
  void onClickDate(DateTime time) async {
    // if (isLoading) return;
    print('hi..');

    isLoading = true;
    EasyLoading.show();
    List<ReservationDataOfDay> res =
        await API.studyroom.getStudyroomsReservationDay(
      year: time.year,
      month: time.month,
      day: time.day,
    );

    studyRoomReservationOfMonth.forEach((element) {
      res.forEach((element2) {
        if (element.id == element2.id) {
          element2.color = element.color;
        }
      });
    });

    isLoading = false;
    EasyLoading.dismiss();
    currentDateTime.value = time;
    reservationDataOfDay.value = res;

    scrollController.animateTo(
      200,
      duration: Duration(milliseconds: 100),
      curve: Curves.easeIn,
    );
  }
}
