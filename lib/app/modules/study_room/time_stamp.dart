import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mentors_web/classes/studyrooms.dart';
import 'package:mentors_web/components/context_menu/context_menu_area.dart';
import 'package:mentors_web/global_colors.dart';

class TimeStamp extends StatelessWidget {
  final DateTime currentDate;
  final List<ReservationDataOfDay> reversedReservations;
  TimeStamp({
    required this.currentDate,
    required this.reversedReservations,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      width: 500,
      child: Column(
        children: [
          Container(
            height: 32,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Color(0xffE5F9F9),
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            child: Center(
              child: Text(
                "${currentDate.month}월 ${currentDate.day}일 - 예약 상세",
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Color(0xff00a6a6),
                ),
              ),
            ),
          ),
          Row(
            children: [
              time(null),
              for (var i = 0; i < 24; i++) time(i),
            ],
          ),
          if (reversedReservations.length == 0)
            Container(
              height: 150,
              child: Center(
                child: Text(
                  "예약이 없습니다.",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          else
            ...List.generate(
              reversedReservations.length,
              (index) => newMethod(reversedReservations[index], context),
            ),
        ],
      ),
    );
  }

  Row newMethod(ReservationDataOfDay data, BuildContext context) {
    bool start = false;
    String lastUserId = '';
    return Row(
      children: [
        Container(
          width: 70,
          child: Text(
            data.name,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w600,
              color: Color(0xff717071),
            ),
          ),
        ),
        ...data.infoList.map((info) {
          if (info.isReservationed && lastUserId != info.userId) {
            start = true;
          } else {
            start = false;
          }
          lastUserId = info.userId ?? '';

          return Expanded(
            child: GestureDetector(
              onTapDown: (detail) {
                int startTime = 0;
                int endTime = 0;

                Offset position = Offset(
                  detail.globalPosition.dx,
                  detail.globalPosition.dy - 110,
                );

                int index =
                    data.infoList.indexWhere((element) => info == element);
                int tempIndex = index;
                while (tempIndex > 0) {
                  final target = data.infoList[tempIndex];
                  if (info.userId == target.userId) {
                    startTime = target.hour;
                    tempIndex--;
                  } else {
                    break;
                  }
                }
                tempIndex = index;
                while (tempIndex < data.infoList.length - 1) {
                  final target = data.infoList[tempIndex];
                  if (info.userId == target.userId) {
                    endTime = target.hour;
                    tempIndex++;
                  } else {
                    break;
                  }
                }

                if (info.isReservationed) {
                  showContextMenu(
                    position,
                    context,
                    [
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 12,
                        ),
                        width: 188,
                        height: 88,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(555),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            text(
                              "예약자명",
                              info.name ?? '손님',
                              Row(
                                children: [
                                  SvgPicture.asset('assets/icons/people.svg'),
                                  SizedBox(width: 6),
                                  Text(
                                    '${info.numberOfPeople}명',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            text("전화번호", info.phone ?? '-', null),
                            text(
                                "이용시간",
                                '$startTime시 ~ ${endTime + 1}시 (${endTime + 1 - startTime}시간)',
                                null),
                          ],
                        ),
                      )
                    ],
                    15.0,
                    188.0,
                    88.0,
                  );
                }
              },
              child: Stack(
                children: [
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(color: Color(0xffF5F5F5)),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 13),
                    color: info.isReservationed
                        ? data.color!.withOpacity(0.15)
                        : Colors.transparent,
                    height: 36,
                    child: Center(
                      child: Text(
                        info.isReservationed && start == true
                            ? info.name.toString()
                            : '',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  if (info.isReservationed && start == true)
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 13),
                      width: 2,
                      height: 36,
                      color: data.color!,
                    ),
                ],
              ),
            ),
          );
        }).toList(),
      ],
    );
  }

  Row text(String title, String content, Widget? widget) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(fontSize: 12, color: Color(0xff717071)),
        ),
        SizedBox(width: 10),
        Text(
          content,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 12,
          ),
        ),
        Expanded(child: SizedBox()),
        if (widget != null) widget,
      ],
    );
  }

  Widget time(int? i) {
    var container = Container(
      margin: EdgeInsets.only(top: 8),
      padding: EdgeInsets.only(left: 5),
      height: 12,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
            color: Color(0xfff5f5f5),
            width: 2,
          ),
        ),
      ),
      child: i == null
          ? Text('')
          : Text(
              "${("00" + i.toString()).substring(i.toString().length)}:00",
              style: TextStyle(
                fontSize: 11,
                color: CustomColors.cC4C4C4,
              ),
            ),
    );
    return i == null
        ? Container(
            width: 70,
            child: container,
          )
        : Expanded(
            child: container,
          );
  }
}
