import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/api/studyroom.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/drawer/drop_box.dart';
import 'package:mentors_web/components/dropbox_with_title.dart';
import 'package:mentors_web/components/find_user_form.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/radio_buttons_with_title.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

class AddStudyroomScheduleModal extends StatefulWidget {
  final DateTime currentDate;
  final Function onUpdated;
  const AddStudyroomScheduleModal({
    required this.currentDate,
    Key? key,
    required this.onUpdated,
  }) : super(key: key);

  @override
  _AddStudyroomScheduleModalState createState() =>
      _AddStudyroomScheduleModalState();
}

class _AddStudyroomScheduleModalState extends State<AddStudyroomScheduleModal> {
  User? selectedUser;
  bool isUser = false;
  late SeatController seatController;
  SeatType? selectedSeatType;
  int selectedNumberOfPeople = 1;
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  StudyRoomDay? day;
  int startHour = -1;
  int endHour = -1;
  @override
  void initState() {
    super.initState();
    seatController = Get.put(SeatController());
    nameController.addListener(textUpdate);
    phoneController.addListener(textUpdate);
  }

  void textUpdate() {
    setState(() {});
  }

  @override
  void dispose() {
    nameController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  bool isCanReservation() {
    var ifUser = selectedUser != null;
    var ifNotUser = nameController.text != '' &&
        GetUtils.isPhoneNumber(phoneController.text);

    return (selectedSeatType != null && isUser ? ifUser : ifNotUser);
  }

  void onClickReservationConfirm() async {
    final res = await API.studyroom.reservationStudyRoom(
      seatId: selectedSeatType!.id,
      isGuest: !isUser,
      year: widget.currentDate.year,
      month: widget.currentDate.month,
      day: widget.currentDate.day,
      startHour: startHour,
      endHour: endHour,
      numberOfPeople: selectedNumberOfPeople,
      phoneNumber:
          selectedUser == null ? phoneController.text : selectedUser!.phone,
      userId: selectedUser == null ? phoneController.text : selectedUser!.id,
      price: selectedSeatType!.priceOfHour * (endHour - startHour),
    );

    if (res.length == 0) {
      Get.back();
    } else {
      widget.onUpdated();
      Get.back();
      Get.back();
    }
  }

  void onClickReservation() {
    Get.dialog(
      Modal(
        content: Alert(
          title: "저장",
          content: "예약하시겠습니까?",
          alertType: AlertType.alert,
          onConfirm: onClickReservationConfirm,
          onCancle: Get.back,
        ),
      ),
    );
  }

  void onClickUser(User user) {
    setState(() {
      selectedUser = user;
    });
  }

  void onChangeSeatType(SeatType seatType) async {
    day = await API.studyroom.getStudyroomReservationDay(
      seatId: seatType.id,
      year: widget.currentDate.year,
      month: widget.currentDate.month,
      day: widget.currentDate.day,
    );

    setState(() {
      selectedSeatType = seatType;
      selectedNumberOfPeople = min(seatType.maxPeople, selectedNumberOfPeople);
    });
  }

  void onClicUserType(dynamic v) {
    setState(() {
      isUser = v;
    });
  }

  void onChangeNumberOfPeople(int num) {
    setState(() {
      selectedNumberOfPeople = num;
    });
  }

  List<int> getEndHour({
    int? start,
  }) {
    List<int> hours = [];

    for (int i = start ?? startHour; i < 24; i++) {
      if (day!.studyRoomDayInfos[i].isCanReservation) {
        hours.add(i);
      } else {
        break;
      }
    }

    return hours;
  }

  dynamic onSetStartTime(dynamic time) {
    setState(() {
      startHour = time;
      endHour = getEndHour(start: time)[0];
    });
  }

  void setEndTime() {}

  @override
  Widget build(BuildContext context) {
    List<Widget> nonUserForm = [
      InputboxWithTitle(
        title: "예약자명",
        titleWidth: 44,
        inputWidth: 136,
        hintText: "이름 입력",
        controller: nameController,
      ),
      SizedBox(height: 12),
      InputboxWithTitle(
        title: "전화번호",
        titleWidth: 44,
        inputWidth: 136,
        hintText: "전화번호 입력",
        controller: phoneController,
      )
    ];

    var selectUseTime = selectedSeatType == null
        ? SizedBox(
            height: 32,
            child: Row(
              children: [
                AutoSpacingText(
                  text: "이용시간",
                  width: 44,
                ),
                SizedBox(width: 12),
                Text(
                  "스터디룸을 먼저 선택해주세요",
                  style: TextStyle(fontSize: 12),
                )
              ],
            ),
          )
        : Row(
            children: [
              AutoSpacingText(
                text: "이용시간",
                width: 44,
              ),
              SizedBox(width: 10),
              day!.isCanReservation
                  ? Row(
                      children: [
                        DropBox(
                          width: 90,
                          height: 32,
                          onChanged: onSetStartTime,
                          currentValue: startHour,
                          values: [
                            -1,
                            ...day!.studyRoomDayInfos
                                .where((element) => element.isCanReservation)
                                .map((e) => e.hour)
                                .toList()
                          ],
                          textList: [
                            "시작 시간",
                            ...day!.studyRoomDayInfos
                                .where((element) => element.isCanReservation)
                                .map((e) => e.hour.toString() + "시")
                                .toList(),
                          ],
                          textStyle: TextStyle(fontSize: 12),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Text("~"),
                        ),
                        startHour == -1
                            ? Text(
                                "시작 시간을 선택해주세요",
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              )
                            : DropBox(
                                textStyle: TextStyle(fontSize: 12),
                                width: 90,
                                height: 32,
                                onChanged: (e) {
                                  setState(() {
                                    endHour = e;
                                    print(endHour);
                                  });
                                },
                                values: getEndHour(),
                                textList: getEndHour()
                                    .map((e) => (e + 1).toString() + "시")
                                    .toList(),
                                currentValue: endHour,
                              )
                      ],
                    )
                  : Container()
            ],
          );

    var selectNumberOfPeople = selectedSeatType == null
        ? SizedBox(
            height: 32,
            child: Row(
              children: [
                AutoSpacingText(
                  text: "이용인원",
                  width: 44,
                ),
                SizedBox(width: 12),
                Text(
                  "스터디룸을 먼저 선택해주세요",
                  style: TextStyle(fontSize: 12),
                )
              ],
            ),
          )
        : DropBoxWithTitle(
            titleWidth: 44,
            inputWidth: 90,
            title: "이용인원",
            values: List.generate(
                selectedSeatType!.maxPeople, (index) => index + 1),
            currentValue: selectedNumberOfPeople,
            textList: List.generate(
                selectedSeatType!.maxPeople, (index) => '${index + 1}인'),
            onChanged: (v) => onChangeNumberOfPeople(v),
            inputTextStyle: TextStyle(fontSize: 12),
          );

    var userForm = Column(
      children: [
        SizedBox(
          height: 32,
          child: Row(
            children: [
              AutoSpacingText(
                width: 44,
                text: "예약자명",
              ),
              SizedBox(width: 10),
              selectedUser != null
                  ? Text(
                      selectedUser!.name,
                      style: TextStyle(fontSize: 12),
                    )
                  : Text(
                      '회원을 선택해주세요',
                      style: TextStyle(fontSize: 12),
                    ),
            ],
          ),
        ),
        SizedBox(height: 12),
        SizedBox(
          height: 32,
          child: Row(
            children: [
              AutoSpacingText(
                width: 44,
                text: "전화번호",
              ),
              SizedBox(width: 10),
              selectedUser != null
                  ? Text(
                      selectedUser!.phone,
                      style: TextStyle(fontSize: 12),
                    )
                  : Text('-'),
            ],
          ),
        ),
      ],
    );

    return Container(
      width: 840,
      height: 750,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RegisterCard(
            title: "예약 일정 추가",
            width: 382,
            height: 400,
            action: Text(
              getKoreaTime(widget.currentDate).substring(0, 10),
              style: TextStyle(
                fontSize: 12,
                color: CustomColors.c717071,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RadioBoxWithTitle(
                  title: "회원구분",
                  titleWidth: 44,
                  inputWidth: double.infinity,
                  containerWidth: 186,
                  values: [true, false],
                  currentValue: isUser,
                  textList: ["회원", "비회원"],
                  onChanged: onClicUserType,
                ),
                SizedBox(height: 12),
                Obx(() {
                  List<SeatType> studyroomList = seatController.seatTypeList
                      .where((e) => e.seatGroup == SeatGroup.studyroom)
                      .toList();

                  return studyroomList.length == 0
                      ? SizedBox(
                          height: 32,
                          child: Row(
                            children: [
                              AutoSpacingText(
                                text: "스터디룸",
                                width: 44,
                              ),
                              SizedBox(width: 10),
                              Text(
                                "사용 가능한 스터디룸이 없습니다.",
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                        )
                      : DropBoxWithTitle(
                          titleWidth: 44,
                          inputWidth: 136,
                          title: "스터디룸",
                          values: studyroomList,
                          currentValue: selectedSeatType,
                          hintText: "선택해주세요",
                          textList: studyroomList.map((e) => e.name).toList(),
                          onChanged: (v) => onChangeSeatType(v),
                          inputTextStyle: TextStyle(fontSize: 12),
                        );
                }),
                SizedBox(height: 12),
                if (isUser) userForm else ...nonUserForm,
                SizedBox(height: 12),
                selectNumberOfPeople,
                SizedBox(height: 12),
                selectUseTime,
                Expanded(child: SizedBox(height: 12)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomButton(
                        width: 120,
                        backgroundColor: Color(0xffd7d0b4),
                        textColor: Color(0xff595232),
                        title: "취소",
                        onPressed: Get.back,
                      ),
                      SizedBox(width: 8),
                      CustomButton(
                        type: isCanReservation()
                            ? CustomButtonType.confirm
                            : CustomButtonType.disable,
                        width: 120,
                        title: "저장",
                        onPressed: onClickReservation,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          isUser
              ? FindUserForm(
                  isDisplayGuest: false,
                  onClickUser: onClickUser,
                  selectedUsers: selectedUser == null ? [] : [selectedUser!],
                )
              : RegisterCard(
                  title: "회원검색",
                  width: 450,
                  height: 750,
                  child: Center(
                    child: Text("비회원은 사용할 수 없는 기능입니다"),
                  ),
                ),
        ],
      ),
    );
  }
}
