import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/components/section_list/section_list.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';

class MobileSelectSectionView extends GetView<SectionListController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text('배치도 선택'),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
      ),
      body: Obx(() {
        return controller.isLoading.value
            ? Center(child: CircularProgressIndicator())
            : SectionList(
                selectedSection: Section(id: '', name: ''),
                axis: Axis.vertical,
                width: double.infinity,
                onClickSection: (section) {
                  Get.toNamed(
                    Routes.MOBILE_VIEW_SECTION,
                    arguments: {'section': section},
                  );
                },
              );
      }),
    );
  }
}
