import 'package:get/get.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';

import '../controllers/mobild_select_section_controller.dart';

class MobildSelectSectionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SectionListController>(
      () => SectionListController(),
    );
  }
}
