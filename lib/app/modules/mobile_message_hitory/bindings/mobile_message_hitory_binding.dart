import 'package:get/get.dart';
import 'package:mentors_web/controller/message_controller.dart';

import '../controllers/mobile_message_hitory_controller.dart';

class MobileMessageHitoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MessageController>(
      () => MessageController(),
    );
  }
}
