import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/message_management/message_use_log_item.dart';
import 'package:mentors_web/controller/message_controller.dart';

class MobileMessageHitoryView extends GetView<MessageController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        title: Text('메세지 이력'),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Obx(() {
        return ListView.separated(
          padding: EdgeInsets.symmetric(horizontal: 5),
          itemCount: controller.currentMsgLogListForMobile.length +
              (controller.isEnd.value ? 0 : 1),
          separatorBuilder: (context, index) => Divider(),
          itemBuilder: (context, index) {
            if (index == controller.currentMsgLogListForMobile.length) {
              controller.getMessageLogsMobile();
              return Container(
                height: 50,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            return MessageUseLogItem(
              messageLog: controller.currentMsgLogListForMobile[index],
            );
          },
        );
      }),
    );
  }
}
