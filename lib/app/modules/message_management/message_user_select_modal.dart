import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/custom_icon_button.dart';
import 'package:mentors_web/components/find_user_form.dart';

class MessageUserSelectModal extends StatefulWidget {
  final Function(List<User>) onClickConfirm;
  final List<User> selectedUser;

  const MessageUserSelectModal({
    this.selectedUser = const [],
    required this.onClickConfirm,
    Key? key,
  }) : super(key: key);

  @override
  _MessageUserSelectModalState createState() => _MessageUserSelectModalState();
}

class _MessageUserSelectModalState extends State<MessageUserSelectModal> {
  List<User> selectedUsers = [];

  @override
  void initState() {
    super.initState();
    selectedUsers.addAll(widget.selectedUser);
  }

  void onClickConfirm() {
    widget.onClickConfirm(selectedUsers);
    Get.back();
  }

  void onClickUser(User user) {
    setState(() {
      if (selectedUsers.contains(user)) {
        selectedUsers.remove(user);
      } else {
        selectedUsers.add(user);
      }
    });
  }

  void onClickDelete(User user) {
    if (selectedUsers.contains(user))
      setState(() {
        selectedUsers.remove(user);
      });
  }

  void onChangedUserList(List<User> list) {
    setState(() {
      selectedUsers = list;
    });
  }

  void reset() {
    setState(() {
      selectedUsers = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 50.0),
      child: Container(
        width: 904,
        height: MediaQuery.of(context).size.height - 124,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FindUserForm(
              height: double.infinity,
              selectedUsers: selectedUsers,
              onClickUser: onClickUser,
              onChangeUserList: onChangedUserList,
            ),
            Container(
              height: MediaQuery.of(context).size.height - 124,
              child: RegisterCard(
                title: "선택된 회원",
                height: 500,
                width: 450,
                child: Column(
                  children: [
                    SizedBox(
                      height: 42,
                      child: Row(
                        children: [
                          Text("전송대상"),
                          SizedBox(width: 16),
                          Text(
                            "${selectedUsers.length}명",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Color(
                                0xff00a6a6,
                              ),
                            ),
                          ),
                          Expanded(child: Container()),
                          CustomIconButton(
                            icon: "clear",
                            iconColor: Color(0xff717071),
                            title: "일괄선택해제",
                            onClick: reset,
                          )
                        ],
                      ),
                    ),
                    Divider(),
                    Expanded(
                      child: selectedUsers.length == 0
                          ? Container(
                              child: Center(
                                child: Text("메세지를 받을 회원을 선택해주세요"),
                              ),
                            )
                          : SingleChildScrollView(
                              child: Wrap(
                                  alignment: WrapAlignment.start,
                                  crossAxisAlignment: WrapCrossAlignment.start,
                                  children: List.generate(
                                    selectedUsers.length + 1,
                                    (index) {
                                      if (index == selectedUsers.length)
                                        return Opacity(
                                          opacity: 0,
                                          child: newMethod(
                                            selectedUsers[index - 1],
                                          ),
                                        );

                                      return newMethod(selectedUsers[index]);
                                    },
                                  )),
                            ),
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomButton(
                          title: "취소",
                          width: 120,
                          height: 40,
                          type: CustomButtonType.cancle,
                          onPressed: Get.back,
                        ),
                        SizedBox(width: 8),
                        CustomButton(
                          width: 120,
                          height: 40,
                          title: "확인",
                          onPressed: onClickConfirm,
                        ),
                      ],
                    ),
                    SizedBox(height: 10)
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container newMethod(User user) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffcccccc)),
        borderRadius: BorderRadius.all(
          Radius.circular(25),
        ),
      ),
      width: 185,
      height: 32,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(user.name),
          Expanded(child: Center(child: Text(user.phone))),
          InkWell(
            onTap: () {
              onClickDelete(user);
            },
            child: SvgPicture.asset('assets/icons/clear.svg'),
          ),
        ],
      ),
    );
  }
}
