import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/radio_buttons_with_title.dart';

class MessageChargeModal extends StatefulWidget {
  const MessageChargeModal({Key? key}) : super(key: key);

  @override
  State<MessageChargeModal> createState() => _MessageChargeModalState();
}

class _MessageChargeModalState extends State<MessageChargeModal> {
  final amount = TextEditingController();
  final userName = TextEditingController();

  void onClickCharge() async {
    if (amount.text == '' || amount.text == '0') return;

    final res = await API.sms.requestChargeCash(amount.text, userName.text);
    if (res) {
      Get.back();
      EasyLoading.showSuccess('충전 요청이 완료되었습니다.');
    } else {
      EasyLoading.showError("요청에 실패했습니다.");
    }
  }

  @override
  Widget build(BuildContext context) {
    return RegisterCard(
      width: 467,
      height: 260,
      title: "캐시 충전 요청",
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RadioBoxWithTitle(
            title: "결제수단",
            textList: ["무통장 입금"],
            values: [true],
            currentValue: true,
            onChanged: (_) {},
            titleWidth: 48,
          ),
          InputboxWithTitle(
            titleWidth: 48,
            title: "충전금액",
            inputWidth: 136,
            hintText: "충전금액",
            controller: amount,
            textInputType: TextInputType.number,
          ),
          InputboxWithTitle(
            titleWidth: 48,
            title: "입금자명",
            hintText: "입금자명",
            inputWidth: 136,
            controller: userName,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomButton(
                width: 120,
                height: 40,
                title: "취소",
                onPressed: Get.back,
                type: CustomButtonType.cancle,
              ),
              SizedBox(width: 8),
              CustomButton(
                width: 120,
                height: 40,
                title: "충전 요청",
                onPressed: onClickCharge,
              ),
            ],
          ),
          SizedBox(),
        ],
      ),
    );
  }
}
