import 'package:flutter/material.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class MessageUseLogItem extends StatelessWidget {
  final MessageLog messageLog;
  const MessageUseLogItem({
    required this.messageLog,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(width: 10),
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      messageLog.phone.length == 1
                          ? Text(messageLog.phone[0])
                          : Text(
                              "${messageLog.phone[0]} 외 ${messageLog.phone.length - 1}명"),
                      SizedBox(height: 6),
                      Text(
                        messageLog.message,
                        style: TextStyle(
                          fontSize: 12,
                          color: CustomColors.c717071,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "발송완료",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: CustomColors.c19A980,
                      ),
                    ),
                    SizedBox(height: 6),
                    Text(
                      messageLog.type.toString().split('.').last.toUpperCase(),
                    ),
                    SizedBox(height: 6),
                    Text(messageLog.cash.toString() + "원"),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10),
          Align(
            alignment: Alignment.centerRight,
            child: Text("발송일자 ${getKoreaTime(messageLog.time)}"),
          )
        ],
      ),
    );
  }
}
