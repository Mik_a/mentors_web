import 'package:flutter/material.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/modules/lib.dart';

class MessageChargeHistoryModal extends StatefulWidget {
  const MessageChargeHistoryModal({Key? key}) : super(key: key);

  @override
  _MessageChargeHistoryModalState createState() =>
      _MessageChargeHistoryModalState();
}

class _MessageChargeHistoryModalState extends State<MessageChargeHistoryModal> {
  List<ChargeLog> logs = [];
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLogs();
  }

  void getLogs() async {
    API.sms.getChargeHistory().then((value) {
      setState(() {
        isLoading = false;
        logs = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return RegisterCard(
      title: "충전 내역",
      width: 467,
      height: 728,
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : Container(
              child: ListView.builder(
                itemCount: logs.length,
                itemBuilder: (context, index) {
                  return item(logs[index]);
                },
              ),
            ),
    );
  }

  Container item(ChargeLog log) {
    return Container(
      height: 75,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Color(0xffe3e3e3),
            width: 1,
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                getKoreaTime(log.requestTime),
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xff9b9b9b),
                ),
              ),
              Row(
                children: [
                  Text(
                    '${log.status.name}',
                    style: TextStyle(
                      fontSize: 12,
                      color: log.status.color,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 4),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                log.requestUserName,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                addCommaInMoney(log.amount) + " 캐시",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          SizedBox(height: 4),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                '${(getKoreaTime(log.responseTime))}',
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xff9b9b9b),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
