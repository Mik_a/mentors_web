import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/message_management/message_use_log_item.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/custom_checkbox.dart';
import 'package:mentors_web/components/custom_icon_button.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/message/message_remain_view.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/components/radio_button.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/global_colors.dart';

class MessageManagementView extends StatelessWidget {
  final MessageController messageController = Get.put(MessageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          var remainCash = RegisterCard(
            title: "보유 캐시",
            width: 450,
            height: 220,
            action: Row(
              children: [
                SquareButton(
                  title: "충전",
                  onClick: messageController.onClickCharge,
                ),
                SizedBox(width: 10),
                SquareButton(
                  title: "충전 내역",
                  onClick: messageController.onClickChargeHistory,
                ),
                InkWell(
                  onTap: messageController.updateCash,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.refresh),
                  ),
                ),
              ],
            ),
            child: Column(
              children: [
                MessageRemainView(),
              ],
            ),
          );

          var messageLogs = Expanded(
            child: RegisterCard(
              title: "메세지 발송 이력",
              height: double.infinity,
              child: Column(
                children: [
                  Expanded(
                    child: PagenationListView(
                      data: List.generate(
                        messageController.currentMsgLogList.length,
                        (index) => [
                          MessageUseLogItem(
                            messageLog:
                                messageController.currentMsgLogList[index],
                          )
                        ],
                      ),
                      headerTitles: [Text("발송이력")],
                    ),
                  ),
                  Pagenation(
                    currentPage: messageController.logCurrentPage.value,
                    totalLength: messageController.logLength.value,
                    itemPerPage: 20,
                    setPage: messageController.setPage,
                  )
                ],
              ),
            ),
          );

          var messageSendForm = Expanded(
            child: RegisterCard(
              title: "메세지 작성",
              action: messageController.sendType.value != MessageSendType.self
                  ? Row(
                      children: [
                        RadioButton(
                          title: "회원에게만",
                          isOn: messageController.isSendToOnlyUser.value,
                          onClick: messageController.onChangeSendToType,
                          value: true,
                        ),
                        SizedBox(width: 12),
                        RadioButton(
                          title: "회원 + 보호자",
                          onClick: messageController.onChangeSendToType,
                          isOn: !messageController.isSendToOnlyUser.value,
                          value: false,
                        ),
                      ],
                    )
                  : null,
              width: 450,
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 68,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              "발송대상",
                              style: TextStyle(
                                fontSize: 12,
                                color: CustomColors.c717071,
                              ),
                            ),
                            Container(
                              height: 12,
                            ),
                          ],
                        ),
                        SizedBox(width: 12),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  CustomCheckbox(
                                    onChange: (_) =>
                                        messageController.onChangeSendType(
                                            MessageSendType.all, context),
                                    value: messageController.sendType.value ==
                                        MessageSendType.all,
                                    title: "전체회원",
                                  ),
                                  SizedBox(width: 12),
                                  CustomCheckbox(
                                    onChange: (_) =>
                                        messageController.onChangeSendType(
                                            MessageSendType.select, context),
                                    value: messageController.sendType.value ==
                                        MessageSendType.select,
                                    title: "회원목록에서 선택",
                                  ),
                                  SizedBox(width: 12),
                                  CustomIconButton(
                                    title: "검색",
                                    height: 28,
                                    icon: "search",
                                    onClick:
                                        messageController.onClickSearchUsers,
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  CustomCheckbox(
                                    onChange: (_) =>
                                        messageController.onChangeSendType(
                                            MessageSendType.self, context),
                                    value: messageController.sendType.value ==
                                        MessageSendType.self,
                                    title: "직접입력",
                                  ),
                                  SizedBox(width: 12),
                                  Expanded(
                                    child: IgnorePointer(
                                      ignoring:
                                          messageController.sendType.value !=
                                              MessageSendType.self,
                                      child: Opacity(
                                        opacity:
                                            messageController.sendType.value ==
                                                    MessageSendType.self
                                                ? 1
                                                : 0.6,
                                        child: InputBox(
                                          hintText: '전화번호는 쉼표로 구분해주세요',
                                          height: 28,
                                          controller: messageController
                                              .phoneListController,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    height: 58,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        titleWithContent("전송대상",
                            messageController.getNumberOfSendUser() + "명"),
                        titleWithContent(
                            "발송금액", messageController.messagePriceTotal),
                        titleWithContent(
                            "메세지 유형", messageController.messageType),
                        titleWithContent(
                            "메세지 금액", messageController.messagePrice),
                      ],
                    ),
                  ),
                  Divider(),
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned.fill(
                          child: InputboxWithTitle(
                            titleWidth: 42,
                            inputWidth: double.infinity,
                            inputHeight: double.infinity,
                            textInputType: TextInputType.multiline,
                            title: "내용",
                            hintText: "내용을 입력해주세요.",
                            controller: messageController.smsContentController,
                          ),
                        ),
                        Positioned(
                          right: 12,
                          bottom: 10,
                          child: Text(
                            '${messageController.currentMessageLength} / 2000bytes',
                            style: TextStyle(color: CustomColors.cA3A2A3),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 6),
                  messageController.sendType.value != MessageSendType.self
                      ? SizedBox(
                          height: 15,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "*메세지 내용에 %고객명%을 입력하면 발송목록의 고객명으로 변환되어 발송됩니다",
                              style: TextStyle(
                                fontSize: 10,
                                color: Color(0xff8a888a),
                              ),
                            ),
                          ),
                        )
                      : Container(height: 15),
                  SizedBox(height: 16),
                  CustomButton(
                    width: double.infinity,
                    height: 48,
                    title: "${messageController.getNumberOfSendUser()}건 문자 발송",
                    onPressed: messageController.sendMessage,
                  ),
                ],
              ),
            ),
          );

          return Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    remainCash,
                    messageSendForm,
                  ],
                ),
                messageLogs,
              ],
            ),
          );
        },
      ),
    );
  }

  Column titleWithContent(String title, String content) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: 12,
            color: CustomColors.c717071,
          ),
        ),
        Text(content),
      ],
    );
  }
}
