import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mentors_web/mobile/mobile_card.dart';
import 'package:mentors_web/mobile/mobile_drawer.dart';

import '../controllers/mobile_sales_controller.dart';

class MobileSalesView extends GetView<MobileSalesController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        title: Text(
          '매출',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      drawer: MobileDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: MobileCard(
          width: double.infinity,
          height: double.infinity,
          title: "항목별 매출",
        ),
      ),
    );
  }
}
