import 'package:get/get.dart';

import '../controllers/mobile_sales_controller.dart';

class MobileSalesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MobileSalesController>(
      () => MobileSalesController(),
    );
  }
}
