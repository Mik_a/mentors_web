import 'package:get/get.dart';
import 'package:mentors_web/app/modules/dashboard/controllers/sales_chart_controller.dart';

class MobileSalesController extends GetxController {
  Rx<DateTime> currentDate = DateTime.now().obs;

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onClickLeft() {
    currentDate.value =
        DateTime(currentDate.value.year, currentDate.value.month - 1);
    Get.put(SalesChartController())
        .onChangedDate(currentDate.value.year, currentDate.value.month);
  }

  void onClickRight() {
    currentDate.value =
        DateTime(currentDate.value.year, currentDate.value.month + 1);
    Get.put(SalesChartController())
        .onChangedDate(currentDate.value.year, currentDate.value.month);
  }

  String getDateKor() {
    return '${currentDate.value.year.toString()}년 ${("0" + currentDate.value.month.toString()).substring(currentDate.value.month.toString().length - 1)}월';
  }
}
