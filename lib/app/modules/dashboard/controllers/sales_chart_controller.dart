import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/modules/lib.dart';

class SalesLogs {
  num total;
  num dayEverage;
  num weekEverage;
  num dayVoucher;
  num timeVoucher;
  num periodVoucher;
  num lockerVoucher;
  List<num>? daySales;

  SalesLogs({
    this.total = 0,
    this.daySales,
    this.dayEverage = 0,
    this.dayVoucher = 0,
    this.lockerVoucher = 0,
    this.periodVoucher = 0,
    this.timeVoucher = 0,
    this.weekEverage = 0,
  });
}

class SalesChartController extends GetxController {
  Rx<SalesLogs> logs = SalesLogs().obs;

  @override
  void onInit() {
    super.onInit();
    DateTime today = DateTime.now();
    onChangedDate(today.year, today.month);
  }

  void onChangedDate(int year, int month) {
    API.store.getSalesLog(year, month).then((value) {
      DateTime today = DateTime.now();

      SalesLogs newLogs = SalesLogs();
      final days = List<num>.filled(31, 0);
      List<num> weeks = [];

      int wIdx = 0;

      value.forEach((element) {
        DateTime date = element.eventTime.toUtc().add(Duration(hours: 9));

        VoucherUseType type = element.voucherType ?? VoucherUseType.day;
        switch (type) {
          case VoucherUseType.day:
            newLogs.dayVoucher += element.price;
            break;
          case VoucherUseType.period:
            if (element.voucherTypeKor == '사물함') {
              newLogs.lockerVoucher += element.price;
            } else {
              newLogs.periodVoucher += element.price;
            }

            break;
          case VoucherUseType.time:
            newLogs.timeVoucher += element.price;
            break;
          case VoucherUseType.locker:
            newLogs.lockerVoucher += element.price;
            break;
          default:
            break;
        }

        days[date.day - 1] += element.price;
        newLogs.total += element.price;
      });

      days.forEach((element) {
        if (today.day > wIdx) {
          if (wIdx % 7 == 0) {
            weeks.add(0);
          }
          weeks[wIdx ~/ 7] += element;
        } else {}
        wIdx += 1;
      });

      if (DateTime(year, month + 1).compareTo(DateTime.now()) == 1) {
        newLogs.dayEverage = newLogs.total / DateTime.now().day;
      } else {
        final day = DateTime(year, month + 1).add(Duration(days: -1)).day;
        newLogs.dayEverage = newLogs.total / day;
      }

      newLogs.weekEverage =
          weeks.reduce((value, element) => value += element) / weeks.length;
      newLogs.daySales = days;
      logs.value = newLogs;
    });
  }
}
