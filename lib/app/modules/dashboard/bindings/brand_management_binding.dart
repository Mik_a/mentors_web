import 'package:get/get.dart';

import '../controllers/brand_management_controller.dart';

class BrandManagementBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BrandManagementController>(
      () => BrandManagementController(),
    );
  }
}
