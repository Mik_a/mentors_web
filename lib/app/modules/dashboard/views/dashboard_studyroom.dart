import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/studyrooms.dart';

class DashBoardStudyroom extends StatefulWidget {
  const DashBoardStudyroom({Key? key}) : super(key: key);

  @override
  State<DashBoardStudyroom> createState() => _DashBoardStudyroomState();
}

class _DashBoardStudyroomState extends State<DashBoardStudyroom> {
  bool isLoading = true;
  bool isToday = true;
  List<StudyRoomReservationOfMonth> _list = [];
  DateTime today = DateTime.now();

  @override
  void initState() {
    super.initState();
    getStudyRoomData();
  }

  void getStudyRoomData() async {
    final list = await API.studyroom.getStudyroomReservationOfMonth(
      year: today.year.toString(),
      month: today.month.toString(),
    );

    setState(() {
      _list = list;
      isLoading = false;
    });
  }

  TextStyle on = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: Color(0xff00a6a6),
  );

  TextStyle off = TextStyle(
    fontSize: 14,
    color: Color(0xffa6a6a6),
  );

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: RegisterCard(
        title: "스터디룸 예약 현황",
        height: double.infinity,
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : _list.length == 0
                ? Center(child: Text("스터디룸이 없습니다"))
                : Padding(
                    padding: EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                      left: 33,
                      right: 33,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 16,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isToday = true;
                                  });
                                },
                                child: Text(
                                  "오늘의 예약현황",
                                  style: isToday ? on : off,
                                ),
                              ),
                              Container(
                                width: 2,
                                height: 16,
                                color: Color(0xffe3e3e3),
                                margin: EdgeInsets.symmetric(horizontal: 16),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isToday = false;
                                  });
                                },
                                child: Text(
                                  "이달의 예약현황",
                                  style: !isToday ? on : off,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 15),
                          height: 54,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ...List.generate(
                                _list.length,
                                (index) => studyroom(_list[index]),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Get.find<HomeController>()
                                .toPage(PageEnum.studyRoom);
                          },
                          child: Container(
                            width: 120,
                            height: 24,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(
                                color: Color(0xffcccccc),
                                width: 1,
                              ),
                            ),
                            child: Center(
                              child: Text(
                                "상세보기",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff242424),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
      ),
    );
  }

  Container studyroom(StudyRoomReservationOfMonth room) {
    String num = room.reservations.length == 0
        ? "0"
        : isToday
            ? room.reservations
                .firstWhere((element) => element.day == today.day)
                .numberOfReservation
                .toString()
            : room.reservations
                .fold<int>(
                    0, (value, element) => value += element.numberOfReservation)
                .toString();

    num = ("000" + num).substring(num.length);

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 17),
      width: 86,
      height: 54,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 86,
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(
                color: Color(0xffd9c87a),
                width: 1,
              ),
            ),
            child: Center(
              child: Text(
                room.name,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Color(0xff595232),
                ),
              ),
            ),
          ),
          Text(
            num.toString() + "건",
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: Color(0xff242424),
            ),
          ),
        ],
      ),
    );
  }
}
