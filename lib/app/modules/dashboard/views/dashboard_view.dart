import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/app/modules/dashboard/controllers/sales_chart_controller.dart';
import 'package:mentors_web/app/modules/dashboard/views/dashboard_studyroom.dart';
import 'package:mentors_web/app/modules/dashboard/views/member_chart.dart';
import 'package:mentors_web/app/modules/dashboard/views/sales_chart.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/app/modules/mobile_sales/controllers/mobile_sales_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/message/message_remain_view.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/mobile/mobile_seat.dart';
import 'package:mentors_web/mobile/mobile_view_controller.dart';
import 'package:mentors_web/modules/lib.dart';
import '../controllers/brand_management_controller.dart';

class DashboardView extends GetView<BrandManagementController> {
  final MobileViewController seatController = Get.put(MobileViewController());
  final MobileSalesController salesController =
      Get.put(MobileSalesController());
  final MessageController messageController = Get.put(MessageController());
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    var userStatus = Expanded(
      flex: 2,
      child: Obx(() {
        return Column(
          children: [
            Expanded(
              child: RegisterCard(
                width: double.infinity,
                title: "회원현황",
                child: Row(
                  children: [
                    SizedBox(
                      width: 180,
                      child: Stack(
                        children: [
                          Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("전체회원"),
                                Text(
                                  userController.userStatus
                                      .reduce((previousValue, element) =>
                                          previousValue + element)
                                      .toInt()
                                      .toString(),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 48,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          MemberChart(
                            values: userController.userStatus,
                            colors: [
                              CustomColors.usingGreen,
                              CustomColors.reservationYellow,
                              CustomColors.endRed,
                              CustomColors.noData,
                            ],
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              numberOfUsers(CustomColors.usingGreen, "이용중",
                                  userController.userStatus[0]),
                              numberOfUsers(CustomColors.reservationYellow,
                                  "예약중", userController.userStatus[1]),
                              numberOfUsers(CustomColors.endRed, "종료",
                                  userController.userStatus[2]),
                              numberOfUsers(CustomColors.noData, "이력없음",
                                  userController.userStatus[3]),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 8),
            SizedBox(
              height: 225,
              child: Row(
                children: [
                  RegisterCard(
                    title: "메세지 잔여수량",
                    action: InkWell(
                      onTap: messageController.updateCash,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.refresh),
                      ),
                    ),
                    width: 350,
                    height: double.infinity,
                    child: MessageRemainView(),
                  ),
                  DashBoardStudyroom()
                ],
              ),
            )
          ],
        );
      }),
    );

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            chart(),
            SizedBox(height: 20),
            SizedBox(
              height: 500,
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: RegisterCard(
                      title: "좌석현황",
                      width: 600,
                      height: double.infinity,
                      child: MobileSeat(controller: seatController),
                    ),
                  ),
                  SizedBox(width: 20),
                  userStatus
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Container numberOfUsers(Color color, String title, double num) {
    final String n = num.toInt().toString();
    return Container(
      child: Row(
        children: [
          Container(
            width: 6,
            height: 76,
            color: color,
          ),
          SizedBox(width: 16),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  color: CustomColors.c717071,
                  fontSize: 14,
                ),
              ),
              Text(
                ("00" + n).substring(n.length - 1),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 48,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget chart() {
    return SizedBox(
      height: 300,
      child: Card(
        child: Container(
          width: double.infinity,
          child: Row(
            children: [
              TotalView(salesController: salesController),
              VerticalDivider(),
              Obx(() {
                return Expanded(
                  child: SalesChart(
                    time: salesController.currentDate.value,
                  ),
                );
              })
            ],
          ),
        ),
      ),
    );
  }
}

class TotalView extends StatelessWidget {
  TotalView({
    Key? key,
    required this.salesController,
  }) : super(key: key);

  final MobileSalesController salesController;
  final SalesChartController salesChartController =
      Get.put(SalesChartController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: salesController.onClickLeft,
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      size: 16,
                      color: kMaterialColor,
                    ),
                  ),
                  SizedBox(width: 8),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      salesController.getDateKor(),
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  SizedBox(width: 8),
                  InkWell(
                    onTap: salesController.onClickRight,
                    child: Transform.rotate(
                      angle: pi,
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        size: 16,
                        color: kMaterialColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(),
            item(
              "월간 매출",
              salesChartController.logs.value.total,
              TextStyle(
                color: CustomColors.c008C66,
                fontWeight: FontWeight.bold,
              ),
            ),
            Divider(),
            item(
              "일 평균 매출",
              salesChartController.logs.value.dayEverage,
              TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            item(
              "주 평균 매출",
              salesChartController.logs.value.weekEverage,
              TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Divider(),
            item(
              "당일권",
              salesChartController.logs.value.dayVoucher,
              TextStyle(),
            ),
            item(
              "시간권",
              salesChartController.logs.value.timeVoucher,
              TextStyle(),
            ),
            item(
              "기간권",
              salesChartController.logs.value.periodVoucher,
              TextStyle(),
            ),
            item(
              "사물함",
              salesChartController.logs.value.lockerVoucher,
              TextStyle(),
            ),
          ],
        ),
        width: 360,
      ),
    );
  }

  Padding item(String title, num value, TextStyle? style) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Row(
        children: [
          Expanded(child: Text(title)),
          Text(
            '${addCommaInMoney(value.toInt())} 원',
            style: style,
          ),
        ],
      ),
    );
  }
}
