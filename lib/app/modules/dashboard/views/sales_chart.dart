import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/dashboard/controllers/sales_chart_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class SalesChart extends StatelessWidget {
  final SalesChartController controller = Get.put(SalesChartController());

  final DateTime time;
  SalesChart({
    required this.time,
    Key? key,
  }) : super(key: key);

  List<BarChartGroupData> getBar() {
    DateTime next =
        DateTime(time.year, time.month + 1).subtract(Duration(days: 1));

    return List.generate(
      next.day,
      (index) => BarChartGroupData(
        x: index + 1,
        barRods: [
          BarChartRodData(
            y: controller.logs.value.daySales == null
                ? 0
                : min(120,
                    controller.logs.value.daySales![index].toDouble() * 0.0001),
            backDrawRodData: BackgroundBarChartRodData(
              y: controller.logs.value.daySales == null
                  ? 0
                  : controller.logs.value.daySales![index].toDouble(),
            ),
            colors: controller.logs.value.daySales == null
                ? [Colors.white]
                : controller.logs.value.daySales![index] * 0.0001 > 120
                    ? [
                        Color(0xffFC0000),
                        Color(0xffDF377E),
                      ]
                    : [
                        Color(0xff7A84D9),
                        Color(0xff00A8FC),
                      ],
          ),
        ],
      ),
    );
  }

  String getLeftTiles(double) {
    switch (double) {
      case 0:
        return "0원";
      case 20:
        return "20만";
      case 40:
        return "40만";
      case 60:
        return "60만";
      case 80:
        return "80만";
      case 100:
        return "100만";
      case 120:
        return "120만";
      default:
        return "";
    }
  }

  bool getLine(double val) {
    final List<double> d = [0, 20, 40, 60, 80, 100, 120];
    return d.contains(val);
  }

  TextStyle getTextStyle(v) {
    return TextStyle(
      color: CustomColors.c717071,
      fontSize: 12,
    );
  }

  TextStyle getWeekTitle(v, int year, int month) {
    DateTime cur = DateTime(year, month, v.toInt());
    return TextStyle(
      color: cur.weekday == 7
          ? Colors.red
          : cur.weekday == 6
              ? Colors.blue
              : CustomColors.c717071,
      fontSize: 12,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.all(24.0),
        child: BarChart(
          BarChartData(
            barTouchData: BarTouchData(
              touchTooltipData: BarTouchTooltipData(
                  tooltipBgColor: Color(0xff8F3636),
                  tooltipRoundedRadius: 18,
                  tooltipPadding:
                      EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                  getTooltipItem: (group, groupIndex, rod, rodIndex) {
                    return BarTooltipItem(
                      addCommaInMoney((rod.backDrawRodData.y).toInt()) + "원",
                      TextStyle(color: Colors.white),
                    );
                  }),
            ),
            titlesData: FlTitlesData(
              leftTitles: SideTitles(
                interval: 5,
                margin: 20,
                showTitles: true,
                getTitles: getLeftTiles,
                getTextStyles: getTextStyle,
              ),
              bottomTitles: SideTitles(
                showTitles: true,
                getTextStyles: (v) => getWeekTitle(v, time.year, time.month),
              ),
            ),
            barGroups: getBar(),
            gridData: FlGridData(
              checkToShowHorizontalLine: getLine,
            ),
            borderData: FlBorderData(show: false),
            maxY: 125,
            minY: 0,
          ),
        ),
      ),
    );
  }
}
