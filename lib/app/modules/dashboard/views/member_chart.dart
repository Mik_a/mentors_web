import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class MemberChart extends StatelessWidget {
  final List<double> values;
  final List<Color>? colors;

  const MemberChart({this.colors, required this.values, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final z = List.generate(
      values.length,
      (index) => ({"value": values[index], "index": index}),
    ).where((element) => element['value']! > 0).toList();

    return Row(
      children: [
        Expanded(
          child: PieChart(
            PieChartData(
              startDegreeOffset: -90,
              sections: List.generate(z.length, (index) {
                return PieChartSectionData(
                  radius: 25,
                  showTitle: false,
                  value: z[index]['value']!.toDouble(),
                  color: colors == null
                      ? null
                      : colors![z[index]['index']!.toInt()],
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
