import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/controller/device_controller.dart';
import 'package:mentors_web/mobile/mobile_card.dart';
import 'package:mentors_web/mobile/mobile_drawer.dart';
import '../../../../controller/store_management_controller.dart';

class MobileStoreManagementView extends GetView<StoreManagementController> {
  final DeviceController deviceController = Get.put(DeviceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: AppBar(
          title: Text(
            '매장 관리',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          foregroundColor: Colors.black,
          backgroundColor: Colors.white,
          centerTitle: true,
        ),
        drawer: MobileDrawer(),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              MobileCard(
                width: double.infinity,
                height: 48,
                padding: EdgeInsets.zero,
                fromTitle: 0,
                onClickCard: deviceController.onClickOpenDoor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/icons/door.svg'),
                    SizedBox(width: 12),
                    Text('출입문 열기'),
                  ],
                ),
              ),
              // Expanded(
              //   child: MobileCard(
              //     title: "매장 정보",
              //     child: controller.isLoading.isTrue
              //         ? Center(
              //             child: CircularProgressIndicator(),
              //           )
              //         : Column(
              //             children: [
              //               info(
              //                 "상호",
              //                 Text(controller.info['name'] ?? ''),
              //               ),
              //               info(
              //                 "성명",
              //                 Text(controller.info['ownerName'] ?? ''),
              //               ),
              //               // info(
              //               //   "주소",
              //               //   Flexible(
              //               //     child: Text(
              //               //       controller.info['address'] ??
              //               //           '' + controller.info['restAddress'] ??
              //               //           '',
              //               //     ),
              //               //   ),
              //               // ),
              //               info(
              //                 "휴대폰",
              //                 Text(controller.info['ownerPhone'] ?? ''),
              //               ),
              //               info(
              //                 "유선전화",
              //                 Text(controller.info['storePhone'] ?? ''),
              //               ),
              //             ],
              //           ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  Container info(String title, Widget context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Row(
        children: [
          Container(
            width: 100,
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          context
        ],
      ),
    );
  }
}
