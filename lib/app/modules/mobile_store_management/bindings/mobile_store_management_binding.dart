import 'package:get/get.dart';

import '../../../../controller/store_management_controller.dart';

class MobileStoreManagementBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StoreManagementController>(
      () => StoreManagementController(),
    );
  }
}
