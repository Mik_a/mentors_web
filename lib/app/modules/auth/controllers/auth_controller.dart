import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/routes/app_pages.dart';

class AuthController extends GetxController {
  //TODO: Implement AuthController

  Color bg = Colors.white;
  @override
  void onInit() {
    super.onInit();

    String? jwt = Get.parameters['jwt'];

    if (jwt != null) {
      tryLogin(jwt);
    } else {}
  }

  void tryLogin(String jwt) async {
    final res = await API.store.storeLoginByManager(jwt);
    if (res == null) {
      Get.offNamed('/');
    } else {
      if (res['success']) {
        await GetStorage().write('jwt', res['data']);
        await GetStorage().write('id', res['id']);
        EasyLoading.showSuccess("로그인에 성공했습니다.");
        Get.offAndToNamed(Routes.HOME);
      } else {
        Get.offNamed('/');
      }
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
