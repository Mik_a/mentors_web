import 'dart:async';

import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/classes/section.dart';

class MobileViewSectionController extends GetxController {
  //TODO: Implement MobileViewSectionController

  late Section section;
  RxBool isLoading = true.obs;

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    section = Get.arguments['section'] as Section;
    Get.put(SeatArrangementController()).onClickSection(section);

    Timer(Duration(seconds: 1), () {
      isLoading.value = false;
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
