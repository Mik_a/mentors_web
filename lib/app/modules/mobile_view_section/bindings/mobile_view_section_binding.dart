import 'package:get/get.dart';

import '../controllers/mobile_view_section_controller.dart';

class MobileViewSectionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MobileViewSectionController>(
      () => MobileViewSectionController(),
    );
  }
}
