import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/components/seat_arrangement/seat_arrangement_image.dart';
import 'package:mentors_web/components/section_list/section_image.dart';

import '../controllers/mobile_view_section_controller.dart';

class MobileViewSectionView extends GetView<MobileViewSectionController> {
  final SeatArrangementController seatController =
      Get.put(SeatArrangementController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        title: Text((Get.arguments['section'] as Section).name),
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
      ),
      body: Obx(() {
        return controller.isLoading.value
            ? Center(child: CircularProgressIndicator())
            : SeatArrangementImage(
                isMobile: true,
              );
      }),
    );
  }
}
