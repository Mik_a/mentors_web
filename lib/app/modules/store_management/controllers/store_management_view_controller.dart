import 'package:get/get.dart';

class StoreManagementViewController extends GetxController {
  Rx<int> currentPage = 0.obs;

  void setCurrentPage(int v) {
    currentPage.value = v;
  }
}
