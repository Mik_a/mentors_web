import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/custom_checkbox.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/controller/store_management_controller.dart';
import 'package:mentors_web/components/form_with_title.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/global_colors.dart';

class StoreTab extends StatelessWidget {
  StoreTab({Key? key}) : super(key: key);
  final StoreManagementController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    final Widget paymentForm = FormWithTitle(
      height: 160,
      title: "결제 설정",
      action: SquareButton(
        title: "저장",
        icon: Icon(
          Icons.add_circle,
          size: 16,
          color: CustomColors.cD9C87A,
        ),
        onClick: controller.onClickPaymentSave,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InputboxWithTitle(
            titleWidth: 60,
            inputWidth: 100,
            title: "CAT   과세",
            controller: controller.cat1,
          ),
          InputboxWithTitle(
            titleWidth: 60,
            inputWidth: 100,
            title: "CAT 비과세",
            controller: controller.cat2,
          ),
        ],
      ),
    );

    return Container(
        child: ListView(
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      children: [
        management(),
        paymentForm,
        kioskAlert(),
      ],
    ));
  }

  FormWithTitle kioskAlert() {
    return FormWithTitle(
      height: 1500,
      title: "키오스크 문구 설정",
      action: SquareButton(
        title: "저장",
        icon: Icon(
          Icons.add_circle,
          size: 16,
          color: CustomColors.cD9C87A,
        ),
        onClick: controller.onClickKioskSave,
      ),
      child: Column(
        children: [
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "서비스 이용약관",
            controller: controller.serviceMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "문의사항",
            controller: controller.callManagerMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "사물함 안내사항",
            controller: controller.lockerMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "사물함 주의사항",
            controller: controller.lockerWarningMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "좌석 안내사항",
            controller: controller.seatMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "좌석 주의사항",
            controller: controller.seatWarningMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "결제 안내사항",
            controller: controller.payMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "결제 주의사항",
            controller: controller.payWarningMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "기타 메세지",
            controller: controller.etcMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
          SizedBox(height: 20),
          InputboxWithTitle(
            title: "기타 주의 메세지",
            controller: controller.etcWarningMessage,
            textInputType: TextInputType.multiline,
            inputHeight: 120,
            inputWidth: double.infinity,
          ),
        ],
      ),
    );
  }

  FormWithTitle management() {
    return FormWithTitle(
      height: 150,
      title: "운영설정",
      action: SquareButton(
        title: "저장",
        icon: Icon(
          Icons.add_circle,
          size: 16,
          color: CustomColors.cD9C87A,
        ),
        onClick: controller.onClickStoreOperationInfoSave,
      ),
      child: Obx(() {
        return Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: IgnorePointer(
                      ignoring: false,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          storeTime(
                            "오픈시간",
                            controller.openHour,
                            controller.openMin,
                          ),
                          SizedBox(width: 50),
                          storeTime(
                            "마감시간",
                            controller.closeHour,
                            controller.closeMin,
                          ),
                          SizedBox(width: 50),
                          CustomCheckbox(
                            title: "24시간",
                            onChange: (_) {
                              controller.is24.value = _;
                            },
                            value: controller.is24.value,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: [
                InputboxWithTitle(
                  titleWidth: 100,
                  inputWidth: 64,
                  inputHeight: 30,
                  containerWidth: 200,
                  title: "외출 퇴실처리",
                  tailText: "분",
                  textAlign: TextAlign.center,
                  controller: controller.outTime,
                  textInputType: TextInputType.number,
                ),
                SizedBox(width: 50),
                InputboxWithTitle(
                  titleWidth: 150,
                  inputWidth: 64,
                  inputHeight: 30,
                  containerWidth: 250,
                  title: "이용 시간 초과 요금 (10분당)",
                  tailText: "원",
                  textAlign: TextAlign.center,
                  controller: controller.overflowPrice,
                  textInputType: TextInputType.number,
                ),
              ],
            ),
          ],
        );
      }),
    );
  }

  Row storeTime(
    String title,
    TextEditingController hour,
    TextEditingController mc,
  ) {
    return Row(
      children: [
        InputboxWithTitle(
          titleWidth: 49,
          inputWidth: 45,
          containerWidth: 110,
          inputHeight: 30,
          title: title,
          controller: hour,
          textAlign: TextAlign.center,
          textInputType: TextInputType.number,
          // onChanged: (v) => {hour.text = v},
        ),
        SizedBox(width: 4),
        Text("시"),
        SizedBox(width: 8),
        InputBox(
          width: 45,
          height: 30,
          controller: mc,
          textAlign: TextAlign.center,
          textInputType: TextInputType.number,
          // onChanged: (v) => {mc.text = v},
        ),
        SizedBox(width: 4),
        Text("분"),
      ],
    );
  }
}
