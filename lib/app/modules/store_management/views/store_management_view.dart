import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mentors_web/app/modules/message_template/views/message_template_view.dart';
import 'package:mentors_web/app/modules/store_management/controllers/store_management_view_controller.dart';
import 'package:mentors_web/app/modules/store_management/views/device_view.dart';
import 'package:mentors_web/app/modules/store_management/views/open_door.dart';
import 'package:mentors_web/app/modules/store_management/views/store_information.dart';
import 'package:mentors_web/app/modules/store_management/views/store_tab.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/custom_tab_bar.dart';

class StoreManagementView extends StatelessWidget {
  final StoreManagementViewController controller =
      Get.put(StoreManagementViewController());

  final List<String> headers = [
    '지역',
    '점포 타입',
    '점포명',
    '고객수',
    '점주명',
    '연락처',
    ' 오픈일자'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          children: [
            Column(
              children: [
                StoreInformation(),
                Expanded(
                  child: RegisterCard(
                    title: "매장 관리",
                    width: 450,
                    child: OpenDoor(),
                  ),
                ),
              ],
            ),
            SizedBox(width: 20),
            Expanded(
              child: CustomTabbar(
                onClickHeader: controller.setCurrentPage,
                headers: ["매장", "기기", "메세지"],
                children: [
                  StoreTab(),
                  DeviceView(),
                  MessageTemplateView(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
