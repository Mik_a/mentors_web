import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/controller/store_management_controller.dart';

import '../../../../global_colors.dart';

class StoreInformation extends StatelessWidget {
  StoreInformation({Key? key}) : super(key: key);

  final StoreManagementController controller =
      Get.put(StoreManagementController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RegisterCard(
        title: "매장 정보",
        width: 450,
        height: 265,
        action: Row(
          children: [
            controller.storeInfo.value.isPass == false
                ? SquareButton(
                    title: "방역패스OFF",
                    icon: SvgPicture.asset("assets/icons/covid_pass_off.svg"),
                    onClick: controller.onClickStoreCovidPassOn,
                  )
                : Container(),
            controller.storeInfo.value.isPass == true
                ? SquareButton(
                    title: "방역패스ON",
                    icon: SvgPicture.asset("assets/icons/covid_pass_on.svg"),
                    backgroundColor: CustomColors.f3fffa,
                    borderColor: kMaterialColor,
                    onClick: controller.onClickStoreCovidPassOff,
                  )
                : Container(),
          ],
        ),
        child: Column(
          children: [
            line(
              form("점포명", controller.storeInfo.value.storeName),
              form("상태", '정상운영'),
            ),
            line(
              form(
                "오픈일",
                controller.storeInfo.value.openDate == null
                    ? ""
                    : controller.storeInfo.value.openDate!
                        .toString()
                        .substring(0, 10),
              ),
              form("점포타입", '독서실'),
            ),
            form("점주성명", controller.storeInfo.value.ownerName),
            line(
              form("휴대폰", controller.storeInfo.value.ownerPhone),
              form("이메일", controller.storeInfo.value.ownerName),
            ),
            line(
              form("유선전화", controller.storeInfo.value.storePhone),
              form("팩스번호", '0000'),
            ),
            form("홈페이지", controller.storeInfo.value.homepage),
            form("주소", controller.storeInfo.value.restAddress),
          ],
        ),
      ),
    );
  }

  Row line(Widget left, Widget right) {
    return Row(
      children: [
        Expanded(child: left),
        Expanded(child: right),
      ],
    );
  }

  Widget form(String title, String data) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Row(
        children: [
          AutoSpacingText(text: title),
          SizedBox(width: 12),
          Text(data),
        ],
      ),
    );
  }
}
