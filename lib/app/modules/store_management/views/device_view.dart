import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/controller/device_controller.dart';
import 'package:mentors_web/modules/lib.dart';

class DeviceView extends StatelessWidget {
  DeviceView({Key? key}) : super(key: key);
  final DeviceController controller = Get.put(DeviceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.deviceList.length == 0
          ? Center(
              child: Text(
                "등록된 기기가 없습니다\n키오스크에 로그인 시 자동으로 등록됩니다",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18),
              ),
            )
          : PagenationListView(
              headerWidths: [50, 200, 100, 300, 90, 90],
              itemHeight: 50,
              data: [
                ...List.generate(controller.deviceList.length, (index) {
                  return [
                    Center(child: Text((index + 1).toString())),
                    Center(child: Text(controller.deviceList[index].deviceId)),
                    Center(child: Text(controller.deviceList[index].name)),
                    Center(
                      child: Text(getKoreaTime(
                          controller.deviceList[index].createDate)),
                    ),
                    Center(
                      child: CustomButton(
                        title: "이름 수정",
                        width: 90,
                        onPressed: () => controller.onClickUpdateDeviceName(
                          controller.deviceList[index],
                        ),
                      ),
                    ),
                    Center(
                      child: CustomButton(
                        title: "삭제",
                        width: 90,
                        backgroundColor: Color(0xffE85B48),
                        onPressed: () => controller.onClickRemoveDevice(
                          controller.deviceList[index],
                        ),
                      ),
                    ),
                  ];
                })
              ],
              headerTitles: [
                Text("No."),
                Text("ID"),
                Text("이름"),
                Text("등록일자"),
                Text(""),
                Text(""),
              ],
            ),
    );
  }
}
