import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_icon_button.dart';
import 'package:mentors_web/controller/device_controller.dart';
import 'package:mentors_web/controller/socket_controller.dart';
import 'package:mentors_web/global_colors.dart';

class OpenDoor extends StatelessWidget {
  OpenDoor({Key? key}) : super(key: key);
  final DeviceController deviceController = Get.put(DeviceController());
  final SocketController socketController = Get.put(SocketController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 50,
            child: AutoSpacingText(
              text: "출입문",
              width: 42,
            ),
          ),
          SizedBox(width: 50),
          Obx(
            () => Expanded(
              child: ListView.separated(
                padding: EdgeInsets.zero,
                itemBuilder: (context, index) => item(index),
                separatorBuilder: (context, index) => SizedBox(height: 8),
                itemCount: deviceController.deviceList.length,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Row item(int index) {
    return Row(
      children: [
        Expanded(
          child: Text(
            deviceController.deviceList[index].name,
            style: TextStyle(color: CustomColors.c717071),
          ),
        ),
        CustomIconButton(
          onClick: () => socketController
              .sendOpenDoor(deviceController.deviceList[index].deviceId),
          height: 30,
          title: "출입문 열기",
          icon: "door",
          iconColor: CustomColors.c717071,
          textStyle: TextStyle(fontSize: 12),
        ),
      ],
    );
  }
}
