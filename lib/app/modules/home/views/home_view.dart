import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/home/views/home_web_version_view.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/mobile/mobile_home.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final UserController userController = Get.put(UserController());
  final UserRegisterController userRegisterController =
      Get.put(UserRegisterController());

  @override
  Widget build(BuildContext context) {
    return MediaQuery.of(context).size.width < 480
        ? MobileHome()
        : HomeWebVersionView();
  }
}
