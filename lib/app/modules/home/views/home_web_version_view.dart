import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/components/appbar/appbar_icon.dart';
import 'package:mentors_web/components/appbar/home_appbar.dart';
import 'package:mentors_web/components/drawer/drawer_view.dart';
import 'package:mentors_web/components/sidebar/side_bar.dart';
import 'package:mentors_web/components/socket_alert/socker_alert_view.dart';
import 'package:mentors_web/global_colors.dart';

import '../controllers/home_controller.dart';

class HomeWebVersionView extends StatelessWidget {
  HomeWebVersionView({Key? key}) : super(key: key);
  final HomeController controller = Get.find();
  final UserRegisterController userRegisterController =
      Get.find<UserRegisterController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() {
        return Stack(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      HomeAppBar(
                        title: "MENTORS",
                        actions: [
                          AppbarIcon(
                            color: kMaterialColor,
                            iconPath: 'assets/icons/account.svg',
                            title: "회원등록",
                            onClick: () {
                              userRegisterController.onClickAdd();
                            },
                          ),
                          controller.isDrawerCollapsed.isTrue
                              ? AppbarIcon(
                                  iconPath: 'assets/icons/hamburger.svg',
                                  title: "회원메뉴",
                                  onClick:
                                      controller.onClickDrawerCollapseButton,
                                )
                              : InkWell(
                                  onTap: controller.onClickDrawerCollapseButton,
                                  child: Container(
                                    height: double.infinity,
                                    child: SizedBox(
                                      width: 30,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 18,
                                        color: CustomColors.cA3A2A3,
                                      ),
                                    ),
                                  ),
                                ),
                        ],
                        color: kMaterialColor,
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            Sidebar(
                              iconList: controller.pageList
                                  .where((element) => !element.hide)
                                  .map((e) => e.iconClass)
                                  .toList(),
                              onClickIcon: controller.setPage,
                              currentPage: controller.currentViewPage,
                            ),
                            Expanded(
                              child: Container(
                                height: double.infinity,
                                child: controller.currentViewWidget,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                DrawerView(isCollapsed: controller.isDrawerCollapsed.value),
              ],
            ),
            SocketAlert(),
          ],
        );
      }),
    );
  }
}
