import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/dashboard/views/dashboard_view.dart';
import 'package:mentors_web/app/modules/locker_management/controllers/locker_management_controller.dart';
import 'package:mentors_web/app/modules/locker_management/views/locker_management_view.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/views/management_seat_voucher_view.dart';
import 'package:mentors_web/app/modules/message_management/message_management_view.dart';
import 'package:mentors_web/app/modules/payment_history/payment_history_view.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/seat_arrangement_view.dart';
import 'package:mentors_web/app/modules/store_management/views/store_management_view.dart';
import 'package:mentors_web/app/modules/study_room/study_room_view.dart';
import 'package:mentors_web/app/modules/user_list_page/user_list_page_view.dart';
import 'package:mentors_web/app/modules/user_register/views/user_register_view.dart';
import 'package:mentors_web/components/appbar/appbar_icon.dart';

class IconClass {
  final String icon;
  final String title;

  const IconClass({
    required this.icon,
    required this.title,
  });
}

enum PageEnum {
  dashboard,
  lockerManagement,
  managementSeatVoucher,
  messageManagement,
  paymentHistory,
  seatArrangement,
  storeManagement,
  studyRoom,
  userListPage,
  userRegister,
}

class PageClass {
  final Widget widget;
  final String title;
  final PageEnum page;
  final IconClass iconClass;
  final bool hide;

  const PageClass({
    required this.widget,
    required this.title,
    required this.page,
    required this.iconClass,
    this.hide = false,
  });
}

class HomeController extends GetxController {
  RxBool isDrawerCollapsed = true.obs;
  RxInt currentViewPage = 0.obs;

  final List<PageClass> pageList = [
    PageClass(
      widget: DashboardView(),
      title: 'Dashboard',
      page: PageEnum.dashboard,
      iconClass: IconClass(icon: "assets/icons/dashboard.svg", title: "대시보드"),
    ),
    PageClass(
      widget: SeatArrangementView(),
      title: 'Seat Arrangement',
      page: PageEnum.seatArrangement,
      iconClass: IconClass(icon: "assets/icons/seat.svg", title: "좌석 현황"),
    ),
    PageClass(
      widget: StoreManagementView(),
      title: 'Store Management',
      page: PageEnum.storeManagement,
      iconClass: IconClass(icon: "assets/icons/store.svg", title: "매장관리"),
    ),
    PageClass(
      widget: LockerManagementView(),
      title: 'Locker Management',
      page: PageEnum.lockerManagement,
      iconClass: IconClass(icon: "assets/icons/locker.svg", title: "사물함"),
    ),
    PageClass(
      widget: StudyRoomView(),
      title: 'Study Room',
      page: PageEnum.studyRoom,
      iconClass: IconClass(icon: "assets/icons/studyroom.svg", title: "스터디룸"),
    ),
    PageClass(
      widget: ManagementSeatVoucherView(),
      title: 'Management Seat Voucher',
      page: PageEnum.managementSeatVoucher,
      iconClass:
          IconClass(icon: "assets/icons/seat_voucher.svg", title: "좌석/이용권"),
    ),
    PageClass(
      widget: MessageManagementView(),
      title: 'Message Management',
      page: PageEnum.messageManagement,
      iconClass: IconClass(icon: "assets/icons/message.svg", title: "메세지 관리"),
    ),
    PageClass(
      widget: PaymentHistoryView(),
      title: 'Payment History',
      page: PageEnum.paymentHistory,
      iconClass: IconClass(icon: "assets/icons/pay_history.svg", title: "결제내역"),
    ),
    PageClass(
      widget: UserListPageView(),
      title: 'User List Page',
      page: PageEnum.userListPage,
      iconClass:
          IconClass(icon: "assets/icons/user_register.svg", title: "회원목록"),
    ),
    PageClass(
      widget: UserRegisterView(),
      title: 'User Register',
      page: PageEnum.userRegister,
      iconClass: IconClass(
        icon: "assets/icons/user_register.svg",
        title: "회원등록",
      ),
      hide: true,
    ),
  ];

  RxList<AppbarIcon> appbarIconList = RxList();

  @override
  void onInit() {
    super.onInit();

    appbarIconList.value = [
      AppbarIcon(
        iconPath: "assets/icons/hamburger.svg",
        title: "회원목록",
        onClick: onClickDrawerCollapseButton,
      ),
    ];
  }

  void setPage(int idx) {
    if (idx == 1) {
      Get.find<SeatArrangementController>().updateSeat();
    } else if (idx == 3) {
      Get.find<LockerManagementController>().updateOnClickFromHome();
    }

    currentViewPage.value = idx;
  }

  void showUserInfo() {
    currentViewPage.value = pageList.length - 1;
  }

  void onClickDrawerCollapseButton() {
    isDrawerCollapsed.value = !isDrawerCollapsed.value;
  }

  void toPage(PageEnum page) {
    currentViewPage.value =
        pageList.indexWhere((element) => element.page == page);
  }

  Widget get currentViewWidget => pageList[currentViewPage.value].widget;
}
