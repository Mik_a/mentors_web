import 'package:get/get.dart';

import '../controllers/message_template_controller.dart';

class MessageTemplateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MessageTemplateController>(
      () => MessageTemplateController(),
    );
  }
}
