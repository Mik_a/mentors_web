import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/message_template.dart';
import 'package:mentors_web/modules/lib.dart';

class TemplateGroup {
  String groupName;
  List<MessageTemplate> list;

  TemplateGroup({required this.groupName, required this.list});
}

class MessageTemplateController extends GetxController {
  RxList<MessageTemplate> list = RxList.empty();
  RxList<TemplateGroup> templateGroup = RxList.empty();
  Rx<MessageTemplate> selectedTemplate = MessageTemplate.empty().obs;
  TextEditingController textController = TextEditingController();

  Rx<String> messageLength = '0'.obs;

  @override
  void onInit() {
    super.onInit();
    API.sms.getTemplate().then(onAddedTemplate);
    textController.addListener(textChanged);
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  void textChanged() {
    messageLength.value = getMessageLength;
  }

  void onClickSaveTemplate() async {
    EasyLoading.show();
    String? result = await API.sms.updateTemplateContent(
      selectedTemplate.value.id,
      textController.text,
    );

    if (result == null) {
      EasyLoading.showError("변경 실패");
    } else {
      selectedTemplate.value.message = result;
      EasyLoading.dismiss();
    }
  }

  void onAddedTemplate(List<MessageTemplate> e) {
    list.addAll(e);
    e.forEach((element) {
      TemplateGroup g = templateGroup
          .firstWhere((groupElement) => groupElement.groupName == element.group,
              orElse: () {
        TemplateGroup newGroup = TemplateGroup(
          groupName: element.group,
          list: [],
        );
        templateGroup.add(newGroup);
        return newGroup;
      });
      g.list.add(element);
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onClickTemplate(MessageTemplate template) {
    selectedTemplate.value = template;
    textController.text = template.message;
    templateGroup.refresh();
  }

  void onChangeOnOff(bool v, MessageTemplate template) async {
    EasyLoading.show();
    bool? result = await API.sms.toggleSendMessageTemplate(template.id, v);
    if (result == null) {
      EasyLoading.showError("변경 실패");
    } else {
      EasyLoading.dismiss();
      template.isSend = result;
      templateGroup.refresh();
    }
  }

  String get getMessageLength =>
      getTextLengthKor(textController.text).toString();
}
