import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/classes/message_template.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/switch.dart';
import 'package:mentors_web/global_colors.dart';
import '../controllers/message_template_controller.dart';

class MessageTemplateView extends GetView<MessageTemplateController> {
  final MessageTemplateController controller =
      Get.put(MessageTemplateController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        Container header(String title) {
          return Container(
            height: 40,
            child: Row(
              children: [
                SizedBox(width: 12),
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          );
        }

        Widget templateItem(MessageTemplate template) {
          return InkWell(
            onTap: () => controller.onClickTemplate(template),
            child: Container(
              color: controller.selectedTemplate.value == template
                  ? CustomColors.cFFF4C2
                  : null,
              height: 42,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      template.title,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    CustomSwitch(
                      onTap: (v) {
                        controller.onChangeOnOff(v, template);
                      },
                      value: template.isSend,
                    ),
                  ],
                ),
              ),
            ),
          );
        }

        Container group(TemplateGroup group) {
          return Container(
            decoration: BoxDecoration(
              border: Border.all(color: CustomColors.cE3E3E3),
            ),
            child: ExpandablePanel(
              theme: ExpandableThemeData(),
              collapsed: Container(),
              expanded: Column(
                children: List.generate(
                  group.list.length * 2,
                  (index) => index % 2 == 0
                      ? Divider(height: 0)
                      : templateItem(group.list[index ~/ 2]),
                ),
              ),

              // controller: ExpandableController(initialExpanded: true),
              header: header(group.groupName),
            ),
          );
        }

        var textInputContainer = Container(
          margin: EdgeInsets.symmetric(horizontal: 57),
          width: 381,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    InputBox(
                      textInputType: TextInputType.multiline,
                      controller: controller.textController,
                      width: 348,
                      height: 365,
                    ),
                    Positioned(
                      right: 5,
                      bottom: 5,
                      child: Text(
                        '${controller.messageLength} / 2000bytes',
                        style: TextStyle(
                          fontSize: 12,
                          color: CustomColors.cA3A2A3,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: CustomButton(
                    onPressed: controller.onClickSaveTemplate,
                    width: 348,
                    height: 48,
                    title: "템플릿 저장",
                  ),
                ),
                Text(
                  "사용가능 명령어",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  controller.selectedTemplate.value.command
                      .map((e) => "%$e%")
                      .join(', '),
                  style: TextStyle(fontSize: 12),
                ),
                SizedBox(height: 25),
                Text(
                  "주의사항",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "하단에 뜨는 bytes 수와 실제 전송되는 bytes 수의 차이가 발생할 수 있습니다.",
                  style: TextStyle(fontSize: 12),
                ),
                SizedBox(height: 25),
                Text(
                  "명령어 예시",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "%지점이름% - 000점\n" +
                      "%인증번호% - 123456\n" +
                      "%이전번호% - 5\n" +
                      "%현재번호% - 6\n" +
                      "%현재시간% - 2021/01/01/12:00\n" +
                      "%만료일자% - 2021/01/02/12:00\n" +
                      "%남은일수% - 5\n" +
                      "%남은시간% - 0시간 00분 \n" +
                      "%좌석이름% - '멘토르석'\n",
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
          ),
        );
        return Container(
          padding: EdgeInsets.all(16),
          child: Row(
            children: [
              Container(
                width: 378,
                height: double.infinity,
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 32,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: CustomColors.cf5f5f5,
                      ),
                      child: Center(
                        child: Text(
                          "템플릿 종류",
                          style: TextStyle(
                            fontSize: 12,
                            color: CustomColors.cA3A2A3,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: ListView.separated(
                          padding: EdgeInsets.all(4),
                          itemBuilder: (context, index) =>
                              group(controller.templateGroup[index]),
                          separatorBuilder: (context, index) =>
                              SizedBox(height: 8),
                          itemCount: controller.templateGroup.length,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              if (controller.selectedTemplate.value.id != '')
                textInputContainer,
            ],
          ),
        );
      },
    );
  }
}
