import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/views/add_seattype_modal.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/views/voucher_add_form.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/global_enums.dart';

class SeatInfo {
  bool isUsing;
  String? usingUserId;
  String sectionName;
  String id;
  int index;

  SeatInfo({
    required this.index,
    required this.id,
    required this.isUsing,
    required this.sectionName,
    required this.usingUserId,
  });
}

class ManagementSeatVoucherController extends GetxController {
  TextEditingController seatNameController = TextEditingController();
  TextEditingController seatMemoController = TextEditingController();

  RxList<Voucher> voucherList = RxList();
  RxList<SeatInfo> seatList = RxList();

  Rx<SeatType> currentSelectedSeatType = SeatType.empty().obs;
  Rx<SeatGroup> currentSelectedGroup = SeatGroup.free.obs;
  RxList<SeatType> seatTypeList = RxList.empty();

  bool isLoadingSeatType = false;
  RxBool isGroupVoucherListLoading = true.obs;
  RxInt seatListTabIndex = 0.obs;
  Rx<Uint8List> tempThumbnail = Uint8List(0).obs;

  final List<Widget> headers = [
    Text('No.'),
    Text("이용권 분류"),
    Text("이용시간"),
    Text("과세금액"),
    Text("비과세금액"),
    Text("유효기간"),
    Text("구매한도"),
    Text("이용권상태"),
    Text("설명"),
    Text("삭제"),
  ];

  final List<SeatGroup> groupListEnums = [
    SeatGroup.free,
    SeatGroup.fixed,
    SeatGroup.studyroom,
    SeatGroup.locker
  ];

  @override
  void onInit() {
    super.onInit();
    onClickSeatGroup(0);
  }

  void onVoucherAdded(Voucher voucher) {
    voucherList.add(voucher);
  }

  void onClickSeatGroup(int i) {
    SeatGroup group = groupListEnums[i];
    onGroupChange(group);
    seatListTabIndex.value = 0;
  }

  void onGroupChange(SeatGroup group) async {
    currentSelectedGroup.value = group;
    Get.lazyPut(() => SeatController());
    SeatController controller = Get.find();
    if (controller.isLoading)
      await Future.wait([
        Future.doWhile(() async {
          await Future.delayed(Duration(milliseconds: 500));
          return controller.isLoading;
        })
      ]);

    List<SeatType> list =
        controller.seatTypeList.where((p0) => p0.seatGroup == group).toList();

    seatTypeList.value = list;
    if (list.length > 0) onClickSeatType(0);

    if (group != SeatGroup.studyroom) groupVoucherSetting(group);
  }

  void groupVoucherSetting(SeatGroup group) async {
    isGroupVoucherListLoading.value = true;
    voucherList.value = await API.voucher.getVoucherListByGroupName(group);
    isGroupVoucherListLoading.value = false;
  }

  void onClickSeatType(int i) async {
    tempThumbnail.value = Uint8List(0);

    SeatType seatType = seatTypeList[i];
    if (seatType == currentSelectedSeatType.value || isLoadingSeatType) return;

    seatListTabIndex.value = i;
    EasyLoading.show();
    isLoadingSeatType = true;
    currentSelectedSeatType.value = seatType;
    seatList.value = await API.seat.getSeatListBySeatType(seatType.id);
    isLoadingSeatType = false;
    EasyLoading.dismiss();
  }

  void onClickAddVoucher() {
    Get.dialog(
      Modal(
        title: "이용권 추가",
        width: 382,
        height: 492,
        content: VoucherAddForm(
          seatGroup: currentSelectedGroup.value,
          onVoucherAdded: (voucher) {
            voucherList.add(voucher);
          },
        ),
      ),
    );
  }

  void onClickEditVoucher(Voucher voucher) {
    Get.dialog(
      Modal(
        title: "이용권 수정",
        action: SquareButton(
          onClick: () => onClickRemoveVoucher(voucher),
          title: "이용권삭제",
          icon: SvgPicture.asset('assets/icons/close.svg'),
        ),
        width: 382,
        height: 492,
        content: VoucherAddForm(
          seatGroup: currentSelectedGroup.value,
          onVoucherAdded: (_vocuher) {
            voucher = _vocuher;
            voucherList.refresh();
          },
          selectedVoucher: voucher,
        ),
      ),
    );
  }

  void onClickRemoveVoucher(Voucher voucher) {
    Get.dialog(
      Modal(
        content: Alert(
          title: "경고",
          alertType: AlertType.warning,
          content: "정말 삭제하시겠습니까?",
          confirmTitle: "삭제",
          onConfirm: () {
            removeVoucher(voucher);
            Get.back();
            Get.back();
          },
          onCancle: Get.back,
        ),
      ),
    );
  }

  void onClickSeatTypeModify(SeatType seatType) {
    Get.dialog(
      Modal(
        width: 382,
        height: 544,
        title: "좌석 수정",
        content: AddSeatTypeModal(
          currentSelectedSeatType: seatType,
        ),
      ),
    );
  }

  void onClickNewSeatType() {
    Get.dialog(
      Modal(
        height: 544,
        width: 382,
        title: "좌석 추가",
        content: AddSeatTypeModal(
          group: currentSelectedGroup.value,
          onAdded: (seat) => {
            if (seat != null) seatTypeList.add(seat),
          },
        ),
      ),
    );
  }

  void onClickDeleteSeatType(SeatType seatType) {
    SeatController controller = Get.find();

    Get.dialog(
      Modal(
        content: Alert(
          title: "경고",
          alertType: AlertType.warning,
          content: "정말 삭제하시겠어요?",
          onCancle: Get.back,
          onConfirm: () => {
            controller.onClickDelete(currentSelectedGroup.value, seatType),
            // seatType = SeatType.empty(),
            seatTypeList.remove(seatType),
            onClickSeatType(
                max(0, min(seatListTabIndex.value, seatTypeList.length - 1)))
          },
        ),
      ),
    );
  }

  void removeVoucher(Voucher voucher) async {
    EasyLoading.show(status: "삭제중입니다");
    bool res = await API.voucher.deleteVoucher(
      voucher,
      currentSelectedGroup.value,
    );

    if (res) {
      EasyLoading.showSuccess("삭제되었습니다");
      voucherList.remove(voucher);
    } else {
      EasyLoading.showError("삭제에 실패했습니다");
    }
    return;
  }

  get tabHeaders =>
      currentSelectedSeatType.value.seatGroup == SeatGroup.studyroom
          ? ["좌석"]
          : ["좌석", "이용권"];
}
