import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/radio_button_filed.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

class VoucherAddForm extends StatefulWidget {
  final Function(Voucher) onVoucherAdded;
  final Voucher? selectedVoucher;
  final SeatGroup seatGroup;

  const VoucherAddForm({
    required this.seatGroup,
    required this.onVoucherAdded,
    this.selectedVoucher,
    Key? key,
  }) : super(key: key);

  @override
  _VoucherAddFormState createState() => _VoucherAddFormState();
}

class _VoucherAddFormState extends State<VoucherAddForm> {
  final priceController = TextEditingController();
  final priceFreeController = TextEditingController();
  final extendPriceController = TextEditingController();
  final extendPriceFreeController = TextEditingController();

  final useableTimeController = TextEditingController();
  final expirationTimeController = TextEditingController();
  final memoController = TextEditingController();
  final limitController = TextEditingController();

  VoucherUseType currentSelectedVoucherType = VoucherUseType.day;

  int buyPrice = 0;
  int extendPrice = 0;

  @override
  void initState() {
    super.initState();

    priceController.addListener(updatePrice);
    priceFreeController.addListener(updatePrice);
    extendPriceController.addListener(updatePrice);
    extendPriceFreeController.addListener(updatePrice);

    currentSelectedVoucherType = widget.selectedVoucher == null
        ? isOnlyPeriod()
            ? VoucherUseType.period
            : VoucherUseType.day
        : widget.selectedVoucher!.type;

    if (widget.selectedVoucher != null) {
      Voucher voucher = widget.selectedVoucher!;

      setState(() {
        currentSelectedVoucherType = voucher.type;
      });

      priceController.text = voucher.price.toString();
      useableTimeController.text = voucher.useableTime.toString();
      expirationTimeController.text = voucher.expirationTime.toString();
      memoController.text = voucher.memo.toString();
      limitController.text = voucher.limitAmount.toString();
      extendPriceController.text = voucher.extendPrice.toString();

      priceFreeController.text = voucher.priceFree.toString();
      extendPriceFreeController.text = voucher.extendPriceFree.toString();
    }
  }

  @override
  void dispose() {
    super.dispose();
    priceController.dispose();
    priceFreeController.dispose();
    extendPriceController.dispose();
    extendPriceFreeController.dispose();
  }

  void controllerInit() {
    priceController.text = "";
    useableTimeController.text = '';
    expirationTimeController.text = '';
    memoController.text = '';
    limitController.text = '';
    extendPriceController.text = '';
  }

  bool isOnlyPeriod() {
    return widget.seatGroup == SeatGroup.fixed ||
        widget.seatGroup == SeatGroup.locker;
  }

  void updatePrice() {
    setState(() {
      buyPrice = (int.tryParse(priceController.text) ?? 0) +
          (int.tryParse(priceFreeController.text) ?? 0);
      extendPrice = (int.tryParse(extendPriceController.text) ?? 0) +
          (int.tryParse(extendPriceFreeController.text) ?? 0);
    });
  }

  void onClickSubmit() async {
    bool isAdd = widget.selectedVoucher == null;
    final String keyword = isAdd ? "등록" : "수정";

    int useableTime = int.tryParse(useableTimeController.text) ?? 0;
    int expirationTime = int.tryParse(expirationTimeController.text) ?? 0;
    int price = int.tryParse(priceController.text) ?? 0;

    if (currentSelectedVoucherType != VoucherUseType.time) {
      expirationTime = 0;
      if (useableTime <= 0) {
        EasyLoading.showError("이용권 $keyword에 실패했습니다\n이용시간을 입력 해주세요");
        return;
      }
    }

    if (currentSelectedVoucherType == VoucherUseType.time) {
      if (expirationTime == 0) {
        EasyLoading.showError("이용권 $keyword에 실패했습니다\n유효기간을 입력 해주세요");
        return;
      }
      if (expirationTime == 0) {
        EasyLoading.showError("이용권 $keyword에 실패했습니다\n유효기간을 입력 해주세요");
        return;
      }
    }

    if (price == 0) {
      EasyLoading.showError("이용권 $keyword에 실패했습니다\n구매금액을 입력 해주세요");
      return;
    }

    final z = {
      "type": currentSelectedVoucherType.toEng(),
      "useableTime": useableTimeController.text,
      "expirationTime": expirationTimeController.text,
      'price': priceController.text,
      'priceFree': priceFreeController.text,
      'extendPrice': extendPriceController.text,
      'extendPriceFree': extendPriceFreeController.text,
      'isAble': true.toString(),
      'memo': memoController.text,
      'limitAmount': limitController.text,
    };

    EasyLoading.show(status: "이용권 $keyword 중 입니다..");
    final res = isAdd
        ? await API.voucher.addVoucher(widget.seatGroup.toKor(), z)
        : await API.voucher.editVoucher(
            widget.seatGroup.toKor(), widget.selectedVoucher!.id, z);

    if (res == null) {
      EasyLoading.showError("이용권 $keyword에 실패했습니다\n입력 정보를 확인해주세요");
    } else {
      if (isAdd) {
        widget.onVoucherAdded(res);
      } else {
        widget.selectedVoucher!.price = int.parse(priceController.text);
        widget.selectedVoucher!.useableTime =
            int.parse(useableTimeController.text);
        widget.selectedVoucher!.expirationTime =
            int.parse(expirationTimeController.text);
        widget.selectedVoucher!.memo = memoController.text;
        widget.selectedVoucher!.limitAmount = int.parse(limitController.text);
        widget.selectedVoucher!.priceFree = int.parse(priceFreeController.text);
        widget.selectedVoucher!.extendPriceFree =
            int.parse(extendPriceFreeController.text);
        widget.selectedVoucher!.extendPrice =
            int.parse(extendPriceController.text);
        widget.selectedVoucher!.type = currentSelectedVoucherType;
        widget.onVoucherAdded(res);
      }
      EasyLoading.showSuccess("이용권이 $keyword되었습니다");
    }

    Get.back();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 448,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16),
          Container(
            height: 32,
            child: Row(
              children: [
                RadioButtonField(
                  width: 280,
                  title: "분류",
                  values: isOnlyPeriod()
                      ? [VoucherUseType.period]
                      : [
                          VoucherUseType.day,
                          VoucherUseType.time,
                          VoucherUseType.period,
                        ],
                  titles: isOnlyPeriod() ? ['기간권'] : ["당일권", "시간권", "기간권"],
                  currentValue: currentSelectedVoucherType,
                  onChange: (v) => setState(
                    () {
                      currentSelectedVoucherType = v;
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 12),
          SizedBox(
            height: 32,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InputboxWithTitle(
                  titleWidth: 48,
                  containerWidth: 150,
                  title: currentSelectedVoucherType != VoucherUseType.period
                      ? "이용시간"
                      : "이용기간",
                  hintText: "0",
                  textInputType: TextInputType.number,
                  controller: useableTimeController,
                  tailText: currentSelectedVoucherType == VoucherUseType.period
                      ? "일"
                      : "시간",
                ),
                IgnorePointer(
                  ignoring: currentSelectedVoucherType != VoucherUseType.time,
                  child: Opacity(
                    opacity: currentSelectedVoucherType != VoucherUseType.time
                        ? 0.5
                        : 1,
                    child: InputboxWithTitle(
                      titleWidth: 48,
                      containerWidth: 130,
                      title: "유효기간",
                      textInputType: TextInputType.number,
                      controller: expirationTimeController,
                      hintText: '0',
                      tailText: "일",
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 12),
          InputboxWithTitle(
            titleWidth: 48,
            containerWidth: 130,
            title: "구매한도",
            hintText: '0',
            textInputType: TextInputType.number,
            controller: limitController,
            tailText: "개",
          ),
          SizedBox(height: 12),
          newMethod("구매금액", priceController, priceFreeController, buyPrice),
          SizedBox(height: 12),
          newMethod(
            "연장금액",
            extendPriceController,
            extendPriceFreeController,
            extendPrice,
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              "* 미입력시 구매금액과 동일하게 입력됩니다",
              style: TextStyle(
                fontSize: 10,
                color: CustomColors.cA3A2A3,
              ),
            ),
          ),
          SizedBox(height: 12),
          InputboxWithTitle(
            titleWidth: 48,
            inputHeight: 80,
            inputWidth: double.infinity,
            title: "설명",
            textInputType: TextInputType.multiline,
            controller: memoController,
          ),
          Expanded(child: SizedBox()),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                  width: 120,
                  title: "취소",
                  type: CustomButtonType.cancle,
                  onPressed: Get.back,
                ),
                SizedBox(width: 8),
                CustomButton(
                  width: 120,
                  title: "저장",
                  type: CustomButtonType.confirm,
                  onPressed: onClickSubmit,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  SizedBox newMethod(
    String title,
    TextEditingController price,
    TextEditingController free,
    int sumPrice,
  ) {
    return SizedBox(
      height: 32,
      child: Row(
        children: [
          AutoSpacingText(text: title),
          SizedBox(width: 8),
          InputBox(
            width: 68,
            hintText: "과세금액",
            controller: price,
            textInputType: TextInputType.number,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("+"),
          ),
          InputBox(
            width: 68,
            hintText: "비과세금액",
            controller: free,
            textInputType: TextInputType.number,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("="),
          ),
          Text("${addCommaInMoney(sumPrice)}원"),
        ],
      ),
    );
  }

  Container title() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      height: 44,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "이용권 추가",
            style: TextStyle(fontSize: 16),
          ),
          Divider(),
        ],
      ),
    );
  }
}
