import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/custom_tab_bar.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

class GroupView extends StatelessWidget {
  final ManagementSeatVoucherController controller = Get.find();
  GroupView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> getVoucherPanelList(List<Voucher> vouchers) {
      Map<VoucherUseType, List<Voucher>> map = {};
      VoucherUseType.values.forEach((element) {
        if (map[element] == null) map[element] = [];
      });
      vouchers.forEach((element) {
        map[element.type]!.add(element);
      });
      return map.entries.map((e) => expandablePanelGenerator(e)).toList();
    }

    Container groupTabRenderer(SeatGroup group) => Container(child: Obx(() {
          return controller.isGroupVoucherListLoading.value
              ? Center(child: CircularProgressIndicator())
              : group == SeatGroup.studyroom
                  ? Center(child: Text("스터디룸은 이용권 추가가 불가능합니다"))
                  : controller.voucherList.length == 0
                      ? Center(child: Text("이용권이 없습니다. 이용권을 추가해주세요"))
                      : ListView(
                          padding: EdgeInsets.zero,
                          children: getVoucherPanelList(controller.voucherList),
                        );
        }));

    return SizedBox(
      width: 580,
      child: CustomTabbar(
        action: controller.currentSelectedGroup.value != SeatGroup.studyroom
            ? SquareButton(
                title: "이용권 추가하기",
                backgroundColor: Colors.white,
                onClick: controller.onClickAddVoucher,
                icon: Icon(
                  Icons.add_circle,
                  color: Color(0xffd9c87a),
                  size: 16,
                ),
              )
            : null,
        headers:
            controller.groupListEnums.map<String>((e) => e.toKor()).toList(),
        children: List.generate(
          controller.groupListEnums.length,
          (index) => groupTabRenderer(controller.groupListEnums[index]),
        ),
        onClickHeader: controller.onClickSeatGroup,
      ),
    );
  }

  Container expandablePanelGenerator(
    MapEntry<VoucherUseType, List<Voucher>> vouchers,
  ) {
    return vouchers.value.length == 0
        ? Container()
        : Container(
            margin: EdgeInsets.symmetric(vertical: 4),
            decoration: BoxDecoration(
              border: Border.all(
                width: 2,
                color: Color(0xffe3e3e3),
              ),
            ),
            child: ExpandablePanel(
              header: Container(
                height: 40,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text(vouchers.key.toKor()),
                  ),
                ),
              ),
              collapsed: Container(),
              expanded: Container(
                child: Column(
                  children: [
                    ...List.generate(
                      vouchers.value.length * 2,
                      (index) => index % 2 == 0
                          ? Divider(height: 0)
                          : voucherItem(
                              vouchers.key, vouchers.value[index ~/ 2]),
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  Container voucherItem(VoucherUseType type, Voucher voucher) {
    return Container(
      padding: EdgeInsets.all(12),
      height: 150,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                  '${voucher.useableTime} ${type == VoucherUseType.period ? "일" : "시간"}'),
              voucher.isEnable
                  ? Text(
                      '판매중',
                      style: TextStyle(
                        color: Color(0xff19a980),
                      ),
                    )
                  : Text('판매 중단'),
            ],
          ),
          SizedBox(height: 8),
          Text(
            voucher.memo == '' ? "메모가 없습니다" : voucher.memo,
            style: TextStyle(
              fontSize: 10,
              color: Color(0xff717071),
            ),
          ),
          Divider(height: 26),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                info(
                  "구매금액",
                  "(과세+비과세)",
                  addCommaInMoney(voucher.price + voucher.priceFree) + "원",
                ),
                info(
                  "연장금액",
                  "(과세+비과세)",
                  addCommaInMoney(
                          voucher.extendPrice! + voucher.extendPriceFree) +
                      "원",
                ),
                info(
                  "이용기간",
                  "",
                  voucher.type == VoucherUseType.day
                      ? '당일'
                      : '${voucher.useableTime}일',
                ),
                info("구매한도", "", "-"),
                SquareButton(
                  title: "상세보기",
                  onClick: () => controller.onClickEditVoucher(voucher),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  SizedBox info(String title, String subtitle, String text) {
    return SizedBox(
      height: 48,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0xff717071),
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    fontSize: 10,
                    color: Color(0xff717071),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 4),
          Text(
            text,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
