import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/custom_tab_bar.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/components/seattype_image.dart';
import 'package:mentors_web/components/square_button.dart';

class SeatListView extends StatelessWidget {
  final ManagementSeatVoucherController controller = Get.find();

  SeatListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        Row newMethod(String title, String content) {
          return Row(
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xff717071),
                ),
              ),
              SizedBox(width: 20),
              Text(
                content,
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
            ],
          );
        }

        Container seatInfo(SeatType type) {
          return Container(
            child: Column(
              children: [
                Container(
                  height: 180,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xffc4c4c4),
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                        height: 44,
                        margin: EdgeInsets.only(
                          left: 16,
                          right: 16,
                        ),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "좌석 정보",
                                style: TextStyle(fontSize: 16),
                              ),
                              Row(
                                children: [
                                  SquareButton(
                                    title: "수정",
                                    icon: SvgPicture.asset(
                                        'assets/icons/edit.svg'),
                                    onClick: () =>
                                        controller.onClickSeatTypeModify(type),
                                  ),
                                  SizedBox(width: 6),
                                  SquareButton(
                                    title: "삭제",
                                    icon: SvgPicture.asset(
                                        'assets/icons/close.svg'),
                                    onClick: () =>
                                        controller.onClickDeleteSeatType(type),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Divider(height: 0),
                      ),
                      Expanded(
                        child: SizedBox(
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(16),
                                child:
                                    controller.tempThumbnail.value.length != 0
                                        ? Image.memory(
                                            controller.tempThumbnail.value)
                                        : SeatTypeImage(id: type.id),
                              ),
                              SizedBox(
                                height: 104,
                                child: Row(
                                  children: [
                                    Text(type.image ?? ""),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        newMethod("좌석이름", type.name),
                                        newMethod("좌석갯수",
                                            '${controller.seatList.length}석'),
                                        newMethod(
                                          "좌석설명",
                                          type.description != ''
                                              ? type.description
                                              : "설명이 없습니다",
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: PagenationListView(
                      data: List.generate(controller.seatList.length, (index) {
                        SeatInfo info = controller.seatList[index];
                        return [
                          Text(
                            (index + 1).toString(),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            info.sectionName,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            info.index.toString(),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            info.isUsing ? "사용중" : "공석",
                            textAlign: TextAlign.center,
                          ),
                        ];
                      }),
                      itemHeight: 32,
                      headerTitles: [
                        Text("No."),
                        Text("배치도"),
                        Text("좌석번호"),
                        Text("좌석상태"),
                      ]),
                )
              ],
            ),
          );
        }

        return Expanded(
          child: controller.seatTypeList.length == 0
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("좌석이 없습니다. 좌석을 생성해주세요"),
                      SizedBox(height: 12),
                      CustomButton(
                        width: 300,
                        title: "좌석 생성하기",
                        onPressed: controller.onClickNewSeatType,
                      )
                    ],
                  ),
                )
              : IgnorePointer(
                  ignoring: controller.isLoadingSeatType,
                  child: CustomTabbar(
                    action: SquareButton(
                      title: "좌석 추가하기",
                      onClick: controller.onClickNewSeatType,
                      backgroundColor: Colors.white,
                      icon: Icon(
                        Icons.add_circle_sharp,
                        size: 16,
                        color: Color(0xffd9c87a),
                      ),
                    ),
                    currentPage: controller.seatListTabIndex.value,
                    headers: controller.seatTypeList
                        .map((element) => element.name)
                        .toList(),
                    children: List.generate(
                      controller.seatTypeList.length,
                      (index) => seatInfo(controller.seatTypeList[index]),
                    ),
                    onClickHeader: controller.onClickSeatType,
                  ),
                ),
        );
      },
    );
  }
}
