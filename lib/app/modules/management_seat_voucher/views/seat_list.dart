import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';

class SeatList extends StatelessWidget {
  final ManagementSeatVoucherController controller =
      Get.put(ManagementSeatVoucherController());
  SeatList({Key? key}) : super(key: key);

  final List<Widget> headers = [
    Text('No.'),
    Text("열람실"),
    Text("좌석타입"),
    Text("좌석이름"),
    Text("좌석번호"),
    Text("좌석상태"),
    Text("관리"),
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => PagenationListView(
        data: [
          ...List.generate(
            controller.seatList.length,
            (index) => [
              Center(child: Text((index + 1).toString())),
              Center(child: Text(controller.seatList[index].sectionName)),
              Center(child: Text('-')),
              Center(child: Text("-")),
              Center(child: Text(controller.seatList[index].index.toString())),
              Center(
                  child:
                      Text(controller.seatList[index].isUsing ? "사용중" : "공석")),
              Center(child: Text("-")),
            ],
          )
        ],
        itemHeight: 30,
        headerTitles: headers,
      ),
    );
  }
}
