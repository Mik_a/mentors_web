import 'dart:typed_data';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/radio_button.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/components/seattype_image.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/global_enums.dart';

class AddSeatTypeModal extends StatefulWidget {
  final SeatGroup? group;
  final SeatType? currentSelectedSeatType;
  final Function(SeatType?)? onAdded;

  AddSeatTypeModal({
    this.currentSelectedSeatType,
    this.group,
    this.onAdded,
    Key? key,
  }) : super(key: key);

  @override
  _AddSeatTypeModalState createState() => _AddSeatTypeModalState();
}

class _AddSeatTypeModalState extends State<AddSeatTypeModal> {
  final TextEditingController name = TextEditingController();
  final TextEditingController memo = TextEditingController();
  final TextEditingController maxPeople = TextEditingController();
  final TextEditingController pricePerHour = TextEditingController();

  Uint8List? imagePath;
  XFile? xfile;
  SeatGroup selecetedSeatGroup = SeatGroup.free;

  @override
  void initState() {
    super.initState();
    if (widget.currentSelectedSeatType != null) {
      name.text = widget.currentSelectedSeatType!.name;
      memo.text = widget.currentSelectedSeatType!.description;
      maxPeople.text = widget.currentSelectedSeatType!.maxPeople.toString();
      pricePerHour.text = widget.currentSelectedSeatType!.maxPeople.toString();
    }

    if (widget.group != null) {
      selecetedSeatGroup = widget.group!;
    }
  }

  @override
  void dispose() {
    name.dispose();
    memo.dispose();
    super.dispose();
  }

  void onClickAddSeat(String name, String memo) async {
    if (name.length == 0) {
      EasyLoading.showError("이름을 지정해주세요");
      return;
    }
    EasyLoading.show(status: "좌석 등록 중 입니다");

    final value = widget.currentSelectedSeatType != null
        ? await API.seat.updateSeatType(
            widget.currentSelectedSeatType!.id,
            name,
            memo,
            pricePerHour.text,
            maxPeople.text,
            selecetedSeatGroup,
            xfile,
          )
        : await API.seat.addNewSeatType(
            name,
            memo,
            pricePerHour.text,
            maxPeople.text,
            selecetedSeatGroup,
            xfile,
          );

    if (value != null) {
      SeatController seatController = Get.find();

      void removeCache(String id) {
        final url =
            '$server/upload?path=${GetStorage().read('id')}/seatThumbnail/$id';
        final NetworkImage provider = NetworkImage(url);
        provider.evict().then<void>((bool success) {
          if (success) debugPrint('removed image!');
        });
      }

      if (widget.currentSelectedSeatType != null) {
        widget.currentSelectedSeatType!.name = value.name;
        widget.currentSelectedSeatType!.description = value.description;
        widget.currentSelectedSeatType!.seatGroup = value.seatGroup;
        widget.currentSelectedSeatType!.image = value.image;

        removeCache(value.id);

        ManagementSeatVoucherController c = Get.find();
        c.seatTypeList.refresh();
      } else {
        seatController.seatTypeList.add(value);
      }

      EasyLoading.showSuccess(
        widget.currentSelectedSeatType != null ? "수정되었습니다" : "등록되었습니다",
      );

      if (imagePath != null)
        Get.find<ManagementSeatVoucherController>().tempThumbnail.value =
            imagePath!;

      if (widget.onAdded != null) widget.onAdded!(value);
      Get.back();
    } else {
      EasyLoading.showError("등록에 실패했습니다");
    }
  }

  void onClickAddThumbnail() async {
    XFile? f = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    if (f != null) {
      final b = await f.readAsBytes();
      final image = await decodeImageFromList(b);

      if (image.width != 1088 || image.height != 408) {
        Get.dialog(
          Alert(
            alertType: AlertType.warning,
            content: "1088 x 408 사이즈의\n이미지만 등록 가능합니다",
            onConfirm: Get.back,
            title: "알림",
          ),
        );
      } else {
        setState(() {
          imagePath = b;
          xfile = f;
        });
      }
    }
  }

  void onClickSeatGroup(SeatGroup v) {
    setState(() {
      selecetedSeatGroup = v;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 12),
      color: Colors.white,
      height: SeatGroup.studyroom == selecetedSeatGroup ? 544 : 505,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Row(
              children: [
                AutoSpacingText(
                  text: "좌석타입",
                  width: 50,
                ),
                SizedBox(width: 20),
                RadioButton(
                  title: "자유석",
                  value: SeatGroup.free,
                  isOn: SeatGroup.free == selecetedSeatGroup,
                  onClick: (v) => onClickSeatGroup(v),
                ),
                SizedBox(width: 20),
                RadioButton(
                  title: "지정석",
                  value: SeatGroup.fixed,
                  isOn: SeatGroup.fixed == selecetedSeatGroup,
                  onClick: (v) => onClickSeatGroup(v),
                ),
                SizedBox(width: 20),
                RadioButton(
                  title: "스터디룸",
                  value: SeatGroup.studyroom,
                  isOn: SeatGroup.studyroom == selecetedSeatGroup,
                  onClick: (v) => onClickSeatGroup(v),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 6.0),
            child: InputboxWithTitle(
              controller: name,
              title: "좌석이름",
              hintText: "좌석이름 입력",
              titleWidth: 50,
              inputWidth: 136,
            ),
          ),
          if (SeatGroup.studyroom == selecetedSeatGroup)
            Container(
              width: double.infinity,
              height: 32,
              child: Row(
                children: [
                  SizedBox(
                    width: 135,
                    child: Row(
                      children: [
                        Expanded(
                          child: InputboxWithTitle(
                            textAlign: TextAlign.center,
                            controller: maxPeople,
                            textInputType: TextInputType.number,
                            title: "최대인원",
                            hintText: "-",
                            titleWidth: 50,
                            inputWidth: 50,
                          ),
                        ),
                        Text(
                          "명",
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 40),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 45,
                          child: Column(
                            children: [
                              Text(
                                "이용금액",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff717071),
                                ),
                              ),
                              Text(
                                "(시간당)",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff717071),
                                ),
                              ),
                            ],
                          ),
                        ),
                        InputBox(
                          hintText: "-",
                          controller: pricePerHour,
                          textAlign: TextAlign.center,
                          width: 85,
                          textInputType: TextInputType.number,
                        ),
                        Text(
                          "원",
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 6.0),
            child: InputboxWithTitle(
              controller: memo,
              title: "좌석설명",
              hintText: "좌석에 대한 간단한 설명 입력",
              titleWidth: 50,
              inputWidth: double.infinity,
              textInputType: TextInputType.multiline,
              inputHeight: 80,
            ),
          ),
          SizedBox(height: 12),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSpacingText(width: 50, text: "썸네일"),
                  AutoSpacingText(width: 50, text: "등록하기"),
                ],
              ),
              SizedBox(width: 8),
              CustomButton(
                width: 150,
                title: '이미지 불러오기',
                backgroundColor: Color(0xff00a6a6),
                onPressed: onClickAddThumbnail,
              )
            ],
          ),
          SizedBox(height: 8),
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              "* 1088×408 사이즈의 .png, .jpg 파일만 등록이 가능합니다.",
              style: TextStyle(
                fontSize: 12,
                color: CustomColors.cA3A2A3,
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 32),
              child: widget.currentSelectedSeatType != null
                  ? imagePath == null
                      ? Container(
                          width: 279,
                          height: 104,
                          child: SeatTypeImage(
                              id: widget.currentSelectedSeatType!.id),
                        )
                      : SizedBox(
                          width: 279,
                          height: 104,
                          child: Image.memory(imagePath!),
                        )
                  : imagePath == null
                      ? DottedBorder(
                          dashPattern: [6, 6],
                          color: CustomColors.cA3A2A3,
                          child: Container(
                              width: 279,
                              height: 104,
                              child: Center(
                                child: Text(
                                  "썸네일을 등록하면 미리보기가 가능합니다.",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: CustomColors.cA3A2A3,
                                  ),
                                ),
                              )),
                        )
                      : SizedBox(
                          width: 279,
                          height: 104,
                          child: Image.memory(imagePath!),
                        ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomButton(
                width: 120,
                type: CustomButtonType.cancle,
                onPressed: Get.back,
                title: "취소",
              ),
              SizedBox(width: 8),
              CustomButton(
                width: 120,
                onPressed: () {
                  onClickAddSeat(name.text, memo.text);
                },
                title: "저장",
              ),
            ],
          ),
        ],
      ),
    );
  }
}
