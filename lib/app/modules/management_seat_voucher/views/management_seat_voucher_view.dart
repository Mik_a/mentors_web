import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/views/group_view.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/views/seat_list_view.dart';
import 'package:mentors_web/global_enums.dart';

class ManagementSeatVoucherView extends StatelessWidget {
  final ManagementSeatVoucherController controller =
      Get.put(ManagementSeatVoucherController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              children: [
                GroupView(),
                SizedBox(width: 20),
                if (controller.currentSelectedGroup.value != SeatGroup.locker)
                  SeatListView(),
              ],
            ),
          );
        },
      ),
    );
  }
}
