import 'dart:typed_data';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/radio_button.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/components/seattype_image.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/global_enums.dart';

class AddVoucherModal extends StatefulWidget {
  final SeatType? currentSelectedSeatType;

  AddVoucherModal({
    this.currentSelectedSeatType,
    Key? key,
  }) : super(key: key);

  @override
  _AddVoucherModalState createState() => _AddVoucherModalState();
}

class _AddVoucherModalState extends State<AddVoucherModal> {
  final TextEditingController memo = TextEditingController();
  Uint8List? imagePath;
  XFile? xfile;
  SeatGroup selecetedSeatGroup = SeatGroup.free;

  @override
  void initState() {
    super.initState();
    if (widget.currentSelectedSeatType != null) {
      memo.text = widget.currentSelectedSeatType!.description;
    }
  }

  @override
  void dispose() {
    memo.dispose();
    super.dispose();
  }

  void onClickAddSeat(String name, String memo) async {
    if (name.length == 0) {
      EasyLoading.showError("이름을 지정해주세요");
      return;
    }

    EasyLoading.show(status: "좌석 등록 중 입니다");
    final value = await API.seat.addNewSeatType(
      name,
      memo,
      "1",
      "1",
      selecetedSeatGroup,
      xfile,
    );

    if (value != null) {
      SeatController seatController = Get.find();
      seatController.seatTypeList.add(value);
      EasyLoading.showSuccess("등록되었습니다");
      Get.back();
    } else {
      EasyLoading.showError("등록에 실패했습니다");
    }
  }

  void onClickAddThumbnail() async {
    XFile? f = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    if (f != null) {
      final b = await f.readAsBytes();
      final image = await decodeImageFromList(b);

      if (image.width != 1088 || image.height != 408) {
        Get.dialog(
          Alert(
            alertType: AlertType.warning,
            content: "1088 x 408 사이즈의\n이미지만 등록 가능합니다",
            onConfirm: Get.back,
            title: "알림",
          ),
        );
      } else {
        setState(() {
          imagePath = b;
          xfile = f;
        });
      }
    }
  }

  void onClickSeatGroup(SeatGroup v) {
    setState(() {
      selecetedSeatGroup = v;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 382,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(height: 12),
          Row(
            children: [
              AutoSpacingText(
                text: "타입",
                width: 50,
              ),
              SizedBox(width: 20),
              RadioButton(
                title: "당일권",
                value: SeatGroup.free,
                isOn: SeatGroup.free == selecetedSeatGroup,
                onClick: (v) => onClickSeatGroup(v),
              ),
              SizedBox(width: 20),
              RadioButton(
                title: "시간권",
                value: SeatGroup.fixed,
                isOn: SeatGroup.fixed == selecetedSeatGroup,
                onClick: (v) => onClickSeatGroup(v),
              ),
              SizedBox(width: 20),
              RadioButton(
                title: "기간권",
                value: SeatGroup.studyroom,
                isOn: SeatGroup.studyroom == selecetedSeatGroup,
                onClick: (v) => onClickSeatGroup(v),
              ),
            ],
          ),
          SizedBox(height: 12),
          SizedBox(height: 12),
          InputboxWithTitle(
            controller: memo,
            title: "좌석설명",
            hintText: "좌석에 대한 간단한 설명 입력",
            titleWidth: 50,
            inputWidth: double.infinity,
            textInputType: TextInputType.multiline,
            inputHeight: 80,
          ),
          SizedBox(height: 12),
          Row(
            children: [
              Column(
                children: [
                  AutoSpacingText(width: 50, text: "썸네일"),
                  AutoSpacingText(width: 50, text: "등록하기"),
                ],
              ),
              SizedBox(width: 8),
              CustomButton(
                width: 150,
                title: '이미지 불러오기',
                backgroundColor: Color(0xff00a6a6),
                onPressed: onClickAddThumbnail,
              )
            ],
          ),
          SizedBox(height: 8),
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              "* 1088×408 사이즈의 .png, .jpg 파일만 등록이 가능합니다.",
              style: TextStyle(
                fontSize: 12,
                color: CustomColors.cA3A2A3,
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 32),
              child: widget.currentSelectedSeatType != null
                  ? Container(
                      width: 279,
                      height: 104,
                      child:
                          SeatTypeImage(id: widget.currentSelectedSeatType!.id),
                    )
                  : imagePath == null
                      ? DottedBorder(
                          dashPattern: [6, 6],
                          color: CustomColors.cA3A2A3,
                          child: Container(
                              width: 279,
                              height: 104,
                              child: Center(
                                child: Text(
                                  "썸네일을 등록하면 미리보기가 가능합니다.",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: CustomColors.cA3A2A3,
                                  ),
                                ),
                              )),
                        )
                      : SizedBox(
                          width: 279,
                          height: 104,
                          child: Image.memory(imagePath!),
                        ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomButton(
                width: 120,
                type: CustomButtonType.cancle,
                onPressed: Get.back,
                title: "취소",
              ),
              SizedBox(width: 8),
              CustomButton(
                width: 120,
                onPressed: () {
                  onClickAddSeat("ㅁㄴ", memo.text);
                },
                title: "저장",
              ),
            ],
          ),
        ],
      ),
    );
  }
}
