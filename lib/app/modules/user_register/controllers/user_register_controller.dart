import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/user_register_modal.dart';
import 'package:mentors_web/app/modules/user_register/views/voucher_detail_view.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

class UserRegisterController extends GetxController {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController birthdayController = TextEditingController();
  TextEditingController kioskController = TextEditingController();
  TextEditingController parentPhoneController = TextEditingController();
  TextEditingController memoController = TextEditingController();
  TextEditingController orderMemoController = TextEditingController();

  TextEditingController orderCashPriceController = TextEditingController();
  TextEditingController orderCardPriceController = TextEditingController();

  late SeatController seatController;

  int orderMethod = 1;

  @override
  void onInit() {
    super.onInit();
    seatController = Get.put(SeatController());
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (!seatController.isLoading) {
        if (seatController.seatTypeList.length > 0)
          onChangeSeatType(seatController.seatTypeList[0].seatGroup);
        timer.cancel();
      }
    });
  }

  // User Info --
  Rx<Gender> gender = Gender.male.obs;
  Rx<User> currentSelectedUser = User.empty().obs;

  // current Service
  Rx<String> currentUsingVoucherId = ''.obs;
  Rx<Voucher> currentUsingVoucher = Voucher.empty().obs;

  // select Voucher
  RxBool isDayOpen = false.obs;
  RxBool isTimeOpen = false.obs;
  RxBool isPeriodOpen = false.obs;

  RxList<Voucher> dayVouchers = RxList();
  RxList<Voucher> timeVouchers = RxList();
  RxList<Voucher> periodVouchers = RxList();

  Rx<Voucher> selectedVoucher = Voucher.empty().obs;

  // order Info
  Rx<SeatInfo> selectedSeat = SeatInfo(
    index: 0,
    id: '',
    isUsing: false,
    sectionName: '',
    usingUserId: '',
  ).obs;

  late Rx<SeatGroup> currentSelectedSeatType = SeatGroup.free.obs;

  RxList<Voucher> voucherList = RxList();
  RxList<SeatInfo> seatList = RxList();
  bool payLoading = false;

  RxList<IOLog> personalIOLogs = RxList();
  RxList<SalesLog> personalSalesLogs = RxList();

  @override
  void onClose() {
    firstNameController.dispose();
    nameController.dispose();
    phoneController.dispose();
    emailController.dispose();
    birthdayController.dispose();
    kioskController.dispose();
    parentPhoneController.dispose();
    memoController.dispose();
    orderMemoController.dispose();
    super.onClose();
  }

  void onChangeSeatType(SeatGroup group) async {
    currentSelectedSeatType.value = group;

    final sl = await API.seat.getSeatListByGroupName(group.toKor());
    seatList.value = sl.where((element) => !element.isUsing).toList();
    selectedSeat.value = seatList[0];

    voucherList.value = await API.voucher.getVoucherListByGroupName(group);

    dayVouchers.value =
        voucherList.where((e) => e.type == VoucherUseType.day).toList();
    timeVouchers.value =
        voucherList.where((e) => e.type == VoucherUseType.time).toList();
    periodVouchers.value =
        voucherList.where((e) => e.type == VoucherUseType.period).toList();
  }

  void onClickVoucher(Voucher voucher) {
    selectedVoucher.value = voucher;
    orderCashPriceController.text = voucher.price.toString();
  }

  bool checkInputs() {
    if (!GetUtils.isPhoneNumber(phoneController.text)) {
      EasyLoading.showError("정확한 휴대전화 번호를 입력해주세요.");
      return false;
    }

    if (!GetUtils.isEmail(emailController.text)) {
      EasyLoading.showError("정확한 이메일을 입력해주세요.");
      return false;
    }

    if (kioskController.text.length != 4) {
      EasyLoading.showError("키오스크 비밀번호 4자리 숫자를 입력해주세요");
      return false;
    }

    if (birthdayController.text.length != 8) {
      EasyLoading.showError("생년월일을 정확히 입력해주세요");
      return false;
    }

    return true;
  }

  Future<bool> onClickRegister() async {
    String firstName = firstNameController.text;
    String name = nameController.text;
    String memo = memoController.text;
    String birthday = birthdayController.text;
    String phone = phoneController.text;
    String email = emailController.text;
    String kioskCode = kioskController.text;

    if (!checkInputs()) return false;
    EasyLoading.show(status: "잠시만 기다려주세요");

    final res = await API.user.registerUser(
      birthday: birthday,
      email: email,
      gender: gender.value,
      memo: memo,
      name: firstName + name,
      phone: phone,
      registerStoreId: '',
      kiosk: kioskCode,
    );

    if (res != null) {
      resetControllers();
      UserController userController = Get.find();
      userController.userList.add(res);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> onClickModifySubmit() async {
    if (!checkInputs()) return false;
    EasyLoading.show(status: "잠시만 기다려주세요");

    final res = await API.user.modifyUserInfo(
      userId: currentSelectedUser.value.id,
      birthday: birthdayController.text,
      email: emailController.text,
      gender: gender.value,
      loginCode: kioskController.text,
      memo: memoController.text,
      name: nameController.text,
      parentPhone: parentPhoneController.text,
      phone: phoneController.text,
    );

    if (res) {
      currentSelectedUser.value.name = nameController.text;
      currentSelectedUser.value.birthday = birthdayController.text;
      currentSelectedUser.value.email = emailController.text;
      currentSelectedUser.value.gender = gender.value;
      currentSelectedUser.value.loginCode = kioskController.text;
      currentSelectedUser.value.memo = memoController.text;
      currentSelectedUser.value.name = nameController.text;
      currentSelectedUser.value.parentPhone = parentPhoneController.text;
      currentSelectedUser.value.phone = phoneController.text;
      currentSelectedUser.refresh();

      EasyLoading.showSuccess("수정되었습니다");
    }

    return res;
  }

  void resetControllers() {
    firstNameController.text = '';
    nameController.text = '';
    memoController.text = '';
    birthdayController.text = '';
    phoneController.text = '';
    emailController.text = '';
    kioskController.text = '';
    parentPhoneController.text = '';
  }

  Future<void> setCurrentUser(User user) async {
    currentSelectedUser.value = user;
    personalSalesLogs.value = [];
    personalIOLogs.value = [];

    gender.value = user.gender;
    nameController.text = user.name;
    memoController.text = user.memo ?? '';
    birthdayController.text = user.birthday ?? '-';
    phoneController.text = user.phone;
    kioskController.text = user.loginCode ?? '-';
    parentPhoneController.text = user.parentPhone ?? '';
    emailController.text = user.email.toString();
    currentUsingVoucherId.value = user.currentUsingVoucherId ?? '';

    if (user.currentUsingVoucherId != null) {
      final value = await API.voucher.getVoucherData(
        user.id,
        user.currentUsingVoucherId!,
      );
      if (value != null) {
        currentUsingVoucher.value = value;
      }
    } else {
      currentUsingVoucher.value.id = '';
      currentUsingVoucher.refresh();
    }

    personalIOLogs.value = await API.log.getIOLogsByUser(user.id);
    personalSalesLogs.value = await API.user.getUserPaymentLogs(user.id);
  }

  void onClickModify() {
    setCurrentUser(currentSelectedUser.value);

    Get.dialog(UserRegisterModal(
      isModifing: true,
    ));
  }

  void onClickAdd() {
    resetControllers();
    Get.dialog(UserRegisterModal(
      isModifing: false,
    ));
  }

  void setGender(Gender g) {
    gender.value = g;
  }

  void onClickPayment() async {
    if (payLoading) return;

    if (currentSelectedUser.value.name == "손님") {
      EasyLoading.showError("손님은 이용권 추가 구매가 불가능 합니다.");
      return;
    }

    payLoading = true;
    final res = await API.payment.appendVoucher(
      currentSelectedUser.value.id,
      selectedVoucher.value,
      selectedSeat.value.sectionName,
      selectedSeat.value.id,
    );

    if (res) {
      if (orderCardPriceController.text.isNotEmpty &&
          orderCardPriceController.text != '0') {
        await API.payment.addPayLog(
            price: int.parse(orderCardPriceController.text == ''
                ? '0'
                : orderCardPriceController.text),
            voucherId: selectedVoucher.value.id,
            seatTypeId: selectedSeat.value.id,
            userid: currentSelectedUser.value.id,
            method: PayMethod.card);
      }

      if (orderCashPriceController.text.isNotEmpty &&
          orderCashPriceController.text != '0') {
        await API.payment.addPayLog(
            price: int.parse(orderCashPriceController.text == ''
                ? '0'
                : orderCashPriceController.text),
            voucherId: selectedVoucher.value.id,
            seatTypeId: selectedSeat.value.id,
            userid: currentSelectedUser.value.id,
            method: PayMethod.cash);
      }

      EasyLoading.showSuccess("이용권 구매에 성공했습니다");

      // selectedVoucher.value = Voucher.empty();
    }

    payLoading = false;
  }

  String getStartTime() {
    return selectedVoucher.value.type == VoucherUseType.day
        ? getKoreaTime(DateTime.now()).substring(11)
        : getKoreaTime(DateTime.now()).substring(0, 16);
  }

  String getEndTime() {
    DateTime now = DateTime.now();
    if (selectedVoucher.value.type == VoucherUseType.day) {
      now = now.add(Duration(hours: selectedVoucher.value.useableTime));
    } else if (selectedVoucher.value.type == VoucherUseType.time) {
      now = now.add(Duration(days: selectedVoucher.value.expirationTime!));
    } else {
      now = now.add(Duration(days: selectedVoucher.value.useableTime));
    }
    return selectedVoucher.value.type == VoucherUseType.day
        ? getKoreaTime(now).substring(11)
        : getKoreaTime(now).substring(0, 16);
  }

  void onClickDetailVoucherView() {
    Get.dialog(
      Modal(
        content: VoucherDetailView(
          currentUsingVoucher: currentSelectedUser.value.currentUsingVoucherId,
          userId: currentSelectedUser.value.id,
        ),
      ),
    );
  }

  void onClickUserExit() {
    Get.dialog(
      Alert(
        alertType: AlertType.warning,
        onConfirm: () async {
          Get.back();
          User? user = await Get.find<UserController>()
              .exitThisUser(currentSelectedUser.value);
          if (user != null) {
            currentSelectedUser.value = user;
            currentUsingVoucher.value = Voucher.empty();
            currentSelectedUser.refresh();
          }
        },
        action: RichText(
          text: TextSpan(
              text: '회원을',
              style: TextStyle(
                color: Color(0xff242424),
                fontSize: 14,
              ),
              children: [
                TextSpan(
                  text: ' 퇴실처리',
                  style: TextStyle(
                    color: Color(0xffff6b6b),
                    fontSize: 14,
                  ),
                ),
                TextSpan(
                  text: ' 하시겠습니까?',
                  style: TextStyle(
                    color: Color(0xff242424),
                    fontSize: 14,
                  ),
                ),
              ]),
        ),
        title: "퇴실",
        confirmTitle: "퇴실",
        cancleTitle: "취소",
      ),
    );
  }

  void onClickUserCovidPassOn() {
    Get.dialog(
      Alert(
        alertType: AlertType.alert,
        onConfirm: () async {
          Get.back();
          await Get.find<UserController>()
              .updateThisUser(currentSelectedUser.value, true);
          currentSelectedUser.refresh();
        },
        action: RichText(
          text: TextSpan(
              text: '회원을',
              style: TextStyle(
                color: Color(0xff242424),
                fontSize: 14,
              ),
              children: [
                TextSpan(
                  text: ' 방역인증',
                  style: TextStyle(
                    color: Color(0xff00A6A6),
                    fontSize: 14,
                  ),
                ),
                TextSpan(
                  text: ' 하시겠습니까?',
                  style: TextStyle(
                    color: Color(0xff242424),
                    fontSize: 14,
                  ),
                ),
              ]),
        ),
        title: "방역인증",
        confirmTitle: "방역인증",
        cancleTitle: "취소",
      ),
    );
  }

  void onClickUserCovidPassOff() {
    Get.dialog(
      Alert(
        alertType: AlertType.alert,
        onConfirm: () async {
          Get.back();
          await Get.find<UserController>()
              .updateThisUser(currentSelectedUser.value, false);
          currentSelectedUser.refresh();
        },
        action: RichText(
          text: TextSpan(
              text: '회원을',
              style: TextStyle(
                color: Color(0xff242424),
                fontSize: 14,
              ),
              children: [
                TextSpan(
                  text: ' 방역인증 해제',
                  style: TextStyle(
                    color: Color(0xff00A6A6),
                    fontSize: 14,
                  ),
                ),
                TextSpan(
                  text: ' 하시겠습니까?',
                  style: TextStyle(
                    color: Color(0xff242424),
                    fontSize: 14,
                  ),
                ),
              ]),
        ),
        title: "해제",
        confirmTitle: "해제",
        cancleTitle: "취소",
      ),
    );
  }

  void onClickDeleteUser() {
    Get.dialog(
      Alert(
        alertType: AlertType.warning,
        onConfirm: () async {
          final result =
              await API.user.deleteUser(currentSelectedUser.value.id);
          if (result) {
            Get.back();

            UserController userController = Get.find();
            userController.userList.remove(currentSelectedUser.value);
            currentSelectedUser.value = User.empty();
            userController.userList.refresh();
            HomeController homeController = Get.find();
            homeController.currentViewPage.value = 0;

            EasyLoading.showSuccess("삭제되었습니다");
          } else {
            Get.back();
            EasyLoading.showError("삭제에 실패했습니다. 새로고침 후 다시 시도하세요");
          }
        },
        title: "삭제",
        action: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("회원정보를 삭제 하시겠습니까?"),
            SizedBox(height: 8),
            Text(
              "삭제 후 되돌릴 수 없습니다.",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color(0xffE85B48),
              ),
            ),
          ],
        ),
        cancleTitle: "취소",
        onCancle: Get.back,
      ),
    );
  }

  void toDetailView(User user) async {
    User? _user = await API.user.getUserById(user.id, user.phone);
    if (_user != null) {
      UserController userController = Get.find();
      userController.userList[userController.userList
          .indexWhere((element) => element.id == _user.id)] = _user;
      setCurrentUser(_user);
      Get.find<HomeController>().showUserInfo();

      debugPrint("userData : ${_user.id}");
      debugPrint("userData : ${_user.phone}");
      debugPrint("userData : ${_user.isPass}");
      debugPrint("userData : ${_user.name}");
    }
  }

  List<dynamic> get seatTypeList =>
      seatController.seatTypeList.map((element) => element.name).toList();
}
