import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/user_log/log_list_view.dart';
import 'package:mentors_web/app/modules/user_register/views/user_log/user_log_tabbar.dart';
import 'package:mentors_web/app/modules/user_register/views/user_log/user_payment_log_view.dart';

class UserLogView extends StatelessWidget {
  UserLogView({Key? key}) : super(key: key);
  final UserRegisterController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Card(
        child: UserLogTabbar(
          children: [
            LogListView(),
            UserPaymentLogView(),
          ],
        ),
      ),
    );
  }
}
