import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/modules/lib.dart';

class UserPaymentLogView extends StatelessWidget {
  UserPaymentLogView({Key? key}) : super(key: key);
  final UserRegisterController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      Container newMethod(int index, SalesLog log) {
        return Container(
          height: 15,
          margin: EdgeInsets.symmetric(vertical: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    ("00000" + index.toString())
                        .substring(index.toString().length),
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0xffa3a2a3),
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(log.itemName),
                ],
              ),
              Row(
                children: [
                  Text(
                    addCommaInMoney(log.price) + "원",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(width: 5),
                  Text(
                    "결제완료",
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0xff19a980),
                    ),
                  ),
                  SizedBox(width: 5),
                  Text(
                    getKoreaTime(log.eventTime)
                        .substring(2, 10)
                        .replaceAll('-', '.'),
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0xffa3a2a3),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      }

      return Padding(
        padding: const EdgeInsets.only(
          left: 21,
          top: 15,
          right: 20,
        ),
        child: ListView.builder(
          padding: EdgeInsets.zero,
          itemBuilder: (context, index) => newMethod(
            controller.personalSalesLogs.length - index,
            controller.personalSalesLogs[index],
          ),
          itemCount: controller.personalSalesLogs.length,
        ),
      );
    });
  }
}
