import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class UserLogTabbar extends StatefulWidget {
  final List<Widget> children;

  const UserLogTabbar({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  _UserLogTabbarState createState() => _UserLogTabbarState();
}

class _UserLogTabbarState extends State<UserLogTabbar>
    with SingleTickerProviderStateMixin {
  int _index = 0;

  @override
  void initState() {
    super.initState();
  }

  Widget tab(String title, int index) {
    bool isOn = _index == index;
    BorderSide borderSide = BorderSide(
      color: isOn ? CustomColors.cE3E3E3 : Colors.transparent,
      width: 1,
    );

    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(8),
        topRight: Radius.circular(8),
      ),
      child: InkWell(
        onTap: () {
          setState(() {
            _index = index;
          });
        },
        child: Container(
          width: 80,
          height: 32,
          decoration: BoxDecoration(
            color: isOn ? Colors.white : Colors.transparent,
            border: Border(
              left: borderSide,
              right: borderSide,
              top: borderSide,
            ),
          ),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                fontSize: 14,
                fontWeight: isOn ? FontWeight.bold : FontWeight.w400,
                color: isOn ? CustomColors.c242424 : CustomColors.cA3A2A3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 460,
          color: Color(0xffF5F5F5),
          height: 50,
          child: Container(
              child: Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Row(
                children: [
                  tab('회원기록', 0),
                  tab('결제내역', 1),
                ],
              ),
            ),
          )),
        ),
        Expanded(
          child: Container(
            width: 460,
            child: widget.children[_index],
          ),
        )
      ],
    );
  }
}
