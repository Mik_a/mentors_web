import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/user_log/log_list_item.dart';

class LogListView extends StatelessWidget {
  LogListView({Key? key}) : super(key: key);

  final UserRegisterController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 21,
        top: 15,
        right: 20,
      ),
      child: Obx(() {
        return ListView.builder(
          padding: EdgeInsets.all(0),
          itemBuilder: (context, index) => LogListItem(
            log: controller.personalIOLogs[index],
            index: controller.personalIOLogs.length - index,
          ),
          itemCount: controller.personalIOLogs.length,
        );
      }),
    );
  }
}
