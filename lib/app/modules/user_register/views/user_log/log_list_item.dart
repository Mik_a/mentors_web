import 'package:flutter/material.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/modules/lib.dart';

class LogListItem extends StatelessWidget {
  final IOLog log;
  final int index;
  const LogListItem({
    Key? key,
    required this.log,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 15,
      margin: EdgeInsets.only(bottom: 8),
      child: Row(
        children: [
          Text(
            ("00000" + "$index").substring(("00000" + "$index").length - 5),
            style: TextStyle(color: Color(0xffA3A2A3), fontSize: 12),
          ),
          SizedBox(width: 15),
          Expanded(
            child: Text(log.ioType == IOType.enter
                ? '${log.seatName} ${log.ioType.name}'
                : log.ioType.name),
          ),
          Text(
            getKoreaTime(log.time).substring(0, 19),
            style: TextStyle(
              fontSize: 12,
              color: Color(0xffa3a2a3),
            ),
          ),
        ],
      ),
    );
  }
}
