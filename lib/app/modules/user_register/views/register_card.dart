import 'package:flutter/material.dart';

class RegisterCard extends StatelessWidget {
  final double width;
  final double height;
  final String title;
  final Widget? action;
  final Widget? child;
  final BoxBorder? boxBorder;

  const RegisterCard({
    this.width = 100,
    this.height = 100,
    this.action,
    this.child,
    this.boxBorder,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(border: boxBorder),
      child: Card(
        elevation: boxBorder == null ? null : 0,
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              SizedBox(
                height: 44,
                child: Row(
                  children: [
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Text(
                            title,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    if (action != null)
                      Align(
                        alignment: Alignment.centerRight,
                        child: action!,
                      ),
                  ],
                ),
              ),
              Divider(height: 0),
              SizedBox(height: 8),
              if (child != null)
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 12.0,
                      right: 12,
                    ),
                    child: Container(
                      child: child!,
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
