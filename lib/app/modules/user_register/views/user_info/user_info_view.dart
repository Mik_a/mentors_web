import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/global_enums.dart';

import '../../../../../global_colors.dart';

class UserInfoView extends StatelessWidget {
  final UserRegisterController controller;

  const UserInfoView({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RegisterCard(
        width: 460,
        height: 292,
        title: "기본 회원 정보",
        action: Row(
          children: [
            SquareButton(
              icon: SvgPicture.asset('assets/icons/clear.svg'),
              height: 28,
              title: "삭제",
              onClick: controller.onClickDeleteUser,
            ),
            SizedBox(width: 8),
            if (controller.currentSelectedUser.value.name != "손님")
              SquareButton(
                icon: SvgPicture.asset('assets/icons/edit.svg'),
                height: 28,
                title: "수정",
                onClick: controller.onClickModify,
              ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                name(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    //방역패스 체크 함수 만들어서 바꾸기
                    controller.currentSelectedUser.value.name != "손님" &&
                            controller.currentSelectedUser.value.isPass == false
                        ? SquareButton(
                            title: "방역미인증",
                            icon: SvgPicture.asset(
                                "assets/icons/covid_pass_off.svg"),
                            onClick: controller.onClickUserCovidPassOn,
                          )
                        : Container(),
                    controller.currentSelectedUser.value.name != "손님" &&
                            controller.currentSelectedUser.value.isPass == true
                        ? SquareButton(
                            title: "방역인증",
                            icon: SvgPicture.asset(
                                "assets/icons/covid_pass_on.svg"),
                            backgroundColor: CustomColors.f3fffa,
                            borderColor: kMaterialColor,
                            onClick: controller.onClickUserCovidPassOff,
                          )
                        : Container(),
                    controller.currentSelectedUser.value.status !=
                            UserEnterStatus.exit
                        ? SquareButton(
                            title: "퇴실처리",
                            margin: 6,
                            icon: SvgPicture.asset('assets/icons/exit.svg'),
                            onClick: controller.onClickUserExit,
                          )
                        : Container(),
                  ],
                )
              ],
            ),
            Row(
              children: [
                birthday(),
                email(),
              ],
            ),
            Row(
              children: [
                phoneNumber(),
                parent(),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                loginCode(),
              ],
            ),
            memo(),
          ],
        ),
      ),
    );
  }

  Row parent() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          children: [
            AutoSpacingText(
              text: "보호자",
              width: 44,
            ),
            AutoSpacingText(
              text: "연락처",
              width: 44,
            ),
          ],
        ),
        SizedBox(width: 8),
        Text(controller.currentSelectedUser.value.parentPhone ?? "-")
      ],
    );
  }

  Widget loginCode() {
    return SizedBox(
      height: 32,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              AutoSpacingText(
                text: "키오스크",
                width: 44,
              ),
              AutoSpacingText(
                text: "비밀번호",
                width: 44,
              ),
            ],
          ),
          SizedBox(width: 8),
          Text(controller.currentSelectedUser.value.loginCode ?? "-")
        ],
      ),
    );
  }

  Widget memo() {
    return IgnorePointer(
      ignoring: true,
      child: InputboxWithTitle(
        title: "메모",
        titleWidth: 44,
        inputWidth: double.infinity,
        inputHeight: 70,
        controller: controller.memoController,
        textInputType: TextInputType.multiline,
      ),
    );
  }

  Widget name() {
    return SizedBox(
      height: 32,
      child: Row(
        children: [
          AutoSpacingText(
            text: "이름",
            width: 44,
          ),
          SizedBox(width: 8),
          Text(controller.nameController.text),
          SizedBox(width: 8),
          controller.currentSelectedUser.value.gender == Gender.male
              ? SvgPicture.asset('assets/icons/male.svg')
              : controller.currentSelectedUser.value.gender == Gender.female
                  ? SvgPicture.asset('assets/icons/female.svg')
                  : SizedBox()
        ],
      ),
    );
  }

  Widget phoneNumber() {
    return Container(
      height: 32,
      width: 186,
      child: Row(
        children: [
          AutoSpacingText(
            text: "연락처",
            width: 50,
          ),
          SizedBox(width: 8),
          Text(controller.currentSelectedUser.value.phone),
        ],
      ),
    );
  }

  Widget birthday() {
    return SizedBox(
      height: 32,
      width: 186,
      child: Row(
        children: [
          AutoSpacingText(
            text: "생일",
            width: 44,
          ),
          SizedBox(width: 8),
          Text(controller.currentSelectedUser.value.birthday ?? '-'),
        ],
      ),
    );
  }

  Row email() {
    return Row(
      children: [
        AutoSpacingText(
          text: "이메일",
          width: 44,
        ),
        SizedBox(width: 8),
        Text(controller.currentSelectedUser.value.email ?? "-")
      ],
    );
  }
}
