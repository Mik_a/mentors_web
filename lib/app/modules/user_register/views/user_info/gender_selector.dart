import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mentors_web/global_enums.dart';

class GenderSelector extends StatelessWidget {
  final Gender currentSelectType;
  final Function(Gender) onClickGender;

  const GenderSelector(
      {required this.currentSelectType, required this.onClickGender, Key? key})
      : super(key: key);

  Widget btn(Gender gender) {
    return Container(
      margin: EdgeInsets.all(3),
      width: 54,
      height: 24,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xffc4c4c4),
          width: 1,
        ),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: InkWell(
        onTap: () => onClickGender(gender),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 16,
              height: 16,
              child: SvgPicture.asset(
                gender == Gender.female
                    ? 'assets/icons/female.svg'
                    : 'assets/icons/male.svg',
                color: gender != currentSelectType ? Colors.grey : null,
              ),
            ),
            SizedBox(
              width: 2,
            ),
            Text(
              gender == Gender.female ? "여자" : "남자",
              style: TextStyle(
                fontSize: 12,
                color: gender != currentSelectType ? Colors.grey : null,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        btn(Gender.male),
        btn(Gender.female),
      ],
    );
  }
}
