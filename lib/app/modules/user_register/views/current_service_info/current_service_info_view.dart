import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class CurrentServiceInfoView extends StatelessWidget {
  CurrentServiceInfoView({Key? key}) : super(key: key);

  final UserRegisterController controller = Get.find();

  Widget form(String title, String value) {
    return Container(
      width: 200,
      child: Row(
        children: [
          AutoSpacingText(
            text: title,
            width: 50,
          ),
          SizedBox(width: 12),
          Text(
            value,
            style: TextStyle(
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RegisterCard(
        title: "서비스 이용 정보",
        height: 145,
        width: 460,
        action: SquareButton(
          title: "더보기",
          onClick: controller.onClickDetailVoucherView,
          icon: Icon(
            Icons.add_circle_outline,
            size: 16,
            color: CustomColors.c242424,
          ),
        ),
        child: controller.currentUsingVoucher.value.id == ''
            ? Center(child: Text("현재 좌석을 사용하고 있지 않습니다"))
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      form("이용권",
                          controller.currentUsingVoucher.value.type.toKor()),
                      form("상태", "이용중"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      form(
                          "시작일",
                          getKoreaTime(controller
                              .currentUsingVoucher.value.createdDate!)),
                      form(
                          "종료일",
                          controller.currentUsingVoucher.value.expiredDate ==
                                  null
                              ? ('-')
                              : getKoreaTime(controller
                                  .currentUsingVoucher.value.expiredDate!)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      form(
                          "이용시간",
                          controller.currentUsingVoucher.value.type ==
                                  VoucherUseType.period
                              ? '-'
                              : '${controller.currentUsingVoucher.value.useableTime}시간'),
                      form(
                        (controller.currentUsingVoucher.value.overflowTime !=
                                    null &&
                                controller.currentUsingVoucher.value
                                        .overflowTime! >
                                    0)
                            ? "초과시간"
                            : "잔여시간",
                        controller.currentUsingVoucher.value.type ==
                                VoucherUseType.period
                            ? '-'
                            : (controller.currentUsingVoucher.value
                                            .overflowTime !=
                                        null &&
                                    controller.currentUsingVoucher.value
                                            .overflowTime! >
                                        0)
                                ? ('${controller.currentUsingVoucher.value.overflowTime!}분')
                                : ('${controller.currentUsingVoucher.value.remainTime}분'),
                      ),
                    ],
                  ),
                ],
              ),
      ),
    );
  }
}
