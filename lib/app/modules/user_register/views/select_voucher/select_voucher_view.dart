import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/app/modules/user_register/views/select_voucher/select_voucher_item.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/dropbox_with_title.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/global_enums.dart';

class SelectVoucherView extends StatelessWidget {
  SelectVoucherView({Key? key}) : super(key: key);

  final TextEditingController s = TextEditingController();
  final SeatController seatController = Get.put(SeatController());
  final UserRegisterController controller = Get.put(UserRegisterController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RegisterCard(
        title: "이용권 선택",
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(width: 8),
                DropBoxWithTitle(
                  onChanged: (group) => controller.onChangeSeatType(group),
                  values: [SeatGroup.free, SeatGroup.fixed],
                  textList: [SeatGroup.free.toKor(), SeatGroup.fixed.toKor()],
                  currentValue: controller.currentSelectedSeatType.value,
                  containerWidth: 186,
                  title: "좌석타입",
                  controller: s,
                  titleWidth: 50,
                  inputHeight: 28,
                  inputWidth: double.infinity,
                ),
              ],
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.only(top: 5),
                children: [
                  voucherPanel(
                    "당일권",
                    controller.isDayOpen,
                    controller.dayVouchers,
                  ),
                  voucherPanel(
                    "시간권",
                    controller.isTimeOpen,
                    controller.timeVouchers,
                  ),
                  voucherPanel(
                    "기간권",
                    controller.isPeriodOpen,
                    controller.periodVouchers,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget voucherPanel(String title, RxBool isOpen, List<Voucher> voucherList) {
    return voucherList.length == 0
        ? Container()
        : Container(
            margin: EdgeInsets.symmetric(vertical: 4),
            decoration: BoxDecoration(
              border: Border.all(color: CustomColors.cE3E3E3),
            ),
            child: ExpandablePanel(
              header: Container(
                padding: EdgeInsets.only(left: 12),
                height: 40,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    title,
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              collapsed: Container(),
              expanded: Column(
                children: [
                  ...List.generate(
                    voucherList.length,
                    (index) => SelectVoucherItem(
                      voucher: voucherList[index],
                      onClickItem: controller.onClickVoucher,
                      isSelected: voucherList[index] ==
                          controller.selectedVoucher.value,
                    ),
                  )
                ],
              ),
            ),
          );
  }
}
