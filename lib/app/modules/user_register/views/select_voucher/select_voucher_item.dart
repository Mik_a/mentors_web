import 'package:flutter/material.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class SelectVoucherItem extends StatelessWidget {
  final Voucher voucher;
  final Function(Voucher) onClickItem;
  final bool isSelected;

  const SelectVoucherItem({
    required this.onClickItem,
    required this.voucher,
    this.isSelected = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onClickItem(voucher);
      },
      child: Container(
        decoration: BoxDecoration(
          color: isSelected ? CustomColors.cFFF4C2 : Colors.transparent,
          border: Border(
            top: BorderSide(color: CustomColors.cE3E3E3),
          ),
        ),
        height: 55,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 15.0,
            top: 7,
            bottom: 7,
            right: 12,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(voucher.useableTime.toString() +
                  (voucher.type == VoucherUseType.period ? "일" : "시간")),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      voucher.memo == '' ? "메모가 없습니다" : voucher.memo,
                      style: TextStyle(
                        fontSize: 10,
                        color: CustomColors.c717071,
                      ),
                    ),
                  ),
                  Text(
                    '${addCommaInMoney(voucher.price)}',
                    style: TextStyle(fontSize: 14),
                  ),
                  SizedBox(width: 4),
                  Text(
                    '원',
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
