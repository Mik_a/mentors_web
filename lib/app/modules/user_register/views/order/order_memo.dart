import 'package:flutter/material.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/components/input_box.dart';

class OrderMemo extends StatelessWidget {
  OrderMemo({Key? key}) : super(key: key);
  final UserRegisterController controller = UserRegisterController();

  @override
  Widget build(BuildContext context) {
    return RegisterCard(
      width: double.infinity,
      height: 115,
      title: "주문 메모",
      child: InputBox(
        width: double.infinity,
        height: 50,
        controller: controller.orderMemoController,
      ),
    );
  }
}
