import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/order/order_info.dart';
import 'package:mentors_web/components/custom_button.dart';

class OrderView extends StatelessWidget {
  OrderView({Key? key}) : super(key: key);

  final UserRegisterController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 450,
      child: Column(
        children: [
          OrderInfo(),
          // SizedBox(height: 4),
          // OrderMemo(),
          // SizedBox(height: 4),
          // OrderType(),
          SizedBox(height: 14),
          Obx(() => CustomButton(
                backgroundColor: controller.selectedVoucher.value.id == ''
                    ? Colors.grey
                    : null,
                title: "결제하기",
                width: double.infinity,
                height: 48,
                onPressed: controller.selectedVoucher.value.id == ''
                    ? null
                    : controller.onClickPayment,
              )),
        ],
      ),
    );
  }
}
