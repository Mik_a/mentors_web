import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/dropbox_with_title.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/global_enums.dart';

class OrderInfo extends StatelessWidget {
  OrderInfo({Key? key}) : super(key: key);

  final UserRegisterController userRegisterController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RegisterCard(
        width: double.infinity,
        height: 200,
        title: "주문 정보",
        child: userRegisterController.selectedVoucher.value.id == ''
            ? Center(child: Text("이용권을 선택해주세요"))
            : Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 200,
                        height: 32,
                        child: Row(
                          children: [
                            AutoSpacingText(
                              text: "이용권",
                              width: 45,
                            ),
                            SizedBox(width: 12),
                            Text(
                              userRegisterController.selectedVoucher.value.type
                                  .toKor(),
                            )
                          ],
                        ),
                      ),
                      if (userRegisterController.selectedVoucher.value.type ==
                              VoucherUseType.day ||
                          userRegisterController
                                  .currentSelectedSeatType.value ==
                              SeatGroup.fixed)
                        Container(
                          width: 200,
                          child: Row(
                            children: [
                              userRegisterController.seatList.length == 0
                                  ? Text("좌석이 없습니다")
                                  : DropBoxWithTitle(
                                      titleWidth: 45,
                                      inputWidth: double.infinity,
                                      containerWidth: 186,
                                      title: "좌석",
                                      inputTextStyle: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                      textList: userRegisterController.seatList
                                          .map((element) =>
                                              '${element.sectionName} ${element.index.toString()}번 ${element.isUsing ? "(사용중)" : ""}')
                                          .toList(),
                                      values: userRegisterController.seatList
                                          .map((element) => element)
                                          .toList(),
                                      currentValue: userRegisterController
                                          .selectedSeat.value,
                                      onChanged: (v) {
                                        userRegisterController
                                                .selectedSeat.value =
                                            userRegisterController.seatList
                                                .firstWhere(
                                                    (element) => element == v);
                                      },
                                    )
                            ],
                          ),
                        ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 200,
                        height: 32,
                        child: Row(
                          children: [
                            AutoSpacingText(
                              text: userRegisterController
                                          .selectedVoucher.value.type ==
                                      VoucherUseType.day
                                  ? "시작시간"
                                  : "시작일",
                              width: 45,
                            ),
                            SizedBox(width: 12),
                            Text(userRegisterController.getStartTime())
                          ],
                        ),
                      ),
                      Container(
                        width: 200,
                        child: Row(
                          children: [
                            AutoSpacingText(
                              text: userRegisterController
                                          .selectedVoucher.value.type ==
                                      VoucherUseType.day
                                  ? "종료시간"
                                  : "종료일",
                              width: 45,
                            ),
                            SizedBox(width: 12),
                            Text(userRegisterController.getEndTime())
                          ],
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text("카드"),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              child: SizedBox(
                                width: 70,
                                height: 30,
                                child: InputBox(
                                  textInputType: TextInputType.number,
                                  controller: userRegisterController
                                      .orderCardPriceController,
                                ),
                              ),
                            ),
                            SizedBox(width: 4),
                            Text("원")
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text("현금"),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              child: SizedBox(
                                width: 70,
                                height: 30,
                                child: InputBox(
                                  textInputType: TextInputType.number,
                                  controller: userRegisterController
                                      .orderCashPriceController,
                                ),
                              ),
                            ),
                            SizedBox(width: 4),
                            Text("원")
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
      ),
    );
  }
}
