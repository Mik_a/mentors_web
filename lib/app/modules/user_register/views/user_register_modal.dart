import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/user_info/gender_selector.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/modal.dart';

class UserRegisterModal extends StatelessWidget {
  UserRegisterModal({required this.isModifing, Key? key}) : super(key: key);

  final bool isModifing;
  final UserRegisterController userRegisterController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Modal(
      width: 460,
      height: 490,
      content: Container(
        height: 490,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 44,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(
                    isModifing ? "회원 수정" : "회원 추가",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
            Divider(),
            SizedBox(height: 6),
            Container(
              height: 334,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InputboxWithTitle(
                    title: "이름",
                    titleWidth: 50,
                    containerWidth: 194,
                    inputWidth: 136,
                    hintText: "이름 입력",
                    controller: userRegisterController.nameController,
                  ),
                  Obx(
                    () => Row(
                      children: [
                        AutoSpacingText(
                          text: "성별",
                          width: 50,
                        ),
                        SizedBox(width: 20),
                        GenderSelector(
                          currentSelectType:
                              userRegisterController.gender.value,
                          onClickGender: userRegisterController.setGender,
                        ),
                      ],
                    ),
                  ),
                  InputboxWithTitle(
                    title: "이메일",
                    titleWidth: 50,
                    containerWidth: 194,
                    inputWidth: 136,
                    hintText: "이메일 주소 입력",
                    controller: userRegisterController.emailController,
                  ),
                  InputboxWithTitle(
                    title: "생일",
                    titleWidth: 50,
                    containerWidth: 194,
                    inputWidth: 136,
                    hintText: "ex) 19900101",
                    controller: userRegisterController.birthdayController,
                  ),
                  Row(
                    children: [
                      InputboxWithTitle(
                        title: "연락처",
                        titleWidth: 50,
                        containerWidth: 194,
                        inputWidth: 136,
                        hintText: "연락처 입력",
                        controller: userRegisterController.phoneController,
                      ),
                      SizedBox(width: 40),
                      Expanded(
                        child: InputboxWithTitle(
                          title: "보호자",
                          titleWidth: 50,
                          containerWidth: 194,
                          inputWidth: double.infinity,
                          hintText: "연락처 입력",
                          controller:
                              userRegisterController.parentPhoneController,
                        ),
                      ),
                    ],
                  ),
                  InputboxWithTitle(
                    title: "키오스크",
                    titleWidth: 50,
                    containerWidth: 194,
                    inputWidth: 136,
                    hintText: "4자리 숫자 입력",
                    controller: userRegisterController.kioskController,
                  ),
                  InputboxWithTitle(
                    title: "메모",
                    titleWidth: 50,
                    inputHeight: 94,
                    textInputType: TextInputType.multiline,
                    containerWidth: double.infinity,
                    inputWidth: double.infinity,
                    hintText: "간단한 메모 입력",
                    controller: userRegisterController.memoController,
                  ),
                ],
              ),
            ),
            Expanded(child: SizedBox()),
            Padding(
              padding: const EdgeInsets.only(bottom: 24.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomButton(
                    title: "취소",
                    width: 120,
                    type: CustomButtonType.cancle,
                    onPressed: () {
                      if (!isModifing)
                        userRegisterController.resetControllers();
                      Navigator.of(context).pop();
                    },
                  ),
                  SizedBox(width: 8),
                  CustomButton(
                    title: isModifing ? "수정" : "저장",
                    width: 120,
                    type: CustomButtonType.confirm,
                    onPressed: () async {
                      if (isModifing) {
                        if (await userRegisterController
                            .onClickModifySubmit()) {
                          Navigator.of(context).pop();
                        }
                      } else {
                        if (await userRegisterController.onClickRegister()) {
                          Navigator.of(context).pop();
                        }
                      }
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
