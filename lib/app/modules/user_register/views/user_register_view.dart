import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/views/current_service_info/current_service_info_view.dart';
import 'package:mentors_web/app/modules/user_register/views/order/order_view.dart';
import 'package:mentors_web/app/modules/user_register/views/select_voucher/select_voucher_view.dart';
import 'package:mentors_web/app/modules/user_register/views/user_info/user_info_view.dart';
import 'package:mentors_web/app/modules/user_register/views/user_log/user_log_view.dart';
import '../controllers/user_register_controller.dart';

class UserRegisterView extends StatelessWidget {
  final UserRegisterController controller = Get.put(UserRegisterController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UserInfoView(controller: controller),
                  SizedBox(height: 15),
                  CurrentServiceInfoView(),
                  SizedBox(height: 15),
                  UserLogView(),
                ],
              ),
              SizedBox(width: 14),
              Expanded(child: SelectVoucherView()),
              SizedBox(width: 14),
              OrderView(),
            ],
          ),
        ),
      ),
    );
  }
}
