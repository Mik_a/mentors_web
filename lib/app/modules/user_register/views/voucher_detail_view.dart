import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/datepicker.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';

class VoucherDetailView extends StatefulWidget {
  final String userId;
  final String? currentUsingVoucher;

  const VoucherDetailView({
    required this.currentUsingVoucher,
    required this.userId,
    Key? key,
  }) : super(key: key);

  @override
  _VoucherDetailViewState createState() => _VoucherDetailViewState();
}

class _VoucherDetailViewState extends State<VoucherDetailView> {
  List<Voucher> voucherList = [];
  Voucher? currentSelectedVoucher;
  bool isEditMode = false;
  bool isLoading = true;

  TextEditingController expiredDateController = TextEditingController();
  TextEditingController expiredDateHourController = TextEditingController();
  TextEditingController expiredDateMinController = TextEditingController();
  TextEditingController remainHourController = TextEditingController();
  TextEditingController remainMinController = TextEditingController();

  @override
  void initState() {
    super.initState();
    API.user.getUserVouchers(widget.userId).then(
          (value) => setState(
            () {
              voucherList = value.reversed.toList();
              setState(() {
                isLoading = false;
                if (voucherList.length > 0) {
                  onItemClick(voucherList[0]);
                }
              });
            },
          ),
        );
  }

  void onItemClick(Voucher voucher) {
    setState(() {
      currentSelectedVoucher = voucher;
      isEditMode = false;
    });
  }

  void onClickEditMode() async {
    if (isEditMode) {
      final split = expiredDateController.text
          .split('-')
          .map((e) => int.parse(e))
          .toList();

      DateTime time = DateTime(
        split[0],
        split[1],
        split[2],
        int.parse(expiredDateHourController.text),
        int.parse(expiredDateMinController.text),
      );

      int remainTime = (int.parse(remainHourController.text) * 60) +
          int.parse(remainMinController.text);

      final value = await API.voucher.setVoucherData(
        userId: widget.userId,
        remainTIme: remainTime,
        datetime: time,
        voucherId: currentSelectedVoucher!.id,
        voucherType: currentSelectedVoucher!.type,
      );

      if (value) {
        print(value);
        EasyLoading.showSuccess("수정되었습니다");
        currentSelectedVoucher!.expiredDate =
            time.subtract(Duration(hours: time.timeZoneName == "GMT" ? 9 : 0));
        currentSelectedVoucher!.remainTime = remainTime;
        setState(() {
          isEditMode = false;
        });
      } else {}
    } else {
      setState(() {
        expiredDateController.text =
            getKoreaTime(currentSelectedVoucher!.expiredDate!).substring(0, 10);
        expiredDateHourController.text =
            (currentSelectedVoucher!.expiredDate!.hour +
                    (currentSelectedVoucher!.expiredDate!.timeZoneName == "GMT"
                        ? 9
                        : 0))
                .toString();
        expiredDateMinController.text =
            currentSelectedVoucher!.expiredDate!.minute.toString();
        remainHourController.text =
            (currentSelectedVoucher!.remainTime! ~/ 60).toString();
        remainMinController.text =
            (currentSelectedVoucher!.remainTime! % 60).toString();
        isEditMode = true;
      });
    }
  }

  void onClickRefund() {
    Get.dialog(
      Modal(
        content: RegisterCard(
          width: 600,
          height: 300,
          title: "환불",
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bool isEnd = currentSelectedVoucher == null
        ? false
        : currentSelectedVoucher!.remainTime! == 0 ||
                currentSelectedVoucher!.expiredDate!.compareTo(DateTime.now()) <
                    0
            ? true
            : false;

    return Align(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RegisterCard(
            title: "이용권 목록",
            width: 432,
            height: 750,
            child: isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : voucherList.length == 0
                    ? Center(child: Text("보유한 이용권이 없습니다"))
                    : ListView.separated(
                        itemBuilder: (context, index) => voucherItem(
                            voucherList[index],
                            currentSelectedVoucher == voucherList[index]),
                        separatorBuilder: (context, index) =>
                            Divider(height: 1),
                        itemCount: voucherList.length,
                      ),
          ),
          SizedBox(width: 8),
          RegisterCard(
            title: "서비스 이용 정보",
            width: 460,
            height: 750,
            action: currentSelectedVoucher == null
                ? Container()
                : isEnd
                    ? Container()
                    : Row(
                        children: [
                          // if (!isEditMode)
                          //   SquareButton(
                          //     icon: SvgPicture.asset('assets/icons/money.svg'),
                          //     title: "환불",
                          //     onClick: onClickRefund,
                          //   ),
                          SizedBox(width: 8),
                          SquareButton(
                            icon: SvgPicture.asset('assets/icons/edit.svg'),
                            title: isEditMode ? "완료" : "수정",
                            onClick: onClickEditMode,
                          ),
                        ],
                      ),
            child: currentSelectedVoucher == null
                ? Center(child: Text('이용권을 선택해주세요'))
                : Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 412,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                box(
                                  "이용권",
                                  Text(
                                      '${currentSelectedVoucher!.group} - ${currentSelectedVoucher!.type.toKor()}'),
                                ),
                                box(
                                  "상태",
                                  Text(
                                    widget.currentUsingVoucher ==
                                            currentSelectedVoucher!.id
                                        ? "사용중"
                                        : isEnd
                                            ? "사용종료"
                                            : "사용가능",
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                box(
                                  "시작일",
                                  Text(
                                    getKoreaTime(
                                      currentSelectedVoucher!.createdDate!,
                                    ).substring(0, 16),
                                  ),
                                ),
                                if (currentSelectedVoucher!.type !=
                                    VoucherUseType.day)
                                  box(
                                    "종료일",
                                    isEditMode
                                        ? DatePicker(
                                            controller: expiredDateController,
                                            firstDate: DateTime.now(),
                                          )
                                        : Text(getKoreaTime(
                                                currentSelectedVoucher!
                                                    .expiredDate!)
                                            .substring(0, 16)),
                                  ),
                              ],
                            ),
                            if (isEditMode &&
                                currentSelectedVoucher!.type !=
                                    VoucherUseType.day)
                              SizedBox(
                                height: 50,
                                child: Row(
                                  children: [
                                    box("", Container()),
                                    box(
                                      "",
                                      Row(
                                        children: [
                                          InputBox(
                                            width: 40,
                                            controller:
                                                expiredDateHourController,
                                          ),
                                          SizedBox(width: 4),
                                          Text("시"),
                                          SizedBox(width: 16),
                                          InputBox(
                                            width: 40,
                                            controller:
                                                expiredDateMinController,
                                          ),
                                          SizedBox(width: 4),
                                          Text("분")
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            Row(
                              children: [
                                box(
                                  "이용시간",
                                  Text(currentSelectedVoucher!.type ==
                                          VoucherUseType.period
                                      ? "-"
                                      : '${currentSelectedVoucher!.useableTime} 시간'),
                                ),
                                box(
                                  "잔여시간",
                                  currentSelectedVoucher!.type ==
                                          VoucherUseType.period
                                      ? Text("-")
                                      : isEditMode
                                          ? Row(
                                              children: [
                                                InputBox(
                                                  width: 40,
                                                  textInputType:
                                                      TextInputType.number,
                                                  controller:
                                                      remainHourController,
                                                ),
                                                SizedBox(width: 4),
                                                Text("시간"),
                                                SizedBox(width: 4),
                                                InputBox(
                                                  width: 40,
                                                  textInputType:
                                                      TextInputType.number,
                                                  controller:
                                                      remainMinController,
                                                ),
                                                SizedBox(width: 4),
                                                Text("분"),
                                              ],
                                            )
                                          : Text(
                                              '${currentSelectedVoucher!.remainTime! ~/ 60} 시간 ${currentSelectedVoucher!.remainTime! % 60} 분'),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 28),
                      Container(
                        width: 428,
                        height: 32,
                        decoration: BoxDecoration(
                          color: CustomColors.cf5f5f5,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        child: Center(
                          child: Text(
                            "이용권 상세내역",
                            style: TextStyle(
                              fontSize: 12,
                              color: CustomColors.cA3A2A3,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView(
                          children: [
                            ...List.generate(
                              currentSelectedVoucher!.logs.length,
                              (index) {
                                return voucherLogItem(
                                    currentSelectedVoucher!.logs[index]);
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  Container voucherLogItem(VoucherLog log) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(log.buyType.name),
          box(
            "결제일",
            Text(
              getKoreaTime(
                log.eventTime,
              ).substring(0, 16),
            ),
          ),
          box(
            "결제금액",
            Text(
              '${log.price} 원',
            ),
          ),
        ],
      ),
    );
  }

  Container box(String title, Widget widget) {
    return Container(
      width: 200,
      height: 32,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AutoSpacingText(width: 44, text: title),
          SizedBox(width: 20),
          Expanded(
            child: SizedBox(
              height: 32,
              child: Align(
                alignment: Alignment.centerLeft,
                child: widget,
              ),
            ),
          ),
        ],
      ),
    );
  }

  InkWell voucherItem(Voucher voucher, bool isSelected) {
    Color color = widget.currentUsingVoucher == voucher.id
        ? Color(0xff00a6a6)
        : voucher.remainTime! == 0 ||
                voucher.expiredDate!.compareTo(DateTime.now()) < 0
            ? Color(0xff8a888a)
            : Color(0xffECA946);

    Color backGroundColor = widget.currentUsingVoucher == voucher.id
        ? Color(0xff88f2f2).withOpacity(0.2)
        : voucher.remainTime! == 0 ||
                voucher.expiredDate!.compareTo(DateTime.now()) < 0
            ? Color(0xff8a888a).withOpacity(0)
            : Color(0xffffe0b0).withOpacity(0.2);

    voucher.remainTime! == 0 ? Color(0xff8a888a) : Color(0xff00a6a6);
    String title = widget.currentUsingVoucher == voucher.id
        ? "사용중"
        : voucher.remainTime! == 0 ||
                voucher.expiredDate!.compareTo(DateTime.now()) < 0
            ? "사용종료"
            : "사용가능";

    return InkWell(
      onTap: () {
        onItemClick(voucher);
      },
      child: Container(
        height: 50,
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
        color: isSelected ? CustomColors.cFFF4C2 : Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${voucher.group} - ${voucher.type.toKor()}'),
                SizedBox(height: 6),
                Text(
                  getKoreaTime(voucher.createdDate!).substring(0, 16),
                  style: TextStyle(
                    color: CustomColors.cA3A2A3,
                    fontSize: 12,
                  ),
                )
              ],
            ),
            Container(
              width: 55,
              height: 24,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                color: backGroundColor,
                border: Border.all(
                  color: color,
                ),
              ),
              child: Center(
                child: Text(
                  title,
                  style: TextStyle(
                    color: color,
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
