import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mentors_web/mobile/mobile_drawer.dart';

import '../controllers/mobile_seat_controller.dart';

class MobileSeatView extends GetView<MobileSeatController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          "좌석",
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      drawer: MobileDrawer(),
      body: Center(
        child: Text(
          'MobileSeatView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
