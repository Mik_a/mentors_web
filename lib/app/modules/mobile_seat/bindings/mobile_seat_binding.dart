import 'package:get/get.dart';

import '../controllers/mobile_seat_controller.dart';

class MobileSeatBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MobileSeatController>(
      () => MobileSeatController(),
    );
  }
}
