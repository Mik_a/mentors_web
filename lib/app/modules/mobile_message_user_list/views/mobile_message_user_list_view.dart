import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/custom_checkbox.dart';
import 'package:mentors_web/components/search_input.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_enums.dart';

import '../controllers/mobile_message_user_list_controller.dart';

class MobileMessageUserListView
    extends GetView<MobileMessageUserListController> {
  final UserController userController = Get.find();
  final MessageController messageController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      var header = Container(
        height: 36,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 20,
              height: 20,
              child: FittedBox(
                child: CustomCheckbox(
                  value: controller.isOnAllUsers(),
                  onChange: controller.onClickAllUser,
                ),
              ),
            ),
            SizedBox(
              width: 80,
              child: Text(
                "No.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              width: 80,
              child: Text(
                "이름",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              width: 140,
              child: Text(
                "이름",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              "상태",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );

      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          title: Text('회원목록'),
          elevation: 1,
          centerTitle: true,
        ),
        resizeToAvoidBottomInset: false,
        body: Column(
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: SizedBox(
                        height: 48,
                        child: Row(
                          children: [
                            Expanded(
                              child: SearchInput(
                                isMobile: true,
                                onChange: (t) {
                                  controller.searchText.value = t;
                                },
                              ),
                            ),
                            // SizedBox(width: 16),
                            // InkWell(
                            //   onTap: () => controller.onClickFilter(context),
                            //   child: Container(
                            //     width: 58,
                            //     height: 48,
                            //     decoration: BoxDecoration(
                            //       borderRadius: BorderRadius.circular(8),
                            //       color: Colors.grey[200],
                            //     ),
                            //     child: Center(
                            //       child: SvgPicture.asset(
                            //           'assets/icons/filter.svg'),
                            //     ),
                            //   ),
                            // )
                          ],
                        ),
                      ),
                    ),
                    header,
                    Divider(
                      height: 2,
                      thickness: 2,
                    ),
                    Expanded(
                      child: ListView(
                        children: [
                          ...List.generate(
                            controller.users.length,
                            (index) => item(controller.users[index], index + 1),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // Container(
            //   height: 68,
            //   width: double.infinity,
            //   color: messageController.selectedUserList.length == 0
            //       ? Color(0xffCFCFD0)
            //       : Color(0xff19a980),
            //   child: Center(
            //     child: Text(
            //       "${messageController.selectedUserList.length}명 추가하기",
            //       style: TextStyle(
            //         fontWeight: FontWeight.bold,
            //         fontSize: 21,
            //         color: Colors.white,
            //       ),
            //     ),
            //   ),
            // )
            // CustomButton(
            //   width: double.infinity,
            //   height: 68
            //   title: "추가하기",
            // )
          ],
        ),
      );
    });
  }

  Container item(User user, int index) {
    return Container(
      height: 23,
      margin: EdgeInsets.symmetric(vertical: 6.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: FittedBox(
              child: CustomCheckbox(
                value: messageController.isUserSelected(user),
                onChange: (_) {
                  messageController.onCheckUser(user);
                },
              ),
            ),
          ),
          SizedBox(
            width: 80,
            child: Text(
              index.toString(),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: 80,
            child: Text(
              user.name,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: 140,
            child: Text(
              user.phone,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            child: Text(
              user.status.toKor(),
            ),
          ),
        ],
      ),
    );
  }
}
