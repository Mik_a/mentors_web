import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/mobile_message_user_list/views/mobile_message_bottom_sheet_bar.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';

class MobileMessageUserListController extends GetxController {
  RxString searchText = "".obs;
  RxBool isAllSelcted = false.obs;
  final UserController userController = Get.find();
  final MessageController messageController = Get.find();

  @override
  void onInit() {
    super.onInit();
    searchText.value = '';
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    searchText.value = '';
  }

  void onClickFilter(BuildContext context) {
    FocusScope.of(context).unfocus();

    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) {
        return MobileMessageBottomSheetBar();
      },
    );
  }

  void onClickAllUser(bool v) {
    !v
        ? messageController.removeThisUsers(users)
        : messageController.onClickAllUsers(users);
  }

  bool isOnAllUsers() {
    for (var i = 0; i < users.length; i++) {
      if (messageController.selectedUserList.contains(users[i])) {
        continue;
      } else {
        return false;
      }
    }
    return true;
  }

  List<User> get users => userController.userList
      .where(
        (p0) => p0.name.contains(searchText) || p0.phone.contains(searchText),
      )
      .toList();
}
