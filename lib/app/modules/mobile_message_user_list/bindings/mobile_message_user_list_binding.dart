import 'package:get/get.dart';

import '../controllers/mobile_message_user_list_controller.dart';

class MobileMessageUserListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MobileMessageUserListController>(
      () => MobileMessageUserListController(),
    );
  }
}
