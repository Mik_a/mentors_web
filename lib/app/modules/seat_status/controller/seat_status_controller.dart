import 'package:get/get.dart';
import 'package:mentors_web/classes/section.dart';

class SeatStatusController extends GetxController {
  Rx<Section> selectedSection = Section(id: '', name: '').obs;

  void onClickSection(Section section) {
    if (section != selectedSection.value) selectedSection.value = section;
  }
}
