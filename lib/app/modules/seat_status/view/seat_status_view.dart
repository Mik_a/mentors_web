import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/app/modules/seat_status/controller/seat_status_controller.dart';
import 'package:mentors_web/components/seat_arrangement/seat_arrangement_image.dart';
import 'package:mentors_web/components/section_list/section_list.dart';

class SeatStatusView extends StatelessWidget {
  SeatStatusView({Key? key}) : super(key: key);
  final controller = Get.put(SeatArrangementController());
  final seatController = Get.put(SeatStatusController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        children: [
          Expanded(
            child: SeatArrangementImage(),
          ),
          SectionList(
            axis: Axis.vertical,
            selectedSection: seatController.selectedSection.value,
            onClickSection: seatController.onClickSection,
          ),
        ],
      ),
    );
  }
}
