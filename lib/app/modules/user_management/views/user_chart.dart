import 'package:flutter/material.dart';
import 'package:mentors_web/components/bar_chart.dart';

class UserChart extends StatelessWidget {
  const UserChart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: Card(
          child: CustomBarChart(
            maxValue: 100,
            titles: ["월", "화", "수", "목", "금", "토", "일"],
            values: [1, 2, 3, 4, 5, 6, 7],
          ),
        ),
      ),
    );
  }
}
