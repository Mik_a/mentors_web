import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_management/controllers/user_management_controller.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/global_enums.dart';

class UserList extends StatelessWidget {
  final UserManagementController controller =
      Get.put(UserManagementController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        children: [
          Container(
            height: 500,
            child: PagenationListView(
              isLoading: controller.userController.isLoading.value,
              itemPerPage: controller.itemPerPage.value,
              currentPage: controller.currentpage.value,
              data: controller.userController.userList
                  .map(
                    (element) => [
                      Text(element.name),
                      Text(element.phone),
                      Text(element.gender.toKor()),
                      Text(element.createdDate.toString()),
                    ],
                  )
                  .toList(),
              headerTitles: [
                Text('이름'),
                Text('전화번호'),
                Text("성별"),
                Text("가입일"),
              ],
            ),
          ),
          Pagenation(
            currentPage: controller.currentpage.value,
            itemPerPage: controller.itemPerPage.value,
            totalLength: controller.userController.userList.length,
            setPage: controller.setPage,
          ),
        ],
      ),
    );
  }
}
