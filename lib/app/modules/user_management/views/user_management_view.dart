import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_management/views/user_chart.dart';
import 'package:mentors_web/app/modules/user_management/views/user_list.dart';
import 'package:mentors_web/app/modules/user_management/views/user_total.dart';
import '../controllers/user_management_controller.dart';

class UserManagementView extends StatelessWidget {
  final controller = Get.put(UserManagementController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(border: Border.all()),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  UserTotal(controller: controller),
                  UserChart(),
                ],
              ),
            ),
          ),
          UserList(),
        ],
      ),
    );
  }
}
