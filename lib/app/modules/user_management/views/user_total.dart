import 'package:flutter/material.dart';
import 'package:mentors_web/app/modules/user_management/controllers/user_management_controller.dart';

class UserTotal extends StatelessWidget {
  final UserManagementController controller;
  const UserTotal({required this.controller});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              children: [
                Text("총 고객"),
                Text(
                  "${controller.userController.userList.length} 명",
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Text("기간 내 신규 가입"),
                Text(
                  "${controller.userController.userList.length} 명",
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          )
        ],
      )),
    );
  }
}
