import 'package:get/get.dart';
import 'package:mentors_web/controller/user_controller.dart';

class UserManagementController extends GetxController {
  UserController userController = Get.put(UserController());
  RxInt currentpage = 0.obs;
  RxInt itemPerPage = 15.obs;

  void setPage(int i) {
    currentpage.value = i;
  }
}
