import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/message/message_remain_view.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/modules/lib.dart';

class MobileMessageHelpModal extends StatelessWidget {
  MobileMessageHelpModal({Key? key}) : super(key: key);

  final MessageController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 40),
        height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "발송 가능 건수",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
            Container(
              height: 70,
              child: Column(
                children: [
                  RichText(
                    text: TextSpan(
                      text: "현재",
                      style: TextStyle(
                        color: Color(0xff242424),
                      ),
                      children: [
                        TextSpan(
                          text:
                              " ${addCommaInMoney(controller.smsData.value.cash)} ",
                          style: TextStyle(
                            color: Color(0xff242424),
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: "캐시를 보유중이며",
                          style: TextStyle(
                            color: Color(0xff242424),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 6),
                  Text("사용할 수 있는 건수는 아래와 같습니다."),
                  SizedBox(height: 12),
                  Text(
                    "(메세지 종류에 따라 최대로 사용할 수 있는 건수입니다)",
                    style: TextStyle(
                      fontWeight: FontWeight.w100,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
            MessageRemainView(
              isShowCash: false,
            )
          ],
        ));
  }
}
