import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/mobile/mobile_drawer.dart';
import 'package:mentors_web/modules/lib.dart';

import '../../../../controller/message_controller.dart';

class MobileMessageView extends GetView<MessageController> {
  final MessageController messageController = Get.put(MessageController());
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 1,
        foregroundColor: Colors.black,
        title: Text(
          '메세지',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      drawer: MobileDrawer(),
      body: Obx(() {
        var gotoUserListButton = CustomButton(
          onPressed: () => {
            FocusScope.of(context).unfocus(),
            Get.toNamed(
              Routes.MOBILE_MESSAGE_USER_LIST,
              arguments: messageController.selectedUserList,
            )
          },
          backgroundColor: Color(0xff19a980),
          title: "회원목록",
          width: 92,
          height: 32,
        );
        return Padding(
          padding: const EdgeInsets.all(16.0),
          child: ListView(
            children: [
              form(
                  "캐시 ${addCommaInMoney(controller.smsData.value.cash)} 원",
                  Container(
                    height: 44,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              FocusScope.of(context).unfocus();

                              Get.toNamed(Routes.MOBILE_MESSAGE_HITORY);
                            },
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset('assets/icons/history.svg'),
                                  SizedBox(width: 10),
                                  Text(
                                    "내역",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 2,
                          height: 16,
                          color: Color(0xffd7d7d7),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              FocusScope.of(context).unfocus();
                              messageController.onClickHelp();
                            },
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset('assets/icons/letter.svg'),
                                  SizedBox(width: 10),
                                  Text(
                                    "발송 가능 건수",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
              form(
                "받는 회원",
                Container(
                  height: messageController.selectedUserList.length == 0 ||
                          messageController.sendType.value ==
                              MessageSendType.self
                      ? 110
                      : 350,
                  child: Column(
                    children: [
                      Container(
                        height: 36,
                        child: Row(
                          children: [
                            tab("회원 목록 선택", MessageSendType.select),
                            Container(
                              width: 2,
                              color: Color(0xffe9e9e9),
                            ),
                            tab("직접 입력", MessageSendType.self)
                          ],
                        ),
                      ),
                      messageController.sendType.value == MessageSendType.select
                          ? Expanded(
                              child: messageController
                                          .selectedUserList.length ==
                                      0
                                  ? Container(
                                      child: Center(
                                        child: gotoUserListButton,
                                      ),
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(16),
                                      child: Column(
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Scrollbar(
                                                controller: _scrollController,
                                                isAlwaysShown: true,
                                                child: ListView.separated(
                                                  controller: _scrollController,
                                                  itemBuilder: (context,
                                                          index) =>
                                                      userItem(messageController
                                                              .selectedUserList[
                                                          index]),
                                                  separatorBuilder:
                                                      (context, index) =>
                                                          Divider(),
                                                  itemCount: messageController
                                                      .selectedUserList.length,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              gotoUserListButton,
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                            )
                          : Padding(
                              padding: const EdgeInsets.all(14.0),
                              child: InputBox(
                                width: double.infinity,
                                height: 45,
                                hintText: '전화번호는 쉼표로 구분해주세요',
                                controller:
                                    messageController.phoneListController,
                              ),
                            ),
                    ],
                  ),
                ),
              ),
              form(
                "메세지 작성",
                Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            height: 224,
                            width: 190,
                            padding: EdgeInsets.all(14),
                            child: InputBox(
                              controller:
                                  messageController.smsContentController,
                              textInputType: TextInputType.multiline,
                              hintText:
                                  "내용을 입력해주세요.\n* 메세지 내용에 %고객명%을 입력하면 발송목록의 고객명으로 변환되어 발송됩니다.",
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            children: [
                              data("보유 캐시",
                                  '${addCommaInMoney(messageController.smsData.value.cash)} 캐시'),
                              data("메세지 유형", messageController.messageType),
                              data("메세지 금액",
                                  '${messageController.messagePrice}'),
                              data("발송건수", '0건'),
                              data("발송금액", '0원'),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CustomButton(
                        onPressed: messageController.sendMessage,
                        height: 44,
                        width: double.infinity,
                        title:
                            "${messageController.getNumberOfSendUser()}건 문자 발송",
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Container userItem(User user) {
    return Container(
      height: 24,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            user.name,
            style: TextStyle(),
          ),
          Text(
            user.phone,
            style: TextStyle(),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: InkWell(
              onTap: () => messageController.onCheckUser(user),
              child: Icon(
                Icons.close,
                color: Color(0xff8a888a),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget tab(String title, MessageSendType type) {
    return Expanded(
      child: InkWell(
        onTap: () => messageController.onChangeSendType(type, null),
        child: Container(
          height: 36,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: type == messageController.sendType.value
                    ? Colors.transparent
                    : Color(0xffe9e9e9),
                width: 1,
              ),
            ),
            color: type != messageController.sendType.value
                ? Color(0xffF4F4F4)
                : Colors.transparent,
          ),
          child: Center(
            child: Text(
              title,
              style: TextStyle(color: Color(0xff242424)),
            ),
          ),
        ),
      ),
    );
  }

  Container data(String title, String context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyle(
                fontSize: 12,
                color: Color(0xff6b6b6b),
              ),
            ),
          ),
          SizedBox(height: 2),
          Text(
            context,
            style: TextStyle(fontSize: 14, color: Color(0xff242424)),
          ),
        ],
      ),
    );
  }

  Card form(String title, Widget child) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            SizedBox(
              height: 44,
              child: Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              height: 1,
              thickness: 1,
            ),
            child,
          ],
        ),
      ),
    );
  }
}
