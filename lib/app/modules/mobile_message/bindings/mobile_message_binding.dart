import 'package:get/get.dart';

import '../../../../controller/message_controller.dart';

class MobileMessageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MessageController>(
      () => MessageController(),
    );
  }
}
