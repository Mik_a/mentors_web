import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/components/radio_button_filed.dart';
import 'package:mentors_web/components/square_button.dart';
import 'package:mentors_web/controller/excel_controller.dart';
import 'package:mentors_web/controller/store_management_controller.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_enums.dart';

class UserListPageView extends StatefulWidget {
  UserListPageView({Key? key}) : super(key: key);

  @override
  State<UserListPageView> createState() => _UserListPageViewState();
}

class _UserListPageViewState extends State<UserListPageView> {
  final UserController controller = Get.find();

  List<User> users = [];

  int page = 0;
  String name = '';
  String email = '';
  String phone = '';
  String birth = '';

  Gender gender = Gender.none;

  void onClickDownloadExcel() {
    ExcelController().downLoadExcelFile(
      fileName:
          "${Get.find<StoreManagementController>().storeInfo.value.storeName} - 회원 목록",
      headers: ["이름", "이메일", "휴대폰", "성별", "생년월일", "이용상태", "가입상태"],
      data: controller.userList
          .map((element) => [
                element.name,
                element.email ?? '-',
                element.phone,
                element.gender.toKor(),
                element.birthday ?? '-',
                element.status.toKor(),
                "정상",
              ])
          .toList(),
    );
  }

  TextEditingController c1 = TextEditingController();
  TextEditingController c2 = TextEditingController();
  TextEditingController c3 = TextEditingController();
  TextEditingController c4 = TextEditingController();

  void searchInit() {
    setState(() {
      c1.text = '';
      c2.text = '';
      c3.text = '';
      c4.text = '';

      name = '';
      email = '';
      phone = '';
      birth = '';

      gender = Gender.none;
    });
  }

  @override
  Widget build(BuildContext context) {
    var searchUser = Container(
      height: 116,
      padding: const EdgeInsets.only(left: 16, right: 16),
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 6),
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xffC4C4C4),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
                // top: 18,s
                ),
            height: 44,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "회원 검색",
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Color(0xff242424),
                  ),
                ),
                SquareButton(
                  height: 28,
                  onClick: searchInit,
                  icon: SvgPicture.asset('assets/icons/refresh.svg'),
                  title: "검색초기화",
                )
              ],
            ),
          ),
          Divider(height: 0),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InputboxWithTitle(
                  hintText: "회원이름 입력",
                  containerWidth: 186,
                  inputHeight: 32,
                  inputWidth: 136,
                  onChanged: (_) {
                    setState(() {
                      name = _;
                      page = 0;
                    });
                  },
                  titleWidth: 42,
                  title: "이름",
                  controller: c1,
                ),
                InputboxWithTitle(
                  hintText: "이메일 입력",
                  containerWidth: 186,
                  inputHeight: 32,
                  inputWidth: 136,
                  onChanged: (_) {
                    setState(() {
                      email = _;
                      page = 0;
                    });
                  },
                  titleWidth: 42,
                  title: "이메일",
                  controller: c2,
                ),
                InputboxWithTitle(
                  hintText: "연락처 입력",
                  containerWidth: 190,
                  inputHeight: 32,
                  inputWidth: 136,
                  onChanged: (_) {
                    setState(() {
                      phone = _;
                      page = 0;
                    });
                  },
                  titleWidth: 46,
                  title: "전화번호",
                  controller: c3,
                ),
                RadioButtonField(
                  currentValue: gender,
                  title: "성별",
                  onChange: (_) {
                    setState(() {
                      if (gender == _)
                        gender = Gender.none;
                      else
                        gender = _;
                    });
                  },
                  titles: [
                    "여자",
                    "남자",
                  ],
                  values: [Gender.female, Gender.male],
                ),
                InputboxWithTitle(
                  hintText: "생년월일 입력",
                  containerWidth: 190,
                  inputHeight: 32,
                  inputWidth: 136,
                  onChanged: (_) {
                    setState(() {
                      birth = _;
                      page = 0;
                    });
                  },
                  titleWidth: 46,
                  title: "생년월일",
                  controller: c4,
                ),
              ],
            ),
          )
        ],
      ),
    );

    List<Widget> returnUserItem(User user) {
      return [
        Text(
          user.name,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        Text(
          user.email ?? '-',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        Text(
          user.phone,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        Text(
          user.gender.toKor(),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        Text(
          user.birthday ?? "-",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        Text(
          user.status.toKor(),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        Text(
          '정상',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xff242424),
          ),
        ),
        InkWell(
          onTap: () {
            Get.find<UserRegisterController>().toDetailView(user);
          },
          child: Container(
            width: 56,
            height: 24,
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xffc4c4c4),
              ),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Center(
              child: Text(
                '상세보기',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xff242424),
                ),
              ),
            ),
          ),
        ),
      ];
    }

    return Obx(() {
      users = controller.userList
          .where(
            (p0) => p0.name.contains(name) && p0.phone.contains(phone),
          )
          .where((element) => email == ''
              ? true
              : element.email == null
                  ? false
                  : element.email!.contains(email))
          .where((element) => birth == ''
              ? true
              : element.birthday == null
                  ? false
                  : element.birthday!.contains(birth))
          .where((element) =>
              gender == Gender.none ? true : element.gender == gender)
          .toList();

      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: RegisterCard(
          action: SquareButton(
            height: 28,
            onClick: onClickDownloadExcel,
            icon: Image.asset('assets/icons/excel.png'),
            title: "엑셀 다운로드",
          ),
          title: "회원 목록",
          child: Column(
            children: [
              searchUser,
              Expanded(
                child: PagenationListView(
                  onClick: null,
                  currentPage: page,
                  itemPerPage: 20,
                  itemHeight: 32,
                  data: [
                    ...List.generate(
                      users.length,
                      (index) => [
                        Text("${index + 1}"),
                        ...returnUserItem(users[index]),
                      ],
                    )
                  ],
                  headerWidths: [60, 80, 200, 100, 40, 80, 60, 60, 60],
                  headerTitles: [
                    Text("No."),
                    Text("이름"),
                    Text("이메일"),
                    Text("휴대폰"),
                    Text("성별"),
                    Text("생년월일"),
                    Text("이용상태"),
                    Text("가입상태"),
                    Text("관리"),
                  ],
                ),
              ),
              Pagenation(
                height: 40,
                currentPage: page,
                totalLength: users.length,
                itemPerPage: 20,
                setPage: (page) {
                  setState(() {
                    this.page = page;
                  });
                },
              ),
              SizedBox(height: 10)
            ],
          ),
        ),
      );
    });
  }
}
