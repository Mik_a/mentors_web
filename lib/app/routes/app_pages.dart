import 'package:get/get.dart';

import 'package:mentors_web/app/modules/auth/bindings/auth_binding.dart';
import 'package:mentors_web/app/modules/auth/views/auth_view.dart';
import 'package:mentors_web/app/modules/home/bindings/home_binding.dart';
import 'package:mentors_web/app/modules/home/views/home_view.dart';
import 'package:mentors_web/app/modules/login/bindings/login_binding.dart';
import 'package:mentors_web/app/modules/login/views/login_view.dart';
import 'package:mentors_web/app/modules/message_template/bindings/message_template_binding.dart';
import 'package:mentors_web/app/modules/message_template/views/message_template_view.dart';
import 'package:mentors_web/app/modules/mobile_message/bindings/mobile_message_binding.dart';
import 'package:mentors_web/app/modules/mobile_message/views/mobile_message_view.dart';
import 'package:mentors_web/app/modules/mobile_message_hitory/bindings/mobile_message_hitory_binding.dart';
import 'package:mentors_web/app/modules/mobile_message_hitory/views/mobile_message_hitory_view.dart';
import 'package:mentors_web/app/modules/mobile_message_user_list/bindings/mobile_message_user_list_binding.dart';
import 'package:mentors_web/app/modules/mobile_message_user_list/views/mobile_message_user_list_view.dart';
import 'package:mentors_web/app/modules/mobile_sales/bindings/mobile_sales_binding.dart';
import 'package:mentors_web/app/modules/mobile_sales/views/mobile_sales_view.dart';
import 'package:mentors_web/app/modules/mobile_seat/bindings/mobile_seat_binding.dart';
import 'package:mentors_web/app/modules/mobile_seat/views/mobile_seat_view.dart';
import 'package:mentors_web/app/modules/mobile_select_section/bindings/mobild_select_section_binding.dart';
import 'package:mentors_web/app/modules/mobile_select_section/views/mobile_select_section_view.dart';
import 'package:mentors_web/app/modules/mobile_store_management/bindings/mobile_store_management_binding.dart';
import 'package:mentors_web/app/modules/mobile_store_management/views/mobile_store_management_view.dart';
import 'package:mentors_web/app/modules/mobile_view_section/bindings/mobile_view_section_binding.dart';
import 'package:mentors_web/app/modules/mobile_view_section/views/mobile_view_section_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.MOBILE_MESSAGE,
      page: () => MobileMessageView(),
      binding: MobileMessageBinding(),
      transition: Transition.leftToRight,
    ),
    GetPage(
      name: _Paths.MOBILE_STORE_MANAGEMENT,
      page: () => MobileStoreManagementView(),
      binding: MobileStoreManagementBinding(),
      transition: Transition.leftToRight,
    ),
    GetPage(
      name: _Paths.MOBILE_SALES,
      page: () => MobileSalesView(),
      binding: MobileSalesBinding(),
      transition: Transition.leftToRight,
    ),
    GetPage(
      name: _Paths.MOBILE_SEAT,
      page: () => MobileSeatView(),
      binding: MobileSeatBinding(),
      transition: Transition.leftToRight,
    ),
    GetPage(
      name: _Paths.MESSAGE_TEMPLATE,
      page: () => MessageTemplateView(),
      binding: MessageTemplateBinding(),
    ),
    GetPage(
      name: _Paths.MOBILD_SELECT_SECTION,
      page: () => MobileSelectSectionView(),
      binding: MobildSelectSectionBinding(),
    ),
    GetPage(
      name: _Paths.MOBILE_VIEW_SECTION,
      page: () => MobileViewSectionView(),
      binding: MobileViewSectionBinding(),
    ),
    GetPage(
      name: _Paths.MOBILE_MESSAGE_HITORY,
      page: () => MobileMessageHitoryView(),
      binding: MobileMessageHitoryBinding(),
    ),
    GetPage(
      name: _Paths.MOBILE_MESSAGE_USER_LIST,
      page: () => MobileMessageUserListView(),
      binding: MobileMessageUserListBinding(),
    ),
    GetPage(
      name: _Paths.AUTH,
      page: () => AuthView(),
      binding: AuthBinding(),
    ),
  ];
}
