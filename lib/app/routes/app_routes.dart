part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const MOBILE_MESSAGE = _Paths.MOBILE_MESSAGE;
  static const MOBILE_STORE_MANAGEMENT = _Paths.MOBILE_STORE_MANAGEMENT;
  static const MOBILE_SALES = _Paths.MOBILE_SALES;
  static const MOBILE_SEAT = _Paths.MOBILE_SEAT;
  static const MESSAGE_TEMPLATE = _Paths.MESSAGE_TEMPLATE;
  static const MOBILD_SELECT_SECTION = _Paths.MOBILD_SELECT_SECTION;
  static const MOBILE_VIEW_SECTION = _Paths.MOBILE_VIEW_SECTION;
  static const MOBILE_MESSAGE_HITORY = _Paths.MOBILE_MESSAGE_HITORY;
  static const MOBILE_MESSAGE_USER_LIST = _Paths.MOBILE_MESSAGE_USER_LIST;
  static const AUTH = _Paths.AUTH;
}

abstract class _Paths {
  static const LOGIN = '/login';
  static const HOME = '/home';
  static const MOBILE_MESSAGE = '/mobile-message';
  static const MOBILE_STORE_MANAGEMENT = '/mobile-store-management';
  static const MOBILE_SALES = '/mobile-sales';
  static const MOBILE_SEAT = '/mobile-seat';
  static const MESSAGE_TEMPLATE = '/message-template';
  static const MOBILD_SELECT_SECTION = '/mobild-select-section';
  static const MOBILE_VIEW_SECTION = '/mobile-view-section';
  static const MOBILE_MESSAGE_HITORY = '/mobile-message-hitory';
  static const MOBILE_MESSAGE_USER_LIST = '/mobile-message-user-list';
  static const AUTH = '/auth';
}
