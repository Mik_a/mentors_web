import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/vertical_indicator.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/mobile/mobile_view_controller.dart';

class MobileSeat extends StatelessWidget {
  final bool isMobile;

  MobileSeat({
    Key? key,
    this.isMobile = false,
    required this.controller,
  }) : super(key: key);
  final MobileViewController controller;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        children: [
          SizedBox(height: isMobile ? 2 : 20),
          Container(
            height: isMobile ? 60 : 106,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                num("이용 좌석", controller.usingSeat.value),
                num("잔여 좌석",
                    controller.totalSeat.value - controller.usingSeat.value),
                num("전체 좌석", controller.totalSeat.value),
              ],
            ),
          ),
          if (!isMobile) Divider(),
          Expanded(
            child: Container(
              width: double.infinity,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.generate(
                    controller.seatStatus.length,
                    (index) => bar(
                      controller.seatStatus[index]['name'],
                      controller.seatStatus[index]['cnt'],
                      controller.seatStatus[index]['currentUsing'],
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget bar(String name, int max, int value) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: isMobile ? 4 : 40.0, vertical: 14),
      child: Column(
        children: [
          VerticalIndicator(
            width: isMobile ? 4 : 12,
            height: isMobile ? 112 : 185,
            value: value / max,
            backgroundColor: Color(0xffd7d7d7),
            foregroundColor: Color(0xff00D89D),
            gradientColors: [
              Color(0xff00D89D),
              Color(0xff007CA3),
            ],
          ),
          SizedBox(height: 16),
          Text(
            name,
            style: TextStyle(
              fontSize: isMobile ? 12 : null,
            ),
          ),
          SizedBox(height: 10),
          if (!isMobile)
            Container(
              width: 64,
              height: 24,
              decoration: BoxDecoration(
                color: CustomColors.cD9C87A,
                borderRadius: BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
              child: Center(
                child: Text(
                  '$value / $max',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  Column num(String title, int num) {
    return Column(
      children: [
        Text(title),
        SizedBox(height: isMobile ? 8 : 16),
        Text(
          num.toString(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: isMobile ? 20 : 48,
          ),
        ),
      ],
    );
  }
}
