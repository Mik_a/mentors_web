import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/global_colors.dart';

class MobileCard extends StatelessWidget {
  final double width;
  final double height;
  final String? title;
  final Widget? child;
  final bool isCanTap;
  final String? route;
  final EdgeInsetsGeometry padding;
  final double fromTitle;
  final Color backgroundColor;
  final Function()? onClickCard;

  const MobileCard({
    Key? key,
    this.onClickCard,
    this.backgroundColor = Colors.white,
    this.title,
    this.fromTitle = 10,
    this.padding = const EdgeInsets.all(16),
    this.isCanTap = false,
    this.child,
    this.width = double.infinity,
    this.route,
    this.height = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: backgroundColor,
          boxShadow: [
            BoxShadow(
              blurRadius: 2, color: Colors.black.withOpacity(0.1),
              // spreadRadius: 2,
            ),
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        padding: padding,
        width: width,
        height: height,
        child: SizedBox.expand(
          child: Container(
            child: InkWell(
              onTap: onClickCard,
              child: Column(
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (title != null)
                        Text(
                          title!,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                      isCanTap
                          ? InkWell(
                              onTap: () {
                                if (route != null) {
                                  Get.toNamed(route!);
                                } else {}
                              },
                              child: Container(
                                width: 21,
                                height: 21,
                                decoration: BoxDecoration(
                                  border:
                                      Border.all(color: CustomColors.cC4C4C4),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Icon(
                                  Icons.add,
                                  size: 16,
                                  color: CustomColors.c717071,
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                  SizedBox(height: fromTitle),
                  if (child != null) Expanded(child: child!)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
