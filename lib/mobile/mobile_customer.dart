import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/dashboard/views/member_chart.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/mobile/mobile_card.dart';
import 'package:mentors_web/mobile/mobile_view_controller.dart';
import 'package:pie_chart/pie_chart.dart';

class MobileCustomer extends StatelessWidget {
  final MobileViewController controller;
  MobileCustomer({required this.controller, Key? key}) : super(key: key);

  final List<Color> colors = [
    Color(0xff19A980),
    Color(0xffFFF59D),
    CustomColors.endRed,
  ];
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      Padding legend(Color color, String title, int value) {
        return Padding(
          padding: const EdgeInsets.all(2.5),
          child: Row(
            children: [
              Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                ),
              ),
              SizedBox(width: 8),
              Expanded(child: Text(title)),
              Text('$value명'),
            ],
          ),
        );
      }

      return MobileCard(
        title: "회원",
        height: 284,
        child: Column(
          children: [
            SizedBox(
              width: 129,
              height: 129,
              child: MemberChart(
                values: userController.userStatus,
                colors: [
                  CustomColors.usingGreen,
                  CustomColors.reservationYellow,
                  CustomColors.endRed,
                  CustomColors.noData,
                ],
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  legend(
                      colors[0], "이용중", userController.userStatus[0].toInt()),
                  legend(colors[2], "종료", userController.userStatus[2].toInt()),
                  legend(
                      colors[1], "예약중", userController.userStatus[1].toInt()),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
