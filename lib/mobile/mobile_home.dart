import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/controller/device_controller.dart';
import 'package:mentors_web/controller/socket_controller.dart';
import 'package:mentors_web/controller/store_management_controller.dart';
import 'package:mentors_web/mobile/mobile_card.dart';
import 'package:mentors_web/mobile/mobile_customer.dart';
import 'package:mentors_web/mobile/mobile_drawer.dart';
import 'package:mentors_web/mobile/mobile_message.dart';
import 'package:mentors_web/mobile/mobile_sale.dart';
import 'package:mentors_web/mobile/mobile_seat.dart';
import 'package:mentors_web/mobile/mobile_view_controller.dart';

class MobileHome extends StatelessWidget {
  MobileHome({Key? key}) : super(key: key);
  final MobileViewController controller = Get.put(MobileViewController());
  final StoreManagementController storeManagementController =
      Get.put(StoreManagementController());
  final DeviceController deviceController = Get.put(DeviceController());
  final SocketController socketController = Get.put(SocketController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => storeManagementController.isLoading.isTrue
          ? Center(child: CircularProgressIndicator())
          : Scaffold(
              appBar: AppBar(
                iconTheme: IconThemeData(color: Colors.black),
                elevation: 1,
                foregroundColor: Colors.black,
                backgroundColor: Colors.white,
                title: Text(
                  storeManagementController.storeInfo.value.storeName,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.person_outlined),
                  ),
                ],
                centerTitle: true,
              ),
              drawer: MobileDrawer(),
              body: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 5,
                ),
                child: ListView(
                  children: [
                    // MobileSale(),
                    MobileCard(
                      title: "좌석",
                      route: Routes.MOBILD_SELECT_SECTION,
                      onClickCard: () =>
                          Get.toNamed(Routes.MOBILD_SELECT_SECTION),
                      isCanTap: true,
                      height: 314,
                      child: MobileSeat(
                        isMobile: true,
                        controller: controller,
                      ),
                    ),
                    Row(
                      children: [
                        // Expanded(
                        //   child: MobileCustomer(controller: controller),
                        // ),
                        SizedBox(width: 4),
                        Expanded(
                          child: Column(
                            children: [
                              MobileMessage(),
                              MobileCard(
                                width: double.infinity,
                                height: 48,
                                padding: EdgeInsets.zero,
                                fromTitle: 0,
                                onClickCard: deviceController.onClickOpenDoor,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset('assets/icons/door.svg'),
                                    SizedBox(width: 12),
                                    Text('출입문 열기'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),

      // Scaffold(
    );
  }
}
