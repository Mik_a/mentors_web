import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';

class MobileViewController extends GetxController {
  RxList<Map<String, dynamic>> seatStatus = RxList();
  RxInt totalSeat = 0.obs;
  RxInt usingSeat = 0.obs;

  RxInt currentUsingUser = 0.obs;
  RxInt newRegisterUser = 0.obs;
  RxInt reservationUser = 0.obs;

  @override
  void onInit() {
    super.onInit();
    API.seat.getSeatStatus().then((value) => {
          value.keys.forEach((element) {
            totalSeat.value +=
                int.tryParse(value[element]['cnt'].toString()) ?? 0;
            usingSeat.value +=
                int.tryParse(value[element]['currentUsing'].toString()) ?? 0;

            seatStatus.add({
              "name": value[element]['name'],
              "cnt": value[element]['cnt'],
              "currentUsing": value[element]['currentUsing'],
            });
          }),
        });
  }
}
