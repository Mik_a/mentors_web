import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/components/message/message_with_icon.dart';
import 'package:mentors_web/mobile/mobile_card.dart';
import 'package:mentors_web/modules/lib.dart';

class MobileMessage extends StatelessWidget {
  MobileMessage({Key? key}) : super(key: key);
  final MessageController controller = Get.put(MessageController());

  @override
  Widget build(BuildContext context) {
    return MobileCard(
      title: "메세지",
      height: 220,
      isCanTap: true,
      route: Routes.MOBILE_MESSAGE,
      child: Obx(
        () => Column(
          children: [
            Row(
              children: [
                Expanded(child: Text("현재")),
                Text(
                  addCommaInMoney(controller.smsData.value.cash),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 2),
                Text("캐시"),
              ],
            ),
            const SizedBox(height: 8),
            Divider(height: 2),
            const SizedBox(height: 8),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MessageWithIcon(
                      iconName: 'sms',
                      amount: controller.smsData.value.sms,
                      color: Color(0xff19A980)),
                  MessageWithIcon(
                      iconName: 'lms',
                      amount: controller.smsData.value.lms,
                      color: Color(0xffFACF91)),
                  MessageWithIcon(
                      iconName: 'mms',
                      amount: controller.smsData.value.mms,
                      color: Color(0xffFC8F90)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
