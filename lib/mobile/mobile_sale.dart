import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/mobile/mobile_card.dart';

class MobileSale extends StatelessWidget {
  const MobileSale({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MobileCard(
      title: "매출",
      isCanTap: true,
      route: Routes.MOBILE_SALES,
      height: 258,
      child: Column(
        children: [
          chart(),
          Text("금주 매출"),
          Divider(),
          Text("12월 매출"),
        ],
      ),
    );
  }

  Expanded chart() {
    return Expanded(
      child: Container(
        // color: Colors.blue,
        child: LineChart(
          LineChartData(
            minY: 0,
            gridData: FlGridData(show: false),
            borderData: FlBorderData(show: false),
            axisTitleData: FlAxisTitleData(
              bottomTitle: AxisTitle(),
            ),
            titlesData: FlTitlesData(
              topTitles: SideTitles(
                showTitles: true,
                getTextStyles: (val) {
                  return TextStyle(color: CustomColors.cA3A2A3, fontSize: 10);
                },
                getTitles: (val) {
                  return val.toInt().toString();
                },
              ),
              leftTitles: SideTitles(
                showTitles: true,
                getTextStyles: (val) {
                  return TextStyle(color: CustomColors.cA3A2A3, fontSize: 10);
                },
                getTitles: (val) {
                  return val.toInt().toString();
                },
              ),
              bottomTitles: SideTitles(showTitles: false),
            ),
            lineTouchData: LineTouchData(),
            lineBarsData: [
              LineChartBarData(
                colors: [Color(0xff19A980)],
                barWidth: 2.5,
                dotData: FlDotData(show: false),
                belowBarData: BarAreaData(
                  show: true,
                  gradientFrom: Offset(0.5, 0),
                  gradientTo: Offset(0.5, 2),
                  colors: [
                    const Color(0xff19A980).withOpacity(0.3),
                    const Color(0xff19A980).withOpacity(0),
                  ],
                ),
                spots: [
                  FlSpot(1, 1),
                  FlSpot(3, 1.5),
                  FlSpot(5, 1.4),
                  FlSpot(7, 3.4),
                  FlSpot(10, 2),
                  FlSpot(12, 2.2),
                  FlSpot(13, 1.8),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
