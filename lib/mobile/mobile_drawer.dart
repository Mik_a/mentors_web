import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/global_colors.dart';

class MobileDrawer extends StatelessWidget {
  const MobileDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).unfocus();
    return Container(
      padding: EdgeInsets.only(left: 23, top: 23),
      width: 180,
      color: Colors.white,
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "MENTORS",
                  style: TextStyle(
                    fontFamily: "Mont",
                    color: CustomColors.c19A980,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 54),
            tile("홈", 'home', Routes.HOME, 20),
            tile("현황", 'chart', null, 20, routes: [
              Routes.MOBILE_SALES,
              Routes.MOBILD_SELECT_SECTION,
            ]),
            Container(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 12),
                    height: 87,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 0.5,
                        color: Color(0xffe2e2e2),
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      // InkWell(
                      //   onTap: () {
                      //     if (Routes.MOBILE_SALES == Get.currentRoute) return;
                      //     Get.offAndToNamed(Routes.MOBILE_SALES);
                      //   },
                      //   child: Container(
                      //     padding: EdgeInsets.symmetric(
                      //         vertical: 10, horizontal: 12),
                      //     height: 34,
                      //     child: Text(
                      //       "매출",
                      //       style: TextStyle(
                      //           color: Routes.MOBILE_SALES == Get.currentRoute
                      //               ? Colors.black
                      //               : Colors.grey),
                      //     ),
                      //   ),
                      // ),
                      InkWell(
                        onTap: () {
                          if (Routes.MOBILD_SELECT_SECTION == Get.currentRoute)
                            return;
                          Get.offAndToNamed(Routes.MOBILD_SELECT_SECTION);
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 12),
                          height: 34,
                          child: Text(
                            "좌석",
                            style: TextStyle(
                              color: Routes.MOBILD_SELECT_SECTION ==
                                      Get.currentRoute
                                  ? Colors.black
                                  : Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            tile("메세지", 'message', Routes.MOBILE_MESSAGE, 24),
            tile(
              "매장 관리",
              'store',
              Routes.MOBILE_STORE_MANAGEMENT,
              24,
            ),
          ],
        ),
      ),
    );
  }

  Widget tile(String title, String asset, String? route, double? size,
      {List<String>? routes}) {
    return Container(
      height: 40,
      child: InkWell(
        onTap: () {
          if (route == Get.currentRoute) return;
          if (route != null) Get.offAndToNamed(route);
        },
        child: Row(
          children: [
            SizedBox(
              width: size,
              height: size,
              child: SvgPicture.asset(
                'assets/icons/$asset.svg',
                color: route == Get.currentRoute ||
                        (routes != null && routes.contains(Get.currentRoute))
                    ? CustomColors.c19A980
                    : CustomColors.cA3A2A3,
              ),
            ),
            SizedBox(width: 12),
            Text(
              title,
              style: TextStyle(
                  color: route == Get.currentRoute ||
                          (routes != null && routes.contains(Get.currentRoute))
                      ? Colors.black
                      : Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}
