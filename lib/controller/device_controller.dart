import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/inputbox_with_title.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/rounded_button.dart';
import 'package:mentors_web/controller/socket_controller.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/mobile/mobile_door_modal.dart';

class Device {
  String id;
  String deviceId;
  String name;
  DateTime createDate;
  Device({
    required this.id,
    required this.deviceId,
    required this.name,
    required this.createDate,
  });
}

class DeviceController extends GetxController {
  RxList<Device> deviceList = RxList();

  @override
  void onInit() {
    super.onInit();
    API.store.getDeviceList().then(
          (value) => {
            deviceList.addAll(value),
          },
        );
  }

  void onClickRemoveDevice(Device device) {
    Get.dialog(
      Alert(
        alertType: AlertType.warning,
        content: "정말 삭제하시겠습니까?",
        onConfirm: () {
          API.store.deleteDevice(device.id).then(
                (value) => {
                  if (value) {deviceList.remove(device), Get.back()},
                },
              );
        },
        onCancle: Get.back,
        title: "경고",
      ),
    );
  }

  // update device name
  void onClickUpdateDeviceName(Device device) {
    TextEditingController controller = TextEditingController();
    Get.dialog(
      Alert(
        alertType: AlertType.alert,
        onConfirm: () {
          if (controller.text == '') Get.back();

          API.store.modifyDeviceName(device.id, controller.text).then(
                (value) => {
                  if (value)
                    {
                      device.name = controller.text,
                      deviceList.refresh(),
                      Get.back()
                    },
                },
              );
        },
        onCancle: Get.back,
        action: InputBox(
          width: 200,
          height: 30,
          controller: controller,
        ),
        title: "변경할 이름을 입력해주세요",
      ),
    );

    // API.store.modifyDeviceName(device.id, name).then((value) => {
    //       if (value) device.name = name,
    //     });
  }

  void onClickOpenDoor() {
    Get.defaultDialog(
      custom: MobileDoorModal(),
      content: Container(
        child: Stack(
          children: [
            Center(
              child: Transform.translate(
                offset: Offset(0, -100),
                child: Container(
                  width: 106,
                  height: 106,
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10000),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: CustomColors.cf8f8f8,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10000),
                      ),
                    ),
                    child: SvgPicture.asset('assets/icons/door_2.svg'),
                  ),
                ),
              ),
            ),
            Column(
              children: [
                SizedBox(height: 10),
                Center(
                  child: Text(
                    "입구 열기",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  child: Wrap(
                    children: [
                      ...List.generate(
                        deviceList.length,
                        (index) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RoundedButton(
                            title: deviceList[index].name,
                            onClickButton: () {
                              final s = Get.put(SocketController());
                              s.sendOpenDoor(deviceList[index].deviceId);
                            },
                            width: 90,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
      title: "",
    );
  }
}
