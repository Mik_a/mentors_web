import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserController extends GetxController {
  RxList<User> userList = RxList();
  RxList<double> userStatus = [0.0, 0.0, 0.0, 0.0].obs;
  RxBool isLoading = true.obs;

  RefreshController refreshController = RefreshController(
    initialRefresh: false,
  );

  @override
  void onInit() {
    super.onInit();
    refresh();
    getUserStatus();
  }

  void getUserStatus() async {
    List<double> value = await API.user.getUsersStatus();
    userStatus.value = value;
  }

  void refresh() {
    API.user.getUserList().then(
          (value) => {
            userList.value = value,
            isLoading.value = false,
            refreshController.refreshCompleted(),
          },
        );
  }

  Future<User?> exitThisUserById(String userId) async {
    try {
      User _user = userList.firstWhere((element) => element.id == userId);
      return await exitThisUser(_user);
    } catch (e) {
      return null;
    }
  }

  Future<User?> exitThisUser(User user) async {
    bool result = await API.user.exitUser(user.id);

    if (result) {
      User _user = userList.firstWhere((element) => element.id == user.id);

      _user.status = UserEnterStatus.exit;
      _user.currentUsingVoucherId = null;
      _user.currentUsingSeat = null;
      _user.currentUsingVoucherId = null;
      _user.seatData = null;
      userList.refresh();
      return _user;
    } else {
      EasyLoading.showError("작업에 실패했습니다.\n새로고침 후 다시 시도해주세요");
      return null;
    }
  }

  Future<User?> updateThisUser(User user, bool value) async {
    bool? res = await API.user.updateUserPass(user.id, value);
    if (res != null) {
      user.isPass = res;
      Get.find<UserController>().userList.refresh();
    }
  }
}
