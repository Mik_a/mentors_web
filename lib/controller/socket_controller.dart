import 'dart:async';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/config.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class SocketController extends GetxController {
  Rx<bool> isShow = false.obs;
  Timer? currentTimer;
  RxList<RxMap<String, dynamic>> alertList = RxList();
  late IO.Socket socket;

  @override
  void onInit() {
    super.onInit();
    socket = IO.io(socketServer, <String, dynamic>{
      'transports': ['websocket']
    });

    socket.on(
      'event',
      (msg) {
        RxMap<String, dynamic> data = {
          "msg": msg,
          "isShow": false,
        }.obs;

        alertList.add(data);

        Timer.periodic(Duration(seconds: 2), (timer) {
          timer.cancel();
          data['isShow'] = true;
          Timer.periodic(Duration(seconds: 3), (timer) {
            timer.cancel();
            data['isShow'] = false;
            Timer.periodic(Duration(seconds: 1), (timer) {
              timer.cancel();
              alertList.remove(data);
            });
          });
        });

        return;
      },
    );
  }

  void sendOpenDoor(String id) {
    print(id);
    EasyLoading.showSuccess("신호를 보냈습니다");
    socket.emit('open', id);
  }
}
