import 'dart:io';

import 'package:excel/excel.dart';

class ExcelController {
  downLoadExcelFile({
    String fileName = "excel",
    required List<String> headers,
    required List<List<String>> data,
  }) async {
    final excel = Excel.createExcel();
    excel.rename("Sheet1", "Users");
    excel.appendRow("Users", headers);

    data.forEach((element) {
      excel.appendRow("Users", element);
    });

    excel.save(fileName: fileName + '.xlsx');
    final v = await excel.encode();
    File("./test.xlsx")
      ..createSync()
      ..writeAsBytesSync(v!);

    return null;
  }
}
