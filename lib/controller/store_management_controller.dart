import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/components/alert.dart';

class CAT {
  String normal;
  String area;
  String taxfree;
  String areaFree;

  CAT({
    required this.area,
    required this.areaFree,
    required this.normal,
    required this.taxfree,
  });

  static CAT fromData(dynamic data) {
    return CAT(
      area: data['area'],
      areaFree: data['areaFree'],
      normal: data['normal'],
      taxfree: data['taxfree'],
    );
  }
}

class StoreInfo {
  String storeName;
  String ownerName;
  String storeId;
  String ownerPhone;
  String storePhone;
  String homepage;
  String restAddress;
  DateTime? openDate;
  CAT? cat;
  bool isPass;

  StoreInfo({
    this.openDate,
    this.homepage = '',
    this.ownerName = '',
    this.ownerPhone = '',
    this.storeId = '',
    this.storeName = '',
    this.storePhone = '',
    this.restAddress = '',
    this.cat,
    this.isPass = false,
  });
}

class StoreOperation {
  String serviceMessage;
  String lockerMessage;
  String lockerWarningMessage;
  String seatMessage;
  String seatWarningMessage;
  String payMessage;
  String payWarningMessage;
  String etcMessage;
  String etcWarningMessage;
  String callManagerMessage;
  int outingWarningTime;
  int outingExitTime;

  int openHour;
  int openMin;
  int closeHour;
  int closeMin;
  bool is24;

  int overFlowTime;
  int overFlowPrice;

  StoreOperation({
    this.closeHour = 0,
    this.closeMin = 0,
    this.callManagerMessage = '',
    this.etcMessage = '',
    this.etcWarningMessage = '',
    this.lockerMessage = '',
    this.lockerWarningMessage = '',
    this.openHour = 0,
    this.openMin = 0,
    this.outingExitTime = 0,
    this.outingWarningTime = 0,
    this.overFlowPrice = 0,
    this.overFlowTime = 0,
    this.payMessage = '',
    this.payWarningMessage = '',
    this.seatMessage = '',
    this.seatWarningMessage = '',
    this.serviceMessage = '',
    this.is24 = true,
  });
}

class StoreManagementController extends GetxController {
  final count = 0.obs;
  RxBool isLoading = true.obs;

  Rx<StoreInfo> storeInfo = StoreInfo().obs;

  Rx<StoreOperation> storeOperation = StoreOperation().obs;
  RxMap<String, dynamic> info = RxMap();

  TextEditingController openHour = TextEditingController(text: "0");
  TextEditingController openMin = TextEditingController(text: "0");
  TextEditingController closeHour = TextEditingController(text: "0");
  TextEditingController closeMin = TextEditingController(text: "0");
  RxBool is24 = true.obs;

  TextEditingController outWarning = TextEditingController(text: "0");
  TextEditingController outTime = TextEditingController(text: "0");
  TextEditingController overflowPrice = TextEditingController(text: "0");

  // Payment -----------------------------------------------------------------
  TextEditingController cat1 = TextEditingController();
  TextEditingController cat2 = TextEditingController();

  // Kiosk -------------------------------------------------------------------xfec

  TextEditingController serviceMessage = TextEditingController(text: "");
  TextEditingController lockerMessage = TextEditingController(text: "");
  TextEditingController lockerWarningMessage = TextEditingController(text: "");
  TextEditingController seatMessage = TextEditingController(text: "");
  TextEditingController seatWarningMessage = TextEditingController(text: "");
  TextEditingController payMessage = TextEditingController(text: "");
  TextEditingController payWarningMessage = TextEditingController(text: "");
  TextEditingController etcMessage = TextEditingController(text: "");
  TextEditingController etcWarningMessage = TextEditingController(text: "");
  TextEditingController callManagerMessage = TextEditingController(text: "");

  @override
  void onInit() {
    super.onInit();
    API.store.getStoreInfo().then(infoInit).then(setCatIdTexts);
    API.store.getStoreOperation().then(setOperTexts).then(setKioskTexts);
  }

  FutureOr<StoreInfo> infoInit(StoreInfo value) {
    storeInfo.value = value;
    isLoading.value = false;
    return value;
  }

  FutureOr setCatIdTexts(StoreInfo value) {
    cat1.text = value.cat!.normal;
    cat2.text = value.cat!.taxfree;
  }

  FutureOr<StoreOperation> setOperTexts(StoreOperation value) {
    storeOperation.value = value;
    openHour.text = value.openHour.toString();
    openMin.text = value.openMin.toString();
    closeHour.text = value.closeHour.toString();
    closeMin.text = value.closeMin.toString();
    outWarning.text = value.outingWarningTime.toString();
    outTime.text = value.outingExitTime.toString();
    is24.value = value.is24;
    overflowPrice.text = value.overFlowPrice.toString();

    return value;
  }

  FutureOr<StoreOperation> setKioskTexts(StoreOperation value) {
    serviceMessage.text = value.serviceMessage;
    lockerMessage.text = value.lockerMessage;
    lockerWarningMessage.text = value.lockerWarningMessage;
    seatMessage.text = value.seatMessage;
    seatWarningMessage.text = value.seatWarningMessage;
    payMessage.text = value.payMessage;
    payWarningMessage.text = value.payWarningMessage;
    etcMessage.text = value.etcMessage;
    etcWarningMessage.text = value.etcWarningMessage;
    callManagerMessage.text = value.callManagerMessage;
    return value;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void onClickKioskSave() {
    final data = {
      "storeOperation": {
        'serviceMessage': serviceMessage.text,
        'lockerMessage': lockerMessage.text,
        'lockerWarningMessage': lockerWarningMessage.text,
        'seatMessage': seatMessage.text,
        'seatWarningMessage': seatWarningMessage.text,
        'payMessage': payMessage.text,
        'payWarningMessage': payWarningMessage.text,
        'callManagerMessage': callManagerMessage.text,
        'etcMessage': etcMessage.text,
        'etcWarningMessage': etcWarningMessage.text,
      }
    };

    API.store.updateStoreInfo(data);
  }

  void onClickPaymentSave() {
    final data = {
      "CAT": {
        'normal': cat1.text,
        'taxfree': cat2.text,
      }
    };

    API.store.updateStoreInfo(data);
  }

  void onClickStoreOperationInfoSave() {
    String openhour = openHour.text == "" ? "0" : openHour.text;
    String openmin = openMin.text == "" ? "0" : openMin.text;
    String closehour = closeHour.text == "" ? "0" : closeHour.text;
    String closemin = closeMin.text == "" ? "0" : closeMin.text;

    int openHourInt = int.parse(openhour);
    int openMinInt = int.parse(openmin);
    int closeHourInt = int.parse(closehour);
    int closeMinInt = int.parse(closemin);

    if (openHourInt > 23 ||
        openMinInt > 59 ||
        closeHourInt > 23 ||
        closeMinInt > 59) {
      EasyLoading.showError("올바르지 않은 시간입니다");
      return;
    }

    final data = {
      "storeOperation": {
        "openingTime": {
          "hour": openHour.text,
          "min": openMin.text,
        },
        "closeTime": {
          "hour": closeHour.text,
          "min": closeMin.text,
        },
        'outingWarningTime': outWarning.text,
        'outingExitTime': outTime.text,
        'is24': is24.value,
        'overflow': {
          'price': overflowPrice.text,
        }
      }
    };

    API.store.updateStoreInfo(data);
  }

  void onClickStoreCovidPassOn() {
    Get.dialog(
      Alert(
        alertType: AlertType.alert,
        onConfirm: () async {
          Get.back();
          await updatePassThisStore(true);
        },
        action: RichText(
          text: TextSpan(
              text: '매장에서',
              style: TextStyle(
                color: Color(0xff242424),
                fontSize: 14,
              ),
              children: [
                TextSpan(
                  text: ' 방역인증 기능을',
                  style: TextStyle(
                    color: Color(0xff00A6A6),
                    fontSize: 14,
                  ),
                ),
                TextSpan(
                  text: ' 사용 하시겠습니까?',
                  style: TextStyle(
                    color: Color(0xff242424),
                    fontSize: 14,
                  ),
                ),
              ]),
        ),
        title: "방역인증 기능사용",
        confirmTitle: "사용",
        cancleTitle: "취소",
      ),
    );
  }

  void onClickStoreCovidPassOff() {
    Get.dialog(
      Alert(
        alertType: AlertType.alert,
        onConfirm: () async {
          Get.back();
          await updatePassThisStore(false);
        },
        action: RichText(
          text: TextSpan(
              text: '매장에서',
              style: TextStyle(
                color: Color(0xff242424),
                fontSize: 14,
              ),
              children: [
                TextSpan(
                  text: ' 방역인증 기능을',
                  style: TextStyle(
                    color: Color(0xff00A6A6),
                    fontSize: 14,
                  ),
                ),
                TextSpan(
                  text: ' 해제 하시겠습니까?',
                  style: TextStyle(
                    color: Color(0xff242424),
                    fontSize: 14,
                  ),
                ),
              ]),
        ),
        title: "방역인증 기능해제",
        confirmTitle: "해제",
        cancleTitle: "취소",
      ),
    );
  }

  Future<StoreInfo?> updatePassThisStore(bool val) async {
    bool? res = await API.store.updateUsingPass(val);
    if (res != null) {
      storeInfo.value.isPass = res;
      storeInfo.refresh();
    }
  }
}
