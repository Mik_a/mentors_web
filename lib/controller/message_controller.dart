import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/app/modules/message_management/message_charge_history_modal.dart';
import 'package:mentors_web/app/modules/message_management/message_charge_modal.dart';
import 'package:mentors_web/app/modules/message_management/message_user_select_modal.dart';
import 'package:mentors_web/app/modules/mobile_message/views/mobile_message_help_modal.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

class MessageLog {
  String id;
  List<String> phone;
  String userId;
  String name;
  String message;
  MessageType type;
  DateTime time;
  int cash;

  MessageLog({
    required this.cash,
    required this.id,
    required this.phone,
    required this.userId,
    required this.name,
    required this.message,
    required this.time,
    required this.type,
  });
}

class SMSData {
  int cash;
  int smsPrice;
  int lmsPrice;
  int mmsPrice;
  int sms;
  int lms;
  int mms;

  SMSData({
    this.cash = 0,
    this.sms = 0,
    this.lms = 0,
    this.mms = 0,
    this.smsPrice = 0,
    this.lmsPrice = 0,
    this.mmsPrice = 0,
  });
}

enum MessageSendType { all, select, self }

class MessageController extends GetxController {
  Rx<SMSData> smsData = SMSData().obs;
  Map<int, List<MessageLog>> msgLogList = {};
  RxList<MessageLog> currentMsgLogList = RxList();
  RxList<MessageLog> currentMsgLogListForMobile = RxList();
  RxList<User> selectedUserList = RxList();
  Rx<MessageSendType> sendType = MessageSendType.self.obs;
  RxList<String> selfPhoneList = RxList.empty();
  RxInt logCurrentPage = 0.obs;
  RxInt logLength = 0.obs;
  RxInt sendUserLength = 0.obs;

  late TextEditingController smsContentController;
  late TextEditingController phoneListController;

  RxInt currentMessageLength = 0.obs;
  RxBool isSendToOnlyUser = true.obs;
  RxBool isEnd = false.obs;

  @override
  void onInit() {
    super.onInit();
    smsContentController = TextEditingController();
    phoneListController = TextEditingController();

    smsContentController.addListener(textLengthChecker);
    phoneListController.addListener(setSelfPhoneList);

    updateCash();
    API.sms.getMessageLogLength().then((value) {
      logLength.value = value;
      getMessageLogs(logCurrentPage.value);
      getMessageLogsMobile();
    });
  }

  void updateCash() {
    API.sms.getCash().then((value) => smsData.value = value);
  }

  void setSelfPhoneList() {
    selfPhoneList.value =
        phoneListController.text.split(',').map((e) => e.trim()).toList();
  }

  void onChangeSendToType(dynamic v) {
    isSendToOnlyUser.value = v;
  }

  void textLengthChecker() {
    currentMessageLength.value = getTextLengthKor(smsContentController.text);
  }

  void onClickHelp() {
    Get.dialog(
      Modal(
        width: 400,
        height: 300,
        content: Container(
          child: MobileMessageHelpModal(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    smsContentController.dispose();
    phoneListController.dispose();
    super.dispose();
  }

  void getMessageLogs(int page) {
    API.sms.getMessageLogs(page).then((value) {
      msgLogList[page] = value;
      currentMsgLogList.value = value;
    });
  }

  void getMessageLogsMobile() {
    API.sms.getMessageLogs(logCurrentPage.value).then((value) {
      currentMsgLogListForMobile.addAll(value);
      if (value.length == 0) isEnd.value = true;

      logCurrentPage.value = logCurrentPage.value + 1;
    });
  }

  void setPage(int value) {
    if (msgLogList.containsKey(value)) {
      currentMsgLogList.value = msgLogList[value]!;
    } else {
      getMessageLogs(value);
    }

    logCurrentPage.value = value;
  }

  void sendMessage() {
    if (getNumberOfSendUser() == '0') {
      EasyLoading.showToast("최소 1명의 수신자가 필요합니다");
      return;
    }

    List<String> phoneNumbers = phoneListController.text
        .split(',')
        .map((e) => e.numericOnly())
        .toList();

    List<Map<String, String>> recivers = [];
    final message = smsContentController.text;

    if (sendType.value == MessageSendType.all) {
      UserController controller = Get.find();
      recivers = controller.userList
          .map(
            (element) => ({
              "name": element.name,
              "phone": element.phone,
              "parent":
                  isSendToOnlyUser.value ? '-' : element.parentPhone ?? '-'
            }),
          )
          .toList();
    } else if (sendType.value == MessageSendType.select) {
      recivers = selectedUserList
          .map(
            (element) => ({
              "name": element.name,
              "phone": element.phone,
              "parent":
                  isSendToOnlyUser.value ? '-' : element.parentPhone ?? '-'
            }),
          )
          .toList();
    } else {
      recivers = phoneNumbers.map((e) => ({"phone": e})).toList();
    }

    Get.dialog(
      Modal(
          content: Alert(
        alertType: AlertType.alert,
        content: '${getNumberOfSendUser()}건의 문자를 보내시겠습니까?',
        onConfirm: () async {
          EasyLoading.show();

          final res = await API.sms.sendMessages(recivers, message);
          Get.back();

          if (res == null || !res) {
            EasyLoading.showError("전송에 실패했습니다.");
          } else {
            EasyLoading.showSuccess("전송되었습니다.");
            updateCash();
            smsContentController.text = '';
          }
        },
        onCancle: Get.back,
        title: "알림",
      )),
    );
  }

  void onConfirmUser(List<User> users) {
    selectedUserList.value = users;
  }

  void onClickAllUsers(List<User> users) {
    users.forEach((element) {
      if (!selectedUserList.contains(element)) {
        selectedUserList.add(element);
      }
    });
  }

  void removeThisUsers(List<User> users) {
    users.forEach((element) {
      selectedUserList.remove(element);
    });
  }

  void onClickSearchUsers() {
    sendType.value = MessageSendType.select;

    Get.dialog(
      Modal(
        content: MessageUserSelectModal(
          onClickConfirm: onConfirmUser,
          selectedUser: selectedUserList,
        ),
      ),
    );
  }

  void onChangeSendType(MessageSendType type, BuildContext? context) {
    if (type == sendType.value) return;
    sendType.value = type;

    if (context != null && sendType.value == MessageSendType.select) {
      onClickSearchUsers();
    }
    if (context != null) FocusScope.of(context).unfocus();
  }

  void onCheckUser(User user) {
    isUserSelected(user)
        ? selectedUserList.remove(user)
        : selectedUserList.add(user);
  }

  bool isUserSelected(User user) {
    return selectedUserList.contains(user);
  }

  String get cash => addCommaInMoney(smsData.value.cash);
  String get messageType => currentMessageLength > 90 ? "LMS" : "SMS";
  String get messagePrice => currentMessageLength > 90
      ? smsData.value.lmsPrice.toString() + "원"
      : smsData.value.smsPrice.toString() + "원";
  String get messagePriceTotal => currentMessageLength > 90
      ? (smsData.value.lmsPrice * sendUserLength.value).toString() + "원"
      : (smsData.value.smsPrice * sendUserLength.value).toString() + "원";

  String getNumberOfSendUser() {
    if (sendType.value == MessageSendType.select) {
      int phone = 0;
      int parent = 0;
      selectedUserList.map(
        (element) {
          if (GetUtils.isPhoneNumber(element.phone)) phone++;
          if (element.parentPhone != null &&
              !isSendToOnlyUser.value &&
              GetUtils.isPhoneNumber(element.parentPhone!)) parent++;

          return {
            "name": element.name,
            "phone": element.phone,
            "parent": element.parentPhone,
          };
        },
      ).toList();
      sendUserLength.value = phone + parent;
      return '${phone + parent}';
    } else if (sendType.value == MessageSendType.all) {
      UserController controller = Get.find();
      int phone = 0;
      int parent = 0;
      controller.userList.map(
        (element) {
          if (GetUtils.isPhoneNumber(element.phone)) phone++;
          if (element.parentPhone != null &&
              !isSendToOnlyUser.value &&
              GetUtils.isPhoneNumber(element.parentPhone!)) parent++;
          return {
            "name": element.name,
            "phone": element.phone,
            "parent": element.parentPhone,
          };
        },
      ).toList();
      sendUserLength.value = phone + parent;

      return '${phone + parent}';
    } else if (sendType.value == MessageSendType.self) {
      sendUserLength.value =
          selfPhoneList.where((e) => GetUtils.isPhoneNumber(e)).length;

      return sendUserLength.value.toString();
    }

    return '';
  }

  void onClickCharge() {
    Get.dialog(
      Modal(
        content: MessageChargeModal(),
      ),
    );
  }

  void onClickChargeHistory() {
    Get.dialog(
      Modal(
        content: MessageChargeHistoryModal(),
      ),
    );
  }
}
