import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/logs.dart';

class IoLogController extends GetxController {
  RxList<IOLog> userLogList = RxList();

  RxBool isLoading = true.obs;
  RxBool isEnd = false.obs;
  int currentIndex = 0;

  @override
  void onInit() {
    super.onInit();
    updateLogs();
  }

  Future<void> onRefresh() async {
    userLogList.value = [];
    currentIndex = 0;
    isEnd.value = false;
    isLoading.value = true;

    updateLogs();
  }

  void updateLogs() async {
    final list = await API.log.getIOLogs(currentIndex);
    isLoading.value = false;

    if (list.length > 0)
      userLogList.addAll(list);
    else
      isEnd.value = true;
    currentIndex++;
  }
}
