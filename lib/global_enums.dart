enum Gender {
  none,
  male,
  female,
}

enum SeatGroup {
  free,
  fixed,
  studyroom,
  locker,
}

extension equal on Enum {
  bool isEqual(dynamic val) {
    return val == this;
  }
}

extension func on SeatGroup {
  String toKor() {
    switch (this) {
      case SeatGroup.fixed:
        return "지정석";
      case SeatGroup.free:
        return "자유석";
      case SeatGroup.studyroom:
        return "스터디룸";
      case SeatGroup.locker:
        return "사물함";
      default:
        return "";
    }
  }
}

enum UserEnterStatus {
  exit,
  out,
  enter,
}

enum UserCovidPass {
  Y,
  N,
}

enum MessageType {
  sms,
  lms,
  mms,
  app,
}

extension genderPrototype on Gender {
  String toKor() {
    switch (this) {
      case Gender.female:
        return "여성";
      case Gender.male:
        return "남성";
      default:
        return '-';
    }
  }
}

extension enterStatusPrototype on UserEnterStatus {
  String toKor() {
    switch (this) {
      case UserEnterStatus.enter:
        return "입실";
      case UserEnterStatus.exit:
        return "퇴실";
      case UserEnterStatus.out:
        return "외출";
      default:
        return '-';
    }
  }
}
