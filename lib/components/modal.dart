import 'package:flutter/material.dart';

class Modal extends StatelessWidget {
  final Widget content;
  final double? width;
  final double? height;
  final String? title;
  final Widget? action;
  // final bool is

  const Modal({
    this.width,
    this.height,
    this.title,
    this.action,
    required this.content,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: Navigator.of(context).pop,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.black.withOpacity(0.2),
        child: Align(
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: width == null && height == null ? null : () {},
            child: Container(
              width: width,
              height: height,
              decoration: BoxDecoration(
                color: width == null && height == null
                    ? Colors.transparent
                    : Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: GestureDetector(
                onTap: width == null && height == null ? () {} : null,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (title != null) ...[
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 16.0,
                          right: 16,
                        ),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 44,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    title!,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  if (action != null) action!
                                ],
                              ),
                            ),
                            Divider(height: 0),
                          ],
                        ),
                      ),
                    ],
                    height == null
                        ? Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 24.0),
                            child: content,
                          )
                        : Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24.0),
                              child: content,
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
