import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mentors_web/global_colors.dart';

class InputBox extends StatelessWidget {
  final TextEditingController? controller;
  final num width;
  final num height;
  final String? hintText;
  final TextInputType? textInputType;
  final TextAlign textAlign;
  final Function(String)? onChanged;
  final bool obscureText;
  final Color borderColor;
  final int maxLines;
  final Function(String)? onClickEnter;
  final FocusNode? focusNode;

  const InputBox({
    this.maxLines = 1,
    this.onClickEnter,
    this.focusNode,
    this.borderColor = const Color(0xffE3E3E3),
    this.width = 50,
    this.height = 50,
    this.hintText,
    this.obscureText = false,
    this.textAlign = TextAlign.left,
    this.textInputType,
    this.controller,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderSide: BorderSide(color: borderColor),
    );

    return Container(
      width: width.toDouble(),
      height: height.toDouble(),
      child: TextField(
        textAlignVertical: TextAlignVertical.top,
        controller: controller,
        keyboardType: textInputType,
        focusNode: focusNode,
        onSubmitted: onClickEnter,
        textAlign: textAlign,
        onChanged: onChanged,
        obscureText: obscureText,
        expands: textInputType == TextInputType.multiline,
        maxLines: textInputType == TextInputType.multiline ? null : 1,
        style: TextStyle(
          fontFamily: obscureText ? "Mont" : null,
          fontSize: 14,
          color: CustomColors.c242424,
        ),
        inputFormatters: [
          if (textInputType == TextInputType.number)
            FilteringTextInputFormatter.digitsOnly
        ],
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(
            left: textAlign == TextAlign.center ? 0 : 10,
            top: 5,
          ),
          focusedBorder: outlineInputBorder,
          enabledBorder: outlineInputBorder,
          disabledBorder: outlineInputBorder,
          hintText: hintText,
          hintStyle: TextStyle(
            color: CustomColors.cA3A2A3,
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}
