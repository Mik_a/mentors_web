import 'package:flutter/material.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/input_box.dart';

class InputboxWithTitle extends StatelessWidget {
  final double titleWidth;
  final double inputWidth;
  final double inputHeight;
  final String? title;
  final List<String>? titles;
  final String? hintText;
  final TextEditingController? controller;
  final TextAlign textAlign;
  final String? tailText;
  final double containerWidth;
  final TextInputType? textInputType;
  final Function(String)? onChanged;

  const InputboxWithTitle({
    this.title,
    this.titles,
    this.onChanged,
    this.controller,
    this.textInputType,
    this.containerWidth = double.infinity,
    this.tailText,
    this.textAlign = TextAlign.left,
    this.hintText,
    this.inputWidth = 50,
    this.inputHeight = 32,
    this.titleWidth = 100,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: containerWidth,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: TextInputType.multiline == textInputType
              ? CrossAxisAlignment.start
              : CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: TextInputType.multiline == textInputType ? 7.0 : 0),
              child: titles == null
                  ? AutoSpacingText(
                      text: title!,
                      width: titleWidth,
                    )
                  : Column(
                      children: [
                        ...List.generate(titles!.length, (index) {
                          return AutoSpacingText(
                            text: titles![index],
                            width: titleWidth,
                          );
                        }),
                      ],
                    ),
            ),
            SizedBox(width: 8),
            inputWidth == double.infinity
                ? Expanded(
                    child: InputBox(
                      controller: controller,
                      height: inputHeight,
                      hintText: hintText,
                      textAlign: textAlign,
                      textInputType: textInputType,
                    ),
                  )
                : InputBox(
                    textInputType: textInputType,
                    controller: controller,
                    height: inputHeight,
                    width: inputWidth,
                    hintText: hintText,
                    textAlign: textAlign,
                    onChanged: onChanged,
                  ),
            if (tailText != null) ...[
              SizedBox(width: 5),
              Text(tailText!),
            ]
          ],
        ),
      ),
    );
  }
}
