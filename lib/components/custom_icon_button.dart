import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mentors_web/global_colors.dart';

class CustomIconButton extends StatelessWidget {
  final String title;
  final String? icon;
  final double? width;
  final double? height;
  final TextStyle? textStyle;
  final Color? iconColor;
  final Function()? onClick;
  final LinearGradient? linearGradient;

  const CustomIconButton({
    this.icon,
    this.onClick,
    this.iconColor,
    this.linearGradient,
    this.width,
    this.height,
    this.textStyle,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 6,
          horizontal: 8,
        ),
        width: width,
        height: height,
        decoration: BoxDecoration(
          gradient: linearGradient,
          border: Border.all(color: CustomColors.cC4C4C4),
          borderRadius: BorderRadius.all(
            Radius.circular(4),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/icons/$icon.svg',
              color: iconColor,
              width: 16,
              height: 16,
            ),
            SizedBox(
              width: 4,
            ),
            Text(
              title,
              style: textStyle,
            ),
          ],
        ),
      ),
    );
  }
}
