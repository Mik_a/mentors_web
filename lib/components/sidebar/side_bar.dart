import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/app/routes/app_pages.dart';
import 'package:mentors_web/components/sidebar/side_icon.dart';

class Sidebar extends StatelessWidget {
  final List<IconClass> iconList;
  final Function(int) onClickIcon;
  final currentPage;

  Sidebar({
    required this.currentPage,
    required this.iconList,
    required this.onClickIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xff00A6A6),
              Color(0xff00A68A),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        width: 80,
        child: Column(
          children: [
            ...List.generate(
              this.iconList.length,
              (index) => InkWell(
                onTap: () => onClickIcon(index),
                child: SideIcon(
                  icondata: iconList[index],
                  isSelected: currentPage == index,
                ),
              ),
            ),
            Expanded(child: Container()),
            InkWell(
              onTap: () {
                Get.offAllNamed(Routes.LOGIN);
                GetStorage().erase();
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 0.5,
                      color: Colors.white,
                    ),
                  ),
                ),
                child: SideIcon(
                  icondata: IconClass(
                    icon: "assets/icons/logout.svg",
                    title: "로그아웃",
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
