import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/global_colors.dart';

class SideIcon extends StatelessWidget {
  final IconClass icondata;
  final bool isSelected;

  const SideIcon({
    required this.icondata,
    this.isSelected = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      decoration: BoxDecoration(
        color: Colors.transparent,
        boxShadow: isSelected
            ? [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                ),
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  blurRadius: 19,
                  spreadRadius: -19,
                )
              ]
            : [],
        border: Border(
          bottom: BorderSide(
            color: Colors.white,
            width: 0.5,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 11.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icondata.icon,
            ),
            SizedBox(height: 2),
            Text(
              icondata.title,
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: kTextWhiteColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
