import 'package:flutter/material.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/input_box.dart';

class PaymentForm extends StatelessWidget {
  final int? price;
  final Function() onClickPayment;
  final bool isEnable;
  final TextEditingController cardPriceController;
  final TextEditingController cashPriceController;

  const PaymentForm({
    required this.isEnable,
    required this.onClickPayment,
    required this.cardPriceController,
    required this.cashPriceController,
    this.price,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Text("카드"),
                  SizedBox(width: 10),
                  InputBox(
                    height: 25,
                    width: 70,
                    textInputType: TextInputType.number,
                    controller: cardPriceController,
                  ),
                  SizedBox(width: 4),
                  Text('원'),
                ],
              ),
              Row(
                children: [
                  Text("현금"),
                  SizedBox(width: 10),
                  InputBox(
                    height: 25,
                    width: 70,
                    textInputType: TextInputType.number,
                    controller: cashPriceController,
                  ),
                  SizedBox(width: 4),
                  Text('원'),
                ],
              ),
            ],
          ),
          SizedBox(height: 20),
          CustomButton(
            title: "결제하기",
            width: double.infinity,
            type:
                isEnable ? CustomButtonType.confirm : CustomButtonType.disable,
            onPressed: onClickPayment,
          ),
        ],
      ),
    );
  }
}
