import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/custom_checkbox.dart';
import 'package:mentors_web/components/pagenation_listview/pagenation_listview.dart';
import 'package:mentors_web/components/search_input.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/global_enums.dart';

class FindUserForm extends StatefulWidget {
  final Function(User)? onClickUser;
  final Function(List<User>)? onChangeUserList;
  final List<User> selectedUsers;
  final bool isDisplayGuest;
  final double height;

  FindUserForm({
    this.isDisplayGuest = true,
    this.onClickUser,
    this.onChangeUserList,
    this.selectedUsers = const [],
    this.height = 750,
    Key? key,
  }) : super(key: key);

  @override
  _FindUserFormState createState() => _FindUserFormState();
}

class _FindUserFormState extends State<FindUserForm> {
  final UserController userController = Get.find();

  String filter = '';

  @override
  Widget build(BuildContext context) {
    List<User> userList = userController.userList;

    if (!widget.isDisplayGuest) {
      userList = userList.where((element) => element.isRegister).toList();
    }

    userList = userList
        .where(
          (element) =>
              (element.phone.contains(filter) || element.name.contains(filter)),
        )
        .toList();

    bool isAllSelected = widget.selectedUsers.length == userList.length;

    void onClickAllSelect(bool v) {
      if (widget.onChangeUserList == null) return;

      if (v) {
        widget.onChangeUserList!(userList);
      } else {
        widget.onChangeUserList!([]);
      }
    }

    void onClickUser(int v) {
      User user = userList[v];
      if (widget.onClickUser != null) widget.onClickUser!(user);
    }

    return RegisterCard(
      title: "회원검색",
      width: 450,
      height: widget.height,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 40,
              width: 402,
              child: SearchInput(
                onChange: (v) {
                  setState(() {
                    filter = v;
                  });
                },
              ),
            ),
          ),
          Expanded(
            child: PagenationListView(
              itemHeight: 32,
              headerWidths: [12, 30, 80, 60, 110],
              onClick: onClickUser,
              data: List.generate(userList.length, (index) {
                User user = userList[index];

                return [
                  CustomCheckbox(
                    value: widget.selectedUsers.contains(user),
                    onChange: (v) {
                      if (widget.onClickUser != null) widget.onClickUser!(user);
                    },
                  ),
                  Center(child: Text((index + 1).toString())),
                  Center(child: Text(user.name)),
                  Center(child: Text(user.status.toKor())),
                  Center(child: Text(user.phone)),
                ];
              }),
              headerTitles: [
                CustomCheckbox(
                  onChange: onClickAllSelect,
                  value: isAllSelected,
                ),
                Text("No."),
                Text("이름"),
                Text("입실여부"),
                Text("전화번호"),
              ],
            ),
          )
        ],
      ),
    );
  }
}
