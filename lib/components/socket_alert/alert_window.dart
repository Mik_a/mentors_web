import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/controller/socket_controller.dart';
import 'package:mentors_web/global_colors.dart';

class AlertWindow extends StatelessWidget {
  final String msg;
  final bool isShow;
  AlertWindow({
    required this.msg,
    required this.isShow,
    Key? key,
  }) : super(key: key);
  final SocketController controller = Get.put(SocketController());

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      right: 25,
      bottom: isShow ? 25 : -100,
      child: SizedBox(
        width: 300,
        height: 50,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: Colors.white,
            border: Border.all(color: CustomColors.cA3A2A3, width: 1),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                msg,
                style: TextStyle(
                  color: CustomColors.c242424,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
