import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/socket_alert/alert_window.dart';
import 'package:mentors_web/controller/socket_controller.dart';

class SocketAlert extends StatelessWidget {
  SocketAlert({Key? key}) : super(key: key);
  final SocketController socket = Get.put(SocketController());

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Obx(
        () => Stack(
          children: [
            ...List.generate(
              socket.alertList.length,
              (index) => AlertWindow(
                msg: socket.alertList[index]['msg'],
                isShow: socket.alertList[index]['isShow'],
              ),
            )
          ],
        ),
      ),
    );
  }
}
