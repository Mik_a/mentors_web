import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class ContextMenuTile extends StatelessWidget {
  final bool isActive;
  final String title;
  final Widget icon;
  final Function onClick;

  const ContextMenuTile({
    required this.title,
    required this.icon,
    required this.onClick,
    required this.isActive,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      child: InkWell(
        onTap: isActive
            ? () async {
                if (isActive) onClick();
              }
            : null,
        child: Padding(
          padding: const EdgeInsets.all(6.0),
          child: Opacity(
            opacity: isActive ? 1 : 0.3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 16,
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: icon,
                  ),
                ),
                Text(
                  title,
                  style: TextStyle(color: CustomColors.c242424),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
