import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class Calender extends StatelessWidget {
  final DateTime currentDate;
  final Function() onClickLeft;
  final Function() onClickRight;
  final Function(DateTime)? onClickDate;
  final Map<int, Widget>? content;
  final Widget? action;

  const Calender({
    required this.currentDate,
    required this.onClickLeft,
    required this.onClickRight,
    this.action,
    this.content,
    this.onClickDate,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime lastday = DateTime(currentDate.year, currentDate.month + 1)
        .add(Duration(days: -1));
    DateTime firstDay = DateTime(currentDate.year, currentDate.month, 1);
    int weekday = firstDay.weekday;

    List<List<Widget>> weeks = [];
    List<Widget> days = [];

    if (weekday != 7)
      for (var i = weekday; i >= 1; i--) {
        DateTime time = firstDay.subtract(Duration(days: i));
        days.add(day(time, isNotCurrentWeek: true));
      }

    for (var i = 0; i < lastday.day; i++) {
      DateTime time = firstDay.add(Duration(days: i));
      if (days.length == 7) {
        weeks.add(days);
        days = [];
      }
      days.add(
        day(
          time,
          content: content?[i] ?? null,
        ),
      );
    }

    if (days.length != 0) {
      int len = days.length;
      for (var i = 1; i <= 7 - len; i++) {
        DateTime time = lastday.add(Duration(days: i));
        days.add(day(time, isNotCurrentWeek: true));
      }

      weeks.add(days);
    }

    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 24.0),
            child: SizedBox(
              height: 28,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: onClickLeft,
                    child: SizedBox(
                      width: 24,
                      height: 24,
                      child: Center(
                        child: Icon(
                          Icons.arrow_back_ios_new_rounded,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 8),
                  Text(
                    '${currentDate.year}년 ${("00" + currentDate.month.toString()).substring(currentDate.month.toString().length)}월',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(width: 8),
                  InkWell(
                    onTap: onClickRight,
                    child: SizedBox(
                      width: 24,
                      height: 24,
                      child: Transform.rotate(
                        angle: 3.14,
                        child: Icon(
                          Icons.arrow_back_ios_new_rounded,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                  if (action != null) action!
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.cC4C4C4,
                border: Border.all(color: CustomColors.cC4C4C4),
                borderRadius: BorderRadius.all(
                  Radius.circular(0),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    height: 40,
                    color: Color(0xfff3f3f3),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          margin: EdgeInsets.all(0.5),
                          child: Text("일"),
                        ),
                        Text("월"),
                        Text("화"),
                        Text("수"),
                        Text("목"),
                        Text("금"),
                        Text("토"),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        ...List.generate(
                          weeks.length,
                          (index) => Expanded(
                            child: Row(
                              children: [
                                ...List.generate(
                                  weeks[index].length,
                                  (index2) => weeks[index][index2],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget day(
    DateTime time, {
    bool isNotCurrentWeek = false,
    Widget? content,
  }) {
    return Expanded(
      child: InkWell(
        onTap: () => onClickDate != null ? onClickDate!(time) : null,
        child: Container(
          margin: EdgeInsets.all(0.5),
          height: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
                width: 3,
                color:
                    currentDate == time ? kMaterialColor : Colors.transparent),
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 8,
            vertical: 12,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                time.day.toString(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: isNotCurrentWeek
                      ? CustomColors.cC4C4C4
                      : currentDate == time
                          ? kMaterialColor
                          : time.weekday == 7
                              ? Colors.red
                              : time.weekday == 6
                                  ? Colors.blue
                                  : Colors.black,
                ),
              ),
              if (content != null) content,
            ],
          ),
        ),
      ),
    );
  }
}
