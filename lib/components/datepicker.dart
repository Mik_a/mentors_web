import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class DatePicker extends StatelessWidget {
  final TextEditingController? controller;
  final DateTime? initDate;
  final String? hintText;
  final DateTime? firstDate;
  final Function(String)? onDateChanged;

  DatePicker({
    this.initDate,
    this.controller,
    this.hintText,
    this.firstDate,
    this.onDateChanged,
    Key? key,
  }) : super(key: key);

  final OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderSide: BorderSide(
    color: Color(0xffE3E3E3),
  ));

  @override
  Widget build(BuildContext context) {
    return DateTimePicker(
      onChanged: onDateChanged,
      type: DateTimePickerType.date,
      controller: controller,
      firstDate: firstDate ?? DateTime(1900),
      lastDate: DateTime(2999, 12, 12),
      dateMask: 'yyyy-MM-dd',
      style: TextStyle(fontSize: 12),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
          left: 10,
          top: 5,
        ),
        hintText: hintText,
        focusColor: Color(0xffE3E3E3),
        focusedBorder: outlineInputBorder,
        enabledBorder: outlineInputBorder,
        border: outlineInputBorder,
        hintStyle: TextStyle(
          color: CustomColors.cA3A2A3,
          fontSize: 12,
        ),
      ),
    );
  }
}
