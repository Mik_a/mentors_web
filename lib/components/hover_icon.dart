import 'package:flutter/material.dart';

class HoverIcon extends StatefulWidget {
  final Color color;
  final Color hoverColor;
  final IconData icon;
  final Function()? onClick;

  const HoverIcon({
    required this.color,
    required this.hoverColor,
    required this.icon,
    this.onClick,
    Key? key,
  }) : super(key: key);

  @override
  _HoverIconState createState() => _HoverIconState();
}

class _HoverIconState extends State<HoverIcon> {
  bool isHover = false;

  void hover(bool b) {
    setState(() {
      isHover = b;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onClick,
      child: MouseRegion(
        onEnter: (_) => hover(true),
        onExit: (_) => hover(false),
        child: Icon(
          widget.icon,
          color: isHover ? widget.hoverColor : widget.color,
        ),
      ),
    );

    // InkWell(
    //   onHover: onHover,
    //   child: Icon(
    //     widget.icon,
    //     color: isHover ? widget.hoverColor : widget.color,
    //   ),
    // );
  }
}
