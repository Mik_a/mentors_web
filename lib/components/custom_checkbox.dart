import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomCheckbox extends StatefulWidget {
  final bool value;
  final Function(bool) onChange;
  final String? title;

  const CustomCheckbox({
    this.title,
    required this.onChange,
    required this.value,
    Key? key,
  }) : super(key: key);

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onChange(!widget.value);
      },
      child: Row(
        children: [
          SizedBox(
            width: 12,
            height: 12,
            child: FittedBox(
              fit: BoxFit.fill,
              child: widget.value
                  ? SvgPicture.asset('assets/icons/checkbox_on.svg')
                  : SvgPicture.asset('assets/icons/checkbox_off.svg'),
            ),
          ),
          if (widget.title != null) ...[
            SizedBox(width: 4),
            Text(widget.title!),
          ]
        ],
      ),
    );
  }
}
