import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/controller/store_management_controller.dart';

class HomeAppBar extends StatelessWidget {
  final String title;
  final Color color;
  final List<Widget> actions;

  final StoreManagementController storeManagementController =
      Get.put(StoreManagementController());

  HomeAppBar({
    Key? key,
    required this.color,
    required this.title,
    this.actions = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: Color(0xffDADADA),
              width: 0.5,
            ),
          ),
        ),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: 10,
                left: 10,
                right: 12,
                bottom: 10,
              ),
              // child: Image.network(
              //   '$server/stores/logo/text?jwt=${GetStorage().read('jwt')}',
              //   color: Colors.black,
              // ),

              child: SvgPicture.asset(
                'assets/icons/logo.svg',
                color: color,
              ),
            ),
            // Text(
            //   title,
            //   style: TextStyle(
            //       color: color,
            //       fontWeight: FontWeight.bold,
            //       fontSize: 28,
            //       fontFamily: 'Mont'),
            // ),
            SizedBox(width: 16),
            Text(storeManagementController.storeInfo.value.storeName),
            Expanded(child: Container()),
            Container(
              child: Row(
                children: [...actions],
              ),
            )
          ],
        ),
      ),
    );
  }
}
