import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppbarIcon extends StatelessWidget {
  final String iconPath;
  final String title;
  final Function()? onClick;
  final double width;
  final Color color;

  const AppbarIcon({
    required this.iconPath,
    required this.title,
    this.color = const Color(0xffDADADA),
    this.width = 60,
    this.onClick,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        width: width,
        decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xffDADADA),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 24,
              height: 24,
              child: SvgPicture.asset(
                iconPath,
                color: color,
              ),
            ),
            SizedBox(height: 4),
            Text(
              title,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
