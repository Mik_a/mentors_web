import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class CustomTabbar extends StatefulWidget {
  final List<String> headers;
  final double headerWidth;
  final double headerHeight;
  final List<Widget> children;
  final int? currentPage;
  final Widget? action;

  final Function(int) onClickHeader;

  const CustomTabbar({
    required this.headers,
    required this.children,
    required this.onClickHeader,
    this.action,
    this.currentPage,
    this.headerWidth = 80,
    this.headerHeight = 32,
    Key? key,
  }) : super(key: key);

  @override
  _CustomTabbarState createState() => _CustomTabbarState();
}

class _CustomTabbarState extends State<CustomTabbar> {
  int currentPage = 0;

  BorderSide borderSide = BorderSide(color: Colors.black.withOpacity(0.3));

  @override
  Widget build(BuildContext context) {
    int getCurrentPage() {
      return widget.currentPage == null ? currentPage : widget.currentPage!;
    }

    Widget header() {
      return Container(
        height: 50,
        child: Column(
          children: [
            SizedBox(
              height: 40,
              child: Row(
                children: [
                  SizedBox(width: 12),
                  Expanded(
                    child: ListView(
                      padding: EdgeInsets.only(left: 8),
                      scrollDirection: Axis.horizontal,
                      children: [
                        ...List.generate(
                          widget.headers.length,
                          (index) => InkWell(
                            onTap: () => setState(() {
                              widget.onClickHeader(index);
                              currentPage = index;
                            }),
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                decoration: BoxDecoration(
                                  boxShadow: getCurrentPage() == index
                                      ? [
                                          BoxShadow(
                                            blurRadius: 5,
                                            color:
                                                Colors.black.withOpacity(0.3),
                                          ),
                                        ]
                                      : [],
                                  color: getCurrentPage() == index
                                      ? Colors.white
                                      : Colors.transparent,
                                  borderRadius: getCurrentPage() != index
                                      ? null
                                      : BorderRadius.vertical(
                                          top: Radius.circular(8),
                                        ),
                                  // border: currentPage == index ? Border.all() : null,
                                ),
                                height: widget.headerHeight,
                                width: widget.headerWidth,
                                child: Center(
                                  child: Text(
                                    widget.headers[index],
                                    style: TextStyle(
                                      fontWeight: getCurrentPage() == index
                                          ? FontWeight.bold
                                          : FontWeight.normal,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (widget.action != null)
                    Align(
                      alignment: Alignment.centerRight,
                      child: widget.action!,
                    )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              height: 10,
              width: double.infinity,
              color: Colors.white,
            )
          ],
        ),
      );
    }

    return Stack(
      children: [
        Column(
          children: [
            SizedBox(height: 40),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 5,
                      color: Colors.black.withOpacity(0.3),
                    ),
                  ],
                ),
                width: double.infinity,
                height: double.infinity,
                child: widget.children[getCurrentPage()],
              ),
            )
          ],
        ),
        header(),
      ],
    );
  }

  BorderSide border = BorderSide(color: CustomColors.cC4C4C4);
}
