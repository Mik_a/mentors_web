import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class RoundedButton extends StatelessWidget {
  final num width;
  final num height;
  final Color backgroundColor;
  final String title;
  final VoidCallback onClickButton;
  final TextStyle? textStyle;
  final double borderRadius;
  final Color borderColor;

  const RoundedButton(
      {required this.title,
      required this.onClickButton,
      this.borderRadius = 4,
      this.width = 56,
      this.height = 30,
      this.backgroundColor = Colors.white,
      this.borderColor = CustomColors.cC4C4C4,
      this.textStyle = const TextStyle(fontSize: 12),
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: onClickButton,
        child: Container(
          padding: EdgeInsets.all(3),
          width: width.toDouble(),
          height: height.toDouble(),
          decoration: BoxDecoration(
            color: backgroundColor,
            border: Border.all(
              color: borderColor,
              width: 1,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(borderRadius),
            ),
          ),
          child: Center(
            child: Text(
              title,
              style: textStyle,
            ),
          ),
        ),
      ),
    );
  }
}
