import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class SquareButton extends StatelessWidget {
  final double height;
  final double? width;
  final double margin;
  final Color borderColor;
  final String title;
  final Color backgroundColor;
  final Widget? icon;
  final Function() onClick;

  const SquareButton({
    required this.title,
    required this.onClick,
    this.backgroundColor = Colors.transparent,
    this.borderColor = CustomColors.cC4C4C4,
    this.width,
    this.icon,
    this.height = 28,
    this.margin = 0,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        margin: EdgeInsets.only(left: margin),
        padding: EdgeInsets.symmetric(horizontal: 6),
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(2)),
          border: Border.all(color: borderColor),
          color: backgroundColor,
        ),
        child: Row(
          children: [
            if (icon != null) ...[
              SizedBox(
                width: 16,
                height: 16,
                child: icon!,
              ),
              SizedBox(width: 4),
            ],
            Text(
              title,
              style: TextStyle(fontSize: 12),
            ),
          ],
        ),
      ),
    );
  }
}
