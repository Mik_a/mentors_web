import 'package:flutter/material.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/global_colors.dart';

class IconInputBox extends StatelessWidget {
  final double width;
  final double height;

  final Widget icon;
  final String hintText;
  final bool obscureText;
  final TextEditingController? controller;
  final Function(String)? onClickEnter;

  final FocusNode? focusNode;

  const IconInputBox({
    this.controller,
    this.focusNode,
    this.hintText = '',
    this.onClickEnter,
    this.width = 60,
    this.height = 30,
    this.obscureText = false,
    required this.icon,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        border: Border.all(
          color: CustomColors.cE3E3E3,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(59),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 10),
              child: icon,
            ),
            VerticalDivider(width: 0),
            Expanded(
              child: InputBox(
                hintText: hintText,
                onClickEnter: onClickEnter,
                borderColor: Colors.transparent,
                obscureText: obscureText,
                focusNode: focusNode,
                controller: controller,
              ),
            ),
            SizedBox(width: 10),
          ],
        ),
      ),
    );
  }
}
