import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class NewSectionIcon extends StatelessWidget {
  final Function() onTap;
  const NewSectionIcon({required this.onTap, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(left: 24),
        width: 224,
        height: 216,
        child: Column(
          children: [
            Container(
              width: 224,
              height: 192,
              decoration: BoxDecoration(
                border: Border.all(
                  color: CustomColors.cC4C4C4,
                ),
              ),
              child: Icon(
                Icons.add_circle_rounded,
                size: 55,
                color: Color(0xffd9c87a),
              ),
            ),
            SizedBox(height: 6),
            Text("배치도 추가하기"),
          ],
        ),
      ),
    );
  }
}
