import 'package:flutter/material.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/components/section_list/section_image.dart';
import 'package:mentors_web/global_colors.dart';

class SectionItem extends StatelessWidget {
  final Section section;
  final bool isSelected;
  final Axis axis;
  const SectionItem(
      {this.isSelected = false,
      required this.section,
      required this.axis,
      Key? key})
      : super(key: key);

  Widget horizontal() {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: isSelected ? Color(0xff31CEFF) : Colors.transparent,
              width: 4,
            ),
          ),
          width: 224,
          height: 192,
          child: SectionImage(section: section),
        ),
        Text(section.name),
      ],
    );
  }

  Widget vertical() {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: isSelected ? CustomColors.cFFF4C2 : Colors.transparent,
        border: Border.all(
          color: CustomColors.cE3E3E3,
          width: 1,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(3),
        ),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 94,
            height: 84,
            child: SectionImage(section: section),
          ),
          SizedBox(width: 2),
          Expanded(
            child: Container(
              height: double.infinity,
              child: Padding(
                padding: const EdgeInsets.only(left: 14),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(section.name),
                    ),
                    isSelected
                        ? Positioned(
                            right: 0,
                            top: 0,
                            child: Icon(
                              Icons.check_circle,
                              size: 28,
                              color: CustomColors.cD4C46B,
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: 12, vertical: axis == Axis.horizontal ? 0 : 8),
      width: axis == Axis.horizontal ? 224 : 322,
      height: axis == Axis.horizontal ? 216 : 103,
      child: axis == Axis.horizontal ? horizontal() : vertical(),
    );
  }
}
