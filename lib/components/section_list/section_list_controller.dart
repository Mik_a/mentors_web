import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/section.dart';

class SectionListController extends GetxController {
  RxList<Section> sectionList = RxList();
  RxBool isLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    API.section.getSections().then((value) {
      isLoading.value = false;
      sectionList.value = value;
    });
  }

  void removeSection(Section section) {
    sectionList.remove(section);
  }

  void addSection(Section section) {
    sectionList.add(section);
  }
}
