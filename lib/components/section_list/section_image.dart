import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/components/custom_image.dart';
import 'package:mentors_web/config.dart';

class SectionImage extends StatelessWidget {
  final Function(Size)? onImageLoaded;
  final Section section;

  const SectionImage({
    this.onImageLoaded,
    required this.section,
    Key? key,
  });

  @override
  Widget build(BuildContext context) {
    Image image = new Image.network(
      '$server/upload?path=${GetStorage().read('id')}/${section.id}',
      key: section.key ?? Key('1'),
    );

    if (onImageLoaded != null)
      image.image.resolve(new ImageConfiguration()).addListener(
        ImageStreamListener(
          (ImageInfo info, bool _) {
            Size size = Size(
              info.image.width.toDouble(),
              info.image.height.toDouble(),
            );
            onImageLoaded!(size);
          },
        ),
      );

    return section.path != null ? CustomImage(path: section.path!) : image;
  }
}
