import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/components/section_list/new_section_icon.dart';
import 'package:mentors_web/components/section_list/section_item.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';

class SectionList extends StatelessWidget {
  final bool isCanAddSection;
  final Axis axis;
  final Section? selectedSection;
  final double? width;

  final Function(Section)? onClickSection;
  final Function()? onClickNewSection;

  SectionList({
    required this.selectedSection,
    required this.onClickSection,
    this.onClickNewSection,
    this.width = 280,
    this.axis = Axis.horizontal,
    this.isCanAddSection = true,
    Key? key,
  }) : super(key: key);

  final SectionListController controller = Get.put(SectionListController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: axis == Axis.horizontal ? double.infinity : width,
        height: axis == Axis.horizontal ? 320 : double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              height: 50,
              child: Align(
                child: Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Text("배치도 리스트"),
                ),
                alignment: Alignment.centerLeft,
              ),
            ),
            Expanded(
                child: Row(
              children: [
                if (isCanAddSection && onClickNewSection != null) ...[
                  NewSectionIcon(
                    onTap: onClickNewSection!,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                    ),
                    child: SizedBox(
                      height: 216,
                      child: VerticalDivider(),
                    ),
                  ),
                ],
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(right: 24),
                    height: axis == Axis.horizontal ? 216 : double.infinity,
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      scrollDirection: axis,
                      itemCount: controller.sectionList.length,
                      itemBuilder: (context, index) => InkWell(
                        onTap: () => {
                          onClickSection!(controller.sectionList[index]),
                        },
                        child: SectionItem(
                          axis: axis,
                          isSelected: selectedSection == null
                              ? false
                              : selectedSection ==
                                  controller.sectionList[index],
                          section: controller.sectionList[index],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
          ],
        ),
      ),
    );
  }
}
