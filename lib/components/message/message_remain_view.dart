import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/components/message/message_with_icon.dart';
import 'package:mentors_web/modules/lib.dart';

class MessageRemainView extends StatelessWidget {
  final bool isShowCash;
  MessageRemainView({
    this.isShowCash = true,
    Key? key,
  }) : super(key: key);
  final MessageController controller = Get.put(MessageController());

//전화번호, 좌석정보, 몇번좌석, 요금제,
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        children: [
          if (isShowCash) ...[
            SizedBox(
              height: 38,
              child: Row(
                children: [
                  Expanded(
                    child: Text("보유 캐시"),
                  ),
                  Text(
                    addCommaInMoney(controller.smsData.value.cash),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(width: 4),
                  Text("원"),
                ],
              ),
            ),
            Divider(),
            SizedBox(height: 23),
          ],
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              MessageWithIcon(
                iconName: 'sms',
                amount: controller.smsData.value.sms,
                color: Color(0xff19A980),
              ),
              MessageWithIcon(
                iconName: 'lms',
                amount: controller.smsData.value.lms,
                color: Color(0xffFACF91),
              ),
              MessageWithIcon(
                iconName: 'mms',
                amount: controller.smsData.value.mms,
                color: Color(0xffFC8F90),
              ),
            ],
          )
        ],
      ),
    );
  }
}
