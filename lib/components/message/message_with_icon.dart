import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MessageWithIcon extends StatelessWidget {
  final String iconName;
  final int amount;
  final Color color;

  MessageWithIcon(
      {required this.iconName,
      required this.amount,
      required this.color,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3),
      height: 33,
      child: Row(
        children: [
          SvgPicture.asset('assets/icons/$iconName.svg'),
          SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                iconName.toUpperCase(),
                style: TextStyle(
                  color: color,
                  fontSize: 12,
                ),
              ),
              Text(
                "${amount.toString()}건",
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
