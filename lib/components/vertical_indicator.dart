import 'dart:ui';

import 'package:flutter/material.dart';

class VerticalIndicator extends StatelessWidget {
  final double width;
  final double height;
  final Color backgroundColor;
  final Color foregroundColor;
  final List<Color>? gradientColors;
  final double value;

  const VerticalIndicator({
    required this.width,
    required this.height,
    required this.value,
    this.gradientColors,
    this.backgroundColor = Colors.grey,
    this.foregroundColor = Colors.blue,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          Positioned(
            top: height - (height * value),
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                gradient: gradientColors == null
                    ? null
                    : LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: gradientColors!,
                      ),
                color: foregroundColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
