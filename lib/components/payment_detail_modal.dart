import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/views/register_card.dart';
import 'package:mentors_web/classes/sales_log.dart';

import 'package:flutter/material.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/modules/lib.dart';

void onClickPaymentDetail(SalesLog log) {
  Get.dialog(PaymentDetailModal(
    log: log,
  ));
}

class PaymentDetailModal extends StatelessWidget {
  final SalesLog log;

  const PaymentDetailModal({
    Key? key,
    required this.log,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Modal(
      content: RegisterCard(
        width: 432,
        height: 750,
        title: "결제 상세 내역",
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 24),
              height: 180,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  text("상품", log.itemName),
                  text(
                      "결제수단",
                      log.isWeb
                          ? log.payType1 == 'cash'
                              ? '현금'
                              : '카드'
                          : log.payType1 == 'cash'
                              ? '현금'
                              : log.payType1 ?? '카드'),
                  text("결제방식", log.isWeb ? "운영PC" : "키오스크"),
                  text("구입회원", log.userName ?? "-"),
                  text("결제일시", getKoreaTime(log.eventTime)),
                  text("할부", log.installment.toString()),
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 24.0),
              child: Container(
                height: 38,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    text(
                      "결제금액",
                      addCommaInMoney(log.price) + "원",
                      width: 55,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    text(
                        "부가세포함",
                        addCommaInMoney(int.tryParse(log.vat ?? '0') ?? 0) +
                            "원",
                        width: 60),
                  ],
                ),
              ),
            ),
            if (log.payDevice == PayDevice.kiosk)
              Container(
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 16),
                width: 396,
                height: 190,
                color: Color(0xfff8f8f8),
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    children: [
                      Container(
                        height: 48,
                        child: Center(
                          child: Text(
                            "승인정보",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 14.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  text2("승인번호", log.code.toString()),
                                  text2("가맹점번호", log.cat ?? '-'),
                                ],
                              ),
                              Row(
                                children: [
                                  text2("카드번호", log.cardNum ?? '-'),
                                  text2("승인일시", log.approvedTime ?? '-'),
                                ],
                              ),
                              Row(
                                children: [
                                  text2("카드사", log.payType1 ?? '-'),
                                  text2("매입사", log.payType2 ?? '-'),
                                ],
                              ),
                              text2("결과", log.result ?? '-'),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            Expanded(child: Container()),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: CustomButton(
                onPressed: Get.back,
                width: 120,
                height: 40,
                title: "확인",
              ),
            )
          ],
        ),
      ),
    );
  }

  Row text2(String title, String value) {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 15),
          width: 55,
          child: Text(
            title,
            style: TextStyle(
              fontSize: 12,
              color: Color(0xff717071),
            ),
          ),
        ),
        Container(
          width: 110,
          child: Text(
            value,
            style: TextStyle(
              fontSize: 12,
              color: Color(0xff242424),
            ),
          ),
        ),
      ],
    );
  }

  Row text(
    String title,
    String value, {
    double width = 44,
    TextStyle style = const TextStyle(
      fontSize: 12,
      color: Color(0xff242424),
    ),
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        AutoSpacingText(
          text: title,
          width: width,
          style: style,
        ),
        Text(
          value,
          style: style,
        ),
      ],
    );
  }
}
