import 'package:flutter/material.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/global_colors.dart';

class SeatTypeItem extends StatelessWidget {
  final SeatType seatType;
  final bool isShowDeleteButton;
  final bool isSelected;
  final Function()? onClickDelete;

  const SeatTypeItem({
    Key? key,
    this.isShowDeleteButton = false,
    this.isSelected = false,
    required this.seatType,
    this.onClickDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12, left: 11, bottom: 8),
      decoration: BoxDecoration(
        color: isSelected ? CustomColors.cFFF4C2 : Colors.transparent,
      ),
      width: 418,
      child: Row(
        children: [
          // Container(
          //   decoration: BoxDecoration(border: Border.all()),
          //   child: Text("이미지"),
          // ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                seatType.name,
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                seatType.description == ''
                    ? "좌석 설명이 없습니다"
                    : seatType.description,
                style: TextStyle(
                  color: CustomColors.cA3A2A3,
                  fontSize: 12,
                ),
              ),
              if (isShowDeleteButton)
                ElevatedButton(
                  onPressed: onClickDelete,
                  child: Text("삭제"),
                )
            ],
          ),
        ],
      ),
    );
  }
}
