import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/global_enums.dart';

class SeatController extends GetxController {
  TextEditingController seatNameController = TextEditingController();
  TextEditingController seatMemoController = TextEditingController();
  RxList<SeatType> seatTypeList = RxList();
  bool isLoading = true;

  @override
  void onInit() {
    super.onInit();
    API.seat
        .getAllSeatTypes()
        .then(seatTypeList.addAll)
        .then((value) => isLoading = false);
  }

  void onClickDelete(SeatGroup group, SeatType type) {
    API.seat.removeSeatType(group, type).then(
      (value) {
        if (value != null) {
          seatTypeList.remove(type);
          Get.back();
        } else {
          Get.back();
        }
      },
    );
  }

  List<SeatType> getSeatTypeFromGroup(SeatGroup group) {
    return seatTypeList.where((e) => e.seatGroup == group).toList();
  }
}
