import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/components/seat_management/seat_controller.dart';
import 'package:mentors_web/components/seat_management/seat_type_item.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/global_enums.dart';

class SeatTypeList extends StatelessWidget {
  final SeatController controller = Get.put(SeatController());
  final bool isShowDeleteButton;
  final bool isCanSelect;
  final SeatType? selectedSeatType;
  final double width;
  final Function(SeatType)? onClickItem;

  SeatTypeList({
    Key? key,
    this.isShowDeleteButton = false,
    this.isCanSelect = false,
    this.selectedSeatType,
    this.width = 200,
    this.onClickItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        child: ListView.separated(
          padding: EdgeInsets.zero,
          itemBuilder: (context, index) =>
              makeSeatGroup(SeatGroup.values[index]),
          separatorBuilder: (context, index) => SizedBox(height: 8),
          itemCount: SeatGroup.values.length,
        )

        // ListView.separated(
        //   separatorBuilder: (context, index) => SizedBox(height: 4),
        //   padding: EdgeInsets.zero,
        //   itemCount: controller.seatTypeList.length,
        //   itemBuilder: (context, index) => InkWell(
        //     onTap: onClickItem != null
        //         ? () {
        //             onClickItem!(controller.seatTypeList[index]);
        //           }
        //         : () {},
        //     child: SeatTypeItem(
        //       isSelected: selectedSeatType == null
        //           ? false
        //           : selectedSeatType! == controller.seatTypeList[index],
        //       isShowDeleteButton: isShowDeleteButton,
        //       seatType: controller.seatTypeList[index],
        //     ),
        //   ),
        // ),
        );
  }

  Container makeSeatGroup(SeatGroup seatGroup) {
    var header = Container(
      height: 40,
      child: Row(
        children: [
          SizedBox(width: 12),
          Text(
            seatGroup.toKor(),
            style: TextStyle(
              fontSize: 12,
            ),
          ),
        ],
      ),
    );

    List<Widget> list = controller
        .getSeatTypeFromGroup(seatGroup)
        .map(
          (e) => InkWell(
            onTap: () {
              if (onClickItem != null) onClickItem!(e);
            },
            child: SeatTypeItem(
              seatType: e,
              isSelected: e == selectedSeatType,
            ),
          ),
        )
        .toList();

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: CustomColors.cE3E3E3),
      ),
      child: ExpandablePanel(
        theme: ExpandableThemeData(),
        header: header,
        expanded: Column(
          children: List.generate(
            list.length * 2,
            (index) => index % 2 != 0 ? list[index ~/ 2] : Divider(height: 0),
          ),
        ),
        collapsed: Container(),
      ),
    );
  }
}
