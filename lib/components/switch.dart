import 'package:flutter/material.dart';

class CustomSwitch extends StatelessWidget {
  final bool value;
  final Function(bool) onTap;

  const CustomSwitch({required this.value, required this.onTap, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(!value),
      child: Container(
        padding: EdgeInsets.all(2),
        width: 36,
        height: 20,
        decoration: BoxDecoration(
          color: value ? Color(0xffbbffa3) : Color(0xffFF8888),
          boxShadow: [
            BoxShadow(
              blurRadius: 5,
              spreadRadius: -5,
            ),
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(40),
          ),
        ),
        child: Stack(
          children: [
            AnimatedPositioned(
              duration: Duration(milliseconds: 100),
              right: value ? 0 : 32 - 16,
              child: Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(999),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
