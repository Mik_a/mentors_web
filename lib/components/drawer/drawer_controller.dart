import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/logs.dart';

enum DrawerViewType { users, userLogs, paymentLogs }

class CustomDrawerController extends GetxController
    with SingleGetTickerProviderMixin {
  RxBool isCollapsed = false.obs;

  Rx<DrawerViewType> viewType = DrawerViewType.users.obs;
  late AnimationController animationController;

  RxString filterString = ''.obs;
  @override
  void onInit() {
    super.onInit();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
  }

  void filterOnChange(String value) {
    filterString.value = value;
  }

  void toggle() {
    animationController.forward();
  }

  void toggleDrawer() {
    isCollapsed.value = !isCollapsed.value;
  }

  void onClickChangeDrawerTypeButton(DrawerViewType type) {
    viewType.value = type;
  }
}
