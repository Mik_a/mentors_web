import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class DropBox extends StatelessWidget {
  final Object? currentValue;
  final List<dynamic> values;
  final List<String>? textList;
  final double height;
  final double width;
  final TextStyle? textStyle;
  final String? hintText;

  final Function(dynamic v) onChanged;

  const DropBox({
    required this.onChanged,
    required this.currentValue,
    required this.values,
    this.hintText,
    this.textStyle = const TextStyle(color: Colors.black),
    this.textList,
    this.height = 24,
    this.width = 44,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: CustomColors.ce4e4e4,
        ),
      ),
      child: DropdownButton(
        isExpanded: true,
        underline: Container(),
        value: currentValue,
        iconEnabledColor: Color(0xff008c66),
        iconDisabledColor: Color(0xff008c66),
        onChanged: onChanged,
        hint: hintText != null
            ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  hintText!,
                  style: TextStyle(fontSize: 12),
                ),
              )
            : null,
        items: [
          ...List.generate(
            values.length,
            (index) => DropdownMenuItem(
              value: values[index],
              child: Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: Text(
                  textList == null
                      ? values[index].toString()
                      : textList![index],
                  style: textStyle,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
