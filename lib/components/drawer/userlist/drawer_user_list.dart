import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/drawer/userlist/drawer_user_list_item.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DrawerUserList extends StatelessWidget {
  final UserController userController = Get.find();
  final String? filter;

  DrawerUserList({
    this.filter,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      List<User> filteredUserList = userController.userList;
      filteredUserList = filteredUserList
          .where(
            (element) =>
                element.name.contains(filter!.replaceAll('-', '')) ||
                element.phone.contains(filter!.replaceAll('-', '')),
          )
          .toList();

      return Expanded(
        child: SmartRefresher(
          controller: userController.refreshController,
          onRefresh: userController.refresh,
          enablePullDown: true,
          child: filteredUserList.length == 0
              ? userController.isLoading.isTrue
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                          CircularProgressIndicator(),
                          SizedBox(height: 20),
                          Text("회원 정보를 가져오는 중 입니다"),
                        ])
                  : Center(
                      child: Text("회원이 없습니다"),
                    )
              : ListView.separated(
                  // physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => DrawerUserListItem(
                    user: filteredUserList[index],
                  ),
                  separatorBuilder: (context, index) => Divider(),
                  itemCount: filteredUserList.length,
                ),
        ),
      );
    });
  }
}
