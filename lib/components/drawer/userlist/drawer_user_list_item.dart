import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/components/rounded_button.dart';
import 'package:mentors_web/global_enums.dart';

class DrawerUserListItem extends StatelessWidget {
  final User user;
  final bool isDragable;
  final bool isPlaced;

  const DrawerUserListItem({
    this.isPlaced = false,
    this.isDragable = false,
    required this.user,
    Key? key,
  }) : super(key: key);

  Widget view() {
    return Opacity(
      opacity: user.status == UserEnterStatus.exit ? 0.7 : 1,
      child: Container(
        height: 68,
        padding: EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 8,
        ),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: 38,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: user.status == UserEnterStatus.enter
                          ? Color(0xff23c7d1)
                          : user.status == UserEnterStatus.out
                              ? Color(0xffffb966)
                              : Color(0xffC9C9C9),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: user.status == UserEnterStatus.enter
                        ? Color(0xff23c7d1)
                        : user.status == UserEnterStatus.out
                            ? Color(0xffffb966)
                            : Color(0xffd7d7d7),
                  ),
                  child: Center(
                    child: Text(
                      user.status == UserEnterStatus.enter
                          ? "입실"
                          : user.status == UserEnterStatus.out
                              ? "외출"
                              : "퇴실",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: Row(
                    children: [
                      Text(
                        user.isRegister ? user.name : "손님 (${user.phone}) ",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(width: 2),
                      user.gender == Gender.none
                          ? SizedBox()
                          : SvgPicture.asset(
                              user.gender == Gender.male
                                  ? 'assets/icons/male.svg'
                                  : 'assets/icons/female.svg',
                            ),
                    ],
                  ),
                ),
                RoundedButton(
                  width: 60,
                  height: 24,
                  title: "상세보기",
                  borderRadius: 10,
                  textStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                  ),
                  onClickButton: () {
                    UserRegisterController userRegisterController = Get.find();
                    userRegisterController.toDetailView(user);
                  },
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              children: [
                SvgPicture.asset('assets/icons/chair.svg'),
                SizedBox(width: 2),
                if (user.seatData != null) ...[
                  Text(user.seatData!.idx.toString() + "번 "),
                  Text(user.seatData!.name),
                ],
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: view(),
    );
  }
}
