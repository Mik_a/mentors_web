import 'package:flutter/material.dart';
import 'package:mentors_web/components/search_input.dart';

class DrawerInput extends StatelessWidget {
  final Function(String) onChange;

  const DrawerInput({
    Key? key,
    required this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(
          color: Color(0xffDADADA),
          width: 0.5,
        ),
      )),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          height: 40,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(5),
            ),
          ),
          child: SearchInput(onChange: onChange),
        ),
      ),
    );
  }
}
