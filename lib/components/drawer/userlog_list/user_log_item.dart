import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/home/controllers/home_controller.dart';
import 'package:mentors_web/app/modules/user_register/controllers/user_register_controller.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/controller/user_controller.dart';
import 'package:mentors_web/modules/lib.dart';

class UserLogItem extends StatelessWidget {
  final IOLog ioLog;
  final bool showDetail;
  final double? height;
  const UserLogItem({
    required this.ioLog,
    this.showDetail = true,
    this.height,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: EdgeInsets.all(5),
      child: Row(
        children: [
          Text(
            getKoreaTime(ioLog.time).substring(5, 16),
            style: TextStyle(
              color: Colors.grey,
              fontSize: 12,
            ),
          ),
          SizedBox(width: 10),
          Text(
            ioLog.name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
          SizedBox(width: 4),
          Text(
            ioLog.ioType.name,
            style: TextStyle(
              color: ioLog.ioType.color,
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(width: 4),
          Expanded(
            child: ioLog.ioType == IOType.moveFrom ||
                    ioLog.ioType == IOType.moveTo
                ? Text(
                    "${ioLog.prevSeatName} (${ioLog.fromIndex}번) => ${ioLog.seatName} (${ioLog.seatIndex}번)",
                    style: TextStyle(color: Color(0xff717071), fontSize: 12),
                  )
                : Text(
                    '${ioLog.seatIndex}번 ${ioLog.seatName} ${ioLog.ioType.status}',
                    style: TextStyle(color: Color(0xff717071), fontSize: 12),
                  ),
          ),
          if (showDetail)
            GestureDetector(
              onTap: () {
                UserController userController = Get.find();
                User user = userController.userList
                    .firstWhere((element) => element.id == ioLog.userId);
                UserRegisterController userRegisterController = Get.find();
                userRegisterController.setCurrentUser(user);
                HomeController homeController = Get.find();
                homeController.showUserInfo();
              },
              child: Container(
                margin: EdgeInsets.only(right: 12),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xffa3a2a3),
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                width: 60,
                height: 24,
                child: Center(
                  child: Text(
                    "상세보기",
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ),
            ),
          // RoundedButton(
          //   borderRadius: 3,
          //   title: "상세보기",
          //   onClickButton: () {
          //     HomeController homeController = Get.find();
          //     homeController.setPage(2);
          //   },
          //   backgroundColor: Color(0xff888f96),
          //   width: 60,
          //   height: 30,
          // )
        ],
      ),
    );
  }
}
