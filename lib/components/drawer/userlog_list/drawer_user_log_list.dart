import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/drawer/userlog_list/user_log_item.dart';
import 'package:mentors_web/controller/io_log_controller.dart';

class DrawerUserLogList extends StatelessWidget {
  DrawerUserLogList({
    Key? key,
  }) : super(key: key);

  final IoLogController controller = Get.put(IoLogController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Expanded(
        child: RefreshIndicator(
          onRefresh: controller.onRefresh,
          child: controller.isLoading.value
              ? Center(child: CircularProgressIndicator())
              : ListView.separated(
                  itemCount: controller.userLogList.length +
                      (controller.isEnd.value ? 0 : 1),
                  separatorBuilder: (context, index) => Divider(),
                  itemBuilder: (context, index) {
                    if (index == controller.userLogList.length) {
                      controller.updateLogs();

                      return Container(
                        height: 50,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }

                    return UserLogItem(
                      ioLog: controller.userLogList[index],
                    );
                  },
                ),
        ),
      ),
    );
  }
}
