import 'package:flutter/material.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/components/payment_detail_modal.dart';
import 'package:mentors_web/global_colors.dart';
import 'package:mentors_web/modules/lib.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DrawerPaymentLogList extends StatefulWidget {
  const DrawerPaymentLogList({Key? key}) : super(key: key);
  @override
  _DrawerPaymentLogListState createState() => _DrawerPaymentLogListState();
}

class _DrawerPaymentLogListState extends State<DrawerPaymentLogList> {
  List<SalesLog> salesLogList = [];
  late DateTime lastTime;
  int tryNum = 0;
  bool isLoading = true;

  RefreshController refreshController = RefreshController();
  @override
  void initState() {
    super.initState();
    lastTime = DateTime.now();
    getLogs(lastTime);
  }

  void init() {
    setState(() {
      lastTime = DateTime.now();
      salesLogList = [];
      getLogs(lastTime);
      refreshController.refreshCompleted();
    });
  }

  void getLogs(DateTime time) {
    API.store.getSalesLog(time.year, time.month).then(
          (value) => setState(
            () {
              if (value.length == 0) {
                tryNum++;
              } else {
                tryNum = 0;
              }

              lastTime = DateTime(time.year, time.month - 1);
              salesLogList.addAll(value);
            },
          ),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SmartRefresher(
        controller: refreshController,
        onRefresh: init,
        child: ListView.separated(
          padding: EdgeInsets.zero,
          separatorBuilder: (context, index) => Divider(height: 1),
          itemCount: salesLogList.length + (tryNum < 3 ? 1 : 0),
          itemBuilder: (context, index) {
            if (index == salesLogList.length) {
              getLogs(lastTime);
              return Container(
                height: 50,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              SalesLog log = salesLogList[index];
              return PaymentLogItem(log: log);
            }
          },
        ),
      ),
    );
  }
}

class PaymentLogItem extends StatelessWidget {
  final SalesLog log;
  PaymentLogItem({
    required this.log,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      height: 84,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: 58,
                    height: 20,
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: log.payDevice == PayDevice.pc
                              ? Color(0xffbeaf6b)
                              : Color(0xff008C8C)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      color: log.payDevice == PayDevice.pc
                          ? Color(0xffd9c87a)
                          : Color(0xff00a6a6),
                    ),
                    child: Center(
                      child: Text(
                        log.payDevice.name,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 8),
                  Text(
                    log.userName ?? '-',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff242424),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Text(
                getKoreaTime(log.eventTime) + "  ${log.payMethod.name}결제",
                style: TextStyle(
                  color: CustomColors.cA3A2A3,
                  fontSize: 12,
                ),
              ),
              Text(
                log.itemName,
                // log.voucherType!.toKor(),
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () => onClickPaymentDetail(log),
                child: Container(
                  width: 60,
                  height: 24,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: CustomColors.cA3A2A3,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      "결제상세",
                      style: TextStyle(
                        color: CustomColors.c242424,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                children: [
                  Text(
                    (log.price > 0
                            ? "+"
                            : log.price == 0
                                ? ''
                                : "-") +
                        addCommaInMoney(log.price),
                    style: TextStyle(
                      color: log.price > 0
                          ? CustomColors.c19A980
                          : log.price == 0
                              ? Colors.black
                              : Color(0xffff6b6b),
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    " 원",
                    style: TextStyle(
                      color: CustomColors.c242424,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
