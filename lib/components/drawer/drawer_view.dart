import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/drawer/drawer_controller.dart';
import 'package:mentors_web/components/drawer/drawer_input.dart';
import 'package:mentors_web/components/drawer/drawer_payment_list.dart';
import 'package:mentors_web/components/drawer/userlist/drawer_user_list.dart';
import 'package:mentors_web/components/drawer/drawer_type_changer.dart';
import 'package:mentors_web/components/drawer/userlog_list/drawer_user_log_list.dart';

class DrawerView extends StatelessWidget {
  final bool isCollapsed;
  final CustomDrawerController controller = Get.put(CustomDrawerController());

  DrawerView({required this.isCollapsed});

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.symmetric(
          vertical: BorderSide(
            width: 0.5,
            color: Color(0xffDADADA),
          ),
        ),
      ),
      width: isCollapsed ? 0 : 400,
      child: isCollapsed
          ? Container()
          : OverflowBox(
              child: Column(
                children: [
                  DrawerInput(onChange: controller.filterOnChange),
                  Obx(
                    () => controller.viewType.value == DrawerViewType.users
                        ? DrawerUserList(
                            filter: controller.filterString.value,
                          )
                        : controller.viewType.value == DrawerViewType.userLogs
                            ? DrawerUserLogList()
                            : DrawerPaymentLogList(),
                  ),
                  DrawerTypeChanger(controller: controller),
                ],
              ),
            ),
    );
  }
}
