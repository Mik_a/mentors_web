import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/drawer/drawer_controller.dart';
import 'package:mentors_web/global_colors.dart';

class DrawerTypeChanger extends StatelessWidget {
  final CustomDrawerController controller;
  const DrawerTypeChanger({
    Key? key,
    required this.controller,
  }) : super(key: key);

  Widget button(String title, DrawerViewType type) {
    return Expanded(
      child: InkWell(
        onTap: () {
          controller.onClickChangeDrawerTypeButton(type);
        },
        child: Container(
          child: Stack(
            children: [
              Center(
                child: Text(
                  title,
                  style: TextStyle(
                    fontWeight: type == controller.viewType.value
                        ? FontWeight.bold
                        : FontWeight.normal,
                    color: type == controller.viewType.value
                        ? CustomColors.c242424
                        : CustomColors.c717071,
                  ),
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                height: 4,
                child: Container(
                  color: type == controller.viewType.value
                      ? kMaterialColor
                      : Colors.transparent,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(color: Color(0xffEFEFEF)),
          ),
        ),
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            button("회원목록", DrawerViewType.users),
            VerticalDivider(
              thickness: 2,
              width: 0,
            ),
            button("회원로그", DrawerViewType.userLogs),
            VerticalDivider(
              thickness: 2,
              width: 0,
            ),
            button("결제로그", DrawerViewType.paymentLogs),
          ],
        ),
      ),
    );
  }
}
