import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RadioButton extends StatelessWidget {
  final String? title;
  final Function(dynamic)? onClick;
  final dynamic value;
  final bool isOn;

  const RadioButton({
    this.title,
    this.value,
    this.onClick,
    this.isOn = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick == null ? null : () => onClick!(value),
      child: Container(
        child: Row(
          children: [
            isOn
                ? SvgPicture.asset('assets/icons/radio_on.svg')
                : SvgPicture.asset('assets/icons/radio_off.svg'),
            if (title != null) ...[
              SizedBox(width: 4),
              Text(
                title!,
                style: TextStyle(fontSize: 12),
              )
            ]
          ],
        ),
      ),
    );
  }
}
