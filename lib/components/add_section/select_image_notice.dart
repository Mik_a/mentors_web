import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mentors_web/global_colors.dart';

class SelectImageNotice extends StatelessWidget {
  final Function()? onClick;
  final String title;

  const SelectImageNotice({
    Key? key,
    required this.title,
    this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 51),
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            width: constraints.maxHeight + 100,
            child: InkWell(
              onTap: onClick,
              child: DottedBorder(
                color: CustomColors.cC4C4C4,
                strokeWidth: 4,
                dashPattern: [8, 12],
                strokeCap: StrokeCap.round,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/icons/album.svg'),
                      SizedBox(height: 26),
                      Text(title),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
