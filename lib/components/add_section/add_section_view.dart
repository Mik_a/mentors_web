import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/add_section/new_section_form.dart';
import 'package:mentors_web/components/add_section/add_section_controller.dart';
import 'package:mentors_web/components/custom_image.dart';
import 'package:mentors_web/components/section_list/section_image.dart';
import 'package:mentors_web/components/section_list/section_list.dart';
import 'package:mentors_web/components/add_section/select_image_notice.dart';

class AddSectionView extends StatelessWidget {
  AddSectionView({Key? key}) : super(key: key);
  final AddSectionController controller = Get.put(AddSectionController());
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            NewSectionForm(controller: controller),
            Expanded(
              child: controller.path.value == ''
                  ? SelectImageNotice(
                      onClick: controller.onClickAddImage,
                      title: "등록된 이미지가 없습니다",
                    )
                  : (controller.isModifing.value &&
                          !controller.isModifiedMap.value)
                      ? SectionImage(
                          section: controller.currentModifingSection!)
                      : CustomImage(path: controller.path.value),
            ),
            SectionList(
              isCanAddSection: true,
              onClickNewSection: controller.onClickNewSection,
              selectedSection: controller.selectdSection,
              onClickSection: controller.onClickSection,
            ),
          ],
        ),
      ),
    );
  }
}
