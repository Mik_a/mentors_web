import 'dart:math';

import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mentors_web/api/api.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/components/alert.dart';
import 'package:mentors_web/components/modal.dart';
import 'package:mentors_web/components/section_list/section_list_controller.dart';
import 'package:mentors_web/config.dart';

class AddSectionController extends GetxController {
  Rx<String> path = ''.obs;
  TextEditingController titleController = TextEditingController();

  Rx<bool> isModifing = false.obs;
  Rx<bool> isModifiedMap = false.obs;
  Section? currentModifingSection;
  XFile? file;

  Section? selectdSection;

  void onClickAddImage() async {
    XFile? f = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (f != null) {
      file = f;
      path.value = f.path;
      if (isModifing.isTrue) isModifiedMap.value = true;
    }
  }

  void onClickNewSection() {
    isModifing.value = false;
    isModifiedMap.value = false;
    currentModifingSection = null;
    titleController.text = '';
    path.value = '';
    selectdSection = null;
  }

  void onClickSection(Section section) async {
    file = null;
    selectdSection = section;
    isModifing.value = true;
    isModifiedMap.value = false;
    currentModifingSection = section;
    titleController.text = section.name;
    path.value = section.id;
  }

  void onClickDelete() async {
    if (currentModifingSection == null) {
      EasyLoading.showError("삭제할 배치도를 선택 후 눌러주세요");
      return;
    }

    void delete() async {
      Get.back();
      EasyLoading.show(status: "잠시만 기다려주세요");
      bool res = await API.section.deleteSection(currentModifingSection!);

      if (res) {
        EasyLoading.showSuccess("삭제 완료");
        SectionListController sectionListController = Get.find();
        sectionListController.removeSection(currentModifingSection!);
        onClickNewSection();
      }
    }

    Get.dialog(
      Alert(
        alertType: AlertType.warning,
        content: "배치도를 삭제하시겠습니까?",
        onConfirm: delete,
        title: "경고",
      ),
    );
  }

  void onClickModify() async {
    if (titleController.text == '') {
      EasyLoading.showError("배치도의 이름을 설정해주세요");
      return;
    }
    EasyLoading.show(status: "잠시만 기다려주세요");

    final section = await API.section.modifySection(
      selectdSection!.id,
      titleController.text,
      file,
    );

    if (section) {
      EasyLoading.showSuccess("수정이 완료되었습니다.");

      removeCache(selectdSection!.id);

      selectdSection!.key = Key(Random(5).toString());
      selectdSection!.name = titleController.text;
      if (file != null) selectdSection!.path = file!.path;
      final SectionListController controller = Get.put(SectionListController());
      controller.sectionList.refresh();
    }
  }

  void removeCache(String id) {
    final url = '$server/upload?path=${GetStorage().read('id')}/$id';
    final NetworkImage provider = NetworkImage(url);
    provider.evict().then<void>((bool success) {
      if (success) debugPrint('removed image!');
    });
  }

  void onClickSubmit() async {
    if (path.value == '') {
      EasyLoading.showError("배치도를 먼저 선택해주세요");
      return;
    }

    if (titleController.text == '') {
      EasyLoading.showError("배치도의 이름을 설정해주세요");
      return;
    }

    EasyLoading.show(status: "잠시만 기다려주세요");
    final res = await API.section.uploadSection(titleController.text, file!);
    if (res.data['success']) {
      EasyLoading.showSuccess("등록이 완료되었습니다.");
      path.value = '';
      titleController.text = '';
      final data = res.data['data'];

      SectionListController sectionListController = Get.find();
      Section newSection = Section(id: data['_id'], name: data['name']);
      sectionListController.addSection(newSection);
    } else {
      EasyLoading.showError("등록에 실패하였습니다. 인터넷 연결상태를 확인 해 주세요");
    }
  }
}
