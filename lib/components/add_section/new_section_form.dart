import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mentors_web/components/add_section/add_section_controller.dart';
import 'package:mentors_web/components/hover_icon.dart';
import 'package:mentors_web/components/input_box.dart';
import 'package:mentors_web/components/rounded_button.dart';

class NewSectionForm extends StatelessWidget {
  final AddSectionController controller;

  const NewSectionForm({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 523,
      height: 60,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 16,
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: HoverIcon(
                  onClick: controller.onClickDelete,
                  color: Color(0xff717071),
                  hoverColor: Color(0xffFF6363),
                  icon: Icons.delete_outline,
                ),
              ),
              VerticalDivider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: HoverIcon(
                  onClick: controller.onClickAddImage,
                  color: Color(0xff717071),
                  hoverColor: Color(0xffD9C87A),
                  icon: Icons.add_photo_alternate_outlined,
                ),
              ),
              VerticalDivider(),
              SizedBox(width: 2),
              InputBox(
                width: 305,
                height: 32,
                controller: controller.titleController,
                hintText: "배치도 이름",
              ),
              SizedBox(width: 10),
              RoundedButton(
                width: 62,
                height: 28,
                title: controller.isModifing.value ? "수정하기" : "등록하기",
                onClickButton: controller.isModifing.value
                    ? controller.onClickModify
                    : controller.onClickSubmit,
              )
            ],
          ),
        ),
      ),
    );
  }
}
