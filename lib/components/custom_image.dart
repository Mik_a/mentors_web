import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class CustomImage extends StatelessWidget {
  final String path;

  const CustomImage({
    required this.path,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return kIsWeb ? Image.network(path) : Image.file(File(path));
  }
}
