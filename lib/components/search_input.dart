import 'package:flutter/material.dart';

class SearchInput extends StatelessWidget {
  final Function(String) onChange;
  final bool isMobile;

  const SearchInput({
    this.isMobile = false,
    required this.onChange,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(
          color: isMobile ? Colors.transparent : Color(0xff969696),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 3,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            child: Icon(
              Icons.search,
              color: isMobile ? Color(0xff717071) : Color(0xff00a6a6),
            ),
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5),
                topLeft: Radius.circular(5),
              ),
            ),
          ),
          if (!isMobile)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: VerticalDivider(width: 0),
            ),
          Expanded(
            child: TextField(
              onChanged: onChange,
              textAlign: TextAlign.left,
              textAlignVertical: TextAlignVertical.center,
              decoration: InputDecoration(
                hintText: "이름 또는 전화번호로 검색해주세요.",
                contentPadding: EdgeInsets.only(top: 2, left: 10),
                hintStyle: TextStyle(color: Color(0xff969696), fontSize: 14),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
