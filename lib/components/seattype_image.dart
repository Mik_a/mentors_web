import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/config.dart';

class SeatTypeImage extends StatelessWidget {
  final Function(Size)? onImageLoaded;
  final String id;
  const SeatTypeImage({
    this.onImageLoaded,
    required this.id,
    Key? key,
  });

  @override
  Widget build(BuildContext context) {
    Image image = new Image.network(
      '$server/upload?path=${GetStorage().read('id')}/seatThumbnail/$id',
      errorBuilder: (context, error, stackTrace) => DottedBorder(
        dashPattern: [8, 10],
        child: Center(
          child: Text("썸네일이 없습니다"),
        ),
      ),
    );

    if (onImageLoaded != null)
      image.image.resolve(new ImageConfiguration()).addListener(
        ImageStreamListener(
          (ImageInfo info, bool _) {
            onImageLoaded!(
              Size(
                info.image.width.toDouble(),
                info.image.height.toDouble(),
              ),
            );
          },
        ),
      );
    return SizedBox(
      width: 284,
      height: 104,
      child: image,
    );
  }
}
