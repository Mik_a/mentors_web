import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/global_colors.dart';

enum AlertType { warning, alert }

class Alert extends StatelessWidget {
  final String title;
  final Function() onConfirm;
  final Function()? onCancle;
  final String? cancleTitle;
  final String confirmTitle;
  final String content;
  final Widget? action;
  final AlertType alertType;

  const Alert({
    this.onCancle,
    this.cancleTitle,
    this.confirmTitle = '확인',
    this.content = '',
    this.action,
    required this.alertType,
    required this.onConfirm,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        margin: EdgeInsets.only(top: 8),
        width: 300,
        height: 180,
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: Column(
          children: [
            SizedBox(
              height: 44,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Divider(height: 0),
            SizedBox(height: 23),
            Expanded(
              child: Align(
                  alignment: Alignment.topLeft,
                  child: action != null ? action! : Text(content)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                if (cancleTitle != null) ...[
                  SizedBox(
                    width: 72,
                    height: 36,
                    child: CustomButton(
                      title: cancleTitle!,
                      onPressed: onCancle ?? Get.back,
                      type: CustomButtonType.cancle,
                    ),
                  ),
                  SizedBox(width: 8),
                ],
                SizedBox(
                  width: 100,
                  height: 36,
                  child: CustomButton(
                    backgroundColor: alertType == AlertType.warning
                        ? Color(0xffE85B48)
                        : CustomColors.cD9C87A,
                    title: confirmTitle,
                    type: CustomButtonType.confirm,
                    onPressed: onConfirm,
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}
