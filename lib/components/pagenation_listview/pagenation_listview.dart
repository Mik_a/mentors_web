import 'dart:math';
import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class PagenationListView extends StatefulWidget {
  final List<Widget> headerTitles;
  final List<List<Widget>> data;
  final double headerHeight;
  final Color headerColor;
  final TextStyle headerTextStyle;
  final int? itemPerPage;
  final int currentPage;
  final int? selectedIndex;
  final bool isLoading;
  final EdgeInsetsGeometry padding;
  final Function(int)? onClick;
  final double? itemHeight;
  final List<double?>? headerWidths;

  const PagenationListView({
    Key? key,
    this.headerWidths,
    this.headerHeight = 30,
    this.headerColor = CustomColors.cf5f5f5,
    this.headerTextStyle = const TextStyle(color: CustomColors.cA3A2A3),
    this.itemPerPage,
    this.currentPage = 0,
    this.isLoading = false,
    this.padding = const EdgeInsets.all(8),
    this.onClick,
    this.selectedIndex,
    this.itemHeight,
    required this.data,
    required this.headerTitles,
  }) : super(key: key);

  @override
  _PagenationListViewState createState() => _PagenationListViewState();
}

class _PagenationListViewState extends State<PagenationListView> {
  @override
  Widget build(BuildContext context) {
    List<List<Widget>> viewList = widget.itemPerPage == null
        ? widget.data
        : widget.data.sublist(
            widget.itemPerPage! * widget.currentPage,
            min(widget.itemPerPage! * (widget.currentPage + 1),
                widget.data.length));

    return Container(
      padding: widget.padding,
      height: double.infinity,
      width: double.infinity,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: widget.headerColor,
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            height: widget.headerHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ...List.generate(
                  widget.headerTitles.length,
                  (index) => widget.headerWidths == null
                      ? Expanded(
                          child: Center(child: widget.headerTitles[index]),
                        )
                      : Container(
                          width: widget.headerWidths?[index] ?? double.infinity,
                          child: Center(child: widget.headerTitles[index]),
                        ),
                ),
              ],
            ),
          ),
          Expanded(
            child: widget.isLoading
                ? Center(child: CircularProgressIndicator())
                : ListView.separated(
                    padding: EdgeInsets.symmetric(vertical: 7),
                    itemBuilder: (context, index) => InkWell(
                          onTap: widget.onClick == null
                              ? null
                              : () {
                                  widget.onClick!(index);
                                },
                          child: Container(
                            height: widget.itemHeight,
                            color: index == widget.selectedIndex
                                ? CustomColors.cFFF4C2
                                : Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ...List.generate(
                                  viewList[index].length,
                                  (index2) => widget.headerWidths == null
                                      ? Expanded(child: viewList[index][index2])
                                      : Container(
                                          width: widget.headerWidths != null
                                              ? widget.headerWidths![index2]
                                              : null,
                                          child: viewList[index][index2],
                                        ),
                                )
                              ],
                            ),
                          ),
                        ),
                    separatorBuilder: (context, index) => Divider(height: 1),
                    itemCount: viewList.length),
          ),
        ],
      ),
    );
  }
}
