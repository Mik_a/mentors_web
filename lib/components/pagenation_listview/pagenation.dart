import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mentors_web/components/drawer/drop_box.dart';
import 'package:mentors_web/global_colors.dart';

class Pagenation extends StatelessWidget {
  final int currentPage;
  final int totalLength;
  final int itemPerPage;
  final int numberOfPages;
  final Function(int) setPage;
  final double height;
  final Color selectedButtonColor;
  final Color selectedTextColor;

  const Pagenation({
    required this.currentPage,
    required this.totalLength,
    required this.itemPerPage,
    required this.setPage,
    this.selectedButtonColor = Colors.blue,
    this.selectedTextColor = Colors.white,
    this.height = 50,
    this.numberOfPages = 7,
    Key? key,
  }) : super(key: key);

  int pageClamp(int num) {
    int minVal = 0;
    int maxVal = (totalLength / itemPerPage).ceil() - 1;
    return min(max(num, minVal), maxVal);
  }

  Widget arrow(IconData icon, int num) {
    int maxPage = (totalLength / itemPerPage).ceil();

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        border: Border.all(color: CustomColors.cE3E3E3),
        color: Colors.white,
      ),
      width: 24,
      height: 24,
      child: Center(
        child: InkWell(
          child: Icon(
            icon,
            color: num > -1 && num < maxPage
                ? Color(0xff008c66)
                : CustomColors.cCECECE,
          ),
          onTap: () => setPage(pageClamp(num)),
        ),
      ),
    );
  }

  Widget number(num index) {
    bool isSelected = currentPage == index;
    return InkWell(
      onTap: () => setPage(pageClamp(index.toInt())),
      child: Container(
        color: isSelected ? this.selectedButtonColor : Colors.transparent,
        width: 50,
        height: 20,
        child: Center(
          child: Text(
            (index + 1).toString(),
            style: TextStyle(
                color: isSelected ? this.selectedTextColor : Colors.black),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    int maxPage = (totalLength / itemPerPage).ceil();

    int idxStart = currentPage - this.numberOfPages ~/ 2;
    int idxLast = idxStart + this.numberOfPages;

    if (idxStart < 0) {
      idxLast = min(maxPage, idxLast + idxStart.abs());
      idxStart = 0;
    }

    if (idxLast > maxPage) {
      idxStart = max(0, idxStart - (idxLast - maxPage));
      idxLast = maxPage;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: CustomColors.cE3E3E3,
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      height: height,
      child: Row(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              '1-$maxPage of ${currentPage + 1} pages',
              style: TextStyle(
                color: Color(0xff717071),
                fontSize: 12,
              ),
            ),
          ),
          Expanded(child: Container()),
          Text(
            "현재 페이지",
            style: TextStyle(color: Color(0xff717071), fontSize: 12),
          ),
          SizedBox(width: 8),
          SizedBox(
            width: 60,
            child: DropBox(
              currentValue: currentPage + 1,
              values: List.generate(maxPage, (index) => index + 1),
              onChanged: (v) => setPage(v - 1 as int),
            ),
          ),
          SizedBox(width: 24),
          arrow(Icons.keyboard_arrow_left, currentPage - 1),
          SizedBox(width: 8),
          arrow(Icons.keyboard_arrow_right, currentPage + 1),
        ],
      ),
    );
  }
}
