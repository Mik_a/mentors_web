import 'package:flutter/material.dart';

class NoSectionList extends StatelessWidget {
  const NoSectionList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "등록된 배치도가 없습니다",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          // SizedBox(height: 12),
          // CustomButton(
          //   width: 200,
          //   title: "배치도 추가하러 가기",
          //   onPressed: () {
          //     Get.find<HomeController>().toPage(PageEnum.dashboard);
          //   },
          // ),
        ],
      ),
    );
  }
}
