import 'package:flutter/material.dart';

class FormWithTitle extends StatelessWidget {
  final String title;
  final Widget child;
  final double? height;
  final Widget? action;

  const FormWithTitle({
    this.height,
    this.action,
    required this.title,
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 20,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(title),
                  ),
                ),
              ),
              if (action != null) action!
            ],
          ),
          Divider(),
          Expanded(child: child),
        ],
      ),
    );
  }
}
