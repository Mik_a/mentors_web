import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

class AutoSpacingText extends StatelessWidget {
  final String text;
  final double width;
  final TextStyle? style;

  const AutoSpacingText({
    required this.text,
    this.width = 50,
    this.style,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> chars = text.split('');
    return Container(
      width: width,
      child: Row(
        mainAxisAlignment: chars.length == 1
            ? MainAxisAlignment.center
            : MainAxisAlignment.spaceBetween,
        children: List.generate(
          chars.length,
          (index) => Text(
            chars[index],
            style: style ??
                TextStyle(
                  color: CustomColors.cA3A2A3,
                  fontSize: 12,
                ),
          ),
        ),
      ),
    );
  }
}
