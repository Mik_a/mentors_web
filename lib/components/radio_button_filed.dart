import 'package:flutter/material.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';

class RadioButtonField extends StatelessWidget {
  final String title;
  final List<dynamic> values;
  final List<String> titles;
  final dynamic currentValue;
  final Function(dynamic) onChange;
  final double width;

  RadioButtonField({
    Key? key,
    this.width = 200,
    required this.title,
    required this.values,
    required this.currentValue,
    required this.onChange,
    required this.titles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSpacingText(text: title),
          SizedBox(width: 20),
          ...List.generate(
            values.length,
            (index) => Expanded(
              child: Container(
                child: InkWell(
                  onTap: () => onChange(values[index]),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 16,
                        height: 16,
                        child: Radio(
                          activeColor: Color(0xffd9c87a),
                          value: values[index],
                          groupValue: currentValue,
                          onChanged: onChange,
                        ),
                      ),
                      SizedBox(width: 4),
                      Text(
                        titles[index],
                        style: TextStyle(fontSize: 12),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
