import 'package:flutter/material.dart';
import 'package:mentors_web/components/auto_spacing_text.dart';
import 'package:mentors_web/components/radio_button.dart';

class RadioBoxWithTitle extends StatelessWidget {
  final double titleWidth;
  final double inputWidth;
  final double inputHeight;
  final String title;
  final String? hintText;
  final TextEditingController? controller;
  final TextAlign textAlign;
  final String? tailText;
  final double containerWidth;
  final TextInputType? textInputType;
  final List<dynamic> values;
  final Function(dynamic v) onChanged;
  final Object? currentValue;
  final List<String>? textList;

  final TextStyle? inputTextStyle;

  const RadioBoxWithTitle({
    required this.title,
    required this.values,
    required this.currentValue,
    required this.onChanged,
    this.inputTextStyle,
    this.textList,
    this.controller,
    this.textInputType,
    this.containerWidth = double.infinity,
    this.tailText,
    this.textAlign = TextAlign.left,
    this.hintText,
    this.inputWidth = 50,
    this.inputHeight = 32,
    this.titleWidth = 100,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> radioButtons = List.generate(
      values.length,
      (index) => RadioButton(
        title: textList![index],
        isOn: currentValue == values[index],
        onClick: onChanged,
        value: values[index],
      ),
    );

    return Container(
      width: containerWidth,
      height: 32,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: TextInputType.multiline == textInputType
              ? CrossAxisAlignment.start
              : CrossAxisAlignment.center,
          children: [
            AutoSpacingText(
              text: title,
              width: titleWidth,
            ),
            SizedBox(width: 20),
            inputWidth == double.infinity
                ? Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: radioButtons,
                    ),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: radioButtons,
                  ),
            if (tailText != null) ...[
              SizedBox(width: 5),
              Text(tailText!),
            ]
          ],
        ),
      ),
    );
  }
}
