import 'package:flutter/material.dart';
import 'package:mentors_web/global_colors.dart';

enum CustomButtonType { confirm, cancle, disable }

class CustomButton extends StatelessWidget {
  final String title;
  final double width;
  final double height;
  final Function()? onPressed;
  final CustomButtonType type;
  final Color? backgroundColor;
  final Color? textColor;

  CustomButton({
    this.onPressed,
    this.height = 35,
    this.type = CustomButtonType.confirm,
    this.width = 80,
    this.backgroundColor,
    this.textColor = Colors.white,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
        onPressed: CustomButtonType.disable == type ? null : onPressed,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            backgroundColor != null
                ? backgroundColor
                : type == CustomButtonType.confirm
                    ? CustomColors.cD9C87A
                    : CustomButtonType.disable == type
                        ? CustomColors.cd7d7d7
                        : Color(0xffD7D0B4),
          ),
        ),
        child: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color:
                type == CustomButtonType.cancle ? Color(0xff595232) : textColor,
          ),
        ),
      ),
    );
  }
}
