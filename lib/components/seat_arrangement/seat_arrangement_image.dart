import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/seat_icon.dart';
import 'package:mentors_web/components/add_section/select_image_notice.dart';
import 'package:mentors_web/components/custom_button.dart';
import 'package:mentors_web/components/seat_arrangement/icon_arrangement.dart';
import 'package:mentors_web/components/section_list/section_image.dart';

class SeatArrangementImage extends StatelessWidget {
  final bool isMobile;

  SeatArrangementImage({
    Key? key,
    this.isMobile = false,
  }) : super(key: key);

  final SeatArrangementController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: LayoutBuilder(
        builder: (context, constraints) {
          RenderBox getBox = context.findRenderObject() as RenderBox;
          final double maxWidth = constraints.maxWidth;
          final double maxHeight = constraints.maxHeight;
          final double width = maxHeight / 10 * 12;
          final double height = maxHeight;
          controller.frameChange(maxWidth, maxHeight);

          return Obx(() {
            return Center(
              child: Container(
                child: Stack(
                  children: [
                    controller.selectedSection.value.id == ''
                        ? SelectImageNotice(title: "배치도를 선택해주세요.")
                        : Stack(
                            children: [
                              Center(
                                child: Container(
                                  color: Colors.grey.withOpacity(1),
                                  child: GestureDetector(
                                    onPanStart: controller.onDragStart,
                                    onPanUpdate: controller.onDragUpdate,
                                    onPanEnd: (event) =>
                                        controller.onDragEnd(event, getBox),
                                    child: SizeChangedLayoutNotifier(
                                      child: SectionImage(
                                        onImageLoaded:
                                            controller.onImageSizeChanged,
                                        section:
                                            controller.selectedSection.value,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              IconArrangement(
                                maxWidth: maxWidth,
                                maxHeight: maxHeight,
                                isMobile: isMobile,
                              ),
                              SeatIcon(
                                isCantDraw: controller
                                            .currentDrawingBoxSize.value.dx <
                                        60 ||
                                    controller.currentDrawingBoxSize.value.dy <
                                        60,
                                position:
                                    controller.getPositionDrawingBox(getBox),
                                size: controller.getSizeDrawingBox(),
                              ),
                              if (controller.isMovingUser.isTrue)
                                Positioned(
                                  left: 15,
                                  top: 15,
                                  child: CustomButton(
                                    width: 200,
                                    title: "자리 이동 취소",
                                    onPressed: controller.cancleMove,
                                  ),
                                ),
                            ],
                          ),
                    DragTarget(
                      onAcceptWithDetails: (details) {
                        // Offset localOffset =
                        //     getBox.globalToLocal(details.offset);
                        // double x = (localOffset.dx ~/
                        //         controller.interval *
                        //         controller.interval) -
                        //     offsetX;
                        // double y = (localOffset.dy ~/
                        //     controller.interval *
                        //     controller.interval);
                        // List<double> offset = [x / width, y / height];
                        // controller.addUser(details.data as User, offset);
                      },
                      builder: (context, candidateData, rejectedData) {
                        return Container(
                          width: width,
                          height: height,
                        );
                      },
                    ),
                  ],
                ),
              ),
            );
          });
        },
      ),
    );
  }
}
