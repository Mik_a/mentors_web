import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/views/seat_icon.dart';

class IconArrangement extends StatelessWidget {
  final SeatArrangementController controller = Get.find();
  final double maxWidth;
  final double maxHeight;
  final bool isMobile;

  IconArrangement({
    required this.maxWidth,
    required this.maxHeight,
    this.isMobile = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Positioned(
        left: (maxWidth - controller.imageSize.value.width) * 0.5,
        top: (maxHeight - controller.imageSize.value.height) * 0.5,
        width: controller.imageSize.value.width,
        height: controller.imageSize.value.height,
        child: Stack(
          children: List.generate(
            controller.seatIconList.length,
            (index) => SeatIcon(
              isMobile: isMobile,
              onClick: controller.isMovingUser.isTrue
                  ? (SeatArrangementClass seatInfo, _, __) {
                      controller.onClickMoveSeat(seatInfo);
                    }
                  : controller.onClickSeatIcon,
              seatInfo: controller.seatIconList[index],
              isSelected: controller.seatIconList[index] ==
                  controller.lastSelectedSeatIcon.value,
              size: controller.getIconSize(controller.seatIconList[index]),
              position:
                  controller.getIconPosition(controller.seatIconList[index]),
            ),
          ),
        ),
      );
    });
  }
}
