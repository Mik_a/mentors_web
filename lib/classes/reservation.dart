class ReservationHour {
  bool isCanReservation;
  bool isReservationed;
  String? userId;
  String? phone;
  String? name;
  int numberOfPeople;
  int paymentPrice;
  String id;
  int hour;

  ReservationHour({
    required this.isCanReservation,
    required this.isReservationed,
    this.userId,
    this.phone,
    this.name,
    required this.numberOfPeople,
    required this.paymentPrice,
    required this.id,
    required this.hour,
  });
}
