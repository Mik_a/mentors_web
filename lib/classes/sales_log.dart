import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/modules/lib.dart';

enum PayDevice { kiosk, pc }
enum PayMethod { card, cash }

extension PayDeviceExtension on PayDevice {
  String get name {
    switch (this) {
      case PayDevice.kiosk:
        return '키오스크';
      case PayDevice.pc:
        return '운영PC';
      default:
        return "-";
    }
  }

  Color get color {
    switch (this) {
      case PayDevice.kiosk:
        return Color(0xff008C8C);
      case PayDevice.pc:
        return Color(0xffbeaf6b);
      default:
        return Colors.black;
    }
  }
}

extension PayMethodExtension on PayMethod {
  String get name {
    switch (this) {
      case PayMethod.card:
        return '카드';
      case PayMethod.cash:
        return '현금';
      default:
        return "-";
    }
  }

  String get value {
    switch (this) {
      case PayMethod.card:
        return 'card';
      case PayMethod.cash:
        return 'cash';
      default:
        return "-";
    }
  }
}

class SalesLog {
  String id;
  String? installment;
  String? userId;
  String? userName;
  String? voucherTypeKor;

  VoucherUseType? voucherType;
  int price;
  DateTime eventTime;
  bool isWeb;
  String? vat;

  String? recognizeCode;
  String? code;

  String? payType1;
  String? payType2;
  String? paymentData;
  String? result;
  String? approvedTime;
  String? cat;
  String? cardNum;
  PayDevice payDevice;
  PayMethod payMethod;

  SalesLog({
    required this.id,
    required this.price,
    required this.eventTime,
    required this.isWeb,
    required this.payDevice,
    required this.payMethod,
    this.userName,
    this.recognizeCode,
    this.code,
    this.installment,
    this.userId,
    this.voucherType,
    this.payType1,
    this.payType2,
    this.voucherTypeKor,
    this.vat,
    this.paymentData,
    this.result,
    this.approvedTime,
    this.cat,
    this.cardNum,
  });

  factory SalesLog.fromJson(Map<String, dynamic> json) {
    String? paymentData = json['paymentData'] as String?;

    String? approvedTime;
    String? cat;
    String? cardNum;

    if (paymentData != null) {
      final split = paymentData.split("");
      approvedTime = split[8];
      cat = split[14];
      cardNum = split[17];
    }

    return SalesLog(
      id: json['_id'] as String,
      price: json['price'] as int,
      eventTime: DateTime.parse(json['date'] as String),
      isWeb: json['isWeb'] as bool,
      recognizeCode: json['recognizeCode'] as String?,
      code: json['code'] as String?,
      installment: json['installment'] ?? "-",
      userId: json['userId'] as String?,
      userName: json['userName'] as String?,
      voucherType: json['voucherUseType'] == null
          ? null
          : VoucherUseType.values.firstWhere(
              (e) {
                return e.toEng() == json['voucherUseType'];
              },
            ),
      payType1: json['payType1'] as String?,
      payType2: json['payType2'] as String?,
      voucherTypeKor: json['voucherTypeKor'] as String?,
      vat: json['VAT'] as String?,
      paymentData: json['paymentData'] as String?,
      result: json['result'] as String?,
      approvedTime: approvedTime,
      cat: cat,
      cardNum: cardNum,
      payDevice: enumFromString(PayDevice.values, json['device'] ?? "pc"),
      payMethod: enumFromString(PayMethod.values, json['method'] ?? "cash"),
    );
  }

  String get itemName =>
      '$voucherTypeKor ${voucherType != null ? voucherType!.toKor() : ''}';
}
