class MessageTemplate {
  Set<String> command;
  bool isSend;
  String message;
  String group;
  String title;
  String id;

  MessageTemplate({
    required this.command,
    required this.isSend,
    required this.message,
    required this.group,
    required this.title,
    required this.id,
  });

  static MessageTemplate convert(dynamic data) {
    Set<String> command = Set.from(data['command']);

    command.addAll(List.from(data['commonCommand']));

    return MessageTemplate(
      command: command,
      isSend: data['isSend'],
      message: data['message'],
      group: data['group'],
      title: data['title'],
      id: data['_id'],
    );
  }

  static MessageTemplate empty() {
    return MessageTemplate(
      command: Set(),
      isSend: false,
      message: '',
      group: '',
      title: '',
      id: '',
    );
  }
}
