import 'package:flutter/cupertino.dart';

class Reservation {
  bool isCanReservation;
  int day;
  int numberOfReservation;

  Reservation({
    required this.isCanReservation,
    required this.day,
    required this.numberOfReservation,
  });

  factory Reservation.fromJson(Map<String, dynamic> json) => Reservation(
        isCanReservation: json["isCanReservation"],
        day: json["day"],
        numberOfReservation: json["numberOfReservation"],
      );
}

class ReservationDataOfDay {
  String name;
  String id;
  Color? color;
  List<ReservationInfo> infoList;

  ReservationDataOfDay({
    required this.name,
    required this.id,
    required this.infoList,
    this.color,
  });

  factory ReservationDataOfDay.fromJson(Map<String, dynamic> json) {
    return ReservationDataOfDay(
      name: json["name"],
      id: json["id"],
      infoList: List<ReservationInfo>.from(
        json["data"].map(
          (x) => ReservationInfo.fromJson(x),
        ),
      ),
    );
  }
}

class ReservationInfo {
  bool isCanReservation;
  bool isReservationed;
  String id;
  String? userId;
  String? phone;
  String? name;
  int numberOfPeople;
  int paymentPrice;
  int hour;

  ReservationInfo({
    required this.isCanReservation,
    required this.isReservationed,
    required this.id,
    required this.userId,
    required this.phone,
    required this.name,
    required this.numberOfPeople,
    required this.paymentPrice,
    required this.hour,
  });

  factory ReservationInfo.fromJson(Map<String, dynamic> json) =>
      ReservationInfo(
        isCanReservation: json["isCanReservation"],
        isReservationed: json["isReservationed"],
        id: json["_id"],
        userId: json["userId"],
        phone: json["phone"],
        name: json["userName"] ?? '-',
        numberOfPeople: json["numberOfPeople"].runtimeType == int
            ? json["numberOfPeople"]
            : int.parse(json["numberOfPeople"]),
        paymentPrice: json["paymentPrice"].runtimeType == int
            ? json["paymentPrice"]
            : int.parse(json["paymentPrice"]),
        hour: json["hour"],
      );
}

class StudyRoomReservationOfMonth {
  String id;
  String name;
  Color? color;
  List<Reservation> reservations;

  StudyRoomReservationOfMonth({
    required this.id,
    required this.name,
    required this.reservations,
    this.color,
  });

  factory StudyRoomReservationOfMonth.fromJson(Map<String, dynamic> json) {
    return StudyRoomReservationOfMonth(
      name: json["name"],
      id: json["_id"],
      reservations: List<Reservation>.from(
          json["reservation"].map((x) => Reservation.fromJson(x))),
    );
  }
}
