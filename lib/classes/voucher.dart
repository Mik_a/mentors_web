import 'package:mentors_web/modules/lib.dart';

enum VoucherUseType {
  day,
  time,
  period,
  locker,
  studyroom,
}

enum VoucherLogType {
  buy,
  extend,
  expired,
}

extension VoucherLogTypeExtension on VoucherLogType {
  String get name {
    switch (this) {
      case VoucherLogType.buy:
        return '구매';
      case VoucherLogType.extend:
        return '연장';
      case VoucherLogType.expired:
        return '만료';
      default:
        return '';
    }
  }
}

extension vouchertype on VoucherUseType {
  String toKor() {
    switch (this) {
      case VoucherUseType.day:
        return "당일권";
      case VoucherUseType.time:
        return "시간권";
      case VoucherUseType.period:
        return "기간권";
      case VoucherUseType.locker:
        return "사물함";
      case VoucherUseType.studyroom:
        return "스터디룸";
      default:
        return "에러";
    }
  }

  String toEng() {
    switch (this) {
      case VoucherUseType.day:
        return "day";
      case VoucherUseType.period:
        return "period";
      case VoucherUseType.time:
        return "time";
      case VoucherUseType.locker:
        return "locker";
      case VoucherUseType.studyroom:
        return "studyroom";
      default:
        return "error";
    }
  }
}

VoucherUseType stringToVoucherType(String str) {
  switch (str) {
    case "day":
      return VoucherUseType.day;
    case "period":
      return VoucherUseType.period;
    case "time":
      return VoucherUseType.time;
    default:
      return VoucherUseType.day;
  }
}

List<Voucher> getVouchersFromList(List<dynamic> list) {
  return list.map(Voucher.getVoucherFromData).toList();
}

class VoucherLog {
  VoucherLogType buyType;
  int price;
  int extendedTime;
  DateTime eventTime;

  VoucherLog({
    required this.buyType,
    required this.price,
    required this.extendedTime,
    required this.eventTime,
  });

  factory VoucherLog.fromJson(Map<String, dynamic> json) {
    return VoucherLog(
      buyType: enumFromString(VoucherLogType.values, json['type']),
      price: json['price'],
      extendedTime: json['extendedTime'],
      eventTime: DateTime.parse(json['eventTime']),
    );
  }
}

class Voucher {
  VoucherUseType type;

  int useableTime;
  int price;
  int priceFree;
  int extendPriceFree;

  String id;
  String memo;
  String group;
  bool isEnable;

  DateTime? createdDate;
  DateTime? expiredDate;
  int? remainTime;
  int? expirationTime;
  int? extendPrice;
  int? limitAmount;
  int? overflowTime;
  int? maxPeople;

  List<VoucherLog> logs;

  Voucher({
    required this.id,
    required this.price,
    required this.type,
    required this.useableTime,
    required this.memo,
    required this.isEnable,
    required this.priceFree,
    required this.extendPriceFree,
    this.createdDate,
    this.expiredDate,
    this.remainTime,
    this.extendPrice,
    this.expirationTime,
    this.limitAmount,
    this.overflowTime,
    this.maxPeople,
    this.group = '',
    this.logs = const [],
  });

  static Voucher empty() {
    return Voucher(
      id: '',
      price: 0,
      type: VoucherUseType.day,
      useableTime: 0,
      memo: '',
      isEnable: false,
      priceFree: 0,
      extendPriceFree: 0,
    );
  }

  static Voucher getVoucherFromData(dynamic data) {
    return Voucher(
      id: data['_id'],
      isEnable: data['isEnable'],
      price: data['price'],
      priceFree: data['priceFree'] ?? 0,
      remainTime: data['remainTime'],
      expirationTime: data['expirationTime'],
      limitAmount: data['limitAmount'],
      overflowTime: data['overflowTime'],
      maxPeople: data['maxPeople'],
      useableTime: data['useableTime'],
      type: stringToVoucherType(data['type']),
      extendPrice: data['extendPrice'],
      memo: data['memo'],
      extendPriceFree: data['extendPriceFree'] ?? 0,
      createdDate: DateTime.tryParse(data['createdDate'] ?? ''),
      expiredDate: DateTime.tryParse(data['expiredDate'] ?? ''),
      group: data['group'] ?? '',
      logs: List.from(data['logs'])
          .map((log) => VoucherLog.fromJson(log))
          .toList()
          .reversed
          .toList(),
    );
  }
}

class LockerVoucher {
  String id;
  String name;
  int price;
  int day;

  LockerVoucher({
    required this.id,
    required this.price,
    required this.day,
    required this.name,
  });

  static LockerVoucher getFromData(dynamic data) {
    return LockerVoucher(
      id: data['_id'],
      price: data['price'],
      day: data['day'],
      name: data['name'],
    );
  }
}
