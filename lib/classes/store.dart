class Store {
  String id;
  String name;
  String type;
  String city;
  DateTime createDate;
  String brandCode;
  String? ePassId;
  int customerCnt;
  bool isUseMobileApplication;
  String ownerName;
  String phoneNumber;

  List<String> get values => [
        city,
        type,
        name,
        customerCnt.toString() + "명",
        ownerName,
        phoneNumber,
        createDate.toString().substring(0, 10)
      ];

  Store({
    required this.ownerName,
    required this.phoneNumber,
    required this.customerCnt,
    required this.id,
    required this.name,
    required this.type,
    required this.city,
    required this.brandCode,
    required this.createDate,
    required this.isUseMobileApplication,
    this.ePassId,
  });
}
