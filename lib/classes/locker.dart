class Locker {
  String id;
  bool isEnable;
  bool isUsing;
  int index;
  String? usingUserId;
  DateTime? startedDate;
  DateTime? expriedDate;
  String? usingUserName;

  Locker({
    required this.id,
    required this.isEnable,
    required this.isUsing,
    required this.index,
    this.usingUserId,
    this.usingUserName,
    this.startedDate,
    this.expriedDate,
  });

  static Locker convertLocker(dynamic data) {
    print(data);

    return Locker(
      id: data['_id'],
      isEnable: data['isEnable'],
      isUsing: data['isUsing'],
      index: data['index'],
      expriedDate: DateTime.tryParse(data['expiredDate']),
      startedDate: DateTime.tryParse(data['startedDate']),
      usingUserId: data['usingUserId'],
      usingUserName: data['usingUserName'],
    );
  }
}
