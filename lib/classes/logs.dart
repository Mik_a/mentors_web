import 'package:flutter/material.dart';
import 'package:mentors_web/modules/lib.dart';

enum IOType { enter, out, exit, outEnter, move, moveFrom, moveTo, outExit }

extension IOTypeExtension on IOType {
  String get name {
    switch (this) {
      case IOType.enter:
        return "입실";
      case IOType.exit:
        return "퇴실";
      case IOType.out:
        return "외출";
      case IOType.outEnter:
        return "입실 (복귀)";
      case IOType.move:
        return "자리이동";
      case IOType.moveTo:
        return "자리이동";
      case IOType.moveFrom:
        return "자리이동";
      case IOType.outExit:
        return "퇴실 처리";
      default:
        return this.toString();
    }
  }

  Color get color {
    switch (this) {
      case IOType.enter:
        return Color(0xff23C7D1);
      case IOType.exit:
        return Color(0xffFF6B6B);
      case IOType.out:
        return Color(0xffFBA136);
      case IOType.outEnter:
        return Color(0xff23C7D1);
      case IOType.moveFrom:
        return Colors.redAccent;
      case IOType.moveFrom:
        return Colors.redAccent;
      default:
        return Colors.black;
    }
  }

  String get status {
    switch (this) {
      case IOType.enter:
        return "(공석 -> 사용중)";
      case IOType.exit:
        return "(사용중 -> 공석)";
      case IOType.out:
        return "(사용중 -> 외출)";
      case IOType.outEnter:
        return "(외출 -> 사용중)";
      case IOType.moveFrom:
        return "자리이동";
      case IOType.moveTo:
        return "자리이동";
      default:
        return this.toString();
    }
  }
}

// "voucherId": "temp",
// "_id": "61765915efa1df836daf15d2",
// "userId": "617658d90438ec5954987187",
// "eventTime": "2021-10-25T07:13:25.476Z",
// "userName": "test",
// "seatName": "빅토르석",
// "seatIndex": 2,
// "type": "exit"
class IOLog {
  String name;
  String seatName;
  String seatIndex;
  DateTime time;
  IOType ioType;
  String userId;

  String? prevSeatName;
  String? toIndex;
  String? fromIndex;

  IOLog({
    required this.name,
    required this.time,
    required this.ioType,
    required this.seatName,
    required this.seatIndex,
    required this.userId,
    this.fromIndex,
    this.toIndex,
    this.prevSeatName,
  });

  factory IOLog.fromJson(Map<String, dynamic> json) {
    return IOLog(
      name: json['userName'] ?? '-',
      seatName: json['seatName'] ?? '-',
      seatIndex: json['seatIndex'].toString(),
      time: DateTime.parse(json['eventTime']),
      ioType: enumFromString(IOType.values, json['type']) ?? IOType.exit,
      userId: json['userId'],
      fromIndex: json['fromIndex'],
      toIndex: json['toIndex'],
      prevSeatName: json['prevSeatName'],
    );
  }
}

// "id": "61775d73efa1df836db008e6",
// "userId": "615d36033021999ceb2af26b",
// "type": "add",
// "date": "2021-11-08T05:17:52.395Z",
// "userName": "이도현"
class Log {
  String id;
  DateTime time;
  String userId;
  String userName;
  String type;
  String? moveIndex;

  Log({
    required this.id,
    required this.time,
    required this.userId,
    required this.userName,
    required this.type,
    this.moveIndex,
  });

  factory Log.fromJson(Map<String, dynamic> json) {
    return Log(
      id: json['id'],
      time: DateTime.parse(json['date']),
      userId: json['userId'],
      userName: json['userName'],
      type: json['type'],
      moveIndex: json['moveIndex'],
    );
  }

  String lockerMessage(int myIndex) {
    switch (type) {
      case 'add':
        return '(공석 -> 배치)';
      case 'delete':
        return '(배치 -> 해제)';
      case 'delete-manager':
        return '관리자에 의한 종료';
      case 'move-to':
        return '자리이동 ($myIndex번 -> $moveIndex번)';
      case 'move-from':
        return '자리이동 ($moveIndex번 -> $myIndex번)';
      case 'enable':
        return '사물함 활성화';
      case 'disable':
        return '사물함 비활성화';
      default:
        return type;
    }
  }
}

enum ChargeStatus {
  pending,
  approved,
  rejected,
}

extension ChargeStatusExtension on ChargeStatus {
  String get name {
    switch (this) {
      case ChargeStatus.pending:
        return "대기중";
      case ChargeStatus.approved:
        return "승인완료";
      case ChargeStatus.rejected:
        return "거절";
      default:
        return this.toString();
    }
  }

  Color get color {
    switch (this) {
      case ChargeStatus.pending:
        return Color(0xffFBA136);
      case ChargeStatus.approved:
        return Color(0xff19a980);
      case ChargeStatus.rejected:
        return Color(0xffFF6B6B);
      default:
        return Colors.black;
    }
  }
}

class ChargeLog {
  DateTime? responseTime;
  String requestUserName;
  String id;
  int amount;
  ChargeStatus status;
  DateTime requestTime;

  ChargeLog({
    this.responseTime,
    required this.requestUserName,
    required this.id,
    required this.amount,
    required this.status,
    required this.requestTime,
  });

  factory ChargeLog.fromJson(Map<String, dynamic> json) {
    return ChargeLog(
      responseTime: json['responseTime'] == null
          ? null
          : DateTime.parse(json['responseTime']),
      requestUserName: json['requestUserName'],
      id: json['_id'],
      amount: json['amount'],
      status: enumFromString(ChargeStatus.values, json['status']),
      requestTime: DateTime.parse(json['requestTime']),
    );
  }
}
