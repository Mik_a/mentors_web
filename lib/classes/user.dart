import 'package:mentors_web/classes/seat.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

class User {
  String id;

  bool isRegister;
  String name;
  String phone;
  Gender gender;
  DateTime createdDate;
  UserEnterStatus status;

  DateTime? outExpiredTime;
  Seat? seatData;

  String? memo;
  String? email;
  String? currentUsingSeat;
  String? currentUsingVoucherId;
  String? loginCode;
  String? parentPhone;
  String? birthday;
  bool? isPass;

  User({
    this.status = UserEnterStatus.exit,
    this.outExpiredTime,
    this.currentUsingVoucherId,
    this.currentUsingSeat,
    this.loginCode,
    this.parentPhone,
    this.email,
    this.seatData,
    this.memo,
    this.birthday,
    this.isPass,
    required this.createdDate,
    required this.id,
    required this.isRegister,
    required this.name,
    required this.phone,
    required this.gender,
  });

  // create empty class
  factory User.empty() {
    return User(
      createdDate: DateTime.now(),
      id: 'id',
      isRegister: false,
      name: 'name',
      phone: 'phone',
      gender: Gender.none,
    );
  }

  factory User.fromJson(dynamic e) {
    print(e);

    return User(
      id: e['_id'],
      currentUsingVoucherId: e['currentUsingVoucher'],
      currentUsingSeat: e['currentUsingSeat'],
      memo: e['memo'],
      parentPhone: e['parentPhone'],
      status: enumFromString(UserEnterStatus.values, e['status']),
      createdDate: DateTime.parse(e['createdDate']),
      isRegister: e['isRegistedUser'],
      name: e['isRegistedUser'] ? e['name'] : '손님',
      email: e['email'],
      phone: e['phone'] ?? "-",
      loginCode: e['loginCode'],
      birthday: e['birthday'],
      seatData: e['seatData'] == null
          ? null
          : Seat(
              idx: e['seatData']['index'],
              name: e['seatData']['name'],
            ),
      gender: e['gender'] == "male"
          ? Gender.male
          : e['gender'] == 'female'
              ? Gender.female
              : Gender.none,
      isPass: e['isPass'],
    );
  }
  // return to json
}
