import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/global_enums.dart';

class SeatType {
  String id;
  String name;
  String description;
  String? image;
  SeatGroup seatGroup;
  List<Voucher> voucherList;
  int maxPeople;
  int priceOfHour;

  SeatType({
    required this.id,
    required this.name,
    required this.description,
    required this.voucherList,
    required this.seatGroup,
    this.maxPeople = 0,
    this.image,
    this.priceOfHour = 0,
  });

  static SeatType empty() {
    return SeatType(
      id: '',
      name: '',
      description: '',
      voucherList: [],
      seatGroup: SeatGroup.free,
    );
  }

  static SeatType getFromData(dynamic data) {
    SeatGroup seatGroup = SeatGroup.free;

    if (data['type'] == "자유석") {
      seatGroup = SeatGroup.free;
    } else if (data['type'] == '지정석') {
      seatGroup = SeatGroup.fixed;
    } else if (data['type'] == '스터디룸') {
      seatGroup = SeatGroup.studyroom;
    }

    return SeatType(
      id: data['_id'],
      name: data['name'],
      description: data['description'],
      voucherList: [],
      seatGroup: seatGroup,
      maxPeople: data['maxPeople'].runtimeType == int
          ? data['maxPeople']
          : int.tryParse(data['maxPeople'] ?? '0') ?? data['maxPeople'],
      image: '',
      priceOfHour: data['priceOfHour'] ?? 0,
    );
  }
}
