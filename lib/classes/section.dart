import 'package:flutter/cupertino.dart';

class Section {
  String id;
  String name;
  String? path;
  Key? key;

  Section({
    required this.id,
    required this.name,
    this.path,
    this.key,
  });
}
