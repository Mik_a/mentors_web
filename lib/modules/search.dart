List<T> search<T>(List<dynamic> list, String searchData) {
  searchData = searchData.replaceAll('-', '');

  return list
      .where((element) =>
          element.values
              .where((element) =>
                  element.toString().replaceAll('-', '').contains(searchData))
              .toList()
              .length >
          0)
      .map<T>((e) => e)
      .toList();
}
