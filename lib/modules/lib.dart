import 'dart:convert';
import 'dart:typed_data';

import 'package:intl/intl.dart';

T enumFromString<T>(Iterable<T> values, String value) {
  try {
    return values.firstWhere(
      (type) => type.toString().split(".").last == value,
    );
  } catch (e) {
    return values.first;
  }
}

String addCommaInMoney(int value) {
  return NumberFormat('###,###,###,###').format(value).replaceAll(' ', '');
}

String getKoreaTime(DateTime? time) {
  if (time == null) return '';
  return time.toUtc().add(Duration(hours: 9)).toString().substring(0, 19);
}

int getTextLengthKor(String text) {
  Uint8List bytes = Uint8List.fromList(utf8.encode(text));
  int length = text.length + (bytes.buffer.lengthInBytes - text.length) ~/ 2;
  return length;
}
