import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/global_colors.dart';
import 'app/routes/app_pages.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() async {
  await GetStorage.init();
  HttpOverrides.global = new MyHttpOverrides();

  runApp(
    GetMaterialApp(
      title: "Mentors Admin Page",
      theme: ThemeData(
          primaryColor: Colors.white,
          fontFamily: 'NanumSquareR',
          appBarTheme: AppBarTheme(
            titleTextStyle: TextStyle(
              color: CustomColors.c242424,
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontFamily: "NanumSquareR",
              backgroundColor: Colors.white,
            ),
          ),
          textTheme: TextTheme(
            bodyText1: TextStyle(color: CustomColors.c242424),
            bodyText2: TextStyle(color: CustomColors.c242424),
          )),
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      builder: EasyLoading.init(),
    ),
  );
}
