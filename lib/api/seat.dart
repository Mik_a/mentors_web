import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/app/modules/management_seat_voucher/controllers/management_seat_voucher_controller.dart';
import 'package:mentors_web/app/modules/seat_arrangement/controllers/seat_arrangement_controller.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/classes/seatType.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

Future<SeatType?> _addNewSeatType(
  String name,
  String memo,
  String priceOfHour,
  String maxPeople,
  SeatGroup group,
  XFile? image,
) async {
  final url = '$server/seat';
  if (name.length == 0) {
    return null;
  }

  String base64 = '';
  if (image != null) {
    final bytes = await image.readAsBytes();
    base64 = base64Encode(bytes);
  }

  final res = await requestPost(url, {
    "name": name,
    "memo": memo,
    "group": group.toKor(),
    "priceOfHour": priceOfHour,
    "maxPeople": maxPeople,
    "thumbnail": image == null ? '' : base64,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return null;
  }

  if (res.data['success']) {
    final data = res.data['data'];
    return SeatType.getFromData(data);
  } else {
    print('error in add new seat type');
    return null;
  }
}

Future<SeatType?> _updateSeatType(
  String seatTypeId,
  String name,
  String memo,
  String priceOfHour,
  String maxPeople,
  SeatGroup group,
  XFile? image,
) async {
  final url = '$server/seat';
  if (name.length == 0) {
    return null;
  }

  String base64 = '';
  if (image != null) {
    final bytes = await image.readAsBytes();
    base64 = base64Encode(bytes);
  }

  final res = await requestPatch(url, {
    "seatTypeId": seatTypeId,
    "name": name,
    "memo": memo,
    "group": group.toKor(),
    "priceOfHour": priceOfHour,
    "maxPeople": maxPeople,
    "thumbnail": image == null ? '' : base64,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return null;
  }

  if (res.data['success']) {
    final data = res.data['data'];
    return SeatType.getFromData(data);
  } else {
    print('error in add new seat type');
    return null;
  }
}

Future<List<SeatType>> _getAllSeatTypes() async {
  final url = '$server/seat';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data']).map(SeatType.getFromData).toList();
  } else {
    print("error in get all seat types");
    return [];
  }
}

Future<SeatType?> _removeSeatType(
    SeatGroup seatGroup, SeatType seatType) async {
  final url = '$server/seat/${seatGroup.toKor()}/${seatType.id}';
  final res = await requestDelete(url, {});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return null;
  }

  if (res.data['success']) {
    return seatType;
  } else {
    if (res.data['message'] == "CAN'T FIND SEAT") {
      EasyLoading.showError("일시적인 오류입니다\n새로고침 후 다시 시도해주세요");
    }
    return null;
  }
}

Future<bool> _saveSeatArrangement(
    List<Map<String, dynamic>> jsonData, String sectionId) async {
  final res = await requestPost('$server/seat/arrangement/$sectionId', {
    'data': json.encode(jsonData),
  });

  if (res == null) {
    return false;
  } else {
    return res.data['success'];
  }
}

Future<List<SeatArrangementClass>> _getSeatArrangement(String sectionId) async {
  if (sectionId == '') return [];

  final res = await requestGet('$server/seat/arrangement/$sectionId');

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data']).map(
      (e) {
        SeatUsingData? seatUsingData;

        if (e['isUsing']) {
          final userData = e['userData'];

          seatUsingData = userData == null
              ? null
              : SeatUsingData(
                  expiredDate: DateTime.tryParse(userData['expiredDate'] ?? ''),
                  remainTime: userData['remainTime'] ?? 0,
                  userName: userData['name'],
                  voucherType: enumFromString(VoucherUseType.values,
                      userData['voucherType'] ?? 'period'),
                  overflowTime: userData['overflowTime'],
                );
        }

        return SeatArrangementClass(
          idx: e['index'],
          id: e['_id'],
          isOut: e['isOut'],
          isUsing: e['isUsing'],
          seatTypeId: e['seatTypeId'],
          position: Offset(e['pos']['x'], e['pos']['y']),
          size: Offset(
            e['size']['x'],
            e['size']['y'],
          ),
          seatTypeName: e['name'],
          usingUserId: e['usingUserId'],
          seatUsingData: seatUsingData,
        );
      },
    ).toList();
  } else {
    return [];
  }
}

Future<Map<String, dynamic>> _getSeatStatus() async {
  final res = await requestGet('$server/seat/status');
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return {};
  }

  if (res.data['success']) {
    return res.data['data'];
  } else {
    return {};
  }
}

Future<List<SeatInfo>> _getSeatListBySeatType(String seatTypeId) async {
  final res = await requestGet('$server/seat/list/$seatTypeId');

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data'])
        .map(
          (e) => SeatInfo(
              id: e['_id'],
              index: e['index'],
              isUsing: e['isUsing'],
              sectionName: e['sectionName'],
              usingUserId: e['usingUserId']),
        )
        .toList();
  } else {
    print("erorr in _getSeatListBySeatType");
    return [];
  }
}

Future<List<SeatInfo>> _getSeatListByGroupName(String groupName) async {
  final res = await requestGet('$server/seat/group/list/$groupName');

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data'])
        .map(
          (e) => SeatInfo(
              id: e['_id'],
              index: e['index'],
              isUsing: e['isUsing'],
              sectionName: e['sectionName'],
              usingUserId: e['usingUserId']),
        )
        .toList();
  } else {
    print("erorr in _getSeatListBySeatType");
    return [];
  }
}

Future<bool> _outThisSeat(String userId) async {
  final res = await requestPatch(
      '$server/io/exit', {'userId': userId, 'force': true.toString()});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    EasyLoading.showSuccess("퇴실처리되었습니다.");
    return true;
  } else {
    if (res.data['message'] == 'NOT_USING_SEAT') {
      EasyLoading.showError("사용중인 좌석이 아닙니다");
      return true;
    }
  }

  return true;
}

Future<bool> _moveSeat(String userId, String seatId) async {
  final url = '$server/io/move';
  final res = await requestPatch(url, {"userId": userId, "seatId": seatId});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    EasyLoading.showSuccess("자리이동 되었습니다");
    return true;
  } else {
    EasyLoading.showError("자리이동에 실패했습니다.\n새로고침 후 다시 시도해주세요");
    return false;
  }
}

Future<List<IOLog>> _getSeatLogs(String seatId) async {
  final url = '$server/seat/logs/$seatId';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data["success"]) {
    return List.from(res.data['data'])
        .map((element) {
          return IOLog.fromJson(element);
        })
        .toList()
        .reversed
        .toList();
  } else {
    print(res.data);
    return [];
  }
}

class SeatAPI {
  final addNewSeatType = _addNewSeatType;
  final updateSeatType = _updateSeatType;
  final getAllSeatTypes = _getAllSeatTypes;
  final removeSeatType = _removeSeatType;
  final saveSeatArrangement = _saveSeatArrangement;
  final getSeatArrangement = _getSeatArrangement;
  final getSeatStatus = _getSeatStatus;
  final getSeatListBySeatType = _getSeatListBySeatType;
  final outThisSeat = _outThisSeat;
  final moveSeat = _moveSeat;
  final getSeatLogs = _getSeatLogs;
  final getSeatListByGroupName = _getSeatListByGroupName;
}
