import 'package:mentors_web/api/locker.dart';
import 'package:mentors_web/api/log.dart';
import 'package:mentors_web/api/payment.dart';
import 'package:mentors_web/api/seat.dart';
import 'package:mentors_web/api/section.dart';
import 'package:mentors_web/api/sms.dart';
import 'package:mentors_web/api/store.dart';
import 'package:mentors_web/api/studyroom.dart';
import 'package:mentors_web/api/user.dart';
import 'package:mentors_web/api/voucher.dart';

abstract class API {
  static final VoucherAPI voucher = VoucherAPI();
  static final LockerAPI locker = LockerAPI();
  static final UserAPI user = UserAPI();
  static final LogAPI log = LogAPI();
  static final SectionAPI section = SectionAPI();
  static final SeatAPI seat = SeatAPI();
  static final StoreAPI store = StoreAPI();
  static final SMSAPI sms = SMSAPI();
  static final PaymentAPI payment = PaymentAPI();
  static final StudyroomAPI studyroom = StudyroomAPI();
}
