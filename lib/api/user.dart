import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/classes/seat.dart';
import 'package:mentors_web/classes/user.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

final Options options = Options(
  headers: {
    "user":
        '{"id":"mentors","name":null,"authority_code":"manager","brand_code":"mentors"}',
  },
);

Future<User?> _registerUser({
  required String email,
  required String phone,
  required String name,
  required String birthday,
  required Gender gender,
  required String registerStoreId,
  required String memo,
  required String kiosk,
  String? parentPhone,
}) async {
  String url = '$server/users/user';

  final res = await requestPost(url, {
    "email": email,
    "phone": phone,
    "password": kiosk,
    "name": name,
    "birthday": birthday,
    "gender": describeEnum(gender),
    "parentPhone": parentPhone ?? '-',
    "memo": memo,
    "loginCode": kiosk,
    "storeId": GetStorage().read('id'),
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return null;
  }

  if (res.data['message'] == 'exist') {
    EasyLoading.showInfo("이미 회원가입된 유저입니다\n기존 가입정보로 회원가입 되었습니다.");

    final u = res.data['data'];

    return User(
      createdDate: DateTime.parse(u['createdAt']),
      gender: enumFromString(Gender.values, u['gender']),
      name: u['name'],
      id: u['_id'],
      isRegister: true,
      phone: u['phone'],
      birthday: u['birthday'],
      email: u['email'],
      parentPhone: u['parentPhone'],
      loginCode: u['loginCode'],
      memo: u['memo'],
    );
  }

  if (!res.data['success']) {
    if (res.data['message'] != null) {
      if (res.data['message'] == "EXIST_EMAIL") {
        EasyLoading.showError("이미 등록된 이메일 입니다");
        return null;
      }

      if (res.data['message'] == "EXIST_PHONE_NUMBER") {
        EasyLoading.showError("이미 등록된 휴대전화 번호입니다");
        return null;
      }

      if (res.data['message'] == "EXIST_DATA") {
        EasyLoading.showError("이미 등록된 회원입니다");
        return null;
      }
    }
  }

  if (res.data['success']) {
    EasyLoading.showSuccess("회원가입되었습니다");

    return User(
        createdDate: DateTime.now(),
        email: email,
        name: name,
        phone: phone,
        gender: gender,
        birthday: birthday,
        parentPhone: parentPhone,
        loginCode: kiosk,
        memo: memo,
        id: res.data['data']['_id'],
        isRegister: true);
  } else {
    return null;
  }
}

Future<List<User>> _getUserList() async {
  String url = '$server/users';

  try {
    final res = await requestGet(url);
    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return [];
    }
    if (res.data['success']) {
      final list =
          List.from(res.data['data']).map((e) => User.fromJson(e)).toList();
      list.sort((a, b) => b.currentUsingVoucherId == null ? -1 : 1);

      return list;
    }
    throw Error();
  } catch (e) {
    print(e);
    return [];
  }
}

Future<User?> _getUserById(String userId, String userPhone) async {
  String url = '$server/users?userId=$userId';

  try {
    final res = await requestGet(url);

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return null;
    }
    if (res.data['success']) {
      print(res.data['data']);
      return User.fromJson(res.data['data'][0]);
    }

    throw Error();
  } catch (e) {
    print(e);
    return null;
  }
}

Future<List<double>> _getUsersStatus() async {
  String url = '$server/stores/users-status';
  final res = await requestGet(url);

  if (res == null) {
    return [];
  }

  if (res.data['success']) {
    final data = res.data['data'];
    return [
      data['using'].toDouble(),
      data['reservation'].toDouble(),
      data['end'].toDouble(),
      data['noData'].toDouble()
    ];
  } else {
    return [];
  }
}

Future<bool> _modifyUserInfo(
    {required String email,
    required String phone,
    required String loginCode,
    required String name,
    required String birthday,
    required Gender gender,
    required String parentPhone,
    required String memo,
    required String userId}) async {
  String url = '$server/users/user';

  final res = await requestPatch(url, {
    "userId": userId,
    "email": email,
    "phone": phone,
    "name": name,
    "birthday": birthday,
    "gender": describeEnum(gender),
    "parentPhone": parentPhone,
    "memo": memo,
    "loginCode": loginCode,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    return true;
  } else {
    if (res.data['message'] != null) {
      if (res.data['message'] == "EXIST_EMAIL") {
        EasyLoading.showError("이미 등록된 이메일 입니다");
        return false;
      }

      if (res.data['message'] == "EXIST_PHONE_NUMBER") {
        EasyLoading.showError("이미 등록된 휴대전화 번호입니다");
        return false;
      }
    }
    return false;
  }
}

Future<List<Voucher>> _getUserVouchers(String userId) async {
  final url = '$server/users/user/vouchers/$userId';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return getVouchersFromList(List.from(res.data['data']));
  } else {
    return [];
  }
}

Future<bool> _deleteUser(String userId) async {
  final url = '$server/stores/user/$userId';

  final res = await requestDelete(url, {});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    return true;
  } else {
    print(url);
    print(res.data);
    return false;
  }
}

Future<bool> _exitUser(String userId) async {
  final url = '$server/io/exit';

  final res = await requestPatch(url, {
    "userId": userId,
    "force": "true",
  });

  if (res == null) {
    print(url);
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    return true;
  } else {
    print(url);
    print(res.data);
    return false;
  }
}

Future<List<SalesLog>> _getUserPaymentLogs(String userId) async {
  final url = '$server/master/user/payment-logs/$userId';
  final res = await requestGet(url);

  if (res == null) {
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data'])
        .map((e) => SalesLog.fromJson(e))
        .toList();
  } else {
    return [];
  }
}

Future<bool?> _updateUserPass(String userId, bool pass) async {
  final url = '$server/users/pass';
  final res = await requestPatch(url, {
    "pass": pass ? "true" : "false",
    "userId": userId,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    return res.data['data'];
  } else {
    EasyLoading.showError("작업에 실패했습니다.\n새로고침 후 다시 시도해주세요");
    return null;
  }
}

class UserAPI {
  final getUserList = _getUserList;
  final registerUser = _registerUser;
  final getUsersStatus = _getUsersStatus;
  final modifyUserInfo = _modifyUserInfo;
  final getUserVouchers = _getUserVouchers;
  final deleteUser = _deleteUser;
  final exitUser = _exitUser;
  final getUserById = _getUserById;
  final getUserPaymentLogs = _getUserPaymentLogs;
  final updateUserPass = _updateUserPass;
}
