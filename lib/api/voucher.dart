import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/global_enums.dart';

Future<Voucher?> _addVoucher(String groupName, Map<String, Object> data) async {
  try {
    final url = '$server/voucher/group/$groupName';
    final res = await requestPost(url, data);

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return null;
    }

    if (res.data['success']) {
      final data = res.data['data'];

      Voucher voucher = Voucher.getVoucherFromData(data);
      return voucher;
    } else {
      print(res.data);
      print("add voucher error");
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}

Future<Voucher> _editVoucher(
  String groupName,
  String voucherId,
  Map<String, Object> data,
) async {
  final url = '$server/voucher/group/$groupName/$voucherId';
  final res = await requestPatch(url, data);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return Voucher.empty();
  }

  return Voucher.empty();
}

Future<List<Voucher>> _getVoucherListByGroupName(SeatGroup group) async {
  try {
    final url = '$server/voucher/group/${group.toKor()}';
    final res = await requestGet(url);

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return [];
    }

    if (res.data['success']) {
      return getVouchersFromList(List.from(res.data['data']));
    } else {
      throw "_getVoucherListBySeatType Error";
    }
  } catch (e) {
    print(e);
    return [];
  }
}

Future<Voucher?> _getVoucherData(String userId, String voucherId) async {
  try {
    final url = '$server/voucher/$userId/$voucherId';
    final res = await requestGet(url);

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return null;
    }

    if (res.data['success']) {
      return Voucher.getVoucherFromData(res.data['data']);
    } else {
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}

Future<bool> _setVoucherData(
    {required String userId,
    required String voucherId,
    required DateTime datetime,
    required int remainTIme,
    required VoucherUseType voucherType}) async {
  try {
    final url = '$server/voucher/$userId/$voucherId';
    final res = await requestPost(url, {
      "expiredTime": datetime.toString(),
      "remainTime": remainTIme.toString(),
      "type": voucherType.toEng()
    });

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return false;
    }

    if (res.data['success']) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    print(e);
    return false;
  }
}

Future<bool> _deleteVoucher(Voucher voucher, SeatGroup seatGroup) async {
  try {
    final url = '$server/voucher/group/${seatGroup.toKor()}/${voucher.id}';
    final res = await requestDelete(url, {});

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return false;
    }
  } catch (e) {}
  return true;
}

class VoucherAPI {
  final addVoucher = _addVoucher;
  final getVoucherData = _getVoucherData;
  final setVoucherData = _setVoucherData;
  final deleteVoucher = _deleteVoucher;
  final editVoucher = _editVoucher;
  final getVoucherListByGroupName = _getVoucherListByGroupName;
}
