import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/locker.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/config.dart';

Future<bool> _appendVoucher(
    String userid, Voucher voucher, String? sectionId, String? seatId) async {
  String url = '$server/payment/voucher/user';

  final res = await requestPost(url, {
    "userId": userid,
    "voucherId": voucher.id,
    "sectionId": sectionId,
    "seatId": seatId
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
  } else {
    if (res.data['message'] == "CANT_BUY_TO_ANOTHER_ON_USING_VOUCHER") {
      EasyLoading.showError("좌석을 이용중일때 당일권 구매는 불가능합니다");
      return false;
    }
    return false;
  }

  return true;
}

Future<bool> _addPayLog({
  String? seatTypeId,
  String? lockerTypeId,
  required String userid,
  required int price,
  required voucherId,
  required PayMethod method,
}) async {
  String payUrl = '$server/payment/log';

  final res = await requestPost(payUrl, {
    "seatTypeId": seatTypeId ?? '',
    "lockerTypeId": lockerTypeId ?? '',
    "voucherId": voucherId,
    "price": price.toString(),
    "isWeb": true.toString(),
    "userId": userid,
    "method": method.value,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  return true;
}

Future<Locker?> _buyLocker({
  required String voucherId,
  required String userId,
  required String lokcerId,
}) async {
  final url = '$server/payment/locker';
  final res = await requestPost(url, {
    "voucherId": voucherId,
    "lockerId": lokcerId,
    "userId": userId,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return null;
  }

  if (res.data['success']) {
    return Locker.convertLocker(res.data['data']);
  } else {
    // throw 'buy locker error';
    return null;
  }
}

class PaymentAPI {
  final appendVoucher = _appendVoucher;
  final addPayLog = _addPayLog;
  final buyLocker = _buyLocker;
}
