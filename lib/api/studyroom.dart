import 'dart:convert';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/studyrooms.dart';
import 'package:mentors_web/config.dart';

class StudyRoomDayInfo {
  bool isCanReservation;
  int hour;

  StudyRoomDayInfo({
    required this.isCanReservation,
    required this.hour,
  });

  factory StudyRoomDayInfo.fromJson(Map<String, dynamic> json) {
    return StudyRoomDayInfo(
      isCanReservation: json['isCanReservation'],
      hour: json['hour'],
    );
  }
}

class StudyRoomDay {
  bool isCanReservation;
  List<StudyRoomDayInfo> studyRoomDayInfos;

  StudyRoomDay({
    required this.isCanReservation,
    required this.studyRoomDayInfos,
  });

  factory StudyRoomDay.fromJson(Map<String, dynamic> json) {
    return StudyRoomDay(
      isCanReservation: json['isCanReservation'],
      studyRoomDayInfos: (json['hours'] as List)
          .map((e) => StudyRoomDayInfo.fromJson(e))
          .toList(),
    );
  }
}

class StudyroomAPI {
  Future<List<StudyRoomReservationOfMonth>> getStudyroomReservationOfMonth({
    required String year,
    required String month,
  }) async {
    final url = '$server/studyroom/all/reservation/$year/$month';
    final response = await requestGet(url);

    if (response == null) {
      return [];
    } else {
      final list = response.data['data'];
      List<StudyRoomReservationOfMonth> ret = list
          .map<StudyRoomReservationOfMonth>(
              (element) => StudyRoomReservationOfMonth.fromJson(element))
          .toList();
      return ret;
    }
  }

  Future<StudyRoomDay?> getStudyroomReservationDay({
    required String seatId,
    required int year,
    required int month,
    required int day,
  }) async {
    final url = '$server/studyroom/info/$seatId/$year/$month/$day';
    final res = await requestGet(url);

    if (res == null) {
      return null;
    }

    if (res.data['success']) {
      return StudyRoomDay.fromJson(res.data['data']);
    } else {
      return null;
    }
  }

  Future<List<ReservationDataOfDay>> getStudyroomsReservationDay(
      {required int year, required int month, required int day}) async {
    final url = '$server/studyroom/all/info/$year/$month/$day';
    final res = await requestGet(url);
    if (res == null) {
      return [];
    }

    if (res.data['success']) {
      final list = res.data['data'];
      List<ReservationDataOfDay> ret = list
          .map<ReservationDataOfDay>(
              (element) => ReservationDataOfDay.fromJson(element))
          .toList();

      return ret;
    } else {
      return [];
    }
  }

  Future<List<ReservationInfo>> reservationStudyRoom({
    required String seatId,
    required int year,
    required int month,
    required int day,
    required int startHour,
    required int endHour,
    required int price,
    required String phoneNumber,
    required int numberOfPeople,
    required bool isGuest,
    required String userId,
  }) async {
    final url = '$server/studyroom/reservation';
    final res = await requestPost(
      url,
      {
        'seatId': seatId,
        'year': year.toString(),
        'month': month.toString(),
        'day': day.toString(),
        'startHour': startHour.toString(),
        'endHour': endHour.toString(),
        'paymentPrice': price.toString(),
        'phoneNumber': phoneNumber,
        'numberOfPeople': numberOfPeople.toString(),
        'isGuest': isGuest.toString(),
        'userId': userId,
      },
    );

    if (res == null) {
      return [];
    }

    if (res.data['success']) {
      return res.data['data']
          .map<ReservationInfo>(
            (e) => ReservationInfo.fromJson(e),
          )
          .toList();
    } else {
      EasyLoading.showError("예약에 실패했습니다.\n새로 고침 후 다시 시도해주세요");
      return [];
    }
  }
}
