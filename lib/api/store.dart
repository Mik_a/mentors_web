import 'dart:convert';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/controller/device_controller.dart';
import 'package:mentors_web/controller/store_management_controller.dart';
import 'package:mentors_web/modules/lib.dart';

Future _storeLogin(String id, String pw) async {
  final url = '$server/stores/login';
  final res = await requestPost(url, {
    "email": id,
    "password": pw,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return;
  }

  return res.data;
}

Future<StoreInfo> _getStoreInfo() async {
  final url = '$server/stores/store/info';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return StoreInfo();
  }

  if (res.data['success']) {
    final data = res.data['data'];

    return StoreInfo(
      homepage: data['homepage'],
      ownerName: data['ownerName'],
      ownerPhone: data['ownerPhone'],
      storeId: data['storeId'].toString(),
      storeName: data['name'],
      storePhone: data['storePhone'],
      restAddress: data['restAddress'],
      openDate: DateTime.parse(data['createdAt']),
      cat: CAT.fromData(data['CAT']),
      isPass: data['isUsingPass'],
    );
  } else {
    return StoreInfo();
  }
}

Future<StoreOperation> _getStoreOperation() async {
  final url = '$server/stores/store/setting';
  final res = await requestGet(url);
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return StoreOperation();
  }

  if (res.data['success']) {
    final data = res.data['data'];
    return StoreOperation(
      callManagerMessage: data['callManagerMessage'],
      closeHour: data['closeTime']['hour'],
      closeMin: data['closeTime']['min'],
      etcMessage: data['etcMessage'],
      etcWarningMessage: data['etcWarningMessage'],
      lockerMessage: data['lockerMessage'],
      lockerWarningMessage: data['lockerWarningMessage'],
      openHour: data['openingTime']['hour'],
      openMin: data['openingTime']['min'],
      outingExitTime: data['outingExitTime'],
      outingWarningTime: data['outingWarningTime'],
      overFlowPrice: data['overflow']['price'],
      overFlowTime: data['overflow']['time'],
      payMessage: data['payMessage'],
      payWarningMessage: data['payWarningMessage'],
      seatMessage: data['seatMessage'],
      seatWarningMessage: data['seatWarningMessage'],
      serviceMessage: data['serviceMessage'],
      is24: data['is24'],
    );
  } else {
    return StoreOperation();
  }
}

Future<List<Device>> _getDeviceList() async {
  final url = '$server/stores/device-list';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    final data = res.data['data'];

    return List.from(data)
        .map(
          (e) => Device(
            deviceId: e['deviceId'],
            id: e['_id'],
            name: e['name'],
            createDate: DateTime.parse(e['createDate']),
          ),
        )
        .toList();
  } else {
    return [];
  }
}

Future<List<SalesLog>> _getSalesLog(int year, int month) async {
  final url = '$server/stores/sales-log/${year.toString()}/${month.toString()}';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data'])
        .map((e) => SalesLog.fromJson(e))
        .toList()
        .reversed
        .toList();
  } else {
    return [];
  }
}

// delete device from store
Future<bool> _deleteDevice(String id) async {
  final url = '$server/stores/device/$id';
  final res = await requestDelete(url, {});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    EasyLoading.showSuccess("삭제되었습니다");
    return true;
  } else {
    EasyLoading.showError(res.data['message']);
    return false;
  }
}

// modify device name
Future<bool> _modifyDeviceName(String id, String name) async {
  final url = '$server/stores/device/$id';
  final res = await requestPatch(url, {'name': name});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    EasyLoading.showSuccess("수정되었습니다");
    return true;
  } else {
    EasyLoading.showError(res.data['message']);
    return false;
  }
}

Future<bool> _updateStoreInfo(Map<String, dynamic> info) async {
  final url = '$server/stores/store';
  final res = await requestPatch(url, {'data': jsonEncode(info)});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  if (res.data['success']) {
    EasyLoading.showSuccess("수정되었습니다");
    return true;
  } else {
    EasyLoading.showError(res.data['message']);
    return false;
  }
}

Future _storeLoginByManager(String jwt) async {
  final url = '$server/stores/login/$jwt';
  final res = await requestGet(url);
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return;
  }
  return res.data;
}

Future<bool?> _updateUsingPass(bool pass) async {
  final url = '$server/stores/pass';
  final res = await requestPatch(url, {'usingPass': pass ? "true" : "false"});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return null;
  }

  if (res.data['success']) {
    return res.data['data'];
  } else {
    return null;
  }
}

class StoreAPI {
  final storeLogin = _storeLogin;
  final getStoreInfo = _getStoreInfo;
  final getDeviceList = _getDeviceList;
  final getStoreOperation = _getStoreOperation;
  final getSalesLog = _getSalesLog;
  final deleteDevice = _deleteDevice;
  final modifyDeviceName = _modifyDeviceName;
  final updateStoreInfo = _updateStoreInfo;
  final storeLoginByManager = _storeLoginByManager;
  final updateUsingPass = _updateUsingPass;
}
