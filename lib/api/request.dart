import 'dart:convert';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

class MyResponse {
  dynamic data;

  MyResponse({this.data});
}

Future<MyResponse?> requestGet(String url) async {
  try {
    final jwt = GetStorage().read('jwt');
    Map<String, String> headers = {};
    if (jwt != null) headers['jwt'] = jwt;
    final response = await http.get(Uri.parse(url), headers: headers);
    try {
      return MyResponse(data: jsonDecode(response.body));
    } catch (e) {
      print('err');
      return MyResponse(data: response.body);
    }
  } catch (err) {
    print(err);
    return null;
  }
}

Future<MyResponse?> requestPost(String url, dynamic data) async {
  try {
    final jwt = GetStorage().read('jwt');
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    if (jwt != null) headers['jwt'] = jwt;
    final response = await http.post(
      Uri.parse(url),
      body: data,
      headers: headers,
    );

    return MyResponse(data: jsonDecode(response.body));
  } catch (err) {
    print(err);

    return null;
  }
}

Future<MyResponse?> requestPassPost(String url, dynamic data) async {
  try {
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    final response = await http.post(
      Uri.parse(url),
      body: data,
      headers: headers,
    );

    return MyResponse(data: jsonDecode(response.body));
  } catch (err) {
    print(err);

    return null;
  }
}

Future<MyResponse?> requestDelete(String url, dynamic data) async {
  try {
    final jwt = GetStorage().read('jwt');
    Map<String, String> headers = {};
    if (jwt != null) headers['jwt'] = jwt;
    final response =
        await http.delete(Uri.parse(url), body: data, headers: headers);
    return MyResponse(data: jsonDecode(response.body));
  } catch (err) {
    return null;
  }
}

Future<MyResponse?> requestPatch(String url, dynamic data) async {
  try {
    final jwt = GetStorage().read('jwt');
    Map<String, String> headers = {};
    if (jwt != null) headers['jwt'] = jwt;

    final response = await http.patch(
      Uri.parse(url),
      body: data,
      headers: headers,
    );

    return MyResponse(data: jsonDecode(response.body));
  } catch (err) {
    print(err);
    return null;
  }
}

String errorCheck(DioError error) {
  if (error.type == DioErrorType.connectTimeout ||
      error.type == DioErrorType.receiveTimeout) {
    return '1';
  } else {
    return '2';
  }
}

class Singleton {
  Dio dio = Dio();

  static final Singleton _instance = Singleton._internal();

  factory Singleton() {
    _instance.dio.options = BaseOptions(
      connectTimeout: 5 * 1000,
      receiveTimeout: 5 * 1000,
    );
    return _instance;
  }

  Singleton._internal() {
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      return client;
      // client.badCertificateCallback =
      //     (X509Certificate cert, String host, int port) => true;
      // return client;
    };
  }
}

Singleton dioSingleton = Singleton();
