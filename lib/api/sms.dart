import 'dart:convert';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/classes/message_template.dart';
import 'package:mentors_web/controller/message_controller.dart';
import 'package:mentors_web/config.dart';
import 'package:mentors_web/global_enums.dart';
import 'package:mentors_web/modules/lib.dart';

Future<List<MessageTemplate>> _getTemplate() async {
  final url = '$server/sms/template';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  return List.from(res.data['data']).map(MessageTemplate.convert).toList();
}

Future<List<int>> _getRemain() async {
  final url = '$server/sms/remain';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [0, 0, 0];
  }

  if (res.data['success']) {
    return [
      res.data['data']['SMS_CNT'],
      res.data['data']['LMS_CNT'],
      res.data['data']['MMS_CNT']
    ];
  } else {
    return [0, 0, 0];
  }
}

Future<SMSData> _getCash() async {
  final url = '$server/sms/cash';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return SMSData();
  }

  if (res.data['success']) {
    return SMSData(
      cash: res.data['data']['cash'],
      sms: res.data['data']['sms'],
      lms: res.data['data']['lms'],
      mms: res.data['data']['mms'],
      smsPrice: res.data['data']['smsPrice'],
      lmsPrice: res.data['data']['lmsPrice'],
      mmsPrice: res.data['data']['mmsPrice'],
    );
  } else {
    return SMSData();
  }
}

Future<int> _getMessageLogLength() async {
  final url = '$server/sms/logs/length';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return 0;
  }
  if (res.data['success']) {
    return res.data['data']['length'];
  } else {
    return 0;
  }
}

Future<List<MessageLog>> _getMessageLogs(int page) async {
  final url = '$server/sms/logs/slice/${page.toString()}';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    final list = res.data['data'][0]['msgLogs'];

    return List.from(list).map((e) {
      return MessageLog(
        cash: e['cash'],
        id: e['_id'],
        phone: e['phone'].split(',') ?? '-',
        userId: e['userId'],
        name: e['name'],
        message: e['message'],
        time: DateTime.parse(e['time']),
        type: enumFromString(MessageType.values, e['type']),
      );
    }).toList();
  } else {
    return [];
  }
}

Future<bool?> _sendMessages(
    List<Map<String, String>> recivers, String message) async {
  final url = '$server/sms/send-messages';

  final res = await requestPost(url, {
    "recivers": json.encode(recivers),
    'message': message,
  });

  if (res == null) {
    return null;
  }

  return res.data['success'];
}

Future<bool?> _toggleSendMessageTemplate(String id, bool value) async {
  final url = '$server/sms/template/set-send';
  final res = await requestPatch(url, {
    "id": id,
    "value": value.toString(),
  });

  if (res == null) {
    return null;
  }

  if (res.data['success']) {
    return res.data['data']['isSend'];
  } else {
    return null;
  }
}

Future<String?> _updateTemplateContent(String id, String value) async {
  final url = '$server/sms/template/content';
  final res = await requestPatch(url, {
    "id": id,
    "content": value,
  });

  if (res == null) {
    return null;
  }

  if (res.data['success']) {
    return res.data['data']['message'];
  } else {
    return null;
  }
}

Future<bool> _requestChargeCash(String amount, String userName) async {
  final url = '$server/sms/charge';
  final res = await requestPost(url, {
    "cash": amount,
    "userName": userName,
  });

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  return res.data['success'];
}

Future<List<ChargeLog>> _getChargeHistory() async {
  final url = '$server/sms/charge-history';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data']).map((e) {
      return ChargeLog.fromJson(e);
    }).toList();
  } else {
    return [];
  }
}

class SMSAPI {
  final getRemain = _getRemain;
  final getCash = _getCash;
  final getMessageLogs = _getMessageLogs;
  final getMessageLogLength = _getMessageLogLength;
  final sendMessages = _sendMessages;
  final getTemplate = _getTemplate;
  final toggleSendMessageTemplate = _toggleSendMessageTemplate;
  final updateTemplateContent = _updateTemplateContent;
  final requestChargeCash = _requestChargeCash;
  final getChargeHistory = _getChargeHistory;
}
