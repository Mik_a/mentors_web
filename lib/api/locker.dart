import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/locker.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/classes/voucher.dart';
import 'package:mentors_web/config.dart';

Future _getLockerStatus(String sectionId) async {
  try {
    final res = await requestGet('$server/locker/status/$sectionId');

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return;
    }

    if (res.data['success']) {
      return res.data['data']['lockerStatus'];
    } else {
      return [];
    }
  } catch (e) {
    return [];
  }
}

Future<Map<String, dynamic>> _setLockerStatus(
    String sectionId, String x, String y, String startIndex) async {
  try {
    final res = await requestPatch(
      '$server/locker/status/$sectionId',
      {
        "x": x,
        "y": y,
        "start": startIndex,
      },
    );

    if (res == null) {
      EasyLoading.showError("서버 상태를 확인해주세요");
      return {
        "success": false,
        "data": [],
      };
    }

    if (res.data['success']) {
      return {"success": true, "data": res.data['data']['list']};
    } else {
      if (res.data['message'] == "ALREADY_USING_LOCKER") {
        EasyLoading.showError("사용중인 사물함이 있어 변경이 불가능합니다");
        return {
          "success": false,
          "data": [],
        };
      }
      return {
        "success": false,
        "data": [],
      };
    }
  } catch (e) {
    return {
      "success": false,
      "data": [],
    };
  }
}

Future<Map<String, bool>> _toggleLockerActivate(
    String sectionId, Locker locker) async {
  final res = await requestPatch(
    '$server/locker/activate',
    {
      "sectionId": sectionId,
      "lockerId": locker.id,
      "value": (!locker.isEnable).toString(),
    },
  );

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return {"success": false};
  }

  if (res.data['success']) {
    return {"success": true, "data": res.data['data']['isEnable']};
  } else {
    return {"success": false};
  }
}

Future<bool> _vacateLocker(String lockerId) async {
  final url = '$server/locker/$lockerId';
  final res = await requestDelete(url, {});

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  return res.data['success'];
}

Future<bool> _moveLocker(String prevId, String nextId) async {
  final url = '$server/locker/move/$prevId/$nextId';
  final res = await requestPatch(url, {});
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  return res.data['success'];
}

Future<List<LockerVoucher>> _getLockerVouchres() async {
  final url = '$server/locker/voucher';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data["success"]) {
    return List.from(res.data['data']).map((element) {
      return LockerVoucher.getFromData(element);
    }).toList();
  } else {
    print(res.data);
    return [];
  }
}

Future<List<Log>> _getLockerLogs(String lockerId) async {
  final url = '$server/locker/logs/$lockerId';
  final res = await requestGet(url);

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data["success"]) {
    return List.from(res.data['data'])
        .map((element) {
          return Log.fromJson(element);
        })
        .toList()
        .reversed
        .toList();
  } else {
    print(res.data);
    return [];
  }
}

class LockerAPI {
  final getLockerStatus = _getLockerStatus;
  final setLockerStatus = _setLockerStatus;
  final toggleLockerActivate = _toggleLockerActivate;
  final vacateLocker = _vacateLocker;
  final moveLocker = _moveLocker;
  final getLockerVouchres = _getLockerVouchres;
  final getLockerLogs = _getLockerLogs;
}
