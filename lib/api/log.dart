import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/logs.dart';
import 'package:mentors_web/classes/sales_log.dart';
import 'package:mentors_web/config.dart';

Future<List<IOLog>> _getIOLogs(int page) async {
  String url = '$server/io/logs/$page/10';

  final res = await requestGet(url);
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }
  if (res.data['success']) {
    List<IOLog> list =
        List.from(res.data['data']).map((e) => IOLog.fromJson(e)).toList();
    return list;
  } else {
    return [];
  }
}

Future<List<IOLog>> _getIOLogsByUser(String userId) async {
  String url = '$server/io/logs/0/10?userId=$userId';

  final res = await requestGet(url);
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }
  if (res.data['success']) {
    List<IOLog> list =
        List.from(res.data['data']).map((e) => IOLog.fromJson(e)).toList();
    return list;
  } else {
    return [];
  }
}

Future<Map<String, dynamic>> _getPaymentLogs(
    int page, int length, Map<String, dynamic> filter) async {
  String url = '$server/stores/log/sales/$page/$length';

  final res = await requestPost(url, filter);
  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return {
      "size": 0,
      "list": [],
    };
  }

  if (res.data['success']) {
    print(res.data);

    if (res.data['data'].length == 0)
      return {
        "size": 0,
        "list": [],
      };

    List<SalesLog> list = List.from(res.data['data']['list'])
        .map((e) => SalesLog.fromJson(e))
        .toList();
    return {
      "size": res.data['data']['size'],
      "list": list,
    };
  } else {
    return {
      "size": 0,
      "list": [],
    };
  }
}

class LogAPI {
  final getIOLogs = _getIOLogs;
  final getIOLogsByUser = _getIOLogsByUser;
  final getPaymentLogs = _getPaymentLogs;
}
