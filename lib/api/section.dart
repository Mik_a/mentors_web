import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mentors_web/api/request.dart';
import 'package:mentors_web/classes/section.dart';
import 'package:mentors_web/config.dart';
import 'dart:convert';

Future _uploadSection(String sectionName, XFile xfile) async {
  String base64 = '';
  final bytes = await xfile.readAsBytes();
  base64 = base64Encode(bytes);
  final res = await requestPost(
    '$server/section/upload',
    {
      'base64': base64,
      'sectionName': sectionName,
    },
  );
  return res;
}

Future<bool> _modifySection(
    String sectionId, String sectionName, XFile? file) async {
  String base64 = '';

  if (file != null) {
    final bytes = await file.readAsBytes();
    base64 = base64Encode(bytes);
  }

  final res = await requestPatch(
    '$server/section/upload',
    {
      'sectionId': sectionId,
      'base64': base64,
      'sectionName': sectionName,
    },
  );

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return false;
  }

  return res.data['success'];
}

Future<List<Section>> _getSections() async {
  final res = await requestGet('$server/section/list');

  if (res == null) {
    EasyLoading.showError("서버 상태를 확인해주세요");
    return [];
  }

  if (res.data['success']) {
    return List.from(res.data['data'])
        .map((e) => Section(id: e['id'], name: e['name']))
        .toList();
  } else {
    return [];
  }
}

Future<bool> _deleteSection(Section section) async {
  final res = await requestDelete('$server/section/list/${section.id}', {});

  if (res == null) {
    return false;
  } else {
    if (!res.data['success']) {
      String msg = res.data['message'];
      if (msg == "ALREADY_USING_LOCKER") {
        EasyLoading.showError("사용중인 사물함을 모두 비워주세요");
      } else if (msg == "ALREADY_USING_SEAT") {
        EasyLoading.showError("사용중인 좌석을 모두 비워주세요");
      }
    }
    return res.data['success'];
  }
}

class SectionAPI {
  final uploadSection = _uploadSection;
  final getSections = _getSections;
  final deleteSection = _deleteSection;
  final modifySection = _modifySection;
}
