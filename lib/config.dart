// final String serverURL = 'https://dmon.works';
final String serverBase = 'dmon.works';
// final String serverBase = '192.168.0.6';
//
final String serverURL = 'https://$serverBase';
final String serverPort = '3000';
final String server = '$serverURL:$serverPort';

final String socketURL = 'https://dmon.works';
final String socketPort = '3002';
final String socketServer = '$socketURL:$socketPort';
