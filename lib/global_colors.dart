import 'package:flutter/material.dart';

const kTextWhiteColor = Color(0xffffffff);
const kMaterialColor = Color(0xff00a6a6);

class CustomColors {
  static const cA3A2A3 = Color(0xffA3A2A3);
  static const cE3E3E3 = Color(0xffE3E3E3);
  static const cC4C4C4 = Color(0xffC4C4C4);
  static const cD4C46B = Color(0xffD4C46B);
  static const cFFF4C2 = Color(0xffFFF4C2);
  static const cF3F3F3 = Color(0xffF3F3F3);
  static const c242424 = Color(0xff242424);
  static const c717071 = Color(0xff717071);
  static const cf5f5f5 = Color(0xfff5f5f5);
  static const cCECECE = Color(0xffCECECE);
  static const c008C66 = Color(0xff008C66);
  static const c00C4D0 = Color(0xff00C4D0);
  static const cACACAC = Color(0xffACACAC);
  static const cFBA136 = Color(0xffFBA136);
  static const cD9C87A = Color(0xffD9C87A);
  static const cBEAF6B = Color(0xffBEAF6B);
  static const c19A980 = Color(0xff19A980);
  static const cdfdfdf = Color(0xffdfdfdf);
  static const cf2df88 = Color(0xfff2df88);
  static const ca9abaf = Color(0xffa9abaf);
  static const c23c7d1 = Color(0xff23c7d1);
  static const cd7d7d7 = Color(0xffd7d7d7);
  static const cfafafa = Color(0xfffafafa);
  static const cf8f8f8 = Color(0xfff8f8f8);
  static const c005959 = Color(0xff005959);
  static const ce4e4e4 = Color(0xffe4e4e4);
  static const usingGreen = Color(0xff00bf8c);
  static const reservationYellow = Color(0xffffec81);
  static const endRed = Color(0xfff47373);
  static const noData = Color(0xffd1d1d1);
  static const f3fffa = Color(0xfff3fffA);
}
